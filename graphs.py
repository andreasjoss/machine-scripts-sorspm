#!/usr/bin/python
from __future__ import division
import argparse
import os,sys
import pylab as pl
import numpy as np
from prettytable import PrettyTable
#import cairosvg
#from scitools.std import movie
from datetime import datetime
import shutil
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from scipy.interpolate import griddata 
import sqlite3
import re


#spline libraries
from scipy.interpolate import Rbf
from matplotlib import cm 
from scipy.stats import gaussian_kde
from scipy import stats
from scipy.interpolate import interp1d
import matplotlib.mlab as mlab
from itertools import chain


#dir = 'optimisation/2017-07-09/optimise_torque_density/'
#dir = 'optimisation/2017-07-17/minimise_pmech/'
#dir = 'optimisation/2017-07-17/pmech_0.8_torque_density_0.2/'
#dir = 'optimisation/2017-07-17/pmech_0.6_torque_density_0.2_torque_ripple_0.2/'
dir = 'optimisation/2017-07-17/pmech_0.4_torque_density_0.3_torque_ripple_0.3/'


markersize=12
LINEWIDTH=2.5
#use latex font
#pl.rc('text', usetex=True)
#pl.rc('font', family='serif')
font_size=18
#font_size=24
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki','g','y','k']


conn = sqlite3.connect(dir+'optimise.db')
c = conn.cursor()



table_name = 'machine_semfem_output'

c = conn.execute("select * from %s"%(table_name))
column_names = list(map(lambda x: x[0], c.description))  
f_objective_names = []

#get column names
for i in range(0,len(column_names)):
  #if ('f_' in column_names[i]) and ('normalised' in column_names[i]):
  if ('normalised' in column_names[i]):  
    #somestring = re.sub('_normalised$', '', column_names[i])
    #f_objective_names.append([somestring])
    f_objective_names.append([column_names[i]])

#raw f_objective value string names (not the f_normalised values at end of database)
string_f_objectives_raw = []
string_f_objectives_raw.append(["p_out_semfem_torque","kW"])
string_f_objectives_raw.append(["total_mass","kg"])
string_f_objectives_raw.append(["torque_ripple_semfem","%"])
string_f_objectives_raw.append(["torque_density","Nm/kg"])
string_f_objectives_raw.append(["magnet_mass","kg"])


    

#get total number of primary_keys
c.execute("SELECT MAX(primary_key) FROM %s"%(table_name))
number_of_primary_keys = c.fetchone()[0]

#first check if there are any NONE values
number_of_columns = len(f_objective_names)
skip_this_primary_value = []
for i in range(0,number_of_columns):
  c.execute("SELECT %s FROM %s"%(f_objective_names[i][0],table_name))
  for j in range(1,number_of_primary_keys+1):
    temp = c.fetchone()[0]
    if temp == None:
      if j not in skip_this_primary_value:
	skip_this_primary_value.append(j)
    
#sys.exit()  

#get all f_objective values
for i in range(0,number_of_columns):
  c.execute("SELECT %s FROM %s"%(f_objective_names[i][0],table_name))
  temp = []
  temp2=0
  for j in range(1,number_of_primary_keys+1):
    temp2 = c.fetchone()[0]
    if j not in skip_this_primary_value:
      temp.append(temp2)
  f_objective_names[i].append(temp)

number_of_primary_keys = number_of_primary_keys - len(skip_this_primary_value)  

#fig = pl.gcf()
#fig.set_size_inches(18.5, 14.5)
#fig.savefig('test2png.png', dpi=100)
ax1 = pl.subplot(211)
#fig = pl.figure(1)
#ax = fig.add_subplot(2, 1, 1)  # specify (nrows, ncols, axnum)
pl.ion()
pl.xlabel(r"iteration_number")
pl.ylabel(r"f_objectives_normalised")
#pl.xlim(xmin=0)
#pl.title("Pareto Approximation of Efficiency against PM Mass")
#pl.scatter(pm_mass,eff, alpha = 0.5)

pl.subplots_adjust(left=0.04, right=0.98, top=0.95, bottom=0.05)


##total_mass_max_normalised = [x / max(f_total_mass_normalised_array) for x in f_total_mass_normalised_array]
#total_mass_max_normalised = [x / f_total_mass_normalised_array[0] for x in f_total_mass_normalised_array]

new_normalised_arr = []
for i in range(0,number_of_columns):
  #f_objective_names[i][1:][0] = [x / max(f_objective_names[i][1:][0]) for x in f_objective_names[i][1:][0]]
  new_normalised_arr.append([x / max(f_objective_names[i][1:][0]) for x in f_objective_names[i][1:][0]])

line = []
scat = []

skip_these_objectives = []
skip_these_objectives.append('power_factor')
skip_these_objectives.append('efficiency')
skip_these_objectives.append('torque_dq')

max_string_len = 0
for i in range(0,number_of_columns):
  skip=False
  for j in range(0,len(skip_these_objectives)):
    if skip_these_objectives[j] in f_objective_names[i][:1][0]:
      skip=True
  if not skip:
    #line.append(pl.plot(range(1,number_of_primary_keys+1),new_normalised_arr[i],label=f_objective_names[i][:1][0],c=colors[i],linewidth=LINEWIDTH))
    string_temp = re.sub('_normalised$', '', f_objective_names[i][:1][0])
    string_temp = re.sub('_semfem$', '', string_temp)
    string_temp = string_temp[2:]
    
    if len(string_temp) > max_string_len: max_string_len = len(string_temp)

k=0
for i in range(0,number_of_columns):
  skip=False
  for j in range(0,len(skip_these_objectives)):
    if skip_these_objectives[j] in f_objective_names[i][:1][0]:
      skip=True
  if not skip:
    #line.append(pl.plot(range(1,number_of_primary_keys+1),new_normalised_arr[i],label=f_objective_names[i][:1][0],c=colors[i],linewidth=LINEWIDTH))
    string_temp = re.sub('_normalised$', '', f_objective_names[i][:1][0])
    string_temp = re.sub('_semfem$', '', string_temp)
    string_temp = string_temp[2:]
    
    for l in range(0,max_string_len-len(string_temp)+1):
      string_temp += ' '

    #string_temp = "%2s"%(string_temp)
    #string_temp+=' [1 p.u. = %.3g]'%(max(f_objective_names[i][1:][0]))
    c.execute("SELECT MAX(%s) FROM %s"%(string_f_objectives_raw[k][0],table_name))
    max_temp = c.fetchone()[0]
    if k==0: max_temp=max_temp/1000
    string_value_temp = ' [1 p.u. = %.3g %s]'%(max_temp,string_f_objectives_raw[k][1])
    string_value_temp = '%14s'%(string_value_temp)
    string_temp = string_temp + string_value_temp
    
    line.append(pl.plot(range(1,number_of_primary_keys+1),new_normalised_arr[i],label=string_temp,c=colors[i],linewidth=LINEWIDTH))
    #line.append(pl.plot(range(1,number_of_primary_keys+1),new_normalised_arr[i],label=re.sub('_normalised$', '', f_objective_names[i][:1][0]),c=colors[i],linewidth=LINEWIDTH))
    scat.append(pl.scatter(range(1,number_of_primary_keys+1),new_normalised_arr[i],alpha = 1.0,c=colors[i],lw = 0,s=40))
    #scat.append(pl.plot(range(1,number_of_primary_keys+1),new_normalised_arr[i],c=colors[i],marker='o',fillstyle='full',markeredgewidth=0.0))
    k=k+1

pl.grid(True)
leg = pl.legend(loc='upper right')
pl.xlim(xmin=0)
pl.xlim(xmax=number_of_primary_keys)

pl.show()