#!/usr/bin/python

import sqlite3
import os, sys
import numpy as np
import pylab as pl

###SETTINGS
##########################################
db_folder = 'magnet_losses/'

#new db that I will extract data from the sweep
db_list_method_1 = []
db_list_method_1.append(db_folder+'magnet_loss_method0.db')
db_list_method_1.append(db_folder+'magnet_loss_method0_J=2.5.db')

db_list_method_2 = []
db_list_method_2.append(db_folder+'magnet_loss_method2.db')
db_list_method_2.append(db_folder+'magnet_loss_method2_J=2.5.db')
db_list_method_2.append(db_folder+'magnet_loss_method2_analytical_B.db')

#db_list.append('magnet_loss_method0_J=0.0_fftdiv2.db')
#db_list.append('magnet_loss_method0_J=2.5_fftdiv2.db')
##########################################

labels_method_1 = []
labels_method_1.append("Open Circuit")#, Method 1")
labels_method_1.append("J = 2.5 A rms")#, Method 1")

labels_method_2 = []
labels_method_2.append("Open Circuit")#, Method 2")
labels_method_2.append("J = 2.5 A rms")#, Method 2")
labels_method_2.append("1D Analytical")#, Method 2")

#labels.append("J = 0.0 A rms,fft/2, Method 1")
#labels.append("J = 2.5 A rms,fft/2 Method 1")

n_rpm_method_1 = []
n_rpm_method_2 = []

p_magnet_losses_method_1 = []
p_magnet_losses_method_2 = []

for k in range(0,len(db_list_method_1)):
  conn = sqlite3.connect(db_list_method_1[k])
  c = conn.cursor()

  #get speed values
  c.execute("SELECT n_rpm FROM machine_input_parameters")
  tmp = c.fetchall()

  n_rpm_method_1.append(np.zeros(len(tmp)))
  for i in range(0,len(tmp)):
    n_rpm_method_1[k][i] = tmp[i][0]

  #get magnet loss values
  c.execute("SELECT p_magnet_losses FROM machine_semfem_output")
  tmp = c.fetchall()

  p_magnet_losses_method_1.append(np.zeros(len(tmp)))
  for i in range(0,len(tmp)):
    p_magnet_losses_method_1[k][i] = tmp[i][0]

for k in range(0,len(db_list_method_2)):
  conn = sqlite3.connect(db_list_method_2[k])
  c = conn.cursor()

  #get speed values
  c.execute("SELECT n_rpm FROM machine_input_parameters")
  tmp = c.fetchall()

  n_rpm_method_2.append(np.zeros(len(tmp)))
  for i in range(0,len(tmp)):
    n_rpm_method_2[k][i] = tmp[i][0]

  #get magnet loss values
  c.execute("SELECT p_magnet_losses FROM machine_semfem_output")
  tmp = c.fetchall()

  p_magnet_losses_method_2.append(np.zeros(len(tmp)))
  for i in range(0,len(tmp)):
    p_magnet_losses_method_2[k][i] = tmp[i][0]



conn.commit()
conn.close()
#sys.exit()



###GRAPHS
##################################
line_method_1 = []
line_method_2 = []
scat_method_1 = []
scat_method_2 = []
art = []

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
#LINEWIDTH=2
#font_size=14
#axis_label_font_size=22
#tick_label_font_size=22
legend_font_size=10
legend_title_fontsize=12
marker_size = 20
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki','g','y','k']

##margin_left = 0.05
##margin_right = 0.74
##margin_bottom = 0.08
##margin_top = 0.92

#margin_left = 0.05
#margin_right = 0.79
#margin_bottom = 0.09
#margin_top = 0.90

margin_left = 0.0
margin_right = 1.0
margin_bottom = 0.09
margin_top = 0.90

#fig_x_cm = 8
#fig_y_cm = 5
#fig_x_inch_pdf = 18
#fig_y_inch_pdf = 8

fig, ax1 = pl.subplots(1, 1)# ,figsize=(fig_x_cm,fig_y_cm))
ax2 = ax1.twiny()

###METHOD 1 LINES
j=0
min_rpm = min(n_rpm_method_1[0])
max_rpm = max(n_rpm_method_1[0])
p_max = max(p_magnet_losses_method_1[0])
for k in range(0,len(db_list_method_1)):
  j=j+1
  number_of_graph_entries = max(len(n_rpm_method_1[k]),len(p_magnet_losses_method_1[k]))

  line_method_1.append(ax1.plot(n_rpm_method_1[k][:number_of_graph_entries],p_magnet_losses_method_1[k][:number_of_graph_entries],c=colors[j],label=labels_method_1[k]))#,linewidth=LINEWIDTH))
  ax1.scatter(n_rpm_method_1[k][:number_of_graph_entries],p_magnet_losses_method_1[k][:number_of_graph_entries], alpha = 1.0, c=colors[j],label=labels_method_1[k],lw = 0,s=marker_size)

  if max(p_magnet_losses_method_1[k]) > p_max:
    p_max = max(p_magnet_losses_method_1[k])

#legend_pos_x = 1.02
legend_pos_x = 0.02 #was previously 1.04

lines_method_1 = line_method_1[0]
for k in range(1,len(db_list_method_1)):
  lines_method_1 = lines_method_1 + line_method_1[k]
labels_collection_method_1 = [l.get_label() for l in lines_method_1]
lgd_method_1 = ax1.legend(lines_method_1, labels_collection_method_1, loc='center left', bbox_to_anchor=(legend_pos_x, 0.87), title='Method 1',fontsize=legend_font_size)
ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
art.append(lgd_method_1)

###METHOD 2 LINES
min_rpm = min(n_rpm_method_2[0])
max_rpm = max(n_rpm_method_2[0])
#p_max = max(p_magnet_losses_method_2[0])
for k in range(0,len(db_list_method_2)):
  j=j+1
  number_of_graph_entries = max(len(n_rpm_method_2[k]),len(p_magnet_losses_method_2[k]))

  line_method_2.append(ax2.plot(n_rpm_method_2[k][:number_of_graph_entries],p_magnet_losses_method_2[k][:number_of_graph_entries],c=colors[j],label=labels_method_2[k]))#,linewidth=LINEWIDTH))
  ax2.scatter(n_rpm_method_2[k][:number_of_graph_entries],p_magnet_losses_method_2[k][:number_of_graph_entries], alpha = 1.0, c=colors[j],label=labels_method_2[k],lw = 0,s=marker_size)

  if max(p_magnet_losses_method_2[k]) > p_max:
    p_max = max(p_magnet_losses_method_2[k])

lines_method_2 = line_method_2[0]
for k in range(1,len(db_list_method_2)):
  lines_method_2 = lines_method_2 + line_method_2[k]
labels_collection_method_2 = [l.get_label() for l in lines_method_2]
lgd_method_2 = ax2.legend(lines_method_2, labels_collection_method_2, loc='center left', bbox_to_anchor=(legend_pos_x, 0.61), title='Method 2',fontsize=legend_font_size)
ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
art.append(lgd_method_2)








ax1.set_xticks(np.linspace(0,465,7))
ax2.xaxis.set_visible(False)
#ax2.set_xticks(np.linspace(0,465,7))

ax1.set_xlabel('Mechanical Speed [rpm]')#,fontsize=axis_label_font_size)
ax1.set_ylabel('Magnet Losses [W]')#,fontsize=axis_label_font_size)

ax1.set_xlim(min_rpm,max_rpm)
ax1.set_ylim(0,p_max*1.1)

ax2.set_xlim(min_rpm,max_rpm)
ax2.set_ylim(0,p_max*1.1)

ax1.grid()

#ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

#box = ax1.get_position()
#ax1.set_position([box.x0, box.y0, box.width, box.height * 0.73])
#ax2.set_position([box.x0, box.y0, box.width, box.height * 0.73])

fig.set_size_inches(8, 4) #default is (8,6)

#pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)

#fig.savefig("P_magnet_vs_speed.pdf",bbox_inches='tight')
fig.savefig(db_folder+'P_magnet_vs_speed.pdf', bbox_inches='tight', additional_artists=art)
pl.show()