#!/usr/bin/python

import sqlite3
import os, sys
import numpy as np
import shutil

###SETTINGS
##########################################
db_original = 'SORSPM.db'
pk_original = 48

#new db that will be created here, and which I will extract data from when plotting graphs
#db_name = 'magnet_losses/magnet_loss_method2_J=2.5.db'
#db_name = 'magnet_losses/magnet_loss_method0_J=2.5_fftdiv2.db'
#db_name = 'magnet_losses/magnet_loss_method0_J=0.0_fftdiv2.db'
#db_name = 'magnet_losses/magnet_loss_method2_analytical_B.db'
#db_name = 'constant_torque_power/pcu200_bars.db'
db_name = 'sweep_turns/J=3.2_lseries_in.db'
#db_name = 'sweep_turns/J=3.2_lseries_out.db'

#sweep_nrpm = np.arange(15,465+15,15)
sweep_nrpm = np.arange(0,465+15,15)
sweep_turns = np.arange(1,21,1)
##########################################

print "Please confirm this action with 'y'."
input_kb = raw_input()
if input_kb != 'y':
  sys.exit()
  
#prepare new db
shutil.copy(db_original,db_name)
conn = sqlite3.connect(db_name)
c = conn.cursor()
c.execute("DELETE FROM machine_input_parameters")
c.execute("DELETE FROM machine_semfem_output")

#create temporary copy of original db
pk_temp = -5
db_name_temp = db_original[:-3]+'_temp.db'
shutil.copy(db_original,db_name_temp)
#prepare temporary db
c.execute("ATTACH DATABASE '%s' as db_name_temp"%(db_name_temp))
c.execute("UPDATE db_name_temp.machine_input_parameters SET primary_key = ? WHERE _rowid_ = ?",(pk_temp,pk_original,))

#tabulate the table in the new db
#for i, val in enumerate(sweep_nrpm):
for i, val in enumerate(sweep_turns):
  #print(i,val)
  c.execute("INSERT INTO machine_input_parameters SELECT * FROM db_name_temp.machine_input_parameters WHERE primary_key = ?",(pk_temp,))  
  c.execute("UPDATE machine_input_parameters SET primary_key = ? WHERE primary_key = ?",(i+1,pk_temp,))
  
  #update column values to be sweeped
  #c.execute("UPDATE machine_input_parameters SET n_rpm = %d WHERE primary_key = ?"%(val),(i+1,))
  c.execute("UPDATE machine_input_parameters SET force_this_number_of_turns = %d WHERE primary_key = ?"%(val),(i+1,))

conn.commit()
conn.close()

#delete temporary table
os.remove(db_name_temp)

#simulate each entry in the sweep
#for i, val in enumerate(sweep_nrpm):
for i, val in enumerate(sweep_turns):
  os.system("./SORSPM_semfem.py -pk %d -db %s"%(i+1,db_name))