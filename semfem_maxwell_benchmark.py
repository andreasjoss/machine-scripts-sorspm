#!/usr/bin/python

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import numpy as np
import pylab as pl
from prettytable import PrettyTable
#import sqlite3
from matplotlib.ticker import FormatStrFormatter


legend_font_size=10
legend_title_fontsize=12
marker_size = 20

plot_torque_from_csv = 1
plot_Br_from_csv = 1
roll_offset_number = 1

dir0 = 'semfem_pk17/'

#dir2 = 'benchmark/maxwell_pk17/run00/'
dir2 = 'maxwell_pk17/run03/'

semfem_torque_file = 'semfem_script_output_best.csv'
maxwell_torque_file = 'SORSPM_l=140.00_Jrms=5_kmr=0.70_hmr=5.5_p=40_hc=32.7-Tm.txt'

semfem_B_file = 'B_airgap_new.csv'
maxwell_B_file = 'SORSPM_l=140.00_Jrms=5_kmr=0.70_hmr=5.5_p=40_hc=32.7-Br.txt'

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
stacklength_multiplier = 1.0
#stacklength_multiplier = 1.5

semfem_stacklength_mulitplier = 140/150
maxwell_stacklength_mulitplier = 1/0.95 #stacking factor compensation

pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

if plot_torque_from_csv == 1:
    semfem_data = np.genfromtxt(dir0+semfem_torque_file,skip_header=1,delimiter=',',usecols=(0,1,2),unpack=True)
    maxwell_transient_data=np.loadtxt(dir2+maxwell_torque_file,skiprows=7,usecols=(0,1),unpack=True)
    
    #scale time to ms
    for i in range(0,len(semfem_data[0])):
      semfem_data[0][i] = semfem_data[0][i]*1000
    
    
    maxwell_transient_data[1] = np.roll(maxwell_transient_data[1], roll_offset_number)
    maxwell_transient_data_1 = maxwell_transient_data[1]
    
    for i in range(0,len(semfem_data[1])):
        semfem_data[2][i] = semfem_data[2][i]*stacklength_multiplier*semfem_stacklength_mulitplier
    for i in range(0,len(maxwell_transient_data[1])):
        maxwell_transient_data[1][i] = maxwell_transient_data[1][i]*stacklength_multiplier*maxwell_stacklength_mulitplier
                                                
    #SEMFEM
    max_torque_semfem = np.max(semfem_data[2])
    min_torque_semfem = np.min(semfem_data[2])
    torque_ripple_semfem = ((max_torque_semfem-min_torque_semfem)/min_torque_semfem)*100     
    average_torque_semfem = np.average(semfem_data[2])
    
#    #MAXWELL automatic transient
    max_torque_maxwell_transient = np.max(maxwell_transient_data_1)
    min_torque_maxwell_transient = np.min(maxwell_transient_data_1)
    torque_ripple_maxwell_transient = ((max_torque_maxwell_transient-min_torque_maxwell_transient)/min_torque_maxwell_transient)*100 
    average_torque_maxwell_transient = np.average(maxwell_transient_data_1)    
    
    pretty_terminal_table.add_row(["----------SEMFEM----------", "---", "---"])
    pretty_terminal_table.add_row(["average_torque", average_torque_semfem, "[Nm]"])
    pretty_terminal_table.add_row(["torque_ripple", torque_ripple_semfem, "[%]"])
    pretty_terminal_table.add_row(["max_torque", max_torque_semfem, "[Nm]"])
    pretty_terminal_table.add_row(["min_torque", min_torque_semfem, "[Nm]"])   
    
    pretty_terminal_table.add_row(["MAXWELL automatic transient", "---", "---"])
    pretty_terminal_table.add_row(["average_torque", average_torque_maxwell_transient, "[Nm]"])
    pretty_terminal_table.add_row(["torque_ripple", torque_ripple_maxwell_transient, "[%]"])
    pretty_terminal_table.add_row(["max_torque", max_torque_maxwell_transient, "[Nm]"])
    pretty_terminal_table.add_row(["min_torque", min_torque_maxwell_transient, "[Nm]"])       
    
    end_time = max(semfem_data[0])
    
    fig = pl.figure(1)
    
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twiny()	
	
    ax1.set_xlabel(r'Time [ms]')#,fontsize=axis_label_font_size)
    ax1.set_ylabel(r'Torque [N.m]')#,fontsize=axis_label_font_size)
    ax2.set_xlabel(r'\textbf{\theta_{e} [^{\circ}]}')#,fontsize=axis_label_font_size)

    #ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    #ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

    time_ticks = np.linspace(0,end_time,7)
    for i in range(0,len(time_ticks)):
      time_ticks[i] = np.round(time_ticks[i],2)
    
    #ax1.set_xticks(np.linspace(0,end_time,7))
    ax1.set_xticks(time_ticks)
    ax2.set_xticks(np.linspace(0,360,7))

    #ax1.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    #pl.xticks(np.arange(0, 420, 60))
    #pl.yticks(np.arange(0, 110, 10))#for pk16
    #pl.yticks(np.arange(0, 210, 5))

    ax1.set_xlim(0,end_time)
    ax1.set_ylim(0,260)
    ax2.set_xlim(0,360)

    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
    #ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    #art.append(lgd)
    ax1.grid()
    ax1.plot(np.linspace(0,max(semfem_data[0]),len(semfem_data[0])),semfem_data[2],label='SEMFEM',c='b')
    ax1.plot(np.linspace(0,max(semfem_data[0]),len(maxwell_transient_data[1])),maxwell_transient_data[1],label='ANSYS Maxwell',c='r')
    
    leg = ax1.legend(loc='lower right')

    for line in leg.get_lines():
	line.set_linewidth(2)
    
    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    
    fig.set_size_inches(8, 4) #default is (8,6)
    
    pl.savefig('benchmark_torque_semfem_maxwell.pdf',bbox_inches='tight')
    pl.savefig('benchmark_torque_semfem_maxwell.png',bbox_inches='tight')
    
    print pretty_terminal_table

if plot_Br_from_csv == 1:
    semfem_data = np.genfromtxt(dir0+semfem_B_file,skip_header=1,delimiter=',',usecols=(0,1),unpack=True)
    maxwell_transient_data=np.loadtxt(dir2+maxwell_B_file,skiprows=7,usecols=(0,1),unpack=True)
    
    mechanical_range_semfem = np.linspace(0,18,len(semfem_data[1]))
    mechanical_range_maxwell = np.linspace(0,18,len(maxwell_transient_data[1]))
    
    min_Br = min(min(semfem_data[1]),min(maxwell_transient_data[1]))
    max_Br = max(max(semfem_data[1]),max(maxwell_transient_data[1]))
    
    fig2 = pl.figure(2)
    
    ax1 = fig2.add_subplot(111)
    #ax2 = ax1.twiny()	
	
    ax1.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)
    ax1.set_ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=axis_label_font_size)
    #ax2.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)

    #ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    #ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

    angle_ticks = np.linspace(0,18,7)
    for i in range(0,len(angle_ticks)):
      angle_ticks[i] = np.round(angle_ticks[i],2)
    
    #ax1.set_xticks(np.linspace(0,end_time,7))
    ax1.set_xticks(angle_ticks)
    #ax2.set_xticks(np.linspace(0,360,7))

    #ax1.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    #pl.xticks(np.arange(0, 420, 60))
    #pl.yticks(np.arange(0, 110, 10))#for pk16
    #pl.yticks(np.arange(0, 210, 5))

    ax1.set_xlim(0,18)
    ax1.set_ylim(-1.5,1.5)
    #ax2.set_xlim(0,360)

    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
    #ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    #art.append(lgd)
    ax1.grid()
    ax1.plot(mechanical_range_semfem,semfem_data[1],label='SEMFEM',c='b')
    ax1.plot(mechanical_range_maxwell,maxwell_transient_data[1],label='ANSYS Maxwell',c='r')
    
    leg = ax1.legend(loc='upper right')

    for line in leg.get_lines():
      line.set_linewidth(2)
    
    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    
    fig2.set_size_inches(8, 4) #default is (8,6)
    
    pl.savefig('benchmark_Br_semfem_maxwell.pdf',bbox_inches='tight')
    pl.savefig('benchmark_Br_semfem_maxwell.png',bbox_inches='tight')
    
    
#pl.show()    

 
