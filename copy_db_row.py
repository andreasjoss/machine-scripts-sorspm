#!/usr/bin/python

import sqlite3
import os, sys

#copies row information from db2 to db1

db_name_1 = 'SORSPM.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_44_pole_round_litz/best.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_48_pole_round_litz/best.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_52_pole_round_litz/best.db'
#db_name_2 = 'best.db'

subfolder = '100'
db_files = []
#db_files.append(['optimisation/2017-11-29/local_best_32/'+subfolder+'/best.db','32 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_36/'+subfolder+'/best.db','36 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_40/'+subfolder+'/best.db','40 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_44/'+subfolder+'/best.db','44 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_48/'+subfolder+'/best.db','48 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_52/'+subfolder+'/best.db','52 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_56/'+subfolder+'/best.db','56 pole','Round Litz'])
#db_files.append(['optimisation/2017-11-29/local_best_60/'+subfolder+'/best.db','60 pole','Round Litz'])

db_files.append(['optimisation/2017-12-05/local_32/'+subfolder+'/best.db','32 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_36/'+subfolder+'/best.db','36 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_40/'+subfolder+'/best.db','40 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_44/'+subfolder+'/best.db','44 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_48/'+subfolder+'/best.db','48 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_52/'+subfolder+'/best.db','52 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_56/'+subfolder+'/best.db','56 pole','Solid Bars'])
db_files.append(['optimisation/2017-12-05/local_60/'+subfolder+'/best.db','60 pole','Solid Bars'])

db_name_2 = db_files[7][0]

pk1 = 72
pk2 = 436

#table_name_from = 'machine_input_parameters'
table_name_from = 'machine_input_parameters_rpm_100'

table_name_to = 'machine_input_parameters'

#################################
conn = sqlite3.connect(db_name_1)
c = conn.cursor()

if db_name_1 is not db_name_2:
  c.execute("ATTACH DATABASE '%s' as db2"%(db_name_2))

c = conn.execute("select * from %s"%(table_name_from))
column_names = list(map(lambda x: x[0], c.description))



for i in range(1,len(column_names)):
  if db_name_1 is not db_name_2:
    c.execute("SELECT %s FROM db2.%s WHERE primary_key = %d"%(column_names[i],table_name_from,pk2))
  else:
    c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name_from,pk2))
    
  val = c.fetchone()[0]
  #print(val)
  if i == 1:
    try:
      c.execute("INSERT INTO %s (primary_key) VALUES (%d)"%(table_name_to,pk1))
    except:
      pass

  try:
    c.execute("UPDATE %s SET %s = ? WHERE primary_key = ?"%(table_name_to,column_names[i]),(val,pk1))
  except:
    print "Column is not in both tables: %s"%(column_names[i])

conn.commit()
conn.close()