#!/usr/bin/python
from __future__ import division
import argparse
import os,sys
import pylab as pl
import numpy as np
from prettytable import PrettyTable
#import cairosvg
#from scitools.std import movie
from datetime import datetime
import shutil
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from scipy.interpolate import griddata 
import sqlite3
import fnmatch

dir = 'optimisation/2017-04-09/'

do_post_analysis = 1
draw_3d_graph_all = 1
draw_3d_graph_filtered = 0

plot_objectives = [2,6,7]
#weights=[0.5,0.4,0.1]
#weights=[0.3,0.4,0.3]
weights=[0.4,0.3,0.3]
#weights=[1/3,1/3,1/3]
#weights=[1.0,0.0,0.0]
#weights=[0.0,1.0,0.0]
#weights=[0.0,0.0,1.0]
#weights=[0.0,0.0,0.0]

LINEWIDTH=1
markersize=12
font_size=18
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

tmp = []
db_files = []
for root, dirnames, filenames in os.walk(dir):
    for filename in fnmatch.filter(filenames, 'best.db'):
      tmp.append(os.path.join(root, filename))

for i in range (0, len(tmp)):
  if 'best_with_improved_mesh_and_steps' in tmp[i]:
    db_files.append(tmp[i])

current_densities = []    
for i in range(0, len(db_files)):
  if db_files[i][-11:-8] not in current_densities:
    current_densities.append(db_files[i][-11:-8])
    
current_densities = sorted(current_densities)
db_files = sorted(db_files)

columns = []
for i in range(0,len(current_densities)):
  columns.append(0)

tmp_flags = []
for i in range(0,len(current_densities)):
  tmp_flags.append(0)

pu_scalars = []
pu_scalars.append(['current_density','min or max desired',current_densities,tmp_flags[:],['filter_min','filter_max']])
pu_scalars.append(['efficiency_indirect_dq',1,columns[:],tmp_flags[:],[0,1]])
pu_scalars.append(['torque_ripple_semfem',-1,columns[:],tmp_flags[:],[1,1.8]])###
pu_scalars.append(['total_mass',-1,columns[:],tmp_flags[:],[0,2]])
pu_scalars.append(['average_torque_dq',1,columns[:],tmp_flags[:],[0,1]])
pu_scalars.append(['power_factor',1,columns[:],tmp_flags[:],[0,1]])
pu_scalars.append(['torque_density',1,columns[:],tmp_flags[:],[0,1]])###
pu_scalars.append(['magnet_mass',-1,columns[:],tmp_flags[:],[1,1.6]])###
pu_scalars.append(['total_losses',-1,columns[:],tmp_flags[:],[1,2]])

#i=5
#for i in range(6,9):
for i in range(0,len(db_files)):
  print(db_files[i])
  #setup databse connection
  conn = sqlite3.connect(db_files[i])
  c = conn.cursor()

  c.execute("SELECT current_density_rms FROM machine_input_parameters WHERE primary_key = 1")
  current_density = c.fetchone()[0]

  tmp=0
  for j in range(1, len(pu_scalars)):
    
    for p in range(0,len(current_densities)):
      if np.isclose(current_density,float(current_densities[p]),rtol=1e-03, atol=1e-03, equal_nan=False):
	break
    
    if pu_scalars[j][1] == 1:
      c.execute("SELECT MAX(%s) FROM machine_semfem_output"%(pu_scalars[j][0]))
    elif pu_scalars[j][1] == -1:
      c.execute("SELECT MIN(%s) FROM machine_semfem_output"%(pu_scalars[j][0]))
    
    tmp=c.fetchone()[0]
    
    if pu_scalars[j][3][p] == 1:
      if pu_scalars[j][1] == 1:
	if tmp > pu_scalars[j][2][p]:
	  pu_scalars[j][2][p] = tmp
      elif pu_scalars[j][1] == -1:
	if tmp < pu_scalars[j][2][p]:
	  pu_scalars[j][2][p] = tmp
    else:
      pu_scalars[j][2][p] = tmp
      pu_scalars[j][3][p] = 1

  if i == 0:
    select_string = 'primary_key'
    c.execute("SELECT %s FROM machine_semfem_output"%(select_string))
    pk = c.fetchall() 
	  
  conn.close()
  
#write all this information to a single global database to be used in that optimisation parent directory
if os.path.isfile(dir+'pu_scalar_values.db'):
  os.remove(dir+'pu_scalar_values.db')

conn = sqlite3.connect(dir+'pu_scalar_values.db')
c = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS pu_scalars (id integer PRIMARY KEY,objective text NOT NULL)")

for i in range(0,len(current_densities)):
  #c.execute("ALTER TABLE pu_scalars ADD COLUMN ja REAL")
  c.execute("ALTER TABLE pu_scalars ADD COLUMN J%d_%dA REAL"%(float(current_densities[i][0]),float(current_densities[i][2])))

for i in range(1,len(pu_scalars)):
  c.execute("INSERT INTO pu_scalars (objective) VALUES (?)",(pu_scalars[i][0],))

  for j in range(0,len(current_densities)):
    #c.execute("INSERT INTO pu_scalars (J1_0A) VALUES (?)",pu_scalars[i][2][j])
    c.execute("UPDATE pu_scalars SET J%d_%dA = (?) WHERE objective = ?"%(float(current_densities[j][0]),float(current_densities[j][2])),(pu_scalars[i][2][j],pu_scalars[i][0]))

db_attach_names = []
for i in range(0,len(current_densities)):
  for j in range(0,len(current_densities)):
    db_attach_names.append("optimised_at_J%d_%dA_tested_at_J%d_%dA"%(int(float(current_densities[i][0])),int(float(current_densities[i][2])),int(float(current_densities[j][0])),int(float(current_densities[j][2]))))  

conn.commit()

if do_post_analysis:
  
  i=0
  while i<len(current_densities):
    for j in range(0,len(db_files)):
      if current_densities[i] in db_files[j][:-11]:
	c.execute("ATTACH DATABASE '%s' AS database_J%d_%dA"%(db_files[j],int(float(current_densities[i][0])),int(float(current_densities[i][2]))))
	i=i+1
	if i>=len(current_densities):
	  break

  #i=0
  #while i<len(current_densities):
    #for j in range(0,len(db_files)):
      #if current_densities[i] in db_files[j][:-11]:
	#c.execute("INSERT INTO best_combined.machine_input_parameters_%d_%dA SELECT * FROM database_J%d_%dA.machine_input_parameters"%(int(float(current_densities[i][0])),int(float(current_densities[i][2])),int(float(current_densities[i][0])),int(float(current_densities[i][2]))))
	#i=i+1
	#if i>=len(current_densities):
	  #break   

  shutil.copy(db_files[0],dir+'best_combined.db')

  c.execute("ATTACH DATABASE '%s' as best_combined"%(dir+'best_combined.db'))

  c.execute("DELETE FROM best_combined.machine_semfem_output")
  c.execute("DELETE FROM best_combined.machine_input_parameters")

  for i in range(0,len(current_densities)):
    c.execute("CREATE TABLE best_combined.machine_semfem_output_optimised_at_J%d_%dA AS SELECT * FROM best_combined.machine_semfem_output WHERE 0"%(float(current_densities[i][0]),float(current_densities[i][2])))
    c.execute("CREATE TABLE best_combined.machine_input_parameters_optimised_at_%d_%dA AS SELECT * FROM best_combined.machine_input_parameters WHERE 0"%(float(current_densities[i][0]),float(current_densities[i][2])))
    
  #c.execute("CREATE TABLE best_combined.machine_semfem_output_all AS SELECT * FROM best_combined.machine_semfem_output WHERE 0") #ALSO WORKS, BUT ADDS UNWANTED COLUMNS
  c.execute("CREATE TABLE IF NOT EXISTS best_combined.machine_semfem_output_all (primary_key integer)")
  c.execute("ALTER TABLE best_combined.machine_semfem_output_all ADD COLUMN optimised_at_J TEXT")
  for i in range(1, len(pu_scalars)):
    c.execute("ALTER TABLE best_combined.machine_semfem_output_all ADD COLUMN %s REAL"%(pu_scalars[i][0]))
  
  c.execute("CREATE TABLE best_combined.machine_semfem_output_filtered AS SELECT * FROM best_combined.machine_semfem_output_all WHERE 0")
  conn.commit()
  #c.execute("CREATE TABLE best_combined.machine_semfem_output_filtered AS SELECT * FROM best_combined.machine_semfem_output_all")
  
  #c.execute("INSERT INTO best_combined.machine_semfem_output_J%d_%dA SELECT * FROM best_combined.machine_semfem_output"%(float(current_densities[j][0]),float(current_densities[j][2])))

  #c.execute("ALTER TABLE best_combined.machine_semfem_output_all ADD COLUMN real_pk INTEGER")
  

  c.execute("DROP TABLE best_combined.machine_semfem_output")
  c.execute("DROP TABLE best_combined.machine_input_parameters")
  c.execute("DROP TABLE best_combined.info")
  c.execute("DROP TABLE best_combined.flux_density_output")

  i=0
  while i<len(current_densities):
    for j in range(0,len(db_files)):
      if current_densities[i] in db_files[j][:-11]:
	c.execute("INSERT INTO best_combined.machine_input_parameters_optimised_at_%d_%dA SELECT * FROM database_J%d_%dA.machine_input_parameters"%(int(float(current_densities[i][0])),int(float(current_densities[i][2])),int(float(current_densities[i][0])),int(float(current_densities[i][2]))))
	i=i+1
	if i>=len(current_densities):
	  break   

  conn.commit()
  conn.close()
  conn = sqlite3.connect(dir+'pu_scalar_values.db')
  c = conn.cursor() 
  c.execute("ATTACH DATABASE '%s' as best_combined"%(dir+'best_combined.db'))  
  #i=0
  #while i<len(current_densities):
    #for j in range(0,len(db_files)):
      #if current_densities[i] in db_files[j][:-11]:
	#c.execute("ATTACH DATABASE '%s' AS database_J%d_%dA"%(db_files[j],int(float(current_densities[i][0])),int(float(current_densities[i][2]))))
	#i=i+1
	#if i>=len(current_densities):
	  #break
  

  if len(current_densities)**2 <=10:
    for i in range(0,len(current_densities)**2):
      c.execute("ATTACH DATABASE '%s' AS '%s'"%(db_files[i],db_attach_names[i]))

  for k in pk:
    for i in range(0,len(current_densities)):
      c.execute("INSERT INTO best_combined.machine_semfem_output_optimised_at_J%d_%dA (%s) VALUES (%d)"%(int(float(current_densities[i][0])),int(float(current_densities[i][2])),select_string,k[0]))
      c.execute("INSERT INTO best_combined.machine_semfem_output_all (primary_key,optimised_at_J) VALUES (%d,%s)"%(k[0],current_densities[i]))
    for j in range(1,len(pu_scalars)):  
      tmp = []
      pu_weighted = 0
      for i in range(0,len(current_densities)**2):
	
	if i%len(current_densities) == 0:
	  tmp = []
	  pu_weighted = 0
	  
	if len(current_densities)**2 > 10:  
	  c.execute("ATTACH DATABASE '%s' AS '%s'"%(db_files[i],db_attach_names[i]))
	c.execute("SELECT %s FROM %s.machine_semfem_output WHERE primary_key = ?"%(pu_scalars[j][0],db_attach_names[i]),(k[0],))
	tmp.append(c.fetchone()[0])
	pu_weighted = pu_weighted + weights[i%len(current_densities)]*(tmp[i%len(current_densities)]/pu_scalars[j][2][i%len(current_densities)])
	if len(current_densities)**2 > 10:
	  c.execute("DETACH DATABASE '%s'"%(db_attach_names[i]))
	
	if (i+1)%len(current_densities) == 0:
	  c.execute("UPDATE best_combined.machine_semfem_output_optimised_at_J%d_%dA SET %s = ? WHERE %s = ?"%(int(float(current_densities[int(i/len(current_densities))][0])),int(float(current_densities[int(i/len(current_densities))][2])),pu_scalars[j][0],select_string),(pu_weighted,k[0]))
	  c.execute("UPDATE best_combined.machine_semfem_output_all SET %s = ? WHERE %s = ? AND optimised_at_J = ?"%(pu_scalars[j][0],select_string),(pu_weighted,k[0],current_densities[int(i/len(current_densities))]))

  #filter_conditions_string = ''
  filter_conditions_string = '%s < %g'%(pu_scalars[1][0],pu_scalars[1][4][1])
  for i in range(2,len(pu_scalars)):
    filter_conditions_string = filter_conditions_string + ' AND %s < %g'%(pu_scalars[i][0],pu_scalars[i][4][1])
    filter_conditions_string = filter_conditions_string + ' AND %s > %g'%(pu_scalars[i][0],pu_scalars[i][4][0])

  #print(filter_conditions_string)
  #c.execute("INSERT INTO best_combined.machine_semfem_output_filtered SELECT * FROM best_combined.machine_semfem_output_all") #copies without filtering
  #c.execute("INSERT INTO best_combined.machine_semfem_output_filtered SELECT * FROM best_combined.machine_semfem_output_all WHERE %s < %g"%(pu_scalars[2][0],pu_scalars[2][4][1])) #copies only filtered results
  c.execute("INSERT INTO best_combined.machine_semfem_output_filtered SELECT * FROM best_combined.machine_semfem_output_all WHERE %s"%(filter_conditions_string)) #copies only filtered results
  #c.execute("SELECT * FROM best_combined.machine_semfem_output_all")
  #print(c.fetchall())

  c.execute("DETACH DATABASE best_combined") 

if draw_3d_graph_filtered or draw_3d_graph_all:
  X = []
  Y = []
  Z = []

  c.execute("ATTACH DATABASE '%s' as best_combined"%(dir+'best_combined.db'))

  if draw_3d_graph_all:
    table = 'best_combined.machine_semfem_output_all'
  elif draw_3d_graph_filtered:
    table = 'best_combined.machine_semfem_output_filtered'

  for i in range(0,len(current_densities)):
    c.execute("SELECT %s FROM %s WHERE optimised_at_J = %s"%(pu_scalars[plot_objectives[0]][0],table,current_densities[i]))
    X.append(c.fetchall())
    
    c.execute("SELECT %s FROM %s WHERE optimised_at_J = %s"%(pu_scalars[plot_objectives[1]][0],table,current_densities[i]))
    Y.append(c.fetchall())
    
    c.execute("SELECT %s FROM %s WHERE optimised_at_J = %s"%(pu_scalars[plot_objectives[2]][0],table,current_densities[i]))
    Z.append(c.fetchall())    

  fig = pl.figure()
  ax = fig.add_subplot(111, projection='3d')

  ax.set_xlabel(pu_scalars[plot_objectives[0]][0]+'_score',fontsize=font_size)
  ax.set_ylabel(pu_scalars[plot_objectives[1]][0]+'_score',fontsize=font_size)
  ax.set_zlabel(pu_scalars[plot_objectives[2]][0]+'_score',fontsize=font_size)

  ax.tick_params(axis='x', labelsize=font_size)
  ax.tick_params(axis='y', labelsize=font_size)
  ax.tick_params(axis='z', labelsize=font_size)  

  #ax.set_xlim3d(sorted_sequence[len(sorted_sequence)-3][4],1.1*lim[0])
  #ax.set_ylim3d(sorted_sequence[len(sorted_sequence)-2][4],1.1*lim[1])
  #ax.set_zlim3d(sorted_sequence[len(sorted_sequence)-1][4],1.1*lim[2])

  for i in range(0,len(current_densities)):
    ax.scatter(X[i], Y[i], Z[i], s=10, color=colors[i], marker='s', label='optimised at J=%d.%dA'%(int(float(current_densities[i][0])),int(float(current_densities[i][2]))))
    #print(len(X[i]))
 
  #pl.legend(loc='upper right', fontsize = font_size)
  pl.legend(loc='upper center', fontsize = font_size)



conn.commit()
conn.close()
pl.show()