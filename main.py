#!/usr/bin/python

#Title : Main script which controls the semfem script, analytical script and optimization processes.
#Author: Andreas Joss

from __future__ import division

import numpy as np
import pylab as pl
import time
import os,sys
import shutil
import threading
import sqlite3
from pygame import mixer

enable_ideal_value_calculation = 0
enable_remaining_points_optimisation = 1

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

#def thread_nugget(thread_num):
  #for i in range (0,len(pk_all_but_ideal_removed_array)):
    #os.system("./optimization.py -pk %d"%(pk_all_but_ideal_removed_array[i]))

#setup databse connection
conn = sqlite3.connect('optimise.db')
c = conn.cursor()

#optimise weight combinations corresponding to ideal values (no threading)
#if enable_ideal_value_calculation == 1:
ideal_optimization_sequence = []

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_eff',))
ideal_optimization_sequence.append(c.fetchone()[0])

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_ripple',))
ideal_optimization_sequence.append(c.fetchone()[0])

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_mass_total',))
ideal_optimization_sequence.append(c.fetchone()[0])

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_average',))
ideal_optimization_sequence.append(c.fetchone()[0])

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pf',))
ideal_optimization_sequence.append(c.fetchone()[0])  

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_density',))
ideal_optimization_sequence.append(c.fetchone()[0])  

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pm_mass',))
ideal_optimization_sequence.append(c.fetchone()[0])  

#sort the sequence
sorted_sequence = sorted(ideal_optimization_sequence)
  
if enable_ideal_value_calculation == 1:  
  for i in range (0,len(sorted_sequence)):
    if sorted_sequence[i] > 0:
      #check if this optimisation for this weight combination has not already been completed
      c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(sorted_sequence[i],))
      already_optimised = c.fetchone()[0]
      
      if already_optimised == 0:
	os.system("./optimization.py -pk %d"%(sorted_sequence[i]))
	mixer.init() #you must initialize the mixer
	#alert=mixer.Sound('../sounds/bubblebrew.wav')
	alert=mixer.Sound('../sounds/Tamba_112.wav')
	alert.play()

#optimise remaining weight combinations (threading implemented)
if enable_remaining_points_optimisation == 1:
  c.execute("SELECT primary_key FROM ideal_value_keys")
  temp_array = c.fetchall()
  pk_ideal_array = []

  for i in range (0,len(temp_array)):
    pk_ideal_array.append(temp_array[i][0]) 

  c.execute("SELECT primary_key FROM machine_input_parameters")
  temp_array = c.fetchall()
  pk_all_but_ideal_removed_array = []
  
  for i in range (3,len(temp_array)): #start at 3 to skip upper and lower limit values
    found = 0
    for j in range (0,len(pk_ideal_array)):
      if temp_array[i][0] == pk_ideal_array[j]:
	found = 1
    
    c.execute("SELECT weight_torque_density FROM machine_input_parameters WHERE primary_key = ?",(temp_array[i][0],))
    weight_torque_density_temp = c.fetchone()[0]

    c.execute("SELECT weight_pm_mass FROM machine_input_parameters WHERE primary_key = ?",(temp_array[i][0],))
    weight_pm_mass_temp = c.fetchone()[0]
    
    c.execute("SELECT weight_torque_ripple FROM machine_input_parameters WHERE primary_key = ?",(temp_array[i][0],))
    weight_torque_ripple_temp = c.fetchone()[0]    
    
    if found == 0 and weight_torque_density_temp>=0.5 and weight_pm_mass_temp<=0.5 and weight_torque_ripple_temp<=0.9:
      pk_all_but_ideal_removed_array.append(temp_array[i][0])


  print "Number of weight combinations to be optimised: %d"%(len(pk_all_but_ideal_removed_array))
  for i in range (0,len(pk_all_but_ideal_removed_array)):
    #print "main.py -- read new primary_key papa"
    c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(pk_all_but_ideal_removed_array[i],))
    already_optimised = c.fetchone()[0]
    
    if already_optimised == 0:
      #print "main.py -- start new primary_key optimisation"
      os.system("./optimization.py -pk %d"%(pk_all_but_ideal_removed_array[i]))
      #mixer.init() #you must initialize the mixer
      ##alert=mixer.Sound('../sounds/bubblebrew.wav')
      #alert=mixer.Sound('../sounds/Tamba_112.wav')
      #alert.play()      
    
    #t = threading.Thread(target=thread_nugget)
    #t.start()
      
      
conn.close()