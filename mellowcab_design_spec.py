#!/usr/bin/python
#-*- coding: utf-8 -*-

#import all other necessary modules
from __future__ import division
from math import pi,degrees,sin,cos,sqrt
#import locale
import numpy as np
import pylab as pl
from prettytable import PrettyTable 

allow_wind = False

LINEWIDTH=2
font_size=14
axis_label_font_size=18
tick_label_font_size=16
legend_font_size=18
legend_title_fontsize=20
legend_RHS = True

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']
fig_x_cm = 8
fig_y_cm = 5
fig_x_inch_pdf = 18
fig_y_inch_pdf = 8
margin_left = 0.05
margin_right = 0.74
margin_bottom = 0.08
margin_top = 0.92
number_of_angles = 4

pretty_table = PrettyTable(['Output Variable','Value','[Unit]'])
pretty_table.align = 'l'
pretty_table.border= True  

max_degrees = 25
mellowcab_max_speed_kmph = 50 #max spoed vanaf Neil
#gear_ratio = 12.25
#gear_ratio = 17.2	#present mellowcabs gearbox
gear_ratio = 1
angle_degrees_at_which_rated_torque = 15
vehicle_speed_kmph_at_which_rated_torque = 10
use_constant_coeff_rolling_resistance = True
power_output_limit_kw = 4


gravitational_acc = 9.807
air_density = 1.2041
coeff_rolling_resistance = 0.015
coeff_drag = 0.5
front_area = 1.2*1.7 #m^2 #dimensies vanaf Neil gekry
unloaded_mass = 250
passenger_mass = 250
number_of_motors = 2
total_mass = unloaded_mass + passenger_mass

wheel_diameter = 0.57 #18 duim profiel (is dit die rim se diameter, of die bande se diameter?)= 0.4572m
wheel_radius = wheel_diameter/2

mellowcab_max_speed_mps  = mellowcab_max_speed_kmph*(1000/(60*60))
motor_max_rpm = (mellowcab_max_speed_mps/wheel_radius)*(60/(2*pi))*gear_ratio


datapoints = 50
speed_range_kmph = np.linspace(0,mellowcab_max_speed_kmph,datapoints)
motor_speed_range_rpm = np.linspace(0,motor_max_rpm,datapoints)


if allow_wind:
  wind_max_speed_mps = 10
else:
  wind_max_speed_mps = 0

coeff_rolling_resistance_max = 0.01*(1+(3.6/100)*mellowcab_max_speed_mps)


def tick_function(X):
    V = 1/(1+X)
    return ["%.3f" % z for z in V]






FORCE_friction = np.zeros(len(speed_range_kmph))
FORCE_drag = np.zeros(len(speed_range_kmph))
FORCE_gravity = np.zeros(len(speed_range_kmph))
FORCE_to_maintain_const_speed = np.zeros(len(speed_range_kmph))

TORQUE_friction = np.zeros(len(speed_range_kmph))
TORQUE_drag = np.zeros(len(speed_range_kmph))
TORQUE_gravity = np.zeros(len(speed_range_kmph))
TORQUE_to_maintain_const_speed = np.zeros(len(speed_range_kmph))
POWER_delivered_kW = np.zeros(len(speed_range_kmph))

#= np.zeros(len(speed_range_kmph))


for v in range(0,len(speed_range_kmph)):
  mps  = speed_range_kmph[v]*(1000/(60*60))

  if use_constant_coeff_rolling_resistance:
    coeff_rolling_resistance = 0.015
  else:
    coeff_rolling_resistance = 0.01*(1+(3.6/100)*mps)

  FORCE_friction[v] = total_mass*gravitational_acc*coeff_rolling_resistance*cos(max_degrees*(pi/180))
  FORCE_drag[v] = 0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)
  FORCE_gravity[v] = total_mass*gravitational_acc*sin(max_degrees*(pi/180))

  FORCE_to_maintain_const_speed[v] = FORCE_friction[v]+FORCE_drag[v]+FORCE_gravity[v]

  #Torque loads which the motor experiences
  TORQUE_friction[v] = (FORCE_friction[v]*wheel_radius)/(gear_ratio*number_of_motors)
  TORQUE_drag[v]     = (FORCE_drag[v]*wheel_radius)/(gear_ratio*number_of_motors)
  TORQUE_gravity[v]  = (FORCE_gravity[v]*wheel_radius)/(gear_ratio*number_of_motors)
  
  TORQUE_to_maintain_const_speed[v] = TORQUE_friction[v] + TORQUE_drag[v] + TORQUE_gravity[v]

  POWER_delivered_kW[v] = ((FORCE_to_maintain_const_speed[v]*mps)/1000)/(gear_ratio*number_of_motors)

mellowcab_max_torque = FORCE_to_maintain_const_speed[v]*wheel_radius
mellowcab_max_wheel_rpm = (mellowcab_max_speed_mps/wheel_radius)*(60/(2*pi))

motor_max_rpm = mellowcab_max_wheel_rpm*gear_ratio
motor_max_torque = (mellowcab_max_torque/gear_ratio)/number_of_motors

motor_max_power_output = (motor_max_torque*motor_max_rpm*((2*pi)/60))/1000


pretty_table.add_row(["Mellowcab_max_speed",mellowcab_max_speed_mps,"[m/s]"])
pretty_table.add_row(["COEFF_rolling_resistance_max",coeff_rolling_resistance_max,"[p.u.]"])
pretty_table.add_row(["COEFF_drag_resistance",coeff_drag,"[p.u.]"])
pretty_table.add_row(["FORCE_friction",FORCE_friction[v],"[N]"])
pretty_table.add_row(["FORCE_drag",FORCE_drag[v],"[N]"])
pretty_table.add_row(["FORCE_gravity",FORCE_gravity[v],"[N]"])
pretty_table.add_row(["FORCE_total",FORCE_to_maintain_const_speed[v],"[N]"])
pretty_table.add_row(["mellowcab_max_torque",mellowcab_max_torque,"[N.m]"])
pretty_table.add_row(["mellowcab_max_wheel_rpm",mellowcab_max_wheel_rpm,"[rpm]"])
pretty_table.add_row(["motor_max_torque",motor_max_torque,"[N.m]"])
pretty_table.add_row(["motor_max_rpm",motor_max_rpm,"[rpm]"])
pretty_table.add_row(["motor_power_output@max_speed",motor_max_power_output,"[kW]"])

print pretty_table

#plotting
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

#text box with basic infocolours
#props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
props = dict(boxstyle='square', facecolor='white', alpha=0.5)
#textstr = 'm^{2} = %f'%(coeff_drag)
textstr = 'Total mass [kg] = %.2f\nIncline angle [deg] = %.2f\nGear ratio = %.2f\nWheel diameter [m] = %.2f\nCoef. rolling resistance @ %d km/h = %.4f\nCoef. drag = %.2f\nFrontal area [m^{2}] = %.2f'%(total_mass,max_degrees,gear_ratio,wheel_diameter,mellowcab_max_speed_kmph,coeff_rolling_resistance_max,coeff_drag,front_area)
#textstr = '$\mu=%.2f$\n$\mathrm{median}=%.2f$\n$\sigma=%.2f$'%(mu, median, sigma)


###################
#fig = pl.figure(1)
#ax1 = fig.add_subplot(111)
#ax2 = ax1.twiny()
#ax3 = ax1.twinx()
#pl.title('Vehichle Forces vs Speed',fontsize=16,y=1.08)
#line1 = ax1.plot(speed_range_kmph,FORCE_friction,linewidth=LINEWIDTH,label='Rolling Resistance [N]',c='b')
#line2 = ax1.plot(speed_range_kmph,FORCE_drag,linewidth=LINEWIDTH,label='Aerodynamic Drag [N]',c='g')
##line3 = ax2.plot(motor_speed_range_rpm,FORCE_gravity,linewidth=LINEWIDTH,label='Gravity',c='r')
#line3 = ax1.plot(speed_range_kmph,FORCE_gravity,linewidth=LINEWIDTH,label='Gravity [N]',c='r')
#line4 = ax1.plot(speed_range_kmph,FORCE_to_maintain_const_speed,linewidth=LINEWIDTH,label='Total Force Load [N]',c='m')
#line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
#ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=font_size)
#ax1.set_ylabel(r'Force [N]',fontsize=font_size)
#ax3.set_ylabel(r'Power [kW]',fontsize=font_size)
#ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

## added these three lines
#lines = line1+line2+line3+line4+line5
#labels = [l.get_label() for l in lines]
#ax1.legend(lines, labels, loc='upper left')
#ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=font_size)
#ax2.set_xlim(0,motor_max_rpm)
#ax1.grid()
##ax2.grid()


##################
fig = pl.figure(2,figsize=(fig_x_cm,fig_y_cm))
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
#pl.title('Motor Load vs Speed',fontsize=16,y=1.12)
line1 = ax1.plot(speed_range_kmph,TORQUE_friction,linewidth=LINEWIDTH,label='Rolling Resistance [N.m]',c='b')
line2 = ax1.plot(speed_range_kmph,TORQUE_drag,linewidth=LINEWIDTH,label='Aerodynamic Drag [N.m]',c='g')
line3 = ax2.plot(motor_speed_range_rpm,TORQUE_gravity,linewidth=LINEWIDTH,label='Gravity [N.m]',c='r')
line4 = ax1.plot(speed_range_kmph,TORQUE_to_maintain_const_speed,linewidth=LINEWIDTH,label='Total Torque Load [N.m]',c='m')
line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=axis_label_font_size)
ax1.set_ylabel(r'Torque [N.m]',fontsize=axis_label_font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=axis_label_font_size)
#ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

#change fontsize of tick labels
#pl.tick_params(axis='both', which='major', labelsize=18)
ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax3.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

# Shrink current axis by 20%colours
box = ax1.get_position()
if legend_font_size<=16:
  ax1.set_position([box.x0, box.y0, box.width, box.height * 0.75])
  ax2.set_position([box.x0, box.y0, box.width, box.height * 0.75])
  ax3.set_position([box.x0, box.y0, box.width, box.height * 0.75])
else:
  ax1.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  ax2.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  ax3.set_position([box.x0, box.y0, box.width, box.height * 0.73])

#ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])

# added these three lines
lines = line1+line2+line3+line4+line5
labels = [l.get_label() for l in lines]
# Put a legend to the right of the current axis
art = []
if legend_RHS == False:
  if legend_font_size<=16:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.15),ncol=number_of_angles, fontsize=legend_font_size)
  else:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.18),ncol=number_of_angles, fontsize=legend_font_size)
else:
  lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.5), fontsize=legend_font_size)
  
art.append(lgd)
#ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.1, 0.5))
#ax1.legend(lines, labels, loc='center left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=axis_label_font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#ax2.grid()
fig.set_size_inches(fig_x_inch_pdf, fig_y_inch_pdf)

if legend_RHS == False:
  pl.subplots_adjust(left=0.05, right=0.95)
else:
  pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)

#pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)
if legend_RHS == False:
  fig.savefig("motor_torque_requirement.pdf", additional_artists=art,bbox_inches='tight')
else:
  fig.savefig("motor_torque_requirement.pdf", additional_artists=art)
  
#fig.savefig("motor_torque_requirement.pdf")
#fig.savefig("motor_torque_requirement.pdf", bbox_inches='tight')

#fig = pl.figure(3)
#ax1 = fig.add_subplot(111)
#ax2 = ax1.twiny()
#ax3 = ax1.twinx()
#pl.title('Traction Torque vs Speed',fontsize=16,y=1.08)
#line1 = ax1.plot(speed_range_kmph,TORQUE_friction*gear_ratio,linewidth=LINEWIDTH,label='Rolling Resistance [N.m]',c='b')
#line2 = ax1.plot(speed_range_kmph,TORQUE_drag*gear_ratio,linewidth=LINEWIDTH,label='Aerodynamic Drag [N.m]',c='g')
#line3 = ax2.plot(motor_speed_range_rpm,TORQUE_gravity*gear_ratio,linewidth=LINEWIDTH,label='Gravity [N.m]',c='r')
#line4 = ax1.plot(speed_range_kmph,TORQUE_to_maintain_const_speed*gear_ratio,linewidth=LINEWIDTH,label='Total Torque Load [N.m]',c='m')
#line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
#ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=font_size)
#ax1.set_ylabel(r'Torque [N.m]',fontsize=font_size)
#ax3.set_ylabel(r'Power [kW]',fontsize=font_size)
#ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)
##ax1.yaxis.set_ticks(np.arange(0,max(TORQUE_to_maintain_const_speed*gear_ratio)),25)

### added these three lines
#lines = line1+line2+line3+line4+line5
#labels = [l.get_label() for l in lines]
#ax1.legend(lines, labels, loc='upper left')
#ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=font_size)
#ax2.set_xlim(0,motor_max_rpm)
#ax1.grid()
##ax2.grid()


##################
fig = pl.figure(4, figsize=(fig_x_cm,fig_y_cm))
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
#pl.title('Motor Load vs Speed for Various Angles of Inclination',fontsize=16,y=1.12)
line = []
line_power = []
lines = None
incline_angle = 0
for i in range(0,number_of_angles):
  for v in range(0,len(speed_range_kmph)):
    mps  = speed_range_kmph[v]*(1000/(60*60))

    if use_constant_coeff_rolling_resistance:
      coeff_rolling_resistance = 0.015
    else:
      coeff_rolling_resistance = 0.01*(1+(3.6/100)*mps)

    FORCE_friction[v] = total_mass*gravitational_acc*coeff_rolling_resistance*cos(incline_angle*(pi/180))
    FORCE_drag[v] = 0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)
    FORCE_gravity[v] = total_mass*gravitational_acc*sin(incline_angle*(pi/180))

    FORCE_to_maintain_const_speed[v] = FORCE_friction[v]+FORCE_drag[v]+FORCE_gravity[v]

    #Torque loads which the motor experiences
    TORQUE_friction[v] = (FORCE_friction[v]*wheel_radius)/(gear_ratio*number_of_motors)
    TORQUE_drag[v]     = (FORCE_drag[v]*wheel_radius)/(gear_ratio*number_of_motors)
    TORQUE_gravity[v]  = (FORCE_gravity[v]*wheel_radius)/(gear_ratio*number_of_motors)
    
    TORQUE_to_maintain_const_speed[v] = TORQUE_friction[v] + TORQUE_drag[v] + TORQUE_gravity[v]
    
    #if(np.isclose(incline_angle, angle_degrees_at_which_rated_torque, rtol=1e-02, atol=1e-02, equal_nan=False) and np.isclose(speed_range_kmph[v], vehicle_speed_kmph_at_which_rated_torque, rtol=1e-02, atol=1e-02, equal_nan=False)):
    ##if (incline_angle == angle_degrees_at_which_rated_torque) and (v == vehicle_speed_kmph_at_which_rated_torque):
      #rated_torque = TORQUE_to_maintain_const_speed[v]
    #else:
      #rated_torque = -99

    POWER_delivered_kW[v] = ((FORCE_to_maintain_const_speed[v]*mps)/1000)/(gear_ratio*number_of_motors)

  line.append(ax1.plot(speed_range_kmph,TORQUE_to_maintain_const_speed,linewidth=LINEWIDTH,label=r'$\alpha$ = $%d^{\circ}$'%(incline_angle)))
  line_power.append(ax3.plot(speed_range_kmph,POWER_delivered_kW,'--',linewidth=LINEWIDTH,label=r'$\alpha$ = $%d^{\circ}$'%(incline_angle)))
  incline_angle = incline_angle+5

lines = line[0]
for i in range(1,len(line)):
  lines = lines + line[i]

lines_power = line_power[0]
for i in range(1,len(line_power)):
  lines_power = lines_power + line_power[i]

#line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=axis_label_font_size)
ax1.set_ylabel(r'Torque [N.m]',fontsize=axis_label_font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=axis_label_font_size)
#ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

#change fontsize of tick labels
#pl.tick_params(axis='both', which='major', labelsize=18)
ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax3.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

## Shrink current axis by 20%
box = ax1.get_position()

#if legend_font_size<=16:
ax1.set_position([box.x0, box.y0, box.width, box.height * 0.75])
ax2.set_position([box.x0, box.y0, box.width, box.height * 0.75])
ax3.set_position([box.x0, box.y0, box.width, box.height * 0.75])
#else:
  #ax1.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  #ax2.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  #ax3.set_position([box.x0, box.y0, box.width, box.height * 0.73])
#ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])

# added these three lines
labels = [l.get_label() for l in lines]
labels_power = [l.get_label() for l in lines_power]

# Put a legend to the right of the current axis
art = []
if legend_RHS == False:
  if legend_font_size<=16:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.13),ncol=number_of_angles, fontsize=legend_font_size)
  else:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.15),ncol=number_of_angles, fontsize=legend_font_size)
else:
  lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.35), fontsize=legend_font_size, title='Torque')
  ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
  lgd_power = ax2.legend(lines_power, labels_power, loc='center left', bbox_to_anchor=(1.05, 0.65), fontsize=legend_font_size, title='Power')
  ax2.get_legend().get_title().set_fontsize(legend_title_fontsize)
  
art.append(lgd)
#ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.1, 0.5))
#ax1.legend(lines, labels, loc='center left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=axis_label_font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#pl.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=1, hspace=1)

fig.set_size_inches(fig_x_inch_pdf, fig_y_inch_pdf)
if legend_RHS == False:
  pl.subplots_adjust(left=0.05, right=0.95)
else:
  pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)

if legend_RHS == False:
  fig.savefig("motor_torque_load_at_various_angles.pdf", additional_artists=art,bbox_inches='tight')
else:
  fig.savefig("motor_torque_load_at_various_angles.pdf", additional_artists=art)
#fig.savefig("motor_torque_load_at_various_angles.pdf")
#fig.savefig("motor_torque_load_at_various_angles.pdf", bbox_inches='tight')





#####################

#obtain angle at which power limit enters at maximum speed
mps  = speed_range_kmph[len(speed_range_kmph)-1]*(1000/(60*60))
C = coeff_rolling_resistance
B = (power_output_limit_kw*1000*gear_ratio - (0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)))/(total_mass*gravitational_acc)

n = [-2,-1,0,1,2,3,4,5]
angle_max_speed = np.zeros(len(n))
angle_max_speed[0] = 1.1

for i in range(0,len(n)):
  #angle_max_speed[i] = 2*(np.arctan((1-np.sqrt(-B**2 + C**2 + 1))/(B+C) + np.pi*n[i]))
  #angle_max_speed[i] = 2*(np.arctan((1+np.sqrt(-B**2 + C**2 + 1))/(B+C) + np.pi*n[i]))
  if i>0: angle_max_speed[i] = angle_max_speed[i-1]+0.005
  
  print("angle_max_speed = %.3f with n = %d"%(angle_max_speed[i],n[i]))
  FORCE_friction_k = total_mass*gravitational_acc*coeff_rolling_resistance*cos(angle_max_speed[i]*(pi/180))
  FORCE_gravity_k = total_mass*gravitational_acc*sin(angle_max_speed[i]*(pi/180))
  FORCE_drag_k = 0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)
  FORCE_to_maintain_const_speed_k = FORCE_friction_k+FORCE_drag_k+FORCE_gravity_k
  POWER_delivered_kW_k = ((FORCE_to_maintain_const_speed_k*mps)/1000)/(gear_ratio*number_of_motors)
  print("POWER_delivered_kW_k = %.3f with n = %d"%(POWER_delivered_kW_k,n[i]))
  

#####################

incline_angle = 15
K = 0.5*air_density*coeff_drag*front_area
coeff_rolling_resistance = 0.015
FORCE_friction_k = total_mass*gravitational_acc*coeff_rolling_resistance*cos(incline_angle*(pi/180))
FORCE_gravity_k = total_mass*gravitational_acc*sin(incline_angle*(pi/180))
A = (FORCE_friction_k+FORCE_gravity_k)/K
B = (power_output_limit_kw*1000*gear_ratio)/K

#solution obtained from Alpha Wolfram "solve for v^3 + A*v = B, for v
v_mps = ((np.sqrt(3)*np.sqrt(4*A**3+27*B**2)+9*B)**(1/3))/(2**(1/3) * 3**(2/3)) - (((2/3)**(1/3))*A)/((np.sqrt(3)*np.sqrt(4*A**3+27*B**2) + 9*B)**(1/3))
kmph = v_mps*(3600/1000)

print("A = %.3f "%(A))
print("B = %.3f "%(B))
print("v_mps = %.3f "%(v_mps))
print("kmph = %.3f "%(kmph))



#SAME AS FIGURE 4, but with Power limit
##################
fig = pl.figure(5, figsize=(fig_x_cm,fig_y_cm))
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
#pl.title('Motor Load vs Speed for Various Angles of Inclination',fontsize=16,y=1.12)
line = []
line_power = []
lines = None

auxiliary_angle = True
if auxiliary_angle: number_of_angles = number_of_angles+1
incline_angle = 0
#for i in range(0,number_of_angles):
for i in range(0,number_of_angles):
  if i == number_of_angles-1: incline_angle = 1.12
  
  TORQUE_to_maintain_const_speed_list = []
  POWER_delivered_kW_list = []
  kmph_list = []

  coeff_rolling_resistance = 0.015

  K = 0.5*air_density*coeff_drag*front_area
  FORCE_friction_k = total_mass*gravitational_acc*coeff_rolling_resistance*cos(incline_angle*(pi/180))
  FORCE_gravity_k = total_mass*gravitational_acc*sin(incline_angle*(pi/180))
  A = (FORCE_friction_k+FORCE_gravity_k)/K
  B = (power_output_limit_kw*1000*gear_ratio)/K
  
  max_v_mps = ((np.sqrt(3)*np.sqrt(4*A**3+27*B**2)+9*B)**(1/3))/(2**(1/3) * 3**(2/3)) - (((2/3)**(1/3))*A)/((np.sqrt(3)*np.sqrt(4*A**3+27*B**2) + 9*B)**(1/3))
  max_kmph = max_v_mps*(3600/1000)
  
  if max_kmph>mellowcab_max_speed_kmph:
    max_kmph=mellowcab_max_speed_kmph
    max_v_mps=mellowcab_max_speed_kmph*(1000/3600)
  
  TORQUE_friction_k = (FORCE_friction_k*wheel_radius)/(gear_ratio*number_of_motors)
  TORQUE_gravity_k  = (FORCE_gravity_k*wheel_radius)/(gear_ratio*number_of_motors)
  
  kmph_list = np.linspace(0,max_kmph)
  #inc_kmph = max_kmph/int(max_kmph) #number should be close to 1
  present_kmph = 0
  for v in range(0,len(kmph_list)):
    present_mps = kmph_list[v]*(1000/3600)
    
    FORCE_drag_k = 0.5*air_density*coeff_drag*front_area*((present_mps+wind_max_speed_mps)**2)

    FORCE_to_maintain_const_speed_k = FORCE_friction_k + FORCE_gravity_k + FORCE_drag_k

    #Torque loads which the motor experiences
    
    TORQUE_drag_k     = (FORCE_drag_k*wheel_radius)/(gear_ratio*number_of_motors)
   
    TORQUE_to_maintain_const_speed_list.append(TORQUE_friction_k + TORQUE_drag_k + TORQUE_gravity_k)
    
    POWER_delivered_kW_list.append(((FORCE_to_maintain_const_speed_k*present_mps)/1000)/(gear_ratio*number_of_motors))
    motor_speed_rpm = (present_mps/wheel_radius)*(60/(2*pi))*gear_ratio
    vehicle_speed_kmph = present_mps*(3600/1000)
    
    if v==len(kmph_list)-1:
      print "Rated power (%.2f) [kW] reached at vehicle speed %.2f [kmph], motor speed %.2f [rpm] and motor torque %.2f [Nm]"%(POWER_delivered_kW_list[len(POWER_delivered_kW_list)-1],vehicle_speed_kmph,motor_speed_rpm,TORQUE_to_maintain_const_speed_list[len(TORQUE_to_maintain_const_speed_list)-1])
    
  if i == number_of_angles-1:
    line.append(ax1.plot(kmph_list,TORQUE_to_maintain_const_speed_list,linewidth=LINEWIDTH,label=r'$\alpha$ = $%.2f^{\circ}$'%(incline_angle)))
    line_power.append(ax3.plot(kmph_list,POWER_delivered_kW_list,'--',linewidth=LINEWIDTH,label=r'$\alpha$ = $%.2f^{\circ}$'%(incline_angle)))
  else:  
    line.append(ax1.plot(kmph_list,TORQUE_to_maintain_const_speed_list,linewidth=LINEWIDTH,label=r'$\alpha$ = $%d^{\circ}$'%(incline_angle)))
    line_power.append(ax3.plot(kmph_list,POWER_delivered_kW_list,'--',linewidth=LINEWIDTH,label=r'$\alpha$ = $%d^{\circ}$'%(incline_angle)))
  incline_angle = incline_angle+5

lines = line[0]
for i in range(1,len(line)):
  lines = lines + line[i]

lines_power = line_power[0]
for i in range(1,len(line_power)):
  lines_power = lines_power + line_power[i]

#line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=axis_label_font_size)
ax1.set_ylabel(r'Torque [N.m]',fontsize=axis_label_font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=axis_label_font_size)
#ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

#change fontsize of tick labels
#pl.tick_params(axis='both', which='major', labelsize=18)
ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax3.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

## Shrink current axis by 20%
box = ax1.get_position()

#if legend_font_size<=16:
ax1.set_position([box.x0, box.y0, box.width, box.height * 0.75])
ax2.set_position([box.x0, box.y0, box.width, box.height * 0.75])
ax3.set_position([box.x0, box.y0, box.width, box.height * 0.75])
#else:
  #ax1.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  #ax2.set_position([box.x0, box.y0, box.width, box.height * 0.73])
  #ax3.set_position([box.x0, box.y0, box.width, box.height * 0.73])
#ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])

# added these three lines
labels = [l.get_label() for l in lines]
labels_power = [l.get_label() for l in lines_power]

# Put a legend to the right of the current axis
art = []
if legend_RHS == False:
  if legend_font_size<=16:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.13),ncol=number_of_angles, fontsize=legend_font_size)
  else:
    lgd = ax1.legend(lines, labels, loc='center', bbox_to_anchor=(0.5, -0.15),ncol=number_of_angles, fontsize=legend_font_size)
else:
  if auxiliary_angle:
    lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.325), fontsize=legend_font_size, title='Torque')
    print "jabaas"
  else:
    lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.35), fontsize=legend_font_size, title='Torque')
  ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
  if auxiliary_angle:
    lgd_power = ax2.legend(lines_power, labels_power, loc='center left', bbox_to_anchor=(1.05, 0.675), fontsize=legend_font_size, title='Power')
    print "jabaas"
  else:
    lgd_power = ax2.legend(lines_power, labels_power, loc='center left', bbox_to_anchor=(1.05, 0.65), fontsize=legend_font_size, title='Power')
  ax2.get_legend().get_title().set_fontsize(legend_title_fontsize)
  
art.append(lgd)
#ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.1, 0.5))
#ax1.legend(lines, labels, loc='center left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=axis_label_font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#pl.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=1, hspace=1)

fig.set_size_inches(fig_x_inch_pdf, fig_y_inch_pdf)
if legend_RHS == False:
  pl.subplots_adjust(left=0.05, right=0.95)
else:
  pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)

if legend_RHS == False:
  fig.savefig("motor_torque_load_at_various_angles_power_limited.pdf", additional_artists=art,bbox_inches='tight')
else:
  fig.savefig("motor_torque_load_at_various_angles_power_limited.pdf", additional_artists=art)






















#CALCULATE RATED TORQUE
mps  = vehicle_speed_kmph_at_which_rated_torque*(1000/(60*60))
motor_speed_rpm_at_which_rated_torque = (mps/wheel_radius)*(60/(2*pi))*gear_ratio

if use_constant_coeff_rolling_resistance:
  coeff_rolling_resistance = 0.015
else:
  coeff_rolling_resistance = 0.01*(1+(3.6/100)*mps)

FORCE_friction = total_mass*gravitational_acc*coeff_rolling_resistance*cos(angle_degrees_at_which_rated_torque*(pi/180))
FORCE_drag = 0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)
FORCE_gravity = total_mass*gravitational_acc*sin(angle_degrees_at_which_rated_torque*(pi/180))

FORCE_to_maintain_const_speed = FORCE_friction+FORCE_drag+FORCE_gravity

#Torque loads which the motor experiences
TORQUE_friction = (FORCE_friction*wheel_radius)/(gear_ratio*number_of_motors)
TORQUE_drag    = (FORCE_drag*wheel_radius)/(gear_ratio*number_of_motors)
TORQUE_gravity  = (FORCE_gravity*wheel_radius)/(gear_ratio*number_of_motors)

rated_torque = TORQUE_friction + TORQUE_drag + TORQUE_gravity



print("Rated torque = %.2f [N.m], at vehicle speed %.2f [kmph], motor speed %.2f [rpm] and incline angle %.2f [deg]"%(rated_torque,vehicle_speed_kmph_at_which_rated_torque,motor_speed_rpm_at_which_rated_torque,angle_degrees_at_which_rated_torque))

pl.show()