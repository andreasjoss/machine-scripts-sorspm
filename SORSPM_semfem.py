#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: 2D FEM simulation of a single-outrunner-rotor radial flux permanent magnet synchronous motor
#Author : Andreas Joss

#os.system("fpl2 project_sorspm_semfem/drawing.poly")
#os.system("fpl2 project_sorspm_semfem/drawing.fpl")
#os.system("fpl2 project_sorspm_semfem/results/fieldplots/band_machine_field001.fpl")
#os.system("post.py project_sorspm_semfem/results/post.res -f %f" % (f_e))
#os.system("post.py --torque project_sorspm_semfem/results/post.res -f %f" % (f_e))

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)

from semfem import *
import pylab as pl
import numpy as np
#from SEMFEM_plotter import *
from prettytable import PrettyTable
import os, sys
import sqlite3
import datetime
import argparse
from fractions import gcd
import time
import shutil

simulation_time_start = time.time()
#shutil.rmtree('project_sorspm_semfem')

parser = argparse.ArgumentParser(description="SORSPM semfem simulation script",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=15, help='Specify the primary_key of the input row of the database that should be used')
parser.add_argument('-db','--database', dest='database', default='SORSPM.db', help='Specify the database name which should be used')
parser.add_argument('-tb','--table', dest='table', default='', help='Specify the table extension name from which inputs should be read') #for eg. the extension could be '_rpm_050' 
parser.add_argument('-pl','--plot', dest='plot', type=int, default=0, help='Reprint and output graphs of previous simulation')

args = parser.parse_args()

#control variables
primary_key = args.primary_key
db_name = args.database
input_table_name = "machine_input_parameters"+args.table
output_table_name = "machine_semfem_output"+args.table

conn = sqlite3.connect(db_name)
c = conn.cursor()

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

def db_output_insert(table,column,primary_key,value):
  global already_commited_something_now
  if already_commited_something_now==0:
    try:
      c.execute("INSERT INTO "+table+"(primary_key,"+column+") "+ "VALUES (?,?)",(primary_key,value))
      already_commited_something_now = 1
      if database_verbosity == 1: print "created new row and added some value to a column"
    except:
      #pass
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
  else:
    try:
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
      if database_verbosity == 1: print "added new value to column of existing row"
    except:
      pass

store_results_in_database = 1
overwrite_previous_result = 1
use_machine_periodicity = 1
use_working_harmonic_step_size_scale = 0
enable_voltage_leeway = 0
minimum_working_harmonic = 2
database_verbosity = 0
number_of_layers_airgap = 3
use_airgap_layers = 1
use_nonairgap_layers = 0
direction = 1 #determines direction which rotor will rotate during simulation

stator_coil_layer_fraction = 0.8
stator_yoke_layer_fraction = 0.85

rotor_radial_pm_layer_fraction = 0.32
rotor_yoke_layer_fraction = 1.0

mesh_yoke_ratio = 1
mesh_stator_ratio = 3
mesh_rotor_ratio = 3

VERBOSITY = 0
LINEWIDTH=1
FONTSIZE = 14

SOLVER = 1 #1--BAND   2--AGE
RADIALLY_STRAIGHT = 0
SQUARE_SLOTS = 1

#db_name = 'optimise.db'
#db_name = 'SORSPM.db'
#db_name = 'best.db'

enable_new_airgap_arclengths = 1
locked_rotor_test = 0 #rotor will not rotate, and frequency of current injected will depend on locked_rotor_test_f_e
locked_rotor_test_f_e = 155 #Hz
replace_magnets_with_air = 0
priority_torque_target1_copper_loss_limit0 = 0 #if 0, then it is more important to adhere to copper loss limit, than achieving the torque target
bossche = True
enable_ac_resistance = 1
rotor_movement_multiple = 1 #dictates if rotor should rotate even further than what is required
draw_magnets = 1
show_fpl_before_band_solver = 0
show_fpl_after_band_solver = 1
exit_before_solving = 0
run_band_solver = 1
enable_flux_density_harmonic_calc = 0
plot_harmonics = 0
enable_terminal_printout = 1
plot_all = 0
plot_input_current = 0
plot_dq_torque = 0
plot_dq_voltage = 0
store_csv = 0
store_Br_airgap = 0
save_fpl2pdf = 0
speed_up_air_regions = 1
analyse_specific_step = [] 
#analyse_specific_step = [5,9,17,25] #imported from maxwell
#analyse_specific_step = [20,24]
#analyse_specific_step = [5,9] #imported from maxwell
#analyse_specific_step = [12,14] #imported from maxwell
step_ratio_between_maxwell_semfem = 2
swap_coil_terminals_polarity = 0
dont_shift_br_array = 0
enable_l_series_connections = 1

enable_shell_mass = 1
rotor_shell_height=10.0/1000
stator_shell_height=10.0/1000

if args.plot == 1:
  show_fpl_after_band_solver = 1
  #exit_before_solving = 1
  run_band_solver = 0
  enable_terminal_printout = 1
  plot_all = 1
  overwrite_previous_result = 0
  store_results_in_database = 0

fft_AR = 0
fft_PM = 0
fft_ALL = 1

colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

#flags
result_already_exists = 1
already_commited_something_now = 0
deleted_row_entry = 0
dont_place_stator_yoke_outer_region_point = 0

if enable_terminal_printout == 1:
  pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
  pretty_terminal_table.align = 'l'
  pretty_terminal_table.border= True  

#check if a result is already stored for this primary_key
if store_results_in_database == 1:
  #try:
  if database_verbosity == 1: print "trying to select"
  c.execute("SELECT magnet_mass FROM machine_semfem_output WHERE primary_key = ?", (primary_key,))
  data=c.fetchone()
  
  if database_verbosity == 1:
    if data is None:
	print('There is no primary_key named %s'%primary_key)
    else:
	print('primary_key %s found with magnet_mass %s'%(primary_key,data[0]))    
  
  if data is None:
    if database_verbosity == 1: print "No entry found"
    result_already_exists = 0
  else:
    if database_verbosity == 1: print "Entry found"
    result_already_exists = 1
  #except:
    ##executes if this entry could not be found
    #print "except clause"
    #result_already_exists = 0

#delete entry if it exists and if we are to overwrite any previous results
if overwrite_previous_result == 1 and result_already_exists == 1 and store_results_in_database == 1:
  #conn.commit()
  c.execute("DELETE FROM machine_semfem_output WHERE primary_key=?",(primary_key,))
  #conn.commit()
  deleted_row_entry = 1
  if database_verbosity == 1: print "deleted previous entry"

poles = db_input_select(input_table_name,"poles",primary_key)
slots = db_input_select(input_table_name,"slots",primary_key)

if slots%3 != 0:
  print "number of slots (%d) is invalid, it should be multiple of 3"%(slots)
  sys.exit()  
  
if poles%2 != 0:  
  print "number of poles (%d) is invalid, it should be multiple of 2"%(poles)
  sys.exit()  
  
if slots == poles:
  print "number of slots (%d) cannot be equal to number of poles (%d), it produces significant cogging torque (Skaar2006) and (Martinez2012 p.41)"%(slots,poles)
  sys.exit()

J = db_input_select(input_table_name,"current_density_rms",primary_key)
system_voltage = db_input_select(input_table_name,"system_voltage",primary_key)
#r -- radius, h -- height, m -- magnet, o -- outer, i -- inner, k -- ratio, r -- radially, t -- tangentially
ryo = db_input_select(input_table_name,"rotor_yoke_outer_radius_in_mm",primary_key)/1000
hyo = db_input_select(input_table_name,"rotor_yoke_height_in_mm",primary_key)/1000
hyi = db_input_select(input_table_name,"stator_yoke_height_in_mm",primary_key)/1000
hc  = db_input_select(input_table_name,"coil_height_in_mm",primary_key)/1000
hs_ratio= db_input_select(input_table_name,"stator_teeth_to_coil_height_ratio",primary_key) #RADIAL SLOT TEETH HEIGHT
hs_square_slots_ratio= db_input_select(input_table_name,"stator_teeth_height_ratio_square_shape",primary_key)
hmr = db_input_select(input_table_name,"radially_magnetized_PM_height_in_mm",primary_key)/1000
kmr = db_input_select(input_table_name,"radially_magnetized_PM_width_ratio",primary_key)
kmt = (1-kmr)*db_input_select(input_table_name,"tangentially_magnetized_PM_width_ratio",primary_key)
km_gap = 1 - kmr - kmt
kc  = db_input_select(input_table_name,"coil_width_ratio",primary_key)
coil_shape = db_input_select(input_table_name,"slots_open0_square1",primary_key)
coil_side_center_placement = db_input_select(input_table_name,"coil_side_center_placement",primary_key)
coil_yoke_spacing = db_input_select(input_table_name,"coil_yoke_spacing_in_mm",primary_key)/1000
coil_teeth_spacing = db_input_select(input_table_name,"coil_teeth_spacing_in_mm",primary_key)/1000
coil_opposing_spacing = db_input_select(input_table_name,"coil_opposing_spacing_in_mm",primary_key)/1000
coil_shoe_spacing = db_input_select(input_table_name,"coil_shoe_spacing_in_mm",primary_key)/1000
stator_shoe_height = db_input_select(input_table_name,"stator_shoe_height_in_mm",primary_key)/1000
stator_shoe_width = db_input_select(input_table_name,"stator_shoe_width_in_mm",primary_key)/1000
stator_shoe_wedge_degrees = db_input_select(input_table_name,"stator_shoe_wedge_degrees",primary_key)
magnet_sleeve_height = db_input_select(input_table_name,"magnet_sleeve_thickness_in_mm",primary_key)/1000
magnet_sleeve_tolerance = db_input_select(input_table_name,"magnet_sleeve_tolerance_spacing_in_mm",primary_key)/1000
#kc == ratio of stator coil side width out of maximum size 1
#ks == ratio of coil core, i.e. stator teeth width 
#kq  = db_input_select(input_table_name,"coils_per_phase_vs_rotor_pole_pair_ratio",primary_key)
l = db_input_select(input_table_name,"stacklength_in_mm",primary_key)/1000
g = db_input_select(input_table_name,"airgap_in_mm",primary_key)/1000
#muR_y = db_input_select(input_table_name,"yokes_relative_permeability",primary_key)
fill_factor = db_input_select(input_table_name,"fill_factor",primary_key)
n_rpm = db_input_select(input_table_name,"n_rpm",primary_key)
Brem= db_input_select(input_table_name,"B_rem",primary_key)						#remnant flux density of NdBFe N48 PMs        [T]
ryoke_minimum_ratio= db_input_select(input_table_name,"rotor_yoke_height_minimum_ratio",primary_key)
minimum_rotor_yoke_thickness = ryoke_minimum_ratio*hyo
km_gap_teeth= 1-db_input_select(input_table_name,"rotor_gap_teeth_height_ratio",primary_key)
rotor_yoke_iron_N_al= db_input_select(input_table_name,"rotor_yoke_iron_N_al",primary_key)
stator_yoke_iron_N_al= db_input_select(input_table_name,"stator_yoke_iron_N_al",primary_key)
N = db_input_select(input_table_name,"force_this_number_of_turns",primary_key)
coil_temperature = db_input_select(input_table_name,"coil_temperature",primary_key)
hmt_slot_depth_ratio = 1-db_input_select(input_table_name,"rotor_slot_depth_ratio",primary_key)
hmt_magnet_height_ratio_of_slot = db_input_select(input_table_name,"tangentially_magnetised_PM_height_ratio_of_slot",primary_key)
initial_electrical_angle = db_input_select(input_table_name,"initial_position_defined_by_electrical_angle_deg",primary_key)
shift_current_electrical_angle = db_input_select(input_table_name,"shift_current_by_electrical_angle_deg",primary_key)
square_magnets = db_input_select(input_table_name,"square_magnets",primary_key)
interior_magnets = db_input_select(input_table_name,"interior_magnets",primary_key)
magnetisation_radial0_normal1 = db_input_select(input_table_name,"magnetisation_radial0_normal1",primary_key)
inverter_squarewave0_sinusoidalPWM1 = db_input_select(input_table_name,"inverter_squarewave0_sinusoidalPWM1",primary_key)
flare_teeth = db_input_select(input_table_name,"flare_teeth",primary_key)
magnet_place_holders = db_input_select(input_table_name,"magnet_place_holders",primary_key)
insert_lamination_hole = db_input_select(input_table_name,"insert_lamination_hole_support_rod",primary_key)
shoe_weakpoint_thickness = db_input_select(input_table_name,"shoe_weakpoint_thickness",primary_key)/1000
copper_losses_allowed = db_input_select(input_table_name,"copper_losses",primary_key)
current_angle_degrees = db_input_select(input_table_name,"current_angle_degrees",primary_key)
conductors_round0_bars1 = db_input_select(input_table_name,"conductors_round0_bars1",primary_key)
input_is_Jrms0_Pcu1_slotcurrent2 = db_input_select(input_table_name,"input_is_Jrms0_Pcu1_slotcurrent2",primary_key)
number_of_parallel_circuits = db_input_select(input_table_name,"number_of_parallel_circuits",primary_key)
three_phase_delta0_wye1 = db_input_select(input_table_name,"three_phase_delta0_wye1",primary_key)
use_parallel_strands = db_input_select(input_table_name,"use_parallel_strands",primary_key)
enable_eddy_current_losses = db_input_select(input_table_name,"enable_eddy_losses",primary_key)
enable_magnet_losses = db_input_select(input_table_name,"enable_magnet_losses",primary_key)
magnet_loss_approximation_method = db_input_select(input_table_name,"magnet_loss_approximation_method",primary_key)
current_through_slot = db_input_select(input_table_name,"current_through_slot_rms",primary_key)
target_mech_power_kW = db_input_select(input_table_name,"target_mech_power_kW",primary_key)

#enable_target_mech_power determines if the script should try to operate machine with a constant power output. It should be disabled when Ls and flm are to be discovered.
enable_target_mech_power = db_input_select(input_table_name,"enable_target_mech_power",primary_key)

#should only be enabled when the adequate number of turns should be determined at top speed, and thereafter the turns should be kept constant
enable_turns_calculation = db_input_select(input_table_name,"enable_turns_calculation",primary_key)

#should enable for round conductors, and disable for bar conductors
enable_automatic_flux_weakening = db_input_select(input_table_name,"enable_automatic_flux_weakening",primary_key)

muR_y = 1400
effective_fill_factor = 0


if np.isclose(0,n_rpm,rtol=1e-02, atol=1e-02, equal_nan=False):
  n_rpm = 1	#do this just to prevent division by zero

parallel_stranded_conductors = 1 #just a starting value

if conductors_round0_bars1==0:
  number_of_copper_bars_per_coil = 1
else:
  use_parallel_strands = 0
  number_of_copper_bars_per_coil = N

if use_parallel_strands:
  strand_diameter=0.2

if insert_lamination_hole == 1:
  stator_rod_distance_from_tooth_tip = db_input_select(input_table_name,"stator_rod_distance_from_tooth_tip",primary_key)/1000
  stator_rod_hole_diameter = db_input_select(input_table_name,"stator_rod_hole_diameter",primary_key)/1000

if interior_magnets == 1 and magnet_place_holders == 1:
  print "Cannot use both magnet sleeves and magnet place holders, simulation will now exit"
  sys.exit()

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
fig_x_cm = 8
fig_y_cm = 5
fig_x_inch_pdf = 18
fig_y_inch_pdf = 8


try:
  conductor_cu0_al1 = db_input_select(input_table_name,"conductor_cu0_al1",primary_key)
except:
  print "Column conductor_cu0_al1 did not exist in this database"
  conductor_cu0_al1 = 0
Ra_analytical = [0,0]
Ra_analytical_per_bar = [0,0]

if np.isclose(hmt_slot_depth_ratio, 0, rtol=1e-02, atol=1e-02, equal_nan=False): hmt_slot_depth_ratio = 0
if np.isclose(hmt_slot_depth_ratio, 1, rtol=1e-02, atol=1e-02, equal_nan=False): hmt_slot_depth_ratio = 1

if np.isclose(hmt_magnet_height_ratio_of_slot, 0, rtol=1e-02, atol=1e-02, equal_nan=False): hmt_magnet_height_ratio_of_slot = 0
if np.isclose(hmt_magnet_height_ratio_of_slot, 1, rtol=1e-02, atol=1e-02, equal_nan=False): hmt_magnet_height_ratio_of_slot = 1

# a small positive current angle will increase the d-axis flux density, thereby aiding the flux produced by the magnets, thus increasing the overall torque produced
if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
  id = J*np.sqrt(2)*np.sin((np.pi/180)*current_angle_degrees)
  iq = J*np.sqrt(2)*np.cos((np.pi/180)*current_angle_degrees)   #current density[A/mm2] should now be given as PEAK values (J is a RMS value)  

#AR_0_PM_1_ALL_2 = db_input_select(input_table_name,"AR_0_PM_1_ALL_2",primary_key)
AR_0_PM_1_ALL_2 = 2
if AR_0_PM_1_ALL_2 == 0:
  Brem = 0
  #replace_magnets_with_iron = 1
  replace_magnets_with_iron = 0
  #hmt_slot_depth_ratio = 0
  #km_gap_teeth = 0
elif AR_0_PM_1_ALL_2 == 1:
  if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
    id = 0
    iq = 0
    J = 0
    replace_magnets_with_iron = 0
elif AR_0_PM_1_ALL_2 == 2:
  replace_magnets_with_iron = 0

steps = db_input_select(input_table_name,"steps",primary_key)

#q = kq*(poles/2)#7#number of coils per phase
q = int(slots/3)#number of coils per phase
periodicity = q
#print q
Q = slots
coils_in_series_per_phase = q/number_of_parallel_circuits
coil_pitch = (2*np.pi)/Q

#periodicity_in_one_full_rotation = gcd(slots,int(poles/2)) #Martinez2012 p.29
periodicity_in_one_full_rotation = gcd(int(slots/3),int(poles/2))
#print(periodicity_in_one_full_rotation)

if int(periodicity_in_one_full_rotation) == 2:
  use_machine_periodicity = 0
  

#print periodicity_in_one_full_rotation

pole_pairs_per_slot_per_phase = int(poles/2)/int(slots/3)

slots_per_phase_per_pole = slots/(poles*3)
#get clockwise harmonics (and maybe their kw polaraties too?)
clockwise = []
#get anti-clockwise harmonics
anti_clockwise = []

#these harmonic directions of the mmf waves are obtained from dr. Randewijk's thesis
#the directions are also confirmed from dr. Rix's theses, p.15 for tau=1

#equations used by dr. Rix chapter 2
M_s = gcd(int(poles/2), slots)
W_s = gcd(poles,slots)
tau = int(W_s/M_s) #answer for this machine is 1, thus positive periodicity, and the following assumptions are valid

#print "M_s = %f"%(M_s)
#print "W_s = %f"%(W_s)
#print "tau = %f"%(tau)

#tau = 1 -- positive periodicity
#tau = 2 -- negative periodicity

stator_mmf_harmonic_direction = []

#if tau == 1:
  #print "positive periodicity"
#elif tau == 2:
  #print "negative periodicity -- even stator MMF harmonics (2,4,6...) will not exist."

for i in range (-5,5):
  stator_mmf_harmonic_direction.append(int(3*tau*i-1))

  
sorted_stator_mmf_harmonic_direction = sorted(stator_mmf_harmonic_direction,key=abs)    
#print(sorted_stator_mmf_harmonic_direction)
#print(sorted(stator_mmf_harmonic_direction,key=abs))
#print(stator_mmf_harmonic_direction)

#for i in range (1,20):
  #clockwise.append(3*i -1)
  #anti_clockwise.append(3*i-2)

#print "clockwise"
#print(clockwise)
#print "anti_clockwise"
#print(anti_clockwise)

#print("pole_pairs_per_slot_per_phase = %.3f"%(pole_pairs_per_slot_per_phase))
#print("slots per pole per phase = %.3f"%(slots_per_phase_per_pole))

working_harmonic = 0
working_fractional = 0
#print(pole_pairs_per_slot_per_phase%1)
if pole_pairs_per_slot_per_phase%1 != 0:
  working_fractional = int(pole_pairs_per_slot_per_phase) + 1/(pole_pairs_per_slot_per_phase%1)
  working_fractional = int(round(working_fractional,0))
  #print "working harmonic for this fractional slot machine should be number = %f"%working_fractional

for i in range(0,len(sorted_stator_mmf_harmonic_direction)):
  #print "working_fractional"
  #print int(working_fractional)
  #print int(round(working_fractional,0))
  #print "sorted_stator_mmf_harmonic_direction[i]"
  #print abs(sorted_stator_mmf_harmonic_direction[i])
  
  if working_fractional == abs(sorted_stator_mmf_harmonic_direction[i]):
    working_harmonic = abs(sorted_stator_mmf_harmonic_direction[i])
    break
    
  elif pole_pairs_per_slot_per_phase == abs(sorted_stator_mmf_harmonic_direction[i]):
    working_harmonic = abs(sorted_stator_mmf_harmonic_direction[i])
    break
  
#if sorted_stator_mmf_harmonic_direction[i] > 0:
  #direction = 1
#elif sorted_stator_mmf_harmonic_direction[i] < 0:
  #direction = -1
#if VERBOSITY == 1 and working_harmonic != 0:
  #print "direction is (%d) and match is found in sorted_stator_mmf_harmonic_direction[%d] with working harmonic %d"%(direction,i,working_harmonic)

#for i in range(0,len(clockwise)):
  #if int(pole_pairs_per_slot_per_phase) == clockwise[i]:
    #direction = -1
    #working_harmonic = clockwise[i]
    #if VERBOSITY == 1:
      #print "direction is (%d) and match is found in clockwise[%d] with working harmonic %d"%(direction,i,working_harmonic)
    #break
  #elif int(pole_pairs_per_slot_per_phase) == anti_clockwise[i]:
    #direction = 1
    #working_harmonic = anti_clockwise[i]
    #if VERBOSITY == 1:
      #print "direction is (%d) and match is found in anti_clockwise[%d] with working harmonic %d"%(direction,i,working_harmonic)
    #break

if working_harmonic == 0:
  print "there is no common harmonic between poles and stator slots to form a working harmonic"
  sys.exit()
 
if minimum_working_harmonic<working_harmonic:
  print "machine might be very ineffective since a high working harmonic is being used"
  #sys.exit()

if pole_pairs_per_slot_per_phase%1 != 0:
  print "pole_pairs_per_slot_per_phase (%.3f) is not a whole number, meaning fractional-slot pitching should be done"%(pole_pairs_per_slot_per_phase)
  #sys.exit()

if slots_per_phase_per_pole<0.25 or slots_per_phase_per_pole>0.5:
  print "slots_per_phase_per_pole (%.3f) is not within range of 0.25 and 0.50"%(slots_per_phase_per_pole)
  sys.exit()

#sys.exit()
#print "gcd(slots,poles) = %f"%(gcd(slots,poles))
#print "slots/gcd(slots,poles) = %f"%(slots/gcd(slots,poles))
#print "poles/gcd(slots,poles) = %f"%(poles/gcd(slots,poles))
#sys.exit()
cogging_torque_harmonic = (slots/gcd(slots,poles))*(poles/gcd(slots,poles)) #works well so far for integral-slot machines
    
#winding_harmonics = []

#kw_pitch = sin((2*np.pi)/3 - 2*delta)
#kw_slot = sin(delta/kq)/(delta/kq) #p.29
############

#kq = 1/2 #coils per phase per pole pair (q/pole_pair)
#kq = (int(slots)/3)/int(poles/2)

#k_delta = 2/3 #is the coil side-width factor, defined as delta/delta_max (p.22)
#k_delta = 2/3 #I think this number comes from top of p.22, where it is discussed that type II machines about 2/3 of the stator surface area is utilised by stator windings
	      #and that it's not only used to describe coil-side width of a winding configuration, but also the space utilisation of a stator's winding configuration.

#delta is 1/2 coil side-width angle of stator coils
#delta = 0.5*(0.5*kc_pitch)

#delta = k_delta*delta_max #p.33 2.112
#delta_max = np.pi/(6*q) #p.34 2.113 (mech degrees) also from p.22 2.63

#kw_m_II = kw_pitch_m_II*kw_slot_m_II #p.31 2.104


#kw_pitch_II = sin((1/kq)*(np.pi/3 - delta) #p.29 2.92
#kw_pitch_II = sin(2*(np.pi/3 - delta) #p.29 2.100
#kw_pitch_m_II = sin(i*(np.pi/3 - delta) #p.31 2.105 #precisely used like this in qhdrfpm.py, with harmonic i as the argument (but i think i should reintroduce kq)



#kw_slot_II = sin(delta/kq)/(delta/kq) #p.29 2.99
#kw_slot_m_II = sin(i*delta)/(i*delta) #p.31 2.106 #precisely used like this in qhdrfpm.py, with harmonic i as the argument (but i think i should reintroduce kq)



#I still want to know
#1. where does k_delta 2/3 come into the above equations?
#2. where does np.pi/3 come from in the kw_pitch equations?
#3. how does the qhdrfpm scripts use these values?
#4. how does the other thesis of non-overlapping windings discuss the winding factors and suitable pole/slot numbers?





#kw_slot = sin(i*delta)/(i*delta) #p.31 with kq = 1/2

#the definitions are extracted from dr. Randewijk's dissertation  






#these winding factor equations are extracted from dr. Randewijk's dissertation  
#for i in range(0,len(clockwise)+len(anti_clockwise)):    
  #if clockwise[i]<anti_clockwise[i]:
    #kw = sin(i*(np.pi/3-delta))*(sin(i*delta)/(i*delta))
    #winding_harmonics.append([clockwise[i],-1,kw)
  #elif clockwise[i]>anti_clockwise[i]:
    #kw = sin(i*(np.pi/3-delta))*(sin(i*delta)/(i*delta))
    #winding_harmonics.append([anti_clockwise[i],1,kw)



#kw[i] = kw_pitch[i] * kw_slot[i]    
    
    
    
    
    
#if pole_pairs_per_slot_per_phase >= 1:
  #direction = 1
#elif pole_pairs_per_slot_per_phase < 1:
  #direction = -1
#print("direction = %d"%(direction))  

#debug_offset = ((2*np.pi)/poles)*(60/360)
debug_offset = 0
#direction = -1

#direction = 9 #hierdie kode was hier om te verseker dat een van die twee if-statements wat hieronder is waar word (sodat ek dadelik kan sien indien dit nie reggebeuer het nie, maw direction = 9)
#print(kq)
#if np.isclose(kq, 0.5, rtol=1e-04, atol=1e-04, equal_nan=False):
  #direction = -1
#elif np.isclose(kq, 1.0, rtol=1e-04, atol=1e-04, equal_nan=False):
  #direction = 1

if np.isclose(periodicity_in_one_full_rotation, 1, rtol=1e-04, atol=1e-04, equal_nan=False):
  use_machine_periodicity = 0

if locked_rotor_test == 0:
  f_e = (n_rpm*(poles))/120 		    #since comparison with Ansys Maxwell flux linkage and induced voltages for single turn series branches, I suspect that this frequency is incorrect with factor two ( 2016/10/14).
					    #the electrical frequency f_e (time dependant frequency) is established by the number of poles in the rotor.
					    #this f_e will produce also a f_e flux linkage in the stator coils, and thus we need to inject f_e current into the stator to produce torque
					    #do not confuse f_e with the space harmonic distributions, that is something entirely different

else:
  f_e = locked_rotor_test_f_e
  n_rpm = 0

time_at_specific_step = []
electrical_rotation_at_specific_step = []
mechanical_rotation_at_specific_step = []

for i in range(0,len(analyse_specific_step)):
  analyse_specific_step[i]=analyse_specific_step[i]*step_ratio_between_maxwell_semfem

  time_at_specific_step.append(((analyse_specific_step[i])/steps)*(1/(f_e/rotor_movement_multiple)))
  electrical_rotation_at_specific_step.append((time_at_specific_step[i]/(1/(f_e/rotor_movement_multiple)))*360)
  
  if locked_rotor_test == 0:
    mechanical_rotation_at_specific_step.append(electrical_rotation_at_specific_step[i]/periodicity)
  else:
    mechanical_rotation_at_specific_step.append(0)

if inverter_squarewave0_sinusoidalPWM1 == 0:
  VLL_pk_max_allowed = (2*np.sqrt(3)/np.pi)*system_voltage
elif inverter_squarewave0_sinusoidalPWM1 == 1:
  VLL_pk_max_allowed = (np.sqrt(3)/2)*system_voltage
elif inverter_squarewave0_sinusoidalPWM1 == 2:
  VLL_pk_max_allowed = system_voltage

#VLL_pk_max_allowed = 0.95*VLL_pk_max_allowed
peak_terminal_line_voltage_limit = VLL_pk_max_allowed
RMS_terminal_line_voltage_limit = peak_terminal_line_voltage_limit/np.sqrt(2)

if three_phase_delta0_wye1 == 1:
  RMS_terminal_phase_voltage_limit = RMS_terminal_line_voltage_limit/np.sqrt(3)  
  peak_terminal_phase_voltage_limit = peak_terminal_line_voltage_limit/np.sqrt(3)
else:
  RMS_terminal_phase_voltage_limit = RMS_terminal_line_voltage_limit
  peak_terminal_phase_voltage_limit = peak_terminal_line_voltage_limit

N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
#N_L=2 means there are only 2 coil sides within one "slot". In this prototype, the teeth are centered between the outgoing and ingoing current of a phase.
#and the sides of different phases are right next to each other (in the same "slot")
N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1

if swap_coil_terminals_polarity == 1:
  phase = [-1,1,-3,3,-2,2]
else:
  phase = [1,-1,3,-3,2,-2] #original sequence

muR= 1.05 						#recoil permeability of the PMs
#cu_resistivity_25 = 1.678e-8				#[Ohm.m] at STD 25 Degrees Celcius
c110000_resistivity_20 = 1.72e-8
copper_resistivity = c110000_resistivity_20*(1+0.0039*(coil_temperature-20)) #copper resistivity is given in [Ohm.m]

al_resistivity_25 = 2.65e-8				#[Ohm.m] at STD 20 Degrees Celcius
aluminium_resistivity = al_resistivity_25*(1+0.0038*(coil_temperature-20)) #aluminium resistivity is given in [Ohm.m] obtained from http://www.engineeringtoolbox.com/resistivity-conductivity-d_418.html

magnet_resistivity = 1.5e-6
magnet_resistivity = magnet_resistivity*(1+0.0002*(coil_temperature-20)) #magnet resistivity is given in [Ohm.m]
#magnet_resistivity = copper_resistivity

#assume only single turn, in order to satisfy voltage limit
turnsPerCoil = 1

stator_iron = 250
rotor_iron = 299
magnet_radially_iron = 270

#if magnet_place_holders == 1:
  #magnet_ridge_height = magnet_sleeve_height
  #magnet_ridge_width = magnet_sleeve_height

if interior_magnets == 0 and magnet_place_holders == 0:
  magnet_sleeve_height = 0
  magnet_sleeve_tolerance = 0

magnet_ns_total = poles #assuming Q-H topology
magnet_radially_pitch = (2*np.pi)/magnet_ns_total

magnet_radially_outer_radius = ryo-hyo-magnet_sleeve_tolerance
magnet_radially_inner_radius = ryo-hyo-hmr-magnet_sleeve_tolerance
magnet_tangentially_slot_radius = ryo-hyo-hmt_slot_depth_ratio*hmr
gap_teeth_radius = magnet_radially_inner_radius + km_gap_teeth*(magnet_radially_outer_radius-magnet_radially_inner_radius)
magnet_tangentially_slot_height = magnet_tangentially_slot_radius-magnet_radially_inner_radius
magnet_tangentially_PM_height = hmt_magnet_height_ratio_of_slot*(magnet_tangentially_slot_height)
magnet_tangentially_inner_radius = magnet_tangentially_slot_radius-hmt_magnet_height_ratio_of_slot*(magnet_tangentially_slot_height)
magnet_tangentially_outer_radius = magnet_tangentially_inner_radius+magnet_tangentially_PM_height

if interior_magnets == 1:
  stator_yoke_inner_radius = magnet_radially_inner_radius - g - hyi - hc - magnet_sleeve_height - 2*magnet_sleeve_tolerance
  stator_yoke_outer_radius = magnet_radially_inner_radius - g - hc - magnet_sleeve_height - 2*magnet_sleeve_tolerance
  stator_coil_radius = magnet_radially_inner_radius - g - magnet_sleeve_height - 2*magnet_sleeve_tolerance
elif interior_magnets == 0:
  stator_yoke_inner_radius = magnet_radially_inner_radius - g - hyi - hc - magnet_sleeve_tolerance
  stator_yoke_outer_radius = magnet_radially_inner_radius - g - hc - magnet_sleeve_tolerance
  stator_coil_radius = magnet_radially_inner_radius - g
  
  if coil_shape == SQUARE_SLOTS and flare_teeth == 1:
    stator_yoke_inner_radius = stator_yoke_inner_radius - stator_shoe_height
    stator_yoke_outer_radius = stator_yoke_outer_radius - stator_shoe_height

#mechRotation = (4*np.pi)/poles
mechRotation = (2*np.pi)/periodicity_in_one_full_rotation

mesh = 1e-3
mesh_scale = db_input_select(input_table_name,"mesh_scale",primary_key)
reference_surface_area = 0.0675568
surface_area = np.pi*(ryo**2-stator_yoke_inner_radius**2)
surface_area_factor = surface_area/reference_surface_area
mesh = mesh*mesh_scale*surface_area_factor

mesh_yoke_layer = mesh*mesh_yoke_ratio
mesh_stator_layer = mesh*mesh_stator_ratio
mesh_rotor_layer = mesh*mesh_rotor_ratio


#Arc length settings
arc_len = 0.5e-3*mesh_scale

inner_airgap_arclen = arc_len
outer_airgap_arclen = arc_len


#pmlines need to be made for each segment in height, thus it is first needed to sort the radii from small to large, in a single array.
rtol_rotor = 0.5*arc_len
atol_rotor = 0.5*arc_len

#rtol_rotor=1e-4
#atol_rotor=1e-4

if np.isclose(magnet_tangentially_inner_radius, magnet_tangentially_slot_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
  if np.isclose(magnet_tangentially_slot_radius, magnet_tangentially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
    if np.isclose(magnet_tangentially_outer_radius, magnet_radially_inner_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) or np.isclose(magnet_tangentially_outer_radius, magnet_radially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
      rotor_radii = [magnet_radially_inner_radius,gap_teeth_radius,magnet_radially_outer_radius,ryo]
if np.isclose(kmt, 0, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
  rotor_radii = [magnet_radially_inner_radius,gap_teeth_radius,magnet_radially_outer_radius,ryo]
else:
  rotor_radii = [magnet_radially_inner_radius,magnet_tangentially_inner_radius,gap_teeth_radius,magnet_tangentially_slot_radius,magnet_radially_outer_radius,magnet_tangentially_outer_radius,ryo]

#print(rotor_radii)
sorted_rotor_radii = sorted(rotor_radii)
#print(sorted_rotor_radii)

whole = []
for i in range(0,len(sorted_rotor_radii)-1):
  if np.isclose(sorted_rotor_radii[i+1],ryo,rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) and not np.isclose(sorted_rotor_radii[i],ryo,rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
    whole.append(abs((sorted_rotor_radii[i]-sorted_rotor_radii[i+1])/(arc_len*mesh_yoke_ratio)))
  else:
    whole.append(abs((sorted_rotor_radii[i]-sorted_rotor_radii[i+1])/arc_len))
  if whole[i] < 1:
    whole[i] = 1
  else:
    whole[i] = int(round(whole[i]))   

# get ROTOR radii nodes to draw layer mesh split lines

# get STATOR radii nodes to draw layer mesh split lines
  whole_number_stator = (stator_yoke_outer_radius-stator_yoke_inner_radius)/(arc_len*mesh_yoke_ratio)
  if whole_number_stator < 1:
    whole_number_stator = 1
  else:
    whole_number_stator = int(round(whole_number_stator))  

  stator_yoke_node_positions = []

  pos = stator_yoke_inner_radius
  temp1 = abs((stator_yoke_inner_radius-stator_yoke_outer_radius)/whole_number_stator)
  for i in range(0,whole_number_stator):
    pos = pos + temp1
    stator_yoke_node_positions.append(pos)  

if use_nonairgap_layers == 1:
  rotor_node_positions = []

  pos = magnet_radially_inner_radius
  for i in range(0,len(whole)):
    temp = abs(sorted_rotor_radii[i]-sorted_rotor_radii[i+1])/whole[i]
    
    for j in range(0,whole[i]):
      #print(pos*1000)
      pos = pos + temp
      rotor_node_positions.append(pos)  

    
  temp = magnet_radially_inner_radius+rotor_radial_pm_layer_fraction*(magnet_radially_outer_radius-magnet_radially_inner_radius)
  
  smallest = rotor_node_positions[0]
  arc_rotor_magnet_layer_split_radius = 0
  for i in range(0,len(rotor_node_positions)):
    if smallest > abs(rotor_node_positions[i] - temp):
      smallest = abs(rotor_node_positions[i] - temp)
      arc_rotor_magnet_layer_split_radius = rotor_node_positions[i]
  
  if len(stator_yoke_node_positions) == 1:
    use_nonairgap_layers = 0
  
  if use_nonairgap_layers == 1:
    temp = stator_yoke_inner_radius+stator_yoke_layer_fraction*(stator_yoke_outer_radius-stator_yoke_inner_radius)
    
    smallest = stator_yoke_node_positions[0]
    arc_stator_yoke_layer_split_radius = 0
    for i in range(0,len(stator_yoke_node_positions)):
      if smallest > abs(stator_yoke_node_positions[i] - temp):
	smallest = abs(stator_yoke_node_positions[i] - temp)
	arc_stator_yoke_layer_split_radius = stator_yoke_node_positions[i]
    
    while(np.isclose(arc_stator_yoke_layer_split_radius,stator_yoke_outer_radius,rtol=0.7*arc_len*mesh_yoke_ratio,atol=0.7*arc_len*mesh_yoke_ratio, equal_nan=False)):
      i = i - 1
      arc_stator_yoke_layer_split_radius = stator_yoke_node_positions[i]

##########################
# User defined functions #
##########################
#the skin_depth is used here to calculate the skin effect (ac resistance) of the conductor. The skin effect arises from the current in the conductor ITSELF, thus the perfect sinusoid current that is injected into this simulation	
w_e = 2*np.pi*f_e
mu = 4*np.pi*10**(-7)
if not np.isclose(w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False): 
  skin_depth = np.sqrt((2*copper_resistivity)/(w_e*mu))
else:
  skin_depth = 1000

def resistance_analytical(conductor_length):
  Ra = [0,0]
  
  #use copper bars
  if conductors_round0_bars1:
    Ra[0] = ((copper_resistivity*conductor_length)/(bar_side_width*bar_side_height))/number_of_parallel_circuits #DC resistance
    
    #largest_dimension = max([bar_side_height,bar_side_width])
    #approximation_value = largest_dimension/skin_depth
    #if largest_dimension > 1.6*skin_depth: #the approximation is only for low frequencies
      #return "Error"      
    Ra[1] = (((copper_resistivity*conductor_length)/(bar_side_width*bar_side_height))*(1+(bar_side_height**4 *w_e**2 *mu**2)/(720*copper_resistivity**2)))/number_of_parallel_circuits

  #use round conductors
  else:
    #wide frequency approximation by Bossche2005, (5.44)
    t = ((single_turn_conductor_radius/1000)*2)/skin_depth
    G = t**6 + 6.1*t**5 + 32*t**4 + 13*t**3 + 90*t**2 + 110*t
    Ra[0] = ((copper_resistivity*conductor_length)/(area_available_per_turn/1e6))/number_of_parallel_circuits #DC resistance
    Ra[1] = Ra[0]*(1+ ((1/48)*(t/2)**4)*(1/np.sqrt(1+G/36864)))

  #print("Ra = %.3f [mOhm]"%(Ra[1]*1000))
  return Ra;

#########################
# SEMFEM Setup settings #
#########################

sf = SEMFEM()	#initialize object
#sf.semfem_set_verbosity(2)

sf.semfem_begin("project_sorspm_semfem")
sf.semfem_set_problem_steps(steps)
sf.semfem_set_air_gap_method(1)			#band solver (was used) (1 denotes 1 airgap!!)
sf.semfem_set_problem_type(sf.ROTATING)
#sf.semfem_current_input_method = sf.PHASE_CURRENT
sf.semfem_current_input_method = sf.PHASE_CURRENT_DENSITY
sf.semfem_set_stack_length(l)
sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/Magnet_M-19_29_Ga.bh")	#apparantly this is the default magnet BH curve data
#sf.semfem_set_bh_curve("steel1010_bh_data_maxwell.bh")	#apparantly this is the default magnet BH curve data
#sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/maxwell_steel_bh.bh")	#apparantly this is the default magnet BH curve data

#sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/M330-50A.bh") #solve nie so mooi nie
#sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/M270-50A.bh") #solve ook nie so goed nie

sf.semfem_set_verbosity(VERBOSITY)			#no print-outs

sf.semfem_set_output(2)	#will generate all the fpl files (for all the steps) #1-- would generate only the fpl file for the first step #0-- would generate 0 fpl files

####################
# Drawing commands #
####################
if np.isclose(kc, 1, rtol=1e-02, atol=1e-02, equal_nan=False): kc = 1
if np.isclose(kc, 0, rtol=1e-02, atol=1e-02, equal_nan=False): kc = 1e-02 #there needs to be at least some coil copper area

ks = 1-kc

#kc == ratio of stator coil side width out of maximum size 1
#ks == ratio of coil core, i.e. stator teeth width 

ks_pitch = ks*coil_pitch
kc_pitch = kc*coil_pitch

temp_angle1 = 0.5*kc_pitch + 0.5*ks_pitch
temp_angle2 = coil_pitch-temp_angle1 

if coil_shape == RADIALLY_STRAIGHT: #radial slots
  coil_offset = 0
elif coil_shape == SQUARE_SLOTS: #square slots
  coil_offset = 0
  #coil_offset = 0.5*(temp_angle1+temp_angle2)

#if use_machine_periodicity == 1:
  ####outer yoke###
  #sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation, ryo, arc_len*mesh_yoke_ratio)

  #if square_magnets == 0:
    #for i in range(0,len(sorted_rotor_radii)-1):
      #sf.pmline(sorted_rotor_radii[i],0,sorted_rotor_radii[i+1],0,arc_len)
      #sf.pmline(sorted_rotor_radii[i],(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[i+1],(2*np.pi)/periodicity_in_one_full_rotation,arc_len)
  
  ####inner yoke### 
  ##sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation-coil_offset, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
  #sf.oarc(0-coil_offset, (2*np.pi)/periodicity_in_one_full_rotation-coil_offset, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
   
  #if use_nonairgap_layers == 1:
    #if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:    
      #sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation, arc_stator_yoke_layer_split_radius, arc_len*mesh_yoke_ratio)
  
#else:
  ####outer yoke###
  #sf.oarc(0, 2*np.pi, ryo, arc_len*mesh_yoke_ratio)
  ####inner yoke###
  #sf.oarc(0, 2*np.pi, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
  
  #if use_nonairgap_layers == 1:
    #arc_rotor_yoke_layer_split_radius = magnet_radially_outer_radius+rotor_yoke_layer_fraction*(ryo-magnet_radially_outer_radius)
    ##sf.oarc(0, 2*np.pi, arc_rotor_yoke_layer_split_radius, arc_len)
    
    ##arc_stator_yoke_layer_split_radius = stator_yoke_inner_radius+stator_yoke_layer_fraction*(stator_yoke_outer_radius-stator_yoke_inner_radius)
    ##if np.isclose(stator_yoke_layer_fraction,1.0,rtol=arc_len*mesh_yoke_ratio,atol=arc_len*mesh_yoke_ratio, equal_nan=False):
    #if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:
      #sf.oarc(0, 2*np.pi, arc_stator_yoke_layer_split_radius, arc_len*mesh_yoke_ratio) 

#stator teeth#
if coil_shape == RADIALLY_STRAIGHT:
  coil_side_area = (((stator_coil_radius)**2-(stator_yoke_outer_radius)**2)*np.pi)*(0.5*kc_pitch/(2*np.pi))  #this is the coil_side_area in [m^2] for ONE side (not a pair of sides)
  coil_side_area_in_mm = coil_side_area*(1000**2)
  current_through_slot = J*(1000**2)*coil_side_area*fill_factor

  stator_teeth_outer_radius = stator_yoke_outer_radius + hc*hs_ratio

  sf.begin_part()

  whole1 = abs((stator_yoke_outer_radius-stator_teeth_outer_radius)/arc_len)
  if whole1 < 1:
    whole1 = 1
    dont_place_stator_yoke_outer_region_point = 1
  else:
    whole1 = int(round(whole1))
    
  #print "whole1 = %d"%(whole1)  
    
  whole2 = abs((stator_teeth_outer_radius-stator_coil_radius)/arc_len)
  if whole2 < 1:
    whole2 = 1
  else:
    whole2 = int(round(whole2))  

  #print "whole2 = %d"%(whole2)
  
  stator_node_positions = []

  pos = stator_yoke_outer_radius
  temp1 = abs((stator_yoke_outer_radius-stator_teeth_outer_radius)/whole1)
  for i in range(0,whole1):
    pos = pos + temp1
    stator_node_positions.append(pos)
    #print(1000*pos)
    
  temp2 = abs((stator_teeth_outer_radius-stator_coil_radius)/whole2)
  for i in range(0,whole2):
    pos = pos + temp2
    stator_node_positions.append(pos)
    #print(1000*pos)
    
    
  if use_nonairgap_layers == 0:
    sf.oarc(0,0.5*ks_pitch,stator_yoke_outer_radius,arc_len) #EDITED
  elif use_nonairgap_layers == 1:
    temp = stator_yoke_outer_radius+stator_coil_layer_fraction*(stator_coil_radius-stator_yoke_outer_radius)
    
    smallest = stator_node_positions[0]
    arc_stator_layer_split_radius = 0
    for i in range(0,len(stator_node_positions)):
      if smallest > abs(stator_node_positions[i] - temp):
	smallest = abs(stator_node_positions[i] - temp)
	arc_stator_layer_split_radius = stator_node_positions[i]
      
    sf.oarc(0,0.5*ks_pitch,arc_stator_layer_split_radius,arc_len) #EDITED
	

  diff=stator_teeth_outer_radius-stator_yoke_outer_radius
  diff2=stator_coil_radius-stator_teeth_outer_radius
  if diff2>=arc_len:
  #if diff>=arc_len and diff2>=arc_len: #replaced this on 2016/07/25
    sf.oarc(0,0.5*ks_pitch,stator_teeth_outer_radius,arc_len) #EDITED
    
    

  sf.pnline(stator_yoke_outer_radius,0,stator_teeth_outer_radius,0,whole1)
  sf.pnline(stator_teeth_outer_radius,0,stator_coil_radius,0,whole2)

  sf.pnline(stator_yoke_outer_radius,0.5*ks_pitch,stator_teeth_outer_radius,0.5*ks_pitch,whole1) #EDITED
  sf.pnline(stator_teeth_outer_radius,0.5*ks_pitch,stator_coil_radius,0.5*ks_pitch,whole2) #EDITED

  sf.oarc(0,0.5*ks_pitch,stator_coil_radius,inner_airgap_arclen) #EDITED


  if not np.isclose(1,kc,rtol=1e-02, atol=1e-02, equal_nan=False):
    diff2 = stator_coil_radius-stator_teeth_outer_radius
    if diff2>=arc_len:
      if use_nonairgap_layers == 0:
	sf.pregion(0.5*(stator_teeth_outer_radius+stator_coil_radius), 0.25*ks_pitch,0, mesh*speed_up_air_regions)				#air region of teeth (EDITED)
      elif use_nonairgap_layers == 1:
	if arc_stator_layer_split_radius<stator_teeth_outer_radius or np.isclose(arc_stator_layer_split_radius,stator_teeth_outer_radius,rtol=0.7*arc_len*mesh_yoke_ratio,atol=0.7*arc_len*mesh_yoke_ratio, equal_nan=False):
	  sf.pregion(0.5*(stator_teeth_outer_radius+stator_coil_radius), 0.25*ks_pitch,0, mesh*speed_up_air_regions)				#air region of teeth (EDITED)
	else:
	  sf.pregion(0.5*(stator_teeth_outer_radius+arc_stator_layer_split_radius), 0.25*ks_pitch,0, mesh_stator_layer*speed_up_air_regions)		#air region of teeth (EDITED)
	  sf.pregion(0.5*(arc_stator_layer_split_radius+stator_coil_radius), 0.25*ks_pitch,0, mesh*speed_up_air_regions)				#air region of teeth (EDITED)

    diff = stator_teeth_outer_radius-stator_yoke_outer_radius
    if diff>=arc_len:
      if stator_yoke_iron_N_al == 1:
	if use_nonairgap_layers == 0:
	  sf.pregion(0.5*(stator_yoke_outer_radius+stator_teeth_outer_radius), 0.25*ks_pitch,stator_iron, mesh)		#stator_iron_teeth (EDITED)
	elif use_nonairgap_layers == 1:
	  if arc_stator_layer_split_radius<stator_teeth_outer_radius:
	    #sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_layer_split_radius), 0.5*ks_pitch,stator_iron, mesh_stator_layer)		#stator_iron_teeth
	    sf.pregion(0.5*(arc_stator_layer_split_radius+stator_teeth_outer_radius), 0.25*ks_pitch,stator_iron, mesh)		#stator_iron_teeth (EDITED)
	  
      else:
	if use_nonairgap_layers == 0:
	  sf.pregion(0.5*(stator_yoke_outer_radius+stator_teeth_outer_radius), 0.25*ks_pitch,0, mesh*speed_up_air_regions)			#stator_alluminium(air-like)_teeth (EDITED)
	elif use_nonairgap_layers == 1:
	  if arc_stator_layer_split_radius<stator_teeth_outer_radius:
	    #sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_layer_split_radius), 0.5*ks_pitch,0, mesh_stator_layer)			#stator_alluminium(air-like)_teeth
	    sf.pregion(0.5*(arc_stator_layer_split_radius+stator_teeth_outer_radius), 0.25*ks_pitch,0, mesh*speed_up_air_regions)			#stator_alluminium(air-like)_teeth (EDITED)
	    

  #coil side area#
  sf.oarc(0.5*ks_pitch,0.5*ks_pitch+0.5*kc_pitch,stator_yoke_outer_radius,arc_len) #EDITED
  sf.oarc(0.5*ks_pitch,0.5*ks_pitch+0.5*kc_pitch,stator_coil_radius,inner_airgap_arclen) #EDITED

  if use_nonairgap_layers == 1:
    sf.oarc(0.5*ks_pitch,0.5*ks_pitch+0.5*kc_pitch,arc_stator_layer_split_radius,arc_len) #EDITED

  sf.pnline(stator_yoke_outer_radius,0.5*ks_pitch+0.5*kc_pitch,stator_teeth_outer_radius,0.5*ks_pitch+0.5*kc_pitch,whole1)#EDITED
  sf.pnline(stator_teeth_outer_radius,0.5*ks_pitch+0.5*kc_pitch,stator_coil_radius,0.5*ks_pitch+0.5*kc_pitch,whole2)#EDITED

  if use_machine_periodicity == 0:
    sf.oarc(0,-0.5*kc_pitch,stator_yoke_outer_radius,arc_len)
    sf.oarc(0,-0.5*kc_pitch,stator_coil_radius,inner_airgap_arclen)
    if use_nonairgap_layers == 1:
      sf.oarc(0,-0.5*kc_pitch,arc_stator_layer_split_radius,arc_len)
      
  else:
    sf.oarc(0,0,stator_yoke_outer_radius,arc_len)
    sf.oarc(0,0,stator_coil_radius,inner_airgap_arclen) 
    
    if use_nonairgap_layers == 1:
      sf.oarc(0,0,arc_stator_layer_split_radius,arc_len) 
      sf.oarc(0.5*ks_pitch+0.5*kc_pitch,0.5*ks_pitch+kc_pitch,arc_stator_layer_split_radius,arc_len)#EDITED 
    
  
  sf.mirror(0,'x')
  sf.rotate(0,0,0.5*coil_pitch)  
    
  if use_machine_periodicity == 1:
    #sf.nrotate(0, 0, coil_pitch, int(q/kq-2)) #poles=8,kq=1 thus q=4 and then int(q/kq-2)=2. If poles=4,kq=1 thus q=2 and then int(q/kq-2)=0 
    #ek kan sommer logies uitredineer dat dit altyd tenminste 2 keer nog gaan moet rotate, sodat daar 3 coils is in totaal (1 vir elke fase)
    sf.nrotate(0, 0, coil_pitch, 2)
  else:
    sf.nrotate(0, 0, coil_pitch, int(Q)-1)
  sf.end_part()

  if use_machine_periodicity == 1:
    sf.pnline(stator_yoke_outer_radius,(2*np.pi)/periodicity_in_one_full_rotation,stator_teeth_outer_radius,(2*np.pi)/periodicity_in_one_full_rotation,whole1)
    sf.pnline(stator_teeth_outer_radius,(2*np.pi)/periodicity_in_one_full_rotation,stator_coil_radius,(2*np.pi)/periodicity_in_one_full_rotation,whole2)  


elif coil_shape == SQUARE_SLOTS and flare_teeth == 0:
  stator_teeth_outer_radius = stator_yoke_outer_radius + hc*hs_ratio
  
  whole1 = abs((stator_yoke_outer_radius-stator_teeth_outer_radius)/arc_len)
  if whole1 < 1:
    whole1 = 1
    dont_place_stator_yoke_outer_region_point = 1
  else:
    whole1 = int(round(whole1))
    
  #print "whole1 = %d"%(whole1)  
    
  whole2 = abs((stator_teeth_outer_radius-stator_coil_radius)/arc_len)
  if whole2 < 1:
    whole2 = 1
  else:
    whole2 = int(round(whole2))  

  #print "whole2 = %d"%(whole2)
  
  stator_node_positions = []

  pos = stator_yoke_outer_radius
  temp1 = abs((stator_yoke_outer_radius-stator_teeth_outer_radius)/whole1) #This is actually the iron between opposing coil sides, not the stator teeth (coil cores) one would assume.
  #print("pos1")
  for i in range(0,whole1):
    pos = pos + temp1
    if pos<=stator_coil_radius:
      stator_node_positions.append(pos)
      #print(1000*pos)
    
  temp2 = abs((stator_teeth_outer_radius-stator_coil_radius)/whole2)
  #print "pos2"
  for i in range(0,whole2):
    pos = pos + temp2
    if pos<=stator_coil_radius:
      stator_node_positions.append(pos)

  #print 'stator_node_positions[i]*1000'
  #for i in range(0,len(stator_node_positions)):
    #print stator_node_positions[i]*1000
  
  sf.begin_part()
  #now ks_pitch represents angle width of stator teeth in the middle of their radial lengths
  #now kc_pitch represents angle width of the stator coil sides in the middle of its radial length
  #rather use line commands than arc commands, thus we need new coordinates of the edges of the rectangle
  #r = 0.5*(stator_coil_radius+stator_yoke_outer_radius)
  r = stator_yoke_outer_radius
  coil_side_width = 0.5*kc_pitch*r
  coil_side_height = stator_coil_radius-stator_yoke_outer_radius
  teeth_width = ks_pitch*r
  #half_teeth_width = 0.5*teeth_width
  half_teeth_width = 0.5*teeth_width*coil_side_center_placement
  x = stator_yoke_outer_radius + coil_side_height
  y = half_teeth_width+coil_side_width-coil_opposing_spacing
  #y = (coil_side_center_placement*half_teeth_width)+coil_side_width #now adjust the angle according to this variable (this variable makes the coil sides of a certain phase move closer to each other, meaning the coil sides can now again be longer in the radial direction)

  R = np.sqrt(y**2 + x**2)
  x_old = x
  if R > stator_coil_radius:
    x = np.sqrt(stator_coil_radius**2 - y**2)
  
  #the following three lines are variables only being used for purposes of terminal output print
  diff = x_old-x
  coil_side_width = coil_side_width - coil_opposing_spacing - coil_teeth_spacing
  coil_side_height = coil_side_height - coil_yoke_spacing - diff
  
  center_of_side_x = stator_yoke_outer_radius + coil_yoke_spacing +0.5*coil_side_height
  center_of_side_y = half_teeth_width + coil_teeth_spacing + 0.5*coil_side_width
  
  center_of_side_r = np.sqrt(center_of_side_x**2 + center_of_side_y**2)
  center_of_side_t = np.arcsin(center_of_side_y/center_of_side_r)
  
  temp_angle0 = np.arcsin(half_teeth_width/stator_coil_radius)
  
  sf.oarc(0,temp_angle0,stator_coil_radius,arc_len) #tooth tip arc (assuming full length teeth)

  x_teeth_outer_pitstop = np.sqrt(stator_coil_radius**2 - y**2)

  whole3 = abs((x_teeth_outer_pitstop-stator_yoke_outer_radius)/arc_len)
  if whole3 < 1:
    whole3 = 1
  else:
    whole3 = int(round(whole3))

  whole4 = abs((x-x_teeth_outer_pitstop)/arc_len)
  if whole4 < 1:
    whole4 = 1
  else:
    whole4 = int(round(whole4))

  stator_coil_height_node_positions = []

  pos = stator_yoke_outer_radius
  stator_coil_height_node_positions.append(pos)
  temp1 = abs((x_teeth_outer_pitstop-stator_yoke_outer_radius)/whole3)
  for i in range(0,whole3):
    pos = pos + temp1
    stator_coil_height_node_positions.append(pos)  

  pos = x_teeth_outer_pitstop
  #stator_coil_height_node_positions.append(pos)
  temp1 = abs((x-x_teeth_outer_pitstop)/whole4)
  if temp1>0.00001:
    for i in range(0,whole4):
      pos = pos + temp1
      #print 'pos = %g'%(pos*1000)
      stator_coil_height_node_positions.append(pos)  

  #for i in range(0,len(stator_coil_height_node_positions)):
    #print stator_coil_height_node_positions[i]*1000

  temp_x = stator_yoke_outer_radius + hs_ratio*(x-stator_yoke_outer_radius)
  temp_y = y

  #temp = stator_yoke_outer_radius + hc*hs_ratio
  smallest = abs(stator_coil_height_node_positions[0]-temp_x)
  stator_teeth_outer_radius = stator_coil_height_node_positions[0]
  for i in range(0,len(stator_coil_height_node_positions)):
    if smallest > abs(stator_coil_height_node_positions[i] - temp_x):
      smallest = abs(stator_coil_height_node_positions[i] - temp_x)
      stator_teeth_outer_radius = stator_coil_height_node_positions[i]


  temp_x_2 = stator_yoke_outer_radius+hs_square_slots_ratio*(stator_coil_radius-stator_yoke_outer_radius)

  smallest = abs(stator_coil_height_node_positions[0]-temp_x_2)
  stator_tooth_tip_radius_square_slot = stator_coil_height_node_positions[0]
  
  
  for i in range(0,len(stator_coil_height_node_positions)):
    if smallest > abs(stator_coil_height_node_positions[i] - temp_x_2):
      smallest = abs(stator_coil_height_node_positions[i] - temp_x_2)
      stator_tooth_tip_radius_square_slot = stator_coil_height_node_positions[i]

  sf.mline(stator_yoke_outer_radius+coil_yoke_spacing,half_teeth_width+coil_teeth_spacing,stator_yoke_outer_radius+coil_yoke_spacing,y,arc_len)
  sf.mline(x,y,x,half_teeth_width+coil_teeth_spacing,arc_len)
  
  sf.mline(stator_yoke_outer_radius+coil_yoke_spacing,y,x,y,arc_len)
  sf.mline(stator_yoke_outer_radius+coil_yoke_spacing,half_teeth_width+coil_teeth_spacing,x,half_teeth_width+coil_teeth_spacing,arc_len)    

  ####coordinates for eddy current losses
  ##get the far most top edge center of the coil, and the nearest (radially to center) coil bottom edge (at the center).  
  #if enable_eddy_current_losses:
    #coil_side_bot_top_edges_cplx_pnt = []
    #coil_side_bot_top_edges_cplx_pnt.append(complex(stator_yoke_outer_radius+coil_yoke_spacing,0.5*(half_teeth_width+coil_teeth_spacing+y)))
    #coil_side_bot_top_edges_cplx_pnt.append(complex(x,0.5*(half_teeth_width+coil_teeth_spacing+y)))
  
    ##for i in range(0,len(coil_side_bot_top_edges_cplx_pnt)):
      ##print("x = %.3f, y = %.3f"%(coil_side_bot_top_edges_cplx_pnt[i].real*1000,coil_side_bot_top_edges_cplx_pnt[i].imag*1000))
    
    ##print "rotate coordinates"
    ##for i in range(0,len(coil_side_bot_top_edges_cplx_pnt)):
      ##coil_side_bot_top_edges_cplx_pnt[i] = np.conj(coil_side_bot_top_edges_cplx_pnt[i])
      ##coil_side_bot_top_edges_cplx_pnt[i] = coil_side_bot_top_edges_cplx_pnt[i]*np.exp(1j*(0.5*coil_pitch))
      ##print("x = %.3f, y = %.3f"%(coil_side_bot_top_edges_cplx_pnt[i].real*1000,coil_side_bot_top_edges_cplx_pnt[i].imag*1000))  
  ####coordinates for eddy current losses

  ###coordinates for eddy current losses
  #get the far most top edge center of the coil, and the nearest (radially to center) coil bottom edge (at the center).
  if enable_eddy_current_losses:
    coil_side_bot_top_edges_cplx_pnt = []
    coil_side_bot_top_edges_cplx_pnt.append(complex(stator_yoke_outer_radius+coil_yoke_spacing,0.5*(half_teeth_width+coil_teeth_spacing+y)))
    coil_side_bot_top_edges_cplx_pnt.append(complex(x,0.5*(half_teeth_width+coil_teeth_spacing+y)))
    #for i in range(0,len(coil_side_bot_top_edges_cplx_pnt)):
      #print("x = %.3f, y = %.3f"%(coil_side_bot_top_edges_cplx_pnt[i].real*1000,coil_side_bot_top_edges_cplx_pnt[i].imag*1000))
    
    #print "rotate coordinates"
    #for i in range(0,len(coil_side_bot_top_edges_cplx_pnt)):
      #coil_side_bot_top_edges_cplx_pnt[i] = np.conj(coil_side_bot_top_edges_cplx_pnt[i])
      #coil_side_bot_top_edges_cplx_pnt[i] = coil_side_bot_top_edges_cplx_pnt[i]*np.exp(1j*(0.5*coil_pitch))
      #print("x = %.3f, y = %.3f"%(coil_side_bot_top_edges_cplx_pnt[i].real*1000,coil_side_bot_top_edges_cplx_pnt[i].imag*1000))	
  ###coordinates for eddy current losses
  
  
  for i in range(0,len(stator_coil_height_node_positions)-1):
    #sf.line(stator_coil_height_node_positions[i],y-coil_opposing_spacing,stator_coil_height_node_positions[i+1],y-coil_opposing_spacing)
    #sf.line(stator_coil_height_node_positions[i],half_teeth_width+coil_teeth_spacing,stator_coil_height_node_positions[i+1],half_teeth_width+coil_teeth_spacing)  

    if np.isclose(coil_opposing_spacing*1000, 0, rtol=1e-3, atol=1e-3, equal_nan=False):
      sf.line(stator_coil_height_node_positions[i],y,stator_coil_height_node_positions[i+1],y)
    if not np.isclose(coil_teeth_spacing*1000, 0, rtol=1e-3, atol=1e-3, equal_nan=False):
      sf.line(stator_coil_height_node_positions[i],half_teeth_width,stator_coil_height_node_positions[i+1],half_teeth_width)  

  temp_angle3 = np.arcsin(half_teeth_width/stator_tooth_tip_radius_square_slot)
  #sf.oarc(0,temp_angle3,stator_tooth_tip_radius_square_slot,arc_len) #tooth tip arc above actual stator tooth    #ADJUSTS TOOTH HEIGHT-->THIS LINE WAS REMOVED BECAUSE IT DOESNT ALWAYS CONNECT NICELY
  
  #close the airgap just above the coil sides
  sf.mline(x,half_teeth_width,stator_coil_radius*np.cos(temp_angle0),half_teeth_width,arc_len)  

  temp_angle1 = np.arctan(y/x)
  sf.oarc(temp_angle0,temp_angle1,stator_coil_radius,arc_len)
  sf.oarc(temp_angle1,temp_angle2,stator_coil_radius,arc_len)
  
  #teeth_r = stator_teeth_outer_radius
  teeth_r = np.sqrt(y**2 + stator_teeth_outer_radius**2)
  teeth_t = np.arctan(y/stator_teeth_outer_radius)
  
  #sf.oarc(teeth_t,temp_angle2,teeth_r,arc_len) #very small arc between opposing coils where they almost touch each other (keep commented, might be used again later)

  start_arc_x = stator_coil_height_node_positions[0]
  start_arc_y = half_teeth_width
  start_angle = np.arctan(start_arc_y/start_arc_x)
  
  end_arc_x = stator_yoke_outer_radius*np.cos(temp_angle2)
  end_arc_y = stator_yoke_outer_radius*np.sin(temp_angle2)
  end_angle = temp_angle2  
  
  
  arc_distance = stator_yoke_outer_radius*abs(temp_angle2-end_angle)
  #sections = int(arc_distance/arc_len)
  sections = 25

  angle_inc = abs(end_angle-start_angle)/sections
  angle_tmp = start_angle
  
  prev_x = start_arc_x
  prev_y = start_arc_y
  
  for i in range(0,sections):
    if i == sections-1:
      next_x = end_arc_x
      next_y = end_arc_y      
    else:  
      next_x = stator_yoke_outer_radius*np.cos(angle_tmp+angle_inc)
      next_y = stator_yoke_outer_radius*np.sin(angle_tmp+angle_inc)
    
    #print("angle_tmp = %g, prev_x = %g, prev_y = %g, next_x = %g, next_y = %g"%(angle_tmp*(180/np.pi),prev_x,prev_y,next_x,next_y))
    
    sf.line(prev_x,prev_y,next_x,next_y) 
    angle_tmp = angle_tmp + angle_inc
    prev_x = next_x
    prev_y = next_y
  
  #declare air region above the tooth tips (BETWEEN OPPOSING COIL SIDES)
  sf.pregion(0.5*(teeth_r+stator_coil_radius), 0.5*(temp_angle1+temp_angle2),0, mesh*speed_up_air_regions)

  #declare air region above the tooth tips (INSIDE COIL CORES) (at this point in code, the coil cores are actually situated on the x-axis!)
  sf.pregion(0.5*(stator_tooth_tip_radius_square_slot+stator_coil_radius), 0.05*(temp_angle1),0, mesh*speed_up_air_regions) #just placing the pregion command sligthly off center to avoid possible bug   
  
  smallest = abs(stator_tooth_tip_radius_square_slot-temp_x_2)
  if smallest > abs(stator_coil_radius - temp_x_2):
    #the entire stator tooth length should be covered in iron
    if stator_yoke_iron_N_al == 1:
      sf.pregion(0.5*(stator_tooth_tip_radius_square_slot+temp_x_2), 0.25*ks_pitch,stator_iron, mesh)	
    elif stator_yoke_iron_N_al == 0:
      sf.pregion(0.5*(stator_tooth_tip_radius_square_slot+temp_x_2), 0.25*ks_pitch,0, mesh*speed_up_air_regions)	

  x_region = 0.5*(x+(stator_coil_radius*np.cos(temp_angle0)))
  y_region = half_teeth_width+0.2*(y-half_teeth_width)
  sf.region(x_region, y_region,0, mesh*speed_up_air_regions)
  
  sf.mirror(0.,"x")

  sf.rotate(0,0,0.5*coil_pitch)  
  
  if use_machine_periodicity == 1:
    ####sf.nrotate(0, 0, coil_pitch, int(q/kq-2)) #poles=8,kq=1 thus q=4 and then int(q/kq-2)=2. If poles=4,kq=1 thus q=2 and then int(q/kq-2)=0 
    #ek kan sommer logies uitredineer dat dit altyd tenminste 2 keer nog gaan moet rotate, sodat daar 3 coils is in totaal (1 vir elke fase)
    sf.nrotate(0, 0, coil_pitch, 2)
  else:
    sf.nrotate(0, 0, coil_pitch, int(Q)-1)
    #sf.nrotate(0, 0, coil_pitch, 2)
  sf.end_part() 

  if conductors_round0_bars1 == 1:
    bar_side_width = coil_side_width
    bar_side_height = coil_side_height
    bar_side_area = bar_side_width*bar_side_height

  coil_side_area = coil_side_width*coil_side_height
  coil_side_area_in_mm = coil_side_area*(1000**2)
  
  if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
    current_through_slot = J*(1000**2)*coil_side_area*fill_factor
  elif input_is_Jrms0_Pcu1_slotcurrent2 == 2:
    J = current_through_slot/((1000**2)*coil_side_area*fill_factor)

#rectangular copper bars are intended for this section
elif coil_shape == SQUARE_SLOTS and flare_teeth == 1:
  #now ks_pitch represents angle width of stator teeth in the middle of their radial lengths
  #now kc_pitch represents angle width of the stator coil sides in the middle of its radial length
  #rather use line commands than arc commands, thus we need new coordinates of the edges of the rectangle
  #r = 0.5*(stator_coil_radius+stator_yoke_outer_radius)
  r = stator_yoke_outer_radius
  coil_side_width = 0.5*kc_pitch*r
  half_slot_width = coil_side_width+0.5*coil_opposing_spacing+coil_teeth_spacing
  did_enter_first_bridge_check = 0
  coil_side_height = stator_coil_radius-stator_yoke_outer_radius-coil_shoe_spacing-stator_shoe_height
  #print("coil_side_height_1 = %f"%(coil_side_height*1000))
  tooth_arc_pitch = coil_pitch-kc_pitch
  
  teeth_width = ks_pitch*r
  half_teeth_width = 0.5*teeth_width*coil_side_center_placement
  #x = stator_yoke_outer_radius + coil_side_height
  x = stator_coil_radius-coil_shoe_spacing-stator_shoe_height
  y = coil_side_width-coil_teeth_spacing
  #print("x = %f"%(x*1000))
  
  R = np.sqrt(y**2 + x**2)
  x_old = x
  if R > stator_coil_radius:
    x = np.sqrt(stator_coil_radius**2 - y**2)
  
  #the following three lines are variables only being used for purposes of terminal output print
  diff = x_old-x
  coil_side_width = coil_side_width - coil_opposing_spacing - coil_teeth_spacing
  coil_side_height = coil_side_height - coil_yoke_spacing - diff
  half_slot_width = coil_side_width+0.5*coil_opposing_spacing+coil_teeth_spacing
  
  stator_shoe_width_original = stator_shoe_width
  if half_slot_width < stator_shoe_width or np.isclose(half_slot_width, stator_shoe_width, rtol=9e-05, atol=9e-05, equal_nan=False): #prevents stator shoe width exceeding unrealisticly
    #print "japon man1"
    #print("half_slot_width = %f"%(half_slot_width*1000))
    #print("stator_shoe_width = %f"%(stator_shoe_width*1000))
    stator_shoe_width = half_slot_width
    did_enter_first_bridge_check = 1
  #print("stator_shoe_width = %f"%(stator_shoe_width*1000))
  
  #print("coil_side_height_2 = %f"%(coil_side_height*1000))
  #print("x_2 = %f"%(x*1000))
  #center_of_side_x = stator_yoke_outer_radius + coil_yoke_spacing +0.5*coil_side_height
  #center_of_side_y = half_teeth_width + coil_teeth_spacing + 0.5*coil_side_width
  
  #center_of_side_r = []
  #center_of_side_t = []
  #if number_of_copper_bars_per_coil == 1:
    #center_of_side_r.append(np.sqrt(center_of_side_x**2 + center_of_side_y**2))
  #elif number_of_copper_bars_per_coil == 2:
    #center_of_side_r.append(np.sqrt((0.5*(stator_yoke_outer_radius+coil_yoke_spacing+(0.5*(x+stator_yoke_outer_radius+coil_yoke_spacing)-0.5*coil_opposing_spacing)))**2 + (center_of_side_y)**2))
    #center_of_side_r.append(np.sqrt(0.5*(0.5*(x+stator_yoke_outer_radius+coil_yoke_spacing)+0.5*coil_opposing_spacing +x))**2 + (center_of_side_y)**2)
    #center_of_side_t.append(np.arcsin(center_of_side_y/center_of_side_r[0]))  
    #center_of_side_t.append(np.arcsin(center_of_side_y/center_of_side_r[1]))
  
  #coil_side_center_cplx_pnts = []
  #coil_side_center_cplx_pnts.append(complex(0.5*(stator_yoke_outer_radius+coil_yoke_spacing+x),0.5*(0.5*coil_opposing_spacing + y)))
  
  #temp_cplx_pnt = complex(0.5*(stator_yoke_outer_radius+coil_yoke_spacing+x),-0.5*(0.5*coil_opposing_spacing + y))
  #temp_cplx_pnt*=np.exp(1j*(coil_pitch))
  
  #coil_side_center_cplx_pnts.append(temp_cplx_pnt)
  
  #coil_side_center_cplx_pnts.append(coil_side_center_cplx_pnts[0]*np.exp(1j*(coil_pitch)))
  #coil_side_center_cplx_pnts.append(coil_side_center_cplx_pnts[1]*np.exp(1j*(coil_pitch)))

  #coil_side_center_cplx_pnts.append(coil_side_center_cplx_pnts[2]*np.exp(1j*(coil_pitch)))
  #coil_side_center_cplx_pnts.append(coil_side_center_cplx_pnts[3]*np.exp(1j*(coil_pitch)))

  #for i in range(0,len(coil_side_center_cplx_pnts)):
      #print("x = %.3f, y = %.3f"%(coil_side_center_cplx_pnts[i].real*1000,coil_side_center_cplx_pnts[i].imag*1000))

  x_shoe = x+coil_shoe_spacing+stator_shoe_height
  y_shoe = y+coil_teeth_spacing-stator_shoe_width

  R = np.sqrt(y_shoe**2 + x_shoe**2)
  x_old_shoe = x_shoe
  if R > stator_coil_radius:
    x_shoe = np.sqrt(stator_coil_radius**2 - y_shoe**2)  
  diff_shoe = abs(x_old_shoe-x_shoe)

  #determine teeth bottom and upper widths
  teeth_bottom_1 = complex(stator_yoke_outer_radius,y+coil_teeth_spacing)
  teeth_top_1 = complex(x+coil_shoe_spacing,y+coil_teeth_spacing) #at the top just below shoe
  
  teeth_bottom_2 = complex(stator_yoke_outer_radius,-(y+coil_teeth_spacing))
  teeth_top_2 = complex(x+coil_shoe_spacing,-(y+coil_teeth_spacing)) #at the top just below shoe

  teeth_bottom_2*=np.exp(1j*(coil_pitch))
  teeth_top_2*=np.exp(1j*(coil_pitch))

  #print(teeth_bottom_1)
  #print(teeth_top_1)
  #print(teeth_bottom_2)
  #print(teeth_top_2)

  teeth_bottom_width = np.sqrt((teeth_bottom_2.real-teeth_bottom_1.real)**2+(teeth_bottom_2.imag-teeth_bottom_1.imag)**2)
  #print(teeth_bottom_width)
  
  teeth_top_width = np.sqrt((teeth_top_2.real-teeth_top_1.real)**2+(teeth_top_2.imag-teeth_top_1.imag)**2)
  #print(teeth_top_width)
  
  shoe_tip_height = x_shoe-(x+coil_shoe_spacing)
  shoe_tip_width = stator_shoe_width
  
  stator_teeth_height_excl_shoe = x+coil_shoe_spacing-stator_yoke_outer_radius
  
  #print(shoe_tip_height*1000)
  #print(shoe_tip_width*1000)
  #print(shoe_inner_height*1000)
  
  #if conductors_round0_bars1 == 1 and flare_teeth == 1:
  coil_side_width = y-(0.5*coil_opposing_spacing) 
  coil_side_height = x-(stator_yoke_outer_radius+coil_shoe_spacing)
  half_slot_width = coil_side_width+0.5*coil_opposing_spacing+coil_teeth_spacing
  
  if half_slot_width < stator_shoe_width_original or np.isclose(half_slot_width, stator_shoe_width_original, rtol=9e-05, atol=9e-05, equal_nan=False): #prevents stator shoe width exceeding unrealisticly
    #print "japon man2"
    #print("half_slot_width = %f"%(half_slot_width*1000))
    #print("stator_shoe_width = %f"%(stator_shoe_width*1000))
    stator_shoe_width = half_slot_width
  else:
    stator_shoe_width = stator_shoe_width_original
    if did_enter_first_bridge_check:
      #print "hoppa"
      y_shoe = y+coil_teeth_spacing-stator_shoe_width
      #y_shoe = y+coil_teeth_spacing
    
  #x_shoe = np.sqrt(stator_coil_radius**2 - y_shoe**2)  
  
  #print("stator_shoe_width = %f"%(stator_shoe_width*1000))
  
  #print("coil_side_height_3 = %f"%(coil_side_height*1000))

  #if number_of_copper_bars_per_coil == 1:
    #bar_side_width = coil_side_width
    #bar_side_height = coil_side_height
  #elif number_of_copper_bars_per_coil == 2:
    #bar_side_width = coil_side_width
    #bar_side_height = 0.5*(x+stator_yoke_outer_radius+coil_yoke_spacing)-0.5*coil_opposing_spacing - (stator_yoke_outer_radius+coil_yoke_spacing)
  
  ##variables used in Autodesk Inventor
  #slot_width = 2*bar_side_width + coil_opposing_spacing+2*coil_yoke_spacing
  
  #if number_of_copper_bars_per_coil == 2:
    #slot_height = 2*bar_side_height + coil_opposing_spacing + coil_yoke_spacing + coil_teeth_spacing
  #else:
    #slot_height = bar_side_height + coil_yoke_spacing + coil_teeth_spacing
    
  shoe_weakpoint_x = 0.5*coil_side_width
  shoe_weakpoint_y = x+coil_shoe_spacing
  shoe_weakpoint_r = np.sqrt(shoe_weakpoint_x**2+shoe_weakpoint_y**2)
  shoe_weakpoint_thickness_compensation = shoe_weakpoint_thickness-(stator_coil_radius-shoe_weakpoint_r-stator_shoe_height)

  #print("stator_yoke_inner_radius_4 = %f"%(stator_yoke_inner_radius*1000))
  #print("stator_yoke_outer_radius_4 = %f"%(stator_yoke_outer_radius*1000))

  stator_yoke_outer_radius = stator_yoke_outer_radius - shoe_weakpoint_thickness_compensation
  x = x - shoe_weakpoint_thickness_compensation
  stator_yoke_inner_radius = stator_yoke_inner_radius - shoe_weakpoint_thickness_compensation
  shoe_tip_height = shoe_tip_height + shoe_weakpoint_thickness_compensation
  #print("x_4 = %f"%(x*1000))
  #print("stator_yoke_inner_radius_4A = %f"%(stator_yoke_inner_radius*1000))
  #print("stator_yoke_outer_radius_4A = %f"%(stator_yoke_outer_radius*1000))
  
  #calculate coordinates for the stator shoe wedge
  wedge_T = stator_shoe_width*np.arctan(stator_shoe_wedge_degrees*np.pi/180)
  wedge_checkpoint = x+coil_shoe_spacing-wedge_T
  
  slot_length_available_for_bars = wedge_checkpoint - stator_yoke_outer_radius - 2*coil_yoke_spacing
  slot_length_available_for_bars = slot_length_available_for_bars - number_of_copper_bars_per_coil*0.5*coil_opposing_spacing
  
  bar_side_width = coil_side_width
  bar_side_height = slot_length_available_for_bars/number_of_copper_bars_per_coil
  
  ###coordinates for eddy current losses
  #get the far most top edge center of the coil, and the nearest (radially to center) coil bottom edge (at the center).
  coil_side_bot_top_edges_cplx_pnt = []
  y_center = 0.5*(0.5*coil_opposing_spacing+y)   
  coil_side_bot_top_edges_cplx_pnt.append([complex(stator_yoke_outer_radius+coil_yoke_spacing,y_center)]) #first edge
  
  for i in range(0,number_of_copper_bars_per_coil):
    if i%2 == 0:
      coil_side_bot_top_edges_cplx_pnt[0].append(coil_side_bot_top_edges_cplx_pnt[0][-1]+complex(bar_side_height,0))
    else:
      coil_side_bot_top_edges_cplx_pnt[0].append(coil_side_bot_top_edges_cplx_pnt[0][-1]+complex(coil_opposing_spacing,0))
    
  if conductors_round0_bars1 == 1:  
    #coil_side_bot_top_edges_cplx_pnt[0].append(complex(x-coil_shoe_spacing,y_center))  #last edge
    coil_side_bot_top_edges_cplx_pnt[0].append(complex(wedge_checkpoint-coil_shoe_spacing,y_center))  #last edge
  else:
    coil_side_bot_top_edges_cplx_pnt[0].append(complex(x,y_center))  #last edge
  
  #now copy and rotate these coordinates so that they describe all the other coil sides as well
  if use_machine_periodicity == 1:
    copy_coils=6
  else:
    copy_coils=Q*2
  for i in range(1,copy_coils):
    coil_side_bot_top_edges_cplx_pnt.append([])
    for j in range(0,len(coil_side_bot_top_edges_cplx_pnt[0])):
      coil_side_bot_top_edges_cplx_pnt[i].append(coil_side_bot_top_edges_cplx_pnt[0][j])
      
  for j in range(0,len(coil_side_bot_top_edges_cplx_pnt[0])):
    coil_side_bot_top_edges_cplx_pnt[1][j] = np.conj(coil_side_bot_top_edges_cplx_pnt[0][j])*np.exp(1j*(coil_pitch))
    coil_side_bot_top_edges_cplx_pnt[2][j] = coil_side_bot_top_edges_cplx_pnt[0][j]*np.exp(1j*(coil_pitch))
    coil_side_bot_top_edges_cplx_pnt[3][j] = coil_side_bot_top_edges_cplx_pnt[1][j]*np.exp(1j*(coil_pitch))
    coil_side_bot_top_edges_cplx_pnt[4][j] = coil_side_bot_top_edges_cplx_pnt[0][j]*np.exp(2j*(coil_pitch))
    coil_side_bot_top_edges_cplx_pnt[5][j] = coil_side_bot_top_edges_cplx_pnt[1][j]*np.exp(2j*(coil_pitch))  
  
  #if use_machine_periodicity == 0:
    #for j in range(0,len(coil_side_bot_top_edges_cplx_pnt[0])):
      #print(coil_side_bot_top_edges_cplx_pnt[0][j]*1000)    
    
    #print("q = %g"%(q))
    #print("Q = %g"%(Q))
    #print("coil_pitch = %g"%(coil_pitch*180/np.pi))
    n=6
    for i in range(1,int(copy_coils/6)):
      for j in range(0,len(coil_side_bot_top_edges_cplx_pnt[0])):
	#multiply by 3j*coil_pitch because that is the symmetry pitch of the machine
	coil_side_bot_top_edges_cplx_pnt[n+0][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+0][j]*np.exp(3j*(coil_pitch))
	coil_side_bot_top_edges_cplx_pnt[n+1][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+1][j]*np.exp(3j*(coil_pitch))
	coil_side_bot_top_edges_cplx_pnt[n+2][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+2][j]*np.exp(3j*(coil_pitch))
	coil_side_bot_top_edges_cplx_pnt[n+3][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+3][j]*np.exp(3j*(coil_pitch))
	coil_side_bot_top_edges_cplx_pnt[n+4][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+4][j]*np.exp(3j*(coil_pitch))
	coil_side_bot_top_edges_cplx_pnt[n+5][j] = coil_side_bot_top_edges_cplx_pnt[(n-6)+5][j]*np.exp(3j*(coil_pitch))
      n=n+6

  #for j in range(0,len(coil_side_bot_top_edges_cplx_pnt)):
    #for i in range(0,len(coil_side_bot_top_edges_cplx_pnt[0])):
      #print "next coil side"
      #print("x = %.3f, y = %.3f"%(coil_side_bot_top_edges_cplx_pnt[j][i].real*1000,coil_side_bot_top_edges_cplx_pnt[j][i].imag*1000))	
  ###coordinates for eddy current losses

  ###coordinates for magnet losses
  shoe_tip_arc_length = 2*(0.5*coil_pitch - np.arctan(y_shoe/x_shoe))*stator_coil_radius
  slot_opening_arc_length = ((2*np.pi*stator_coil_radius) - shoe_tip_arc_length*Q)/Q
  ###coordinates for magnet losses

  #determine effective slot copper fill factor
  area_copper = bar_side_width*bar_side_height*number_of_copper_bars_per_coil*2 #times 2 because double layer slots
  area_slot = 2*half_slot_width*(wedge_checkpoint - stator_yoke_outer_radius)
  effective_fill_factor = area_copper/area_slot

  x_shoe_fake = np.sqrt(stator_coil_radius**2 - y_shoe**2)

  #first coil side/bars
  sf.begin_part()  
  
  if conductors_round0_bars1 == 1:
    j = 0
    for i in range(0,number_of_copper_bars_per_coil):
      #horisontal lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,0.5*coil_opposing_spacing)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,y,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,y)
      
      #vertical lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j].real,y)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j+1].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,y)
      j = j + 2
  
  if conductors_round0_bars1 == 0:
    sf.line(x_shoe-shoe_tip_height,y_shoe,x_shoe-shoe_tip_height,0) #vertical line  
  
  #stator teeth/shoes
  sf.line(stator_yoke_outer_radius,0,stator_yoke_outer_radius,y+coil_teeth_spacing) #vertical
  if not np.isclose(stator_shoe_wedge_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
    #print "pretzels"
    sf.line(stator_yoke_outer_radius,y+coil_teeth_spacing,wedge_checkpoint,y+coil_teeth_spacing)#horisontal
    #draw wedge
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,wedge_checkpoint,y+coil_teeth_spacing)
  else:
    #print "lays"
    sf.line(stator_yoke_outer_radius,y+coil_teeth_spacing,x+coil_shoe_spacing,y+coil_teeth_spacing)#horisontal
    #draw stator shoe
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing,x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width)#vertical

  if not np.isclose(half_slot_width, stator_shoe_width, rtol=9e-05, atol=9e-05, equal_nan=False):
    #print "simba"
    #draw stator shoe
    #sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,x_shoe,y_shoe)#horisontal
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,x_shoe_fake,y_shoe)#horisontal
    
  
  #print("x_5 = %f"%(x*1000))
  #print("coil_shoe_spacing_5 = %f"%(coil_shoe_spacing*1000))
  #print("x+coil_shoe_spacing_5 = %f,y+coil_teeth_spacing = %f,x+coil_shoe_spacing = %f,y+coil_teeth_spacing-stator_shoe_width = %f"%((x+coil_shoe_spacing)*1000,(y+coil_teeth_spacing)*1000,(x+coil_shoe_spacing)*1000,(y+coil_teeth_spacing-stator_shoe_width)*1000))

  #sf.oarc(0,np.arctan(y_shoe/x_shoe),stator_coil_radius,arc_len)
  #sf.oarc(np.arctan(y_shoe/x_shoe),0.5*coil_pitch,stator_coil_radius,arc_len)

  sf.oarc(0,np.arctan(y_shoe/x_shoe_fake),stator_coil_radius,arc_len)
  sf.oarc(np.arctan(y_shoe/x_shoe_fake),0.5*coil_pitch,stator_coil_radius,arc_len)
  
  sf.end_part()
  
  if insert_lamination_hole == 1:
    sf.begin_part()
    rod_angle = 0.5*coil_pitch
    #print("np.arctan(y_shoe/x_shoe) = %.2f"%(np.arctan(y_shoe/x_shoe)*(180/np.pi)))
    #print("0.5*coil_pitch = %.2f"%(0.5*coil_pitch*(180/np.pi)))
    rod_x = (stator_coil_radius-stator_rod_distance_from_tooth_tip)*np.cos(rod_angle)
    rod_y = (stator_coil_radius-stator_rod_distance_from_tooth_tip)*np.sin(rod_angle)
    rod_cplx_pnt = complex(rod_x,rod_y)
    sf.arc(rod_cplx_pnt.real,rod_cplx_pnt.imag,0,2*np.pi,0.5*stator_rod_hole_diameter,0.5*arc_len)
    if use_machine_periodicity == 1:
      sf.nrotate(0, 0, coil_pitch, 2)
    else:
      sf.nrotate(0, 0, coil_pitch, Q-1)
    sf.end_part()
  
  
  ##middle coil side/bars
  sf.begin_part()  
  if conductors_round0_bars1 == 1:
    j = 0
    for i in range(0,number_of_copper_bars_per_coil):
      #horisontal lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,0.5*coil_opposing_spacing)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,y,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,y)
      
      #vertical lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j].real,y)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j+1].real,0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,y)
      j = j + 2
  
  if conductors_round0_bars1 == 0:
    sf.line(coil_side_bot_top_edges_cplx_pnt[0][0].real,0,coil_side_bot_top_edges_cplx_pnt[0][-1].real,0) #horisontal
    sf.line(x_shoe-shoe_tip_height,y_shoe,x_shoe-shoe_tip_height,0) #vertical line

  #stator teeth/shoes
  sf.line(stator_yoke_outer_radius,0,stator_yoke_outer_radius,y+coil_teeth_spacing) #vertical
  if not np.isclose(stator_shoe_wedge_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
    sf.line(stator_yoke_outer_radius,y+coil_teeth_spacing,wedge_checkpoint,y+coil_teeth_spacing)#horisontal
    #draw wedge
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,wedge_checkpoint,y+coil_teeth_spacing)
  else:
    sf.line(stator_yoke_outer_radius,y+coil_teeth_spacing,x+coil_shoe_spacing,y+coil_teeth_spacing)#horisontal
    #draw stator shoe
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing,x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width)#vertical

  if not np.isclose(half_slot_width, stator_shoe_width, rtol=9e-05, atol=9e-05, equal_nan=False):
    #draw stator shoe
    #sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,x_shoe,y_shoe)#horisontal
    sf.line(x+coil_shoe_spacing,y+coil_teeth_spacing-stator_shoe_width,x_shoe_fake,y_shoe)#horisontal
  
  #sf.oarc(0,np.arctan(y_shoe/x_shoe),stator_coil_radius,arc_len)
  #sf.oarc(np.arctan(y_shoe/x_shoe),0.5*coil_pitch,stator_coil_radius,arc_len)

  sf.oarc(0,np.arctan(y_shoe/x_shoe_fake),stator_coil_radius,arc_len)
  sf.oarc(np.arctan(y_shoe/x_shoe_fake),0.5*coil_pitch,stator_coil_radius,arc_len)

  sf.mirror(0.,"x")
  sf.rotate(0,0,coil_pitch) 
  ###sf.nrotate(0,0,coil_pitch, 1) 

  if use_machine_periodicity == 1:
    ###sf.nrotate(0, 0, coil_pitch, int(q/kq-2)) #poles=8,kq=1 thus q=4 and then int(q/kq-2)=2. If poles=4,kq=1 thus q=2 and then int(q/kq-2)=0 
    #ek kan sommer logies uitredineer dat dit altyd tenminste 2 keer nog gaan moet rotate, sodat daar 3 coils is in totaal (1 vir elke fase)
    sf.nrotate(0, 0, coil_pitch, 1)
  else:
    sf.nrotate(0, 0, coil_pitch, int(Q)-1)
    #sf.nrotate(0, 0, coil_pitch, 2)
  sf.end_part()
  
  #last coil side/bars
  sf.begin_part()
  if conductors_round0_bars1 == 1:
    j = 0
    for i in range(0,number_of_copper_bars_per_coil):
      #horisontal lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,-0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,-0.5*coil_opposing_spacing)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,-y,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,-y)
      
      #vertical lines
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j].real,-0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j].real,-y)
      sf.line(coil_side_bot_top_edges_cplx_pnt[0][j+1].real,-0.5*coil_opposing_spacing,coil_side_bot_top_edges_cplx_pnt[0][j+1].real,-y)
      j = j + 2
  
  if conductors_round0_bars1 == 0:
    sf.line(x_shoe-shoe_tip_height,-y_shoe,x_shoe-shoe_tip_height,0) #vertical line

  #stator teeth/shoes
  sf.line(stator_yoke_outer_radius,0,stator_yoke_outer_radius,-(y+coil_teeth_spacing)) #vertical
  if not np.isclose(stator_shoe_wedge_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
    sf.line(stator_yoke_outer_radius,-(y+coil_teeth_spacing),wedge_checkpoint,-(y+coil_teeth_spacing))#horisontal
    #draw wedge
    sf.line(x+coil_shoe_spacing,-(y+coil_teeth_spacing-stator_shoe_width),wedge_checkpoint,-(y+coil_teeth_spacing))
  else:
    sf.line(stator_yoke_outer_radius,-(y+coil_teeth_spacing),x+coil_shoe_spacing,-(y+coil_teeth_spacing))#horisontal
    #draw stator shoe
    sf.line(x+coil_shoe_spacing,-(y+coil_teeth_spacing),x+coil_shoe_spacing,-(y+coil_teeth_spacing-stator_shoe_width))#vertical  
  
  if not np.isclose(half_slot_width, stator_shoe_width, rtol=9e-05, atol=9e-05, equal_nan=False):
    #draw stator shoe
    #sf.line(x+coil_shoe_spacing,-(y+coil_teeth_spacing-stator_shoe_width),x_shoe,-y_shoe)#horisontal
    sf.line(x+coil_shoe_spacing,-(y+coil_teeth_spacing-stator_shoe_width),x_shoe_fake,-y_shoe)#horisontal

  #sf.oarc(0,np.arctan(-y_shoe/x_shoe),stator_coil_radius,arc_len)
  #sf.oarc(np.arctan(-y_shoe/x_shoe),-0.5*coil_pitch,stator_coil_radius,arc_len)  

  sf.oarc(0,np.arctan(-y_shoe/x_shoe_fake),stator_coil_radius,arc_len)
  sf.oarc(np.arctan(-y_shoe/x_shoe_fake),-0.5*coil_pitch,stator_coil_radius,arc_len)  
  
  sf.rotate(0,0,3*coil_pitch) 
  sf.end_part()  
  
  coil_side_area = (coil_side_width*coil_side_height) - 0.5*wedge_T*stator_shoe_width #subtract area of wedge triangle
  coil_side_area_in_mm = coil_side_area*(1000**2)
  if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
    current_through_slot = J*(1000**2)*coil_side_area*fill_factor    
  elif input_is_Jrms0_Pcu1_slotcurrent2 == 2:
    J = current_through_slot/((1000**2)*coil_side_area*fill_factor)
  
  if number_of_copper_bars_per_coil == 2:
    bar_side_area = bar_side_width*bar_side_height*number_of_copper_bars_per_coil
    if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
      current_through_slot = J*(1000**2)*bar_side_area*fill_factor
    elif input_is_Jrms0_Pcu1_slotcurrent2 == 2:
      J = current_through_slot/((1000**2)*bar_side_area*fill_factor)   
   
#Draw yoke lines
if use_machine_periodicity == 1:
  ###outer yoke###
  sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation, ryo, arc_len*mesh_yoke_ratio)

  if square_magnets == 0:
    for i in range(0,len(sorted_rotor_radii)-1):
      sf.pmline(sorted_rotor_radii[i],0,sorted_rotor_radii[i+1],0,arc_len)
      sf.pmline(sorted_rotor_radii[i],(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[i+1],(2*np.pi)/periodicity_in_one_full_rotation,arc_len) #was commented because causes pk 16 (Rix's prototype) not simulate successfully
  
  ###inner yoke### 
  #sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation-coil_offset, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
  sf.oarc(0-coil_offset, (2*np.pi)/periodicity_in_one_full_rotation-coil_offset, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
   
  if use_nonairgap_layers == 1:
    if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:    
      sf.oarc(0, (2*np.pi)/periodicity_in_one_full_rotation, arc_stator_yoke_layer_split_radius, arc_len*mesh_yoke_ratio)
  
else:
  ###outer yoke###
  sf.oarc(0, 2*np.pi, ryo, arc_len*mesh_yoke_ratio)
  ###inner yoke###
  sf.oarc(0, 2*np.pi, stator_yoke_inner_radius, arc_len*mesh_yoke_ratio)
  
  if use_nonairgap_layers == 1:
    arc_rotor_yoke_layer_split_radius = magnet_radially_outer_radius+rotor_yoke_layer_fraction*(ryo-magnet_radially_outer_radius)
    #sf.oarc(0, 2*np.pi, arc_rotor_yoke_layer_split_radius, arc_len)
    
    #arc_stator_yoke_layer_split_radius = stator_yoke_inner_radius+stator_yoke_layer_fraction*(stator_yoke_outer_radius-stator_yoke_inner_radius)
    #if np.isclose(stator_yoke_layer_fraction,1.0,rtol=arc_len*mesh_yoke_ratio,atol=arc_len*mesh_yoke_ratio, equal_nan=False):
    if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:
      sf.oarc(0, 2*np.pi, arc_stator_yoke_layer_split_radius, arc_len*mesh_yoke_ratio) 


#MAGNETS_REIMPLEMENTED
radially_magnet_pitch = (2*np.pi)/magnet_ns_total

##pmlines need to be made for each segment in height, thus it is first needed to sort the radii from small to large, in a single array.
#rotor_radii = [magnet_radially_inner_radius,magnet_tangentially_inner_radius,gap_teeth_radius,magnet_tangentially_slot_radius,magnet_radially_outer_radius,magnet_tangentially_outer_radius]
#sorted_rotor_radii = sorted(rotor_radii)

#works very well to record node positions
#radial_node_positions = []
#inc_size = 0
#start = sorted_rotor_radii[0]
#current_pos = start
#for i in range(0,len(whole)):
  #inc_size = abs(sorted_rotor_radii[i+1]-sorted_rotor_radii[i])/whole[i]
  #for j in range(0,whole[i]):
    #radial_node_positions.append(current_pos)
    #current_pos = current_pos + inc_size

if draw_magnets:
  sf.begin_part()

  if square_magnets == 0:
    #km_gap (1)
    angle1 = 0
    angle2 = angle1 + km_gap*0.5*radially_magnet_pitch#EDITED
    diff4 = (angle2-angle1)*magnet_radially_inner_radius


    if diff4<arc_len:
      angle2 = angle1
    else:
      diff  = magnet_radially_outer_radius-gap_teeth_radius
      diff2 = gap_teeth_radius-magnet_radially_inner_radius
      diff3 = magnet_radially_outer_radius-gap_teeth_radius
      sf.oarc(angle1,angle2, magnet_radially_inner_radius,outer_airgap_arclen)
      sf.oarc(angle1,angle2, magnet_radially_outer_radius, arc_len)
      if use_nonairgap_layers == 1:
	sf.oarc(angle1,angle2, arc_rotor_magnet_layer_split_radius, arc_len)

      if diff3>=arc_len:
	sf.oarc(angle1,angle2, gap_teeth_radius , arc_len)
	if use_nonairgap_layers == 1:
	  sf.oarc(angle1,angle2, arc_rotor_magnet_layer_split_radius , arc_len)
	  
      if diff2>=arc_len and diff4>=arc_len:
	if use_nonairgap_layers == 0:
	  if interior_magnets == 0:
	    sf.pregion(0.5*(magnet_radially_inner_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions)
	  else:
	    sf.pregion(0.5*(magnet_radially_inner_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh) #rotor iron surrounding the interior magnets
	elif use_nonairgap_layers == 1:
	  sf.pregion(0.5*(magnet_radially_inner_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions)
	  sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh_rotor_layer*speed_up_air_regions)
	  
      if diff>=arc_len:
	if rotor_yoke_iron_N_al == 1:
	  if use_nonairgap_layers == 0:
	    sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor iron teeth
	  elif use_nonairgap_layers == 1:
	    sf.pregion(0.5*(magnet_radially_outer_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor iron teeth
	    sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh_rotor_layer) #assign rotor iron teeth
	else:
	  if use_nonairgap_layers == 0:
	    if interior_magnets == 0:
	      sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions) #assign rotor alluminium (air-like) teeth
	    else:
	      sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor alluminium (air-like) teeth
	  elif use_nonairgap_layers == 1:
	    sf.pregion(0.5*(magnet_radially_outer_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions) #assign rotor alluminium (air-like) teeth
	    sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh_rotor_layer*speed_up_air_regions) #assign rotor alluminium (air-like) teeth

      for i in range (0,len(sorted_rotor_radii)-1):
	if (sorted_rotor_radii[i+1] < magnet_radially_outer_radius) or (np.isclose(sorted_rotor_radii[i+1], magnet_radially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) or np.isclose(sorted_rotor_radii[i+1], gap_teeth_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False)):
	  sf.pnline(sorted_rotor_radii[i],angle1,sorted_rotor_radii[i+1],angle1,whole[i])

    angle1_save = angle1
    angle2_save = angle2

    #kmr
    angle1 = angle2
    angle2 = angle1 + kmr*radially_magnet_pitch#EDITED
    diff4 = (angle2-angle1)*magnet_radially_inner_radius
    if diff4<arc_len:
      angle1 = angle2

    for i in range(0,len(sorted_rotor_radii)-1):
      if (sorted_rotor_radii[i+1] < magnet_radially_outer_radius) or (np.isclose(sorted_rotor_radii[i+1], magnet_radially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) or np.isclose(sorted_rotor_radii[i+1], gap_teeth_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False)):
	sf.pnline(sorted_rotor_radii[i],angle1,sorted_rotor_radii[i+1],angle1,whole[i])#EDITED

    sf.oarc(angle1, angle2, magnet_radially_inner_radius,outer_airgap_arclen)#EDITED
    sf.oarc(angle1, angle2, magnet_radially_outer_radius, arc_len)#EDITED
    if use_nonairgap_layers == 1:
      sf.oarc(angle1, angle2, arc_rotor_magnet_layer_split_radius, arc_len)


    for i in range (0,len(sorted_rotor_radii)-1):
      if (sorted_rotor_radii[i+1] < magnet_radially_outer_radius) or (np.isclose(sorted_rotor_radii[i+1], magnet_radially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) or np.isclose(sorted_rotor_radii[i+1], gap_teeth_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False)):
	sf.pnline(sorted_rotor_radii[i],angle2,sorted_rotor_radii[i+1],angle2,whole[i])
	


    #I'm leaving kmt out because I will probably not need it in the future
    ##kmt
    angle1 = angle2
    angle2 = angle1 + kmt*radially_magnet_pitch
    angle3 = angle2 + km_gap*0.5*radially_magnet_pitch
    diff5 = (angle3 - angle2)*magnet_radially_inner_radius

    #if diff5<arc_len:
      #angle2 = angle3

      #angle1_save = angle1
      #angle2_save = angle2
      #diff2 = magnet_tangentially_outer_radius - magnet_tangentially_inner_radius
      #diff3 = magnet_tangentially_inner_radius - magnet_radially_inner_radius

      #if diff2>=arc_len and diff3>=arc_len:
	#sf.oarc(angle1, angle2, magnet_tangentially_inner_radius ,arc_len)
      #sf.oarc(angle1, angle2, magnet_radially_inner_radius ,outer_airgap_arclen)
      #sf.oarc(angle1, angle2, magnet_radially_outer_radius, arc_len)
      #sf.oarc(angle1, angle2, magnet_tangentially_outer_radius, arc_len)
      #if use_nonairgap_layers == 1:
	#sf.oarc(angle1, angle2, arc_rotor_magnet_layer_split_radius, arc_len)
	
      #for i in range(0,len(sorted_rotor_radii)-1):
	#if not np.isclose(sorted_rotor_radii[i+1],ryo,rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
	  #sf.pnline(sorted_rotor_radii[i],angle2,sorted_rotor_radii[i+1],angle2,whole[i])   
    #else:
      #angle1_save = angle1
      #angle2_save = angle2
      #diff2 = magnet_tangentially_outer_radius - magnet_tangentially_inner_radius
      #diff3 = magnet_tangentially_inner_radius - magnet_radially_inner_radius

      #if diff2>=arc_len and diff3>=arc_len:
	#sf.oarc(angle1, angle2, magnet_tangentially_inner_radius ,arc_len)
      #sf.oarc(angle1, angle2, magnet_radially_inner_radius ,outer_airgap_arclen)
      #if use_nonairgap_layers == 1:
	#sf.oarc(angle1, angle2, arc_rotor_magnet_layer_split_radius ,outer_airgap_arclen)
      
      #if not np.isclose(abs(angle2-angle1),0,rtol=1e-02, atol=1e-02, equal_nan=False):
	#sf.oarc(angle1, angle2, magnet_tangentially_outer_radius, arc_len)	#this arc is not in sync with the nodes caused by lines being drawn by whole numbers
	#if use_nonairgap_layers == 1:
	  #sf.oarc(angle1, angle2,arc_rotor_magnet_layer_split_radius, arc_len)
	#for i in range(0,len(sorted_rotor_radii)-1):
	  #if not np.isclose(sorted_rotor_radii[i+1],ryo,rtol=rtol_rotor, atol=atol_rotor, equal_nan=False):
	    #sf.pnline(sorted_rotor_radii[i],angle1,sorted_rotor_radii[i+1],angle1,whole[i])
	    #sf.pnline(sorted_rotor_radii[i],angle2,sorted_rotor_radii[i+1],angle2,whole[i])  
      #else:
	#sf.oarc(angle1, angle2, magnet_radially_outer_radius, arc_len)
	#if use_nonairgap_layers == 1:
	  #sf.oarc(angle1, angle2, arc_rotor_magnet_layer_split_radius, arc_len)
      

    #km_gap (2)
    if diff5>=arc_len:
      angle1 = angle2
      angle2 = angle1 + km_gap*0.5*radially_magnet_pitch
      diff3 = magnet_radially_outer_radius-gap_teeth_radius
      diff4 = (angle2-angle1)*magnet_radially_inner_radius
      sf.oarc(angle1,angle2, magnet_radially_inner_radius,outer_airgap_arclen)
      sf.oarc(angle1,angle2, magnet_radially_outer_radius, arc_len)
      if use_nonairgap_layers == 1:
	sf.oarc(angle1,angle2, arc_rotor_magnet_layer_split_radius, arc_len)
      
      if diff3>=arc_len:
	sf.oarc(angle1,angle2, gap_teeth_radius , arc_len)
	if use_nonairgap_layers == 1:
	  sf.oarc(angle1,angle2, arc_rotor_magnet_layer_split_radius , arc_len)
	
      diff2 = gap_teeth_radius-magnet_radially_inner_radius
      if diff2>=arc_len and diff4>=arc_len:
	if use_nonairgap_layers == 0:
	  if interior_magnets == 0:
	    sf.pregion(0.5*(magnet_radially_inner_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions)
	  else:
	    sf.pregion(0.5*(magnet_radially_inner_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh)
	elif use_nonairgap_layers == 1:
	  sf.pregion(0.5*(magnet_radially_inner_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions)
	  sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh_rotor_layer*speed_up_air_regions)

      diff=magnet_radially_outer_radius-gap_teeth_radius
      if diff>=arc_len:
	if rotor_yoke_iron_N_al == 1:
	  if use_nonairgap_layers == 0:
	    sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor iron teeth
	  elif use_nonairgap_layers == 1:
	    sf.pregion(0.5*(magnet_radially_outer_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor iron teeth
	    sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh_rotor_layer) #assign rotor iron teeth
	else:
	  if use_nonairgap_layers == 0:
	    if interior_magnets == 0:
	      sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions) #assign rotor alluminium (air-like) teeth
	    else:
	      sf.pregion(0.5*(magnet_radially_outer_radius+gap_teeth_radius),0.5*(angle1+angle2),rotor_iron,mesh) #assign rotor alluminium (air-like) teeth
	  elif use_nonairgap_layers == 1:
	    sf.pregion(0.5*(magnet_radially_outer_radius+arc_rotor_magnet_layer_split_radius),0.5*(angle1+angle2),0,mesh*speed_up_air_regions) #assign rotor alluminium (air-like) teeth
	    sf.pregion(0.5*(arc_rotor_magnet_layer_split_radius+gap_teeth_radius),0.5*(angle1+angle2),0,mesh_rotor_layer*speed_up_air_regions) #assign rotor alluminium (air-like) teeth

      #for i in range (0,len(sorted_rotor_radii)-1):
		###sf.mline(mag_place_cplx_6[i].real,mag_sleeve_cplx_6[i].imag,mag_sleeve_center_point_airgap[i].real,mag_sleeve_center_point_airgap[i].imag, arc_len)
	###sf.mline(mag_sleeve_center_point_airgap[i].real,mag_sleeve_center_point_airgap[i].imag,mag_sleeve_cplx_7[i].real,mag_sleeve_cplx_7[i].imag, arc_len)if (sorted_rotor_radii[i+1] < magnet_radially_outer_radius) or (np.isclose(sorted_rotor_radii[i+1], magnet_radially_outer_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False) or np.isclose(sorted_rotor_radii[i+1], gap_teeth_radius, rtol=rtol_rotor, atol=atol_rotor, equal_nan=False)):
	  #sf.pnline(sorted_rotor_radii[i],angle2,sorted_rotor_radii[i+1],angle2,whole[i]) #comment because causes pk 16 (Rix's machine) to fail

  elif square_magnets == 1:
    magnet_side_length = kmr*magnet_radially_pitch*magnet_radially_inner_radius
    
    mag_cplx_1 = []
    mag_cplx_2 = []
    mag_cplx_3 = []
    mag_cplx_4 = []
    mag_cplx_5 = []
    mag_beneath_cplx_1 = []
    mag_beneath_cplx_2 = []
    
    mag_cplx_1.append(complex(magnet_radially_inner_radius,-0.5*magnet_side_length))
    mag_cplx_2.append(complex(magnet_radially_inner_radius,0.5*magnet_side_length))
    mag_cplx_3.append(complex(magnet_radially_inner_radius+hmr,0.5*magnet_side_length))
    mag_cplx_4.append(complex(magnet_radially_inner_radius+hmr,-0.5*magnet_side_length))
    mag_cplx_5.append(complex(magnet_radially_inner_radius+0.5*hmr,0)) #center point of magnet    
    
    mag_beneath_cplx_1.append(complex(ryo-hyo,0.5*magnet_side_length))
    mag_beneath_cplx_2.append(complex(ryo-hyo,-0.5*magnet_side_length))    
    
    #first initial small rotate
    mag_cplx_1[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_cplx_2[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_cplx_3[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_cplx_4[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_cplx_5[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_beneath_cplx_1[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    mag_beneath_cplx_2[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
  
    if magnet_place_holders == 1:
      mag_place_cplx_1 = []
      mag_place_cplx_2 = []
      mag_place_cplx_3 = []
      mag_place_cplx_4 = []
      mag_place_cplx_5 = []
      mag_place_cplx_6 = []
      mag_place_cplx_7 = []
      mag_place_cplx_8 = []
      #mag_place_cplx_center_i = []
      #mag_place_cplx_center_o = []
      
      #mag_place_cplx_center_i.append(complex(ryo-hyo,0))
      #mag_place_cplx_center_o.append(complex(ryo,0))
   
      y_temp = 0.5*magnet_side_length+magnet_sleeve_tolerance+magnet_sleeve_height
      r_temp = ryo-hyo
      t_temp = np.arcsin(y_temp/r_temp)
      x_temp = r_temp*np.cos(t_temp)
      
      mag_place_cplx_1.append(complex(ryo-hyo,-0.5*magnet_side_length-magnet_sleeve_tolerance))    
      mag_place_cplx_8.append(complex(x_temp,-y_temp)) 
      #print("mag_place_cplx_1 = %s"%(mag_place_cplx_1[0]*1000))
      #print("mag_place_cplx_8 = %s"%(mag_place_cplx_8[0]*1000))
   
      mag_place_cplx_4.append(complex(ryo-hyo,+0.5*magnet_side_length+magnet_sleeve_tolerance))        
      mag_place_cplx_5.append(complex(x_temp,y_temp)) 
      #print("mag_place_cplx_4 = %s"%(mag_place_cplx_4[0]*1000))
      #print("mag_place_cplx_5 = %s"%(mag_place_cplx_5[0]*1000))  
      
      mag_place_cplx_2.append(complex(ryo-hyo-magnet_sleeve_tolerance-magnet_sleeve_height,-0.5*magnet_side_length-magnet_sleeve_tolerance))
      mag_place_cplx_7.append(complex(ryo-hyo-magnet_sleeve_tolerance-magnet_sleeve_height,-0.5*magnet_side_length-magnet_sleeve_tolerance-magnet_sleeve_height))
      #print("mag_place_cplx_2 = %s"%(mag_place_cplx_2[0]*1000))
      #print("mag_place_cplx_7 = %s"%(mag_place_cplx_7[0]*1000))
        
      mag_place_cplx_3.append(complex(ryo-hyo-magnet_sleeve_tolerance-magnet_sleeve_height,+0.5*magnet_side_length+magnet_sleeve_tolerance))
      mag_place_cplx_6.append(complex(ryo-hyo-magnet_sleeve_tolerance-magnet_sleeve_height,+0.5*magnet_side_length+magnet_sleeve_tolerance+magnet_sleeve_height))       
      #print("mag_place_cplx_3 = %s"%(mag_place_cplx_3[0]*1000))
      #print("mag_place_cplx_6 = %s"%(mag_place_cplx_6[0]*1000))

      #first initial small rotate
      #mag_place_cplx_center_i[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      #mag_place_cplx_center_o[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_place_cplx_1[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_place_cplx_2[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_place_cplx_3[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_place_cplx_4[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_place_cplx_5[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))    
      mag_place_cplx_6[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))   
      mag_place_cplx_7[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))   
      mag_place_cplx_8[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))

      #print("mag_cplx")
      #print(mag_cplx_1[0])
      #print(mag_cplx_2[0])
      #print(mag_cplx_3[0])
      #print(mag_cplx_4[0])
      #print(mag_cplx_5[0])
      
      #print("mag_place_cplx")
      #print(mag_place_cplx_1[0]*1000)
      #print(mag_place_cplx_2[0]*1000)
      #print(mag_place_cplx_3[0]*1000)
      #print(mag_place_cplx_4[0]*1000)
      #print(mag_place_cplx_5[0]*1000)
      #print(mag_place_cplx_6[0]*1000)
      #print(mag_place_cplx_7[0]*1000)
      #print(mag_place_cplx_8[0]*1000)
      
    if interior_magnets == 1:
      mag_sleeve_cplx_1 = []
      mag_sleeve_cplx_2 = []
      mag_sleeve_cplx_3 = []
      mag_sleeve_cplx_4 = []
      mag_sleeve_cplx_5 = []
      mag_sleeve_cplx_6 = []
      mag_sleeve_cplx_7 = []
      mag_sleeve_cplx_8 = []
      mag_sleeve_center_point_airgap = []
      
      mag_sleeve_cplx_1.append(complex(magnet_radially_inner_radius+hmr,-0.5*magnet_side_length-magnet_sleeve_tolerance))
      mag_sleeve_cplx_8.append(complex(magnet_radially_inner_radius+hmr,-0.5*magnet_side_length-magnet_sleeve_tolerance-magnet_sleeve_height))

      mag_sleeve_cplx_4.append(complex(magnet_radially_inner_radius+hmr,+0.5*magnet_side_length+magnet_sleeve_tolerance))
      mag_sleeve_cplx_5.append(complex(magnet_radially_inner_radius+hmr,+0.5*magnet_side_length+magnet_sleeve_tolerance+magnet_sleeve_height))    
      
      mag_sleeve_cplx_2.append(complex(magnet_radially_inner_radius-2*magnet_sleeve_tolerance,-0.5*magnet_side_length-magnet_sleeve_tolerance))
      mag_sleeve_cplx_7.append(complex(magnet_radially_inner_radius-2*magnet_sleeve_tolerance-magnet_sleeve_height,-0.5*magnet_side_length-magnet_sleeve_tolerance-magnet_sleeve_height))
      
      mag_sleeve_cplx_3.append(complex(magnet_radially_inner_radius-2*magnet_sleeve_tolerance,+0.5*magnet_side_length+magnet_sleeve_tolerance))
      mag_sleeve_cplx_6.append(complex(magnet_radially_inner_radius-2*magnet_sleeve_tolerance-magnet_sleeve_height,+0.5*magnet_side_length+magnet_sleeve_tolerance+magnet_sleeve_height))    

      mag_sleeve_center_point_airgap.append(complex(0.5*(mag_sleeve_cplx_6[0].real+mag_sleeve_cplx_7[0].real),0.5*(mag_sleeve_cplx_6[0].imag+mag_sleeve_cplx_7[0].imag)))

      #first initial small rotate
      mag_sleeve_cplx_1[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_sleeve_cplx_2[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_sleeve_cplx_3[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_sleeve_cplx_4[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_sleeve_cplx_5[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))    
      mag_sleeve_cplx_6[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))   
      mag_sleeve_cplx_7[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))   
      mag_sleeve_cplx_8[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
      mag_sleeve_center_point_airgap[0]*=np.exp(1j*(0.5*(km_gap+kmr)*radially_magnet_pitch))
    
    for i in range(0,4):
      sf.mline(mag_cplx_1[i].real,mag_cplx_1[i].imag,mag_cplx_2[i].real,mag_cplx_2[i].imag, arc_len)
      sf.mline(mag_cplx_2[i].real,mag_cplx_2[i].imag,mag_cplx_3[i].real,mag_cplx_3[i].imag, arc_len)
      sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_cplx_4[i].real,mag_cplx_4[i].imag, arc_len)
      sf.mline(mag_cplx_4[i].real,mag_cplx_4[i].imag,mag_cplx_1[i].real,mag_cplx_1[i].imag, arc_len)
      
      if magnet_place_holders == 1:
	delete_some_lines = 0
	
	mag_cplx_1.append(mag_cplx_1[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_cplx_2.append(mag_cplx_2[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_cplx_3.append(mag_cplx_3[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_cplx_4.append(mag_cplx_4[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_cplx_5.append(mag_cplx_5[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_beneath_cplx_1.append(mag_beneath_cplx_1[i]*np.exp(1j*(radially_magnet_pitch)))
	mag_beneath_cplx_2.append(mag_beneath_cplx_2[i]*np.exp(1j*(radially_magnet_pitch)))

	if magnet_place_holders == 1:
	  #mag_place_cplx_center_i.append(mag_place_cplx_center_i[i]*np.exp(1j*(radially_magnet_pitch)))
	  #mag_place_cplx_center_o.append(mag_place_cplx_center_o[i]*np.exp(1j*(radially_magnet_pitch)))  
	  mag_place_cplx_1.append(mag_place_cplx_1[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_2.append(mag_place_cplx_2[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_3.append(mag_place_cplx_3[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_4.append(mag_place_cplx_4[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_5.append(mag_place_cplx_5[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_6.append(mag_place_cplx_6[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_7.append(mag_place_cplx_7[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_place_cplx_8.append(mag_place_cplx_8[i]*np.exp(1j*(radially_magnet_pitch)))

	if interior_magnets == 1:
	  mag_sleeve_cplx_1.append(mag_sleeve_cplx_1[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_2.append(mag_sleeve_cplx_2[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_3.append(mag_sleeve_cplx_3[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_4.append(mag_sleeve_cplx_4[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_5.append(mag_sleeve_cplx_5[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_6.append(mag_sleeve_cplx_6[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_7.append(mag_sleeve_cplx_7[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_cplx_8.append(mag_sleeve_cplx_8[i]*np.exp(1j*(radially_magnet_pitch)))
	  mag_sleeve_center_point_airgap.append(mag_sleeve_center_point_airgap[i]*np.exp(1j*(radially_magnet_pitch)))	
	
	delte_some_lines_tolerance = 2e-03
	
	#print(np.arctan(mag_place_cplx_5[i].imag/mag_place_cplx_5[i].real))
	#print(np.arctan(mag_place_cplx_8[i+1].imag/mag_place_cplx_8[i+1].real))
	
	#print(np.arctan(mag_place_cplx_6[i].imag/mag_place_cplx_6[i].real))
	#print(np.arctan(mag_place_cplx_7[i+1].imag/mag_place_cplx_7[i+1].real))
	
	if np.arctan(mag_place_cplx_5[i].imag/mag_place_cplx_5[i].real)>np.arctan(mag_place_cplx_8[i+1].imag/mag_place_cplx_8[i+1].real) or np.isclose(np.arctan(mag_place_cplx_5[i].imag/mag_place_cplx_5[i].real), np.arctan(mag_place_cplx_8[i+1].imag/mag_place_cplx_8[i+1].real), rtol=delte_some_lines_tolerance, atol=delte_some_lines_tolerance, equal_nan=False):
	  #print "FIRST WARNING: Not enough space for magnet sleeves (pnts 5 & 8)"
	  delete_some_lines = 1	
	
	if np.arctan(mag_place_cplx_6[i].imag/mag_place_cplx_6[i].real)>np.arctan(mag_place_cplx_7[i+1].imag/mag_place_cplx_7[i+1].real) or np.isclose(np.arctan(mag_place_cplx_6[i].imag/mag_place_cplx_6[i].real), np.arctan(mag_place_cplx_7[i+1].imag/mag_place_cplx_7[i+1].real), rtol=delte_some_lines_tolerance, atol=delte_some_lines_tolerance, equal_nan=False):
	  #print "FIRST WARNING: Not enough space for magnet sleeves (pnts 6 & 7)"
	  delete_some_lines = 1
	  
    for i in range(0,4):
      if magnet_place_holders == 1:
	#print "uncomment this"
	#sf.mline(mag_place_cplx_center_i[i].real,mag_place_cplx_center_i[i].imag,mag_place_cplx_center_o[i].real,mag_place_cplx_center_o[i].imag, arc_len)
	
	sf.mline(mag_place_cplx_1[i].real,mag_place_cplx_1[i].imag,mag_place_cplx_2[i].real,mag_place_cplx_2[i].imag, arc_len)
	sf.mline(mag_place_cplx_3[i].real,mag_place_cplx_3[i].imag,mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag, arc_len)
	if delete_some_lines == 0:
	  sf.mline(mag_place_cplx_5[i].real,mag_place_cplx_5[i].imag,mag_place_cplx_6[i].real,mag_place_cplx_6[i].imag, arc_len)
	  sf.mline(mag_place_cplx_7[i].real,mag_place_cplx_7[i].imag,mag_place_cplx_8[i].real,mag_place_cplx_8[i].imag, arc_len)
	else:
	  if i!=3:
	    sf.oarc(np.angle(mag_place_cplx_3[i]),np.angle(mag_place_cplx_2[i+1]),abs(mag_place_cplx_3[i]),0.5*arc_len)
	  else: #draw lines which touch simulation boundary
	    if use_machine_periodicity == 1:
	      sf.oarc(np.angle(mag_place_cplx_2[0]),0,abs(mag_place_cplx_3[i]),0.5*arc_len)
	      sf.oarc(np.angle(mag_place_cplx_3[i]),(2*np.pi)/periodicity_in_one_full_rotation,abs(mag_place_cplx_3[i]),0.5*arc_len)
	    
	    #print("abs(mag_place_cplx_3[i]) = %f"%(abs(mag_place_cplx_3[i])*1000))
	    
	    #for u in range(0,len(sorted_rotor_radii)):
	      #print("sorted_rotor_radii[%d] = %f"%(u,sorted_rotor_radii[u]*1000))
	    
	    #replace closest existing number within sorted_rotor_radii
	    closest_radius_diff = abs(sorted_rotor_radii[0]-abs(mag_place_cplx_3[i]))
	    closest_radius_u = 0
	    for u in range(1,len(sorted_rotor_radii)):
	      if abs(sorted_rotor_radii[u]-abs(mag_place_cplx_3[i]))<closest_radius_diff:
		closest_radius_diff = abs(sorted_rotor_radii[u]-abs(mag_place_cplx_3[i]))
		closest_radius_u = u
	    
	    #sorted_rotor_radii.append(abs(mag_place_cplx_3[i]))
	    sorted_rotor_radii[closest_radius_u] = abs(mag_place_cplx_3[i])
	    #sorted_rotor_radii = sorted(sorted_rotor_radii)

	    #for u in range(0,len(sorted_rotor_radii)):
	      #print("sorted_rotor_radii[%d] = %f"%(u,sorted_rotor_radii[u]*1000))
	
	if delete_some_lines == 0:
	  sf.mline(mag_place_cplx_7[i].real,mag_place_cplx_7[i].imag,mag_place_cplx_2[i].real,mag_place_cplx_2[i].imag, arc_len)
	  sf.mline(mag_place_cplx_6[i].real,mag_place_cplx_6[i].imag,mag_place_cplx_3[i].real,mag_place_cplx_3[i].imag, arc_len)

	#line beneath magnet surface
	if magnet_sleeve_tolerance>0:
	  sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_beneath_cplx_2[i].real,mag_beneath_cplx_2[i].imag, arc_len)
      
      if interior_magnets == 1:
	sf.mline(mag_sleeve_cplx_1[i].real,mag_sleeve_cplx_1[i].imag,mag_sleeve_cplx_2[i].real,mag_sleeve_cplx_2[i].imag, arc_len)
	sf.mline(mag_sleeve_cplx_2[i].real,mag_sleeve_cplx_2[i].imag,mag_sleeve_cplx_3[i].real,mag_sleeve_cplx_3[i].imag, arc_len)
	sf.mline(mag_sleeve_cplx_3[i].real,mag_sleeve_cplx_3[i].imag,mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag, arc_len)
	if delete_some_lines == 0:
	  sf.mline(mag_sleeve_cplx_5[i].real,mag_sleeve_cplx_5[i].imag,mag_sleeve_cplx_6[i].real,mag_sleeve_cplx_6[i].imag, arc_len)  
	  sf.mline(mag_sleeve_cplx_7[i].real,mag_sleeve_cplx_7[i].imag,mag_sleeve_cplx_8[i].real,mag_sleeve_cplx_8[i].imag, arc_len)

	if delete_some_lines == 0:
	  sf.mline(mag_sleeve_cplx_6[i].real,mag_sleeve_cplx_6[i].imag,mag_sleeve_center_point_airgap[i].real,mag_sleeve_center_point_airgap[i].imag, arc_len)
	  sf.mline(mag_sleeve_center_point_airgap[i].real,mag_sleeve_center_point_airgap[i].imag,mag_sleeve_cplx_7[i].real,mag_sleeve_cplx_7[i].imag, arc_len)

	#line beneath magnet surface
	if magnet_sleeve_tolerance>0:
	  sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_beneath_cplx_2[i].real,mag_beneath_cplx_2[i].imag, arc_len)
      
      if replace_magnets_with_air == 0:
	if i%2 == 0:
	  if magnetisation_radial0_normal1 == 0:
	    sf.cmagnet(mag_cplx_5[i].real,mag_cplx_5[i].imag, "rt", Brem, 0, muR, mesh, 1, 0, 0)    
	  elif magnetisation_radial0_normal1 == 1:
	    sf.cmagnet(mag_cplx_5[i].real,mag_cplx_5[i].imag, "xy", Brem*(mag_cplx_5[i].real/abs(mag_cplx_5[i])), Brem*(mag_cplx_5[i].imag/abs(mag_cplx_5[i])), muR, mesh, 1, 0, 0)
	else:
	  if magnetisation_radial0_normal1 == 0:
	    sf.cmagnet(mag_cplx_5[i].real,mag_cplx_5[i].imag, "rt", -Brem, 0, muR, mesh, 0, 0, 1)
	  elif magnetisation_radial0_normal1 == 1:
	    sf.cmagnet(mag_cplx_5[i].real,mag_cplx_5[i].imag, "xy", -Brem*(mag_cplx_5[i].real/abs(mag_cplx_5[i])), -Brem*(mag_cplx_5[i].imag/abs(mag_cplx_5[i])), muR, mesh, 0, 0, 1)
      else:
	sf.cmagnet(mag_cplx_5[i].real,mag_cplx_5[i].imag, "rt", 0, 0, 1, mesh, 0, 0, 1)
    
      
      if i != 3:
	#print "dalla uncomment dit alles"
	if interior_magnets == 0 and magnet_place_holders == 0:
	  sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_cplx_4[i+1].real,mag_cplx_4[i+1].imag, arc_len)
	
	elif magnet_place_holders == 1:
	  if magnet_sleeve_tolerance>0:
	    sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag, arc_len)
	  else:
	    sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag, arc_len)
	  
	  #sf.mline(mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag,mag_place_cplx_5[i].real,mag_place_cplx_5[i].imag, arc_len)
	  #if np.arctan(mag_place_cplx_5[i].imag/mag_place_cplx_5[i].real)>np.arctan(mag_place_cplx_8[i+1].imag/mag_place_cplx_8[i+1].real):
	    #print "WARNING: Not enough space for magnet sleeves (pnts 5 & 8)"
	    #delete_some_lines = 1
	    
	  #if np.arctan(mag_place_cplx_6[i].imag/mag_place_cplx_6[i].real)>np.arctan(mag_place_cplx_7[i+1].imag/mag_place_cplx_7[i+1].real):
	    #print "WARNING: Not enough space for magnet sleeves (pnts 6 & 7)"
	    #delete_some_lines = 1
	  
	  if delete_some_lines == 0:
	    sf.mline(mag_place_cplx_5[i].real,mag_place_cplx_5[i].imag,mag_place_cplx_8[i+1].real,mag_place_cplx_8[i+1].imag, arc_len)
	  #sf.mline(mag_place_cplx_8[i+1].real,mag_place_cplx_8[i+1].imag,mag_place_cplx_1[i+1].real,mag_place_cplx_1[i+1].imag, arc_len)
	  if magnet_sleeve_tolerance>0:
	    sf.mline(mag_place_cplx_1[i+1].real,mag_place_cplx_1[i+1].imag,mag_beneath_cplx_2[i+1].real,mag_beneath_cplx_2[i+1].imag, arc_len)
	  else:
	    sf.mline(mag_place_cplx_1[i+1].real,mag_place_cplx_1[i+1].imag,mag_cplx_4[i+1].real,mag_cplx_4[i+1].imag, arc_len)
	    
	elif interior_magnets == 1:
	  if magnet_sleeve_tolerance>0:
	    sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag, arc_len)
	  else:
	    sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag, arc_len)
	  
	  #if delete_some_lines == 0:
	  sf.mline(mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag,mag_sleeve_cplx_5[i].real,mag_sleeve_cplx_5[i].imag, arc_len)
	  #if np.arctan(mag_sleeve_cplx_5[i].imag/mag_sleeve_cplx_5[i].real)>np.arctan(mag_sleeve_cplx_8[i+1].imag/mag_sleeve_cplx_8[i+1].real):
	    #print "WARNING: Not enough space for magnet sleeves (pnts 5 & 8)"
	    #delete_some_lines = 1
	    
	  #if np.arctan(mag_sleeve_cplx_6[i].imag/mag_sleeve_cplx_6[i].real)>np.arctan(mag_sleeve_cplx_7[i+1].imag/mag_sleeve_cplx_7[i+1].real):
	    #print "WARNING: Not enough space for magnet sleeves (pnts 6 & 7)"
	    #delete_some_lines = 1
	  
	  if delete_some_lines == 0:
	    sf.mline(mag_sleeve_cplx_5[i].real,mag_sleeve_cplx_5[i].imag,mag_sleeve_cplx_8[i+1].real,mag_sleeve_cplx_8[i+1].imag, arc_len)
	  sf.mline(mag_sleeve_cplx_8[i+1].real,mag_sleeve_cplx_8[i+1].imag,mag_sleeve_cplx_1[i+1].real,mag_sleeve_cplx_1[i+1].imag, arc_len)
	  if magnet_sleeve_tolerance>0:
	    sf.mline(mag_sleeve_cplx_1[i+1].real,mag_sleeve_cplx_1[i+1].imag,mag_beneath_cplx_2[i+1].real,mag_beneath_cplx_2[i+1].imag, arc_len)
	  else:
	    sf.mline(mag_sleeve_cplx_1[i+1].real,mag_sleeve_cplx_1[i+1].imag,mag_cplx_4[i+1].real,mag_cplx_4[i+1].imag, arc_len)
    
    if interior_magnets == 0 and magnet_place_holders == 0:
      boundary = complex(0.5*(mag_cplx_4[1].real+mag_cplx_3[0].real), 0.5*(mag_cplx_4[1].imag+mag_cplx_3[0].imag))
      
      #if delete_some_lines == 0:
      sf.mline(mag_cplx_4[0].real,mag_cplx_4[0].imag,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).real,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).imag, arc_len)
      sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,(boundary*np.exp(1j*(3*radially_magnet_pitch))).real,(boundary*np.exp(1j*(3*radially_magnet_pitch))).imag, arc_len)    

    elif magnet_place_holders == 1:
      boundary = complex(0.5*(mag_place_cplx_8[1].real+mag_place_cplx_5[0].real), 0.5*(mag_place_cplx_8[1].imag+mag_place_cplx_5[0].imag))
      #print "UNCOMMENT"
      if delete_some_lines == 0:
	sf.mline(mag_place_cplx_8[0].real,mag_place_cplx_8[0].imag,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).real,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).imag, arc_len)
      ###sf.mline(mag_place_cplx_1[0].real,mag_place_cplx_1[0].imag,mag_place_cplx_8[0].real,mag_place_cplx_8[0].imag, arc_len)
      if magnet_sleeve_tolerance>0:
	sf.mline(mag_beneath_cplx_2[0].real,mag_beneath_cplx_2[0].imag,mag_place_cplx_1[0].real,mag_place_cplx_1[0].imag, arc_len)
      else:  
	sf.mline(mag_cplx_4[0].real,mag_cplx_4[0].imag,mag_place_cplx_1[0].real,mag_place_cplx_1[0].imag, arc_len)
      if delete_some_lines == 0:
	sf.mline(mag_place_cplx_5[i].real,mag_place_cplx_5[i].imag,(boundary*np.exp(1j*(3*radially_magnet_pitch))).real,(boundary*np.exp(1j*(3*radially_magnet_pitch))).imag, arc_len)
      ###sf.mline(mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag,mag_place_cplx_5[i].real,mag_place_cplx_5[i].imag, arc_len)
      if magnet_sleeve_tolerance>0:
	sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag, arc_len)
      else:  
	sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_place_cplx_4[i].real,mag_place_cplx_4[i].imag, arc_len)
    
    elif interior_magnets == 1:
      boundary = complex(0.5*(mag_sleeve_cplx_8[1].real+mag_sleeve_cplx_5[0].real), 0.5*(mag_sleeve_cplx_8[1].imag+mag_sleeve_cplx_5[0].imag))
    
      sf.mline(mag_sleeve_cplx_8[0].real,mag_sleeve_cplx_8[0].imag,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).real,(boundary*np.exp(1j*(-1*radially_magnet_pitch))).imag, arc_len)
      sf.mline(mag_sleeve_cplx_1[0].real,mag_sleeve_cplx_1[0].imag,mag_sleeve_cplx_8[0].real,mag_sleeve_cplx_8[0].imag, arc_len)
      if magnet_sleeve_tolerance>0:
	sf.mline(mag_beneath_cplx_2[0].real,mag_beneath_cplx_2[0].imag,mag_sleeve_cplx_1[0].real,mag_sleeve_cplx_1[0].imag, arc_len)
      else:  
	sf.mline(mag_cplx_4[0].real,mag_cplx_4[0].imag,mag_sleeve_cplx_1[0].real,mag_sleeve_cplx_1[0].imag, arc_len)
      
      sf.mline(mag_sleeve_cplx_5[i].real,mag_sleeve_cplx_5[i].imag,(boundary*np.exp(1j*(3*radially_magnet_pitch))).real,(boundary*np.exp(1j*(3*radially_magnet_pitch))).imag, arc_len)
      sf.mline(mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag,mag_sleeve_cplx_5[i].real,mag_sleeve_cplx_5[i].imag, arc_len)
      if magnet_sleeve_tolerance>0:
	sf.mline(mag_beneath_cplx_1[i].real,mag_beneath_cplx_1[i].imag,mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag, arc_len)
      else:  
	sf.mline(mag_cplx_3[i].real,mag_cplx_3[i].imag,mag_sleeve_cplx_4[i].real,mag_sleeve_cplx_4[i].imag, arc_len)
    
    
  if use_machine_periodicity == 1:
    #sf.rotate(0, 0, 0.5*km_gap*radially_magnet_pitch)
    if square_magnets == 0:
      sf.nrotate(0, 0, radially_magnet_pitch, int(magnet_ns_total/periodicity_in_one_full_rotation-1))
    #elif square_magnets == 1:
      #sf.rotate(0, 0, (0.5*km_gap + 0.5*kmr)*radially_magnet_pitch)
      ###sf.rotate(0, 0, radially_magnet_pitch)
      #sf.nrotate(0, 0, radially_magnet_pitch, int(magnet_ns_total/periodicity_in_one_full_rotation-1))
      
    angle_temp0 = (2*np.pi)/periodicity_in_one_full_rotation
  else:
    sf.nrotate(0, 0, 2*radially_magnet_pitch, int(magnet_ns_total/2))
    
  sf.end_part()

if use_machine_periodicity == 1:
  loop_limit = int(magnet_ns_total/periodicity_in_one_full_rotation)
else:
  loop_limit = int(magnet_ns_total)

if square_magnets == 0:
  #give magnet orientation radially (only if magnet area is big enough, otherwise magnet area becomes unassigned as air)
  magnet_centre_point_r = 0.5*(magnet_radially_inner_radius+magnet_radially_outer_radius)
  if use_nonairgap_layers == 1:
    magnet_centre_point_r_inside = 0.5*(magnet_radially_inner_radius+arc_rotor_magnet_layer_split_radius)
    magnet_centre_point_r_outside = 0.5*(arc_rotor_magnet_layer_split_radius+magnet_radially_outer_radius)

  if kmr*radially_magnet_pitch*magnet_centre_point_r>=arc_len:
    angle = km_gap*0.5*radially_magnet_pitch #EDITED
    orientation = 1

    for i in range (loop_limit): #orientate inner magnets in Halbach formation
      if orientation == 1:	#radially outward direction (red)
	if replace_magnets_with_iron == 1:
	  if use_nonairgap_layers == 0:
	    sf.pregion(magnet_centre_point_r, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh) #pm magnet replaced with iron
	  elif use_nonairgap_layers == 1:
	    sf.pregion(magnet_centre_point_r_inside, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh) #pm magnet replaced with iron
	    sf.pregion(magnet_centre_point_r_outside, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh_rotor_layer) #pm magnet replaced with iron
	else:
	  if use_nonairgap_layers == 0:
	    sf.cpmagnet(magnet_centre_point_r, angle + 0.5*kmr*radially_magnet_pitch, "rt", Brem, 0, muR, mesh, 1, 0, 0)
	  elif use_nonairgap_layers == 1:
	    sf.cpmagnet(magnet_centre_point_r_inside, angle + 0.5*kmr*radially_magnet_pitch, "rt", Brem, 0, muR, mesh, 1, 0, 0)
	    sf.cpmagnet(magnet_centre_point_r_outside, angle + 0.5*kmr*radially_magnet_pitch, "rt", Brem, 0, muR, mesh_rotor_layer, 1, 0, 0)
	    
      
      else:			#radially inward direction (blue)
	if replace_magnets_with_iron == 1:
	  if use_nonairgap_layers == 0:
	    sf.pregion(magnet_centre_point_r, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh) #pm magnet replaced with iron
	  elif use_nonairgap_layers == 1:
	    sf.pregion(magnet_centre_point_r_inside, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh) #pm magnet replaced with iron
	    sf.pregion(magnet_centre_point_r_outside, angle + 0.5*kmr*radially_magnet_pitch,magnet_radially_iron,mesh_rotor_layer) #pm magnet replaced with iron
	else:
	  if use_nonairgap_layers == 0:
	    sf.cpmagnet(magnet_centre_point_r, angle + 0.5*kmr*radially_magnet_pitch, "rt", -Brem, 0, muR, mesh, 0, 0, 1)
	  elif use_nonairgap_layers == 1:
	    sf.cpmagnet(magnet_centre_point_r_inside, angle + 0.5*kmr*radially_magnet_pitch, "rt", -Brem, 0, muR, mesh, 0, 0, 1)
	    sf.cpmagnet(magnet_centre_point_r_outside, angle + 0.5*kmr*radially_magnet_pitch, "rt", -Brem, 0, muR, mesh_rotor_layer, 0, 0, 1)
      
      angle = angle + radially_magnet_pitch
      orientation = orientation + 1 

      if orientation > 2:
	orientation = 1

  #give magnet orientation tangentially
  magnet_centre_point_t = 0.5*(magnet_tangentially_inner_radius+magnet_tangentially_outer_radius)
  if use_nonairgap_layers == 1:
    magnet_centre_point_t_inside = 0.5*(magnet_tangentially_inner_radius+arc_rotor_magnet_layer_split_radius)
    magnet_centre_point_t_outside = 0.5*(arc_rotor_magnet_layer_split_radius+magnet_tangentially_outer_radius)

  diff=magnet_tangentially_outer_radius-magnet_tangentially_inner_radius
  angle = angle1_save
  diff2=(angle2_save-angle1_save)*magnet_centre_point_t
  diff3=magnet_tangentially_inner_radius-magnet_radially_inner_radius
  remaining_air_centre_point_t = 0.5*(magnet_radially_inner_radius+magnet_tangentially_inner_radius)

  orientation = 1

  for i in range (loop_limit): #orientate inner magnets in Halbach formation
    if diff>=arc_len and diff2>=arc_len:
      if orientation == 2:	#tangential clockwise direction (yellow)
      #if orientation == 1:	#tangential anti-clockwise direction (light blue)
	if use_nonairgap_layers == 0:
	  sf.cpmagnet(magnet_centre_point_t, angle + 0.5*(angle2_save-angle1_save), "rt", 0, -Brem, muR, mesh, 0, 1, 1)
	elif use_nonairgap_layers == 1:
	  sf.cpmagnet(magnet_centre_point_t_inside, angle + 0.5*(angle2_save-angle1_save), "rt", 0, -Brem, muR, mesh, 0, 1, 1)
	  sf.cpmagnet(magnet_centre_point_t_outside, angle + 0.5*(angle2_save-angle1_save), "rt", 0, -Brem, muR, mesh_rotor_layer, 0, 1, 1)

      if orientation == 1:	#tangential anti-clockwise direction (light blue)
      #if orientation == 2:	#tangential clockwise direction (yellow)
	if use_nonairgap_layers == 0:
	  sf.cpmagnet(magnet_centre_point_t, angle + 0.5*(angle2_save-angle1_save), "rt", 0, Brem, muR, mesh, 1, 1, 0)
	elif use_nonairgap_layers == 1:
	  sf.cpmagnet(magnet_centre_point_t_inside, angle + 0.5*(angle2_save-angle1_save), "rt", 0, Brem, muR, mesh, 1, 1, 0)
	  sf.cpmagnet(magnet_centre_point_t_outside, angle + 0.5*(angle2_save-angle1_save), "rt", 0, Brem, muR, mesh_rotor_layer, 1, 1, 0)
    
    if not np.isclose(kmt, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      if diff3>=arc_len:
	if use_nonairgap_layers == 0:
	  sf.pregion(remaining_air_centre_point_t, angle + 0.5*(angle2_save-angle1_save),0,mesh*speed_up_air_regions)
	elif use_nonairgap_layers == 1:
	  sf.pregion(remaining_air_centre_point_t_inside, angle + 0.5*(angle2_save-angle1_save),0,mesh*speed_up_air_regions)
	  sf.pregion(remaining_air_centre_point_t_inside, angle + 0.5*(angle2_save-angle1_save),0,mesh_rotor_layer*speed_up_air_regions)
	  
      #assign remaining gap as air
      #sf.pregion(0.5*(magnet_tangentially_inner_radius+magnet_radially_inner_radius),0.5*(angle2_save-angle1_save),0,mesh) #air gap
    
    angle = angle + radially_magnet_pitch
    orientation = orientation + 1 

    if orientation > 2:
      orientation = 1



#print(0.5*(magnet_tangentially_inner_radius+magnet_radially_inner_radius))


#give stator coil phases
winding = zeros((int(Q),2))

#Error: Invalid winding (q_ph not an integer)
#phase_A_offset = sf.assign_winding2(3, int(poles/2), int(Q), N_L, N_S, winding)			#This gives the correct answer as it correlates with fpl viewer's theta (t) reading
# I took the above line out because it is anyways UNECCESARY because I'm assigning manual value to phase_A_offset

#declare coil sides and specify which windings they belong to
if use_machine_periodicity == 1 and coil_shape == SQUARE_SLOTS:
  present_phase = 4 #also edited this to be the same sequnce as RADIAL slots and also to be identical with Maxwell simulation
else:
  present_phase = 4 #EDITED

if coil_shape == RADIALLY_STRAIGHT:
  angle = 0.5*ks_pitch+0.25*kc_pitch #EDITED
elif coil_shape == SQUARE_SLOTS and flare_teeth == 0:
  if use_machine_periodicity == 0:
    angle = center_of_side_t
  elif use_machine_periodicity == 1:
    #angle = -center_of_side_t #used to be like this until 2016/11/05
    angle = center_of_side_t-0.5*coil_pitch +ks_pitch + kc_pitch  -2*center_of_side_t
    
#angle_temp1 = ks_pitch + 0.5*kc_pitch
angle_temp1 = 0.5*kc_pitch

if use_machine_periodicity == 1:
  #loop_limit = int((q/kq)*2)-2
  loop_limit = 6 #used this equation most recently
  #loop_limit = slots/periodicity_in_one_full_rotation
else:
  loop_limit = int(Q*2) #used this equation most recently
  #loop_limit = slots

for i in range (loop_limit):  
  if(present_phase > 5):
    present_phase = 0
  
  #if i > 10 and i < 17:
    #if use_nonairgap_layers == 0:
      #sf.pregion(0.5*(stator_yoke_outer_radius+stator_coil_radius), angle, -phase[present_phase], mesh)		#coils has same permeability as air
    #elif use_nonairgap_layers == 1:
      #sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_layer_split_radius), angle, -phase[present_phase], mesh_stator_layer)	#coils has same permeability as air
      #sf.pregion(0.5*(arc_stator_layer_split_radius+stator_coil_radius), angle, -phase[present_phase], mesh)				#coils has same permeability as air    
  #else:  
  if use_nonairgap_layers == 0:
    if coil_shape == RADIALLY_STRAIGHT:
      sf.pregion(0.5*(stator_yoke_outer_radius+stator_coil_radius), angle, phase[present_phase], mesh*speed_up_air_regions)		#coils has same permeability as air
    elif coil_shape == SQUARE_SLOTS and flare_teeth == 0:
      sf.pregion(center_of_side_r, angle, phase[present_phase], mesh*speed_up_air_regions) #coils has same permeability as air
      
  elif use_nonairgap_layers == 1:
    if coil_shape == RADIALLY_STRAIGHT:
      sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_layer_split_radius), angle, phase[present_phase], mesh_stator_layer*speed_up_air_regions)		#coils has same permeability as air
      sf.pregion(0.5*(arc_stator_layer_split_radius+stator_coil_radius), angle, phase[present_phase], mesh*speed_up_air_regions)				#coils has same permeability as air
  
  if coil_shape == RADIALLY_STRAIGHT:  
    if(i%2 == 0):
      angle = angle + angle_temp1
    else:
      angle = angle + ks_pitch+0.5*kc_pitch
    present_phase = present_phase + 1  
     
  elif coil_shape == SQUARE_SLOTS and flare_teeth == 0:
    if(i%2 == 0):
      if use_machine_periodicity == 0:
	angle = angle + ks_pitch + kc_pitch  -2*center_of_side_t
      elif use_machine_periodicity == 1:
	angle = angle + 2*center_of_side_t
	#angle = angle + ks_pitch + kc_pitch  -2*center_of_side_t
    else:
      if use_machine_periodicity == 0:
	angle = angle + 2*center_of_side_t
      elif use_machine_periodicity == 1:
	angle = angle + ks_pitch + kc_pitch  -2*center_of_side_t
	#angle = angle + 2*center_of_side_t
	
    #angle = angle + ks_pitch+kc_pitch
    present_phase = present_phase + 1

if coil_shape == SQUARE_SLOTS and flare_teeth == 1:
  #print "arrived"
  
  for i in range(0,loop_limit):
    if(present_phase > 5):
      present_phase = 0
    
    k=0
    for j in range(0,number_of_copper_bars_per_coil):
      center_coil_tmp = 0.5*(coil_side_bot_top_edges_cplx_pnt[i][k] + coil_side_bot_top_edges_cplx_pnt[i][k+1])
      #print("i = %2d, k = %2d: abs(center_coil_tmp) = %f,  angle(center_coil_tmp) = %f"%(i,k,abs(center_coil_tmp),angle(center_coil_tmp)*180/np.pi))
      sf.pregion(abs(center_coil_tmp), angle(center_coil_tmp), phase[present_phase], mesh*speed_up_air_regions)		#coils has same permeability as air
      k = k + 2
    present_phase = present_phase + 1  


  if not np.isclose(half_slot_width, stator_shoe_width, rtol=9e-05, atol=9e-05, equal_nan=False):
    angle_gip = 0.0001 #intentional offset
    for i in range(0,int(Q/periodicity_in_one_full_rotation)):
      sf.pregion(stator_coil_radius-0.5*(shoe_weakpoint_thickness+stator_shoe_height), angle_gip, 0, mesh*speed_up_air_regions) #define slot opening mesh density
      angle_gip = angle_gip + coil_pitch
    sf.pregion(stator_coil_radius-0.5*(shoe_weakpoint_thickness+stator_shoe_height), ((Q/periodicity_in_one_full_rotation)*coil_pitch)-0.0001, 0, mesh*speed_up_air_regions)

if use_machine_periodicity == 1:
  pregion_angle = 0.5*((2*np.pi)/periodicity_in_one_full_rotation)
else:
  pregion_angle = 0

if stator_yoke_iron_N_al == 1:
  if use_nonairgap_layers == 0:
    sf.pregion(0.5*(stator_yoke_inner_radius+stator_yoke_outer_radius),pregion_angle,stator_iron,mesh) #stator iron yoke
  elif use_nonairgap_layers == 1:
    if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:
      sf.pregion(0.5*(stator_yoke_inner_radius+arc_stator_yoke_layer_split_radius),pregion_angle,stator_iron,mesh_yoke_layer) #stator iron yoke
      #if dont_place_stator_yoke_outer_region_point == 0:
      sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_yoke_layer_split_radius),pregion_angle,stator_iron,mesh_stator_layer) #stator iron yoke
    else:
      sf.pregion(0.5*(stator_yoke_inner_radius+stator_yoke_outer_radius),pregion_angle,stator_iron,mesh) #stator iron yoke
else:
  if use_nonairgap_layers == 0:
    sf.pregion(0.5*(stator_yoke_inner_radius+stator_yoke_outer_radius),pregion_angle,0,mesh*speed_up_air_regions) #stator alluminium (air-like) yoke
  elif use_nonairgap_layers == 1:
    if arc_stator_yoke_layer_split_radius>stator_yoke_inner_radius and arc_stator_yoke_layer_split_radius<stator_yoke_outer_radius:
      sf.pregion(0.5*(stator_yoke_inner_radius+arc_stator_yoke_layer_split_radius),pregion_angle,0,mesh_yoke_layer*speed_up_air_regions) #stator alluminium (air-like) yoke
      #if dont_place_stator_yoke_outer_region_point == 0:
      sf.pregion(0.5*(stator_yoke_outer_radius+arc_stator_yoke_layer_split_radius),pregion_angle,0,mesh_stator_layer*speed_up_air_regions) #stator alluminium (air-like) yoke
    else:
      sf.pregion(0.5*(stator_yoke_inner_radius+stator_yoke_outer_radius),pregion_angle,0,mesh*speed_up_air_regions) #stator alluminium (air-like) yoke

if square_magnets == 0:
  angle1 = 0.5*kmr*radially_magnet_pitch 					#place pregion behind middle of radially magnetised PM
  if np.isclose(angle1, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
    angle2 = 2*angle1 + 0.5*(km_gap*0.5*radially_magnet_pitch)		#place pregion behind middle of the first km_gap
    angle1 = angle2
  if np.isclose(angle2, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
    angle2 = angle1 + 0.5*(km_gap*0.5*radially_magnet_pitch) + 0.5*(kmt*radially_magnet_pitch)	#place pregion behind middle of tangentially magnetised PM
    angle1 = angle2

  if rotor_yoke_iron_N_al == 1:
    if use_nonairgap_layers == 0:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,angle1,rotor_iron,mesh) #rotor iron yoke
    elif use_nonairgap_layers == 1:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,angle1,rotor_iron,mesh_yoke_layer) #rotor iron yoke
  else:
    if use_nonairgap_layers == 0:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,angle1,0,mesh*speed_up_air_regions) #rotor alluminium (air-like) yoke
    elif use_nonairgap_layers == 1:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,angle1,0,mesh_yoke_layer*speed_up_air_regions) #rotor alluminium (air-like) yoke
elif square_magnets == 1:
  ang = (0.5*(2*np.pi))/periodicity_in_one_full_rotation
  if rotor_yoke_iron_N_al == 1:
    if use_nonairgap_layers == 0:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,ang,rotor_iron,mesh) #rotor iron yoke
    elif use_nonairgap_layers == 1:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,ang,rotor_iron,mesh_yoke_layer) #rotor iron yoke
  else:
    if use_nonairgap_layers == 0:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,ang,0,mesh*speed_up_air_regions) #rotor alluminium (air-like) yoke
    elif use_nonairgap_layers == 1:
      sf.pregion(magnet_radially_outer_radius+0.95*hyo,ang,0,mesh_yoke_layer*speed_up_air_regions) #rotor alluminium (air-like) yoke  
  
  if interior_magnets == 1: #assign iron material to magnet sleeves
    temp_r = abs(mag_sleeve_center_point_airgap[0])+0.5*magnet_sleeve_height
    for i in range(0,len(mag_sleeve_center_point_airgap)):
      sf.pregion(temp_r,np.arctan(mag_sleeve_center_point_airgap[i].imag/mag_sleeve_center_point_airgap[i].real),rotor_iron,mesh) #rotor iron yoke
  
## Multilayer air-gaps
if use_airgap_layers == 1:
  airgap_radii = []
  if interior_magnets == 1:
    if square_magnets:
      inc_size = (magnet_radially_inner_radius-stator_coil_radius-magnet_sleeve_height-2*magnet_sleeve_tolerance)/number_of_layers_airgap
    else:
      inc_size = g/number_of_layers_airgap
  else:
    inc_size = (magnet_radially_inner_radius-stator_coil_radius-magnet_sleeve_tolerance)/number_of_layers_airgap
  #print(inc_size)
  #print(number_of_layers_airgap)
  if interior_magnets == 0:
    for i in range(1,number_of_layers_airgap):
      airgap_radii.append(stator_coil_radius+i*inc_size)
  elif interior_magnets == 1:
    for i in range(1,number_of_layers_airgap+1):
      airgap_radii.append(stator_coil_radius+i*inc_size)
  
  if enable_new_airgap_arclengths:
    step_angle = (mechRotation/2)/steps
    airgap_outer_arc_len = airgap_radii[-1]*step_angle
    airgap_inner_arc_len = airgap_radii[0]*step_angle
    
    while airgap_inner_arc_len > 0.5*arc_len:
      airgap_inner_arc_len = 0.5*airgap_inner_arc_len
      airgap_outer_arc_len = 0.5*airgap_outer_arc_len
  else:
    airgap_outer_arc_len = 0.5*arc_len
    airgap_inner_arc_len = 0.5*arc_len
  
  if coil_shape == RADIALLY_STRAIGHT:
    for i in range(0,len(airgap_radii)):
      if use_machine_periodicity == 1:
	sf.oarc(0,mechRotation,airgap_radii[i],0.5*arc_len)
      else:
	sf.oarc(0,2*np.pi,airgap_radii[i],0.5*arc_len)
  elif coil_shape == SQUARE_SLOTS:
    if use_machine_periodicity == 1:
      if interior_magnets == 0:
	if enable_new_airgap_arclengths:
	  sf.oarc(0,mechRotation,airgap_radii[0],airgap_inner_arc_len)
	  arc_offset = 0.7*(airgap_inner_arc_len/airgap_radii[0])
	  
	  sf.oarc(0,arc_offset,airgap_radii[-1],airgap_outer_arc_len)
	  sf.oarc(arc_offset,mechRotation-arc_offset,airgap_radii[-1],airgap_outer_arc_len)
	  sf.oarc(mechRotation-arc_offset,mechRotation,airgap_radii[-1],airgap_outer_arc_len)
	else:
	  sf.oarc(0,mechRotation,airgap_radii[0],airgap_inner_arc_len)
	  sf.oarc(0,mechRotation,airgap_radii[-1],airgap_outer_arc_len)
      elif interior_magnets == 1:
	sf.oarc(0,mechRotation,airgap_radii[0],airgap_inner_arc_len)
	sf.oarc(0,mechRotation,airgap_radii[-2],0.5*arc_len)
	sf.oarc(0,mechRotation,airgap_radii[-1],airgap_outer_arc_len)
	sf.pregion(0.5*(airgap_radii[-1]+(magnet_radially_inner_radius-magnet_sleeve_tolerance)), 0.5*mechRotation,rotor_iron, mesh)		#interior magnet sleeves
	#sf.oarc(0,np.arctan(mag_sleeve_center_point_airgap[i].imag/mag_sleeve_center_point_airgap[i].real),airgap_radii[-1],0.5*arc_len)
	#for i in range(0,len(mag_sleeve_center_point_airgap)-1):
	  #sf.oarc(np.arctan(mag_sleeve_center_point_airgap[i].imag/mag_sleeve_center_point_airgap[i].real),np.arctan(mag_sleeve_center_point_airgap[i+1].imag/mag_sleeve_center_point_airgap[i+1].real),airgap_radii[-1],0.5*arc_len)
	#sf.oarc(np.arctan(mag_sleeve_center_point_airgap[i].imag/mag_sleeve_center_point_airgap[i].real),mechRotation,airgap_radii[-1],0.5*arc_len)
    
    if use_machine_periodicity == 0:
      for i in range(0,len(airgap_radii)):
	sf.oarc(0,2*np.pi,airgap_radii[i],0.5*arc_len)    
  
  if use_machine_periodicity == 1 and number_of_layers_airgap>=2:
    if coil_shape == RADIALLY_STRAIGHT:
      sf.pline(stator_coil_radius,0,airgap_radii[0],0)
      sf.pline(stator_coil_radius,mechRotation,airgap_radii[0],mechRotation)
      sf.pline(airgap_radii[-1],0,magnet_radially_inner_radius,0)
      sf.pline(airgap_radii[-1],mechRotation,magnet_radially_inner_radius,mechRotation)
    
      if number_of_layers_airgap>2:
	for j in range(1,number_of_layers_airgap-2):
	  sf.pline(airgap_radii[j-1],0,airgap_radii[j],0)
	  sf.pline(airgap_radii[j-1],mechRotation,airgap_radii[j],mechRotation)
    elif coil_shape == SQUARE_SLOTS:
      if interior_magnets == 0:
	sf.pline(stator_coil_radius,0,airgap_radii[0],0)
	sf.pline(stator_coil_radius,mechRotation,airgap_radii[0],mechRotation)     
	sf.pline(airgap_radii[-1],0,magnet_radially_inner_radius,0)
	sf.pline(airgap_radii[-1],mechRotation,magnet_radially_inner_radius,mechRotation)  
      elif interior_magnets == 1:
	sf.pline(stator_coil_radius,0,airgap_radii[0],0)
	sf.pline(stator_coil_radius,mechRotation,airgap_radii[0],mechRotation)  
	#sf.pline(airgap_radii[0],0,airgap_radii[1],0)
	#sf.pline(airgap_radii[0],mechRotation,airgap_radii[1],mechRotation)  	
	sf.pline(airgap_radii[1],0,airgap_radii[2],0)
	sf.pline(airgap_radii[1],mechRotation,airgap_radii[2],mechRotation)  
	sf.pline(airgap_radii[2],0,magnet_radially_inner_radius,0)
	sf.pline(airgap_radii[2],mechRotation,magnet_radially_inner_radius,mechRotation)  	
      
    if square_magnets == 1:
      sf.pregion(0.5*(airgap_radii[-1]+magnet_radially_inner_radius),ang,0,mesh*speed_up_air_regions) #rotor alluminium (air-like) yoke
 
  #sf.pmline(stator_yoke_inner_radius,0-coil_offset,stator_coil_radius,0-coil_offset,arc_len)
  #sf.pmline(stator_yoke_inner_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,stator_coil_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,arc_len)  
  
  if use_machine_periodicity == 1:
    whole1 = max(int((stator_yoke_outer_radius-stator_yoke_inner_radius)/arc_len),1)
    #whole2 = max(int((stator_coil_radius-stator_yoke_outer_radius)/arc_len),1) #should be commented out

    if flare_teeth == 1:
      whole2 = max(int(((x_shoe-shoe_tip_height)-stator_yoke_outer_radius)/arc_len),1)    
      whole3 = max(int((stator_coil_radius-(x_shoe-shoe_tip_height))/arc_len),1)
      
    if coil_shape == SQUARE_SLOTS:
      sf.pnline(stator_yoke_inner_radius,0-coil_offset,stator_yoke_outer_radius,0-coil_offset,whole1)
      #sf.pnline(stator_yoke_outer_radius,0-coil_offset,stator_coil_radius,0-coil_offset,whole2) #should be commented out

      if flare_teeth == 1:
	sf.pnline(stator_yoke_outer_radius,0-coil_offset,(x_shoe-shoe_tip_height),0-coil_offset,whole2)
	sf.pnline((x_shoe-shoe_tip_height),0-coil_offset,stator_coil_radius,0-coil_offset,whole3)
     
      sf.pnline(stator_yoke_inner_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,stator_yoke_outer_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,whole1)
      #sf.pnline(stator_yoke_outer_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,stator_coil_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,whole2) #should be commented out

      if flare_teeth == 1:
	sf.pnline(stator_yoke_outer_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,(x_shoe-shoe_tip_height),((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,whole2)
	sf.pnline((x_shoe-shoe_tip_height),((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,stator_coil_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,whole3)
      
    elif coil_shape == RADIALLY_STRAIGHT:
      sf.pnline(stator_yoke_inner_radius,0-coil_offset,stator_yoke_outer_radius,0-coil_offset,whole1)
      sf.pnline(stator_yoke_inner_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,stator_yoke_outer_radius,((2*np.pi)/periodicity_in_one_full_rotation)-coil_offset,whole2) 
  
#sf.oarc(0, 2*mechRotation, innerMagnetRadiusB + innerGap/3, ((innerMagnetRadiusB + innerGap/3)*mechRotation)/(steps-1)) #this line is copied from idrfpm


if use_machine_periodicity == 1 and square_magnets == 1 and draw_magnets:
  if delete_some_lines == 0:
    whole1 = max(int((abs(boundary)-sorted_rotor_radii[0])/arc_len),1)
    whole2 = max(int((sorted_rotor_radii[len(sorted_rotor_radii)-1]-abs(boundary))/arc_len),1)  

    sf.pnline(sorted_rotor_radii[0],0,abs(boundary),0,whole1)
    sf.pnline(abs(boundary),0,sorted_rotor_radii[len(sorted_rotor_radii)-1],0,whole2)
    
    sf.pnline(sorted_rotor_radii[0],(2*np.pi)/periodicity_in_one_full_rotation,abs(boundary),(2*np.pi)/periodicity_in_one_full_rotation,whole1)
    sf.pnline(abs(boundary),(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[len(sorted_rotor_radii)-1],(2*np.pi)/periodicity_in_one_full_rotation,whole2)
  else:
    #whole1 = max(int((sorted_rotor_radii[-1]-sorted_rotor_radii[0])/arc_len),1)

    whole1 = max(int((sorted_rotor_radii[closest_radius_u]-sorted_rotor_radii[0])/arc_len),1)
    whole2 = max(int((sorted_rotor_radii[-1]-sorted_rotor_radii[closest_radius_u])/arc_len),1) 

    sf.pnline(sorted_rotor_radii[0],0,sorted_rotor_radii[closest_radius_u],0,whole1)
    sf.pnline(sorted_rotor_radii[closest_radius_u],0,sorted_rotor_radii[-1],0,whole2)
    
    sf.pnline(sorted_rotor_radii[0],(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[closest_radius_u],(2*np.pi)/periodicity_in_one_full_rotation,whole1)
    sf.pnline(sorted_rotor_radii[closest_radius_u],(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[-1],(2*np.pi)/periodicity_in_one_full_rotation,whole2)
    
    #sf.pnline(sorted_rotor_radii[0],0,sorted_rotor_radii[-1],0,whole1)
    #sf.pnline(sorted_rotor_radii[0],(2*np.pi)/periodicity_in_one_full_rotation,sorted_rotor_radii[-1],(2*np.pi)/periodicity_in_one_full_rotation,whole1)   

#air yoke regions
#inline = 0.9*stator_yoke_inner_radius
#outline = 1.1*(ryo)
inline = stator_yoke_inner_radius-0.0025
outline= ryo+0.0025

if use_machine_periodicity == 0:
  print("Outside air-like mesh removed")
  #sf.oarc(0,2*np.pi,inline,arc_len)
  #outline= ryo+0.001
  outline=0.136 #this value is chosen so that all pdfs have the same outer dimensions
  sf.oarc(0,2*np.pi,outline,arc_len)  
else:
  sf.oarc(0,mechRotation,inline,arc_len)
  sf.oarc(0,mechRotation,outline,arc_len)
  sf.pmline(inline,0,stator_yoke_inner_radius,0,arc_len)
  sf.pmline(inline,mechRotation,stator_yoke_inner_radius,mechRotation,arc_len)  
  sf.pmline(ryo,0,outline,0,arc_len)
  sf.pmline(ryo,mechRotation,outline,mechRotation,arc_len)

if use_machine_periodicity == 0:  
  sf.hole(stator_coil_radius+0.5*g,0) #airgap  
  sf.hole(0,0)
  
#else:
  #r=stator_coil_radius+0.5*g
  #x=r*np.cos(pregion_angle)
  #y=r*np.sin(pregion_angle)
  #sf.hole(x,y) #airgap  

#if use_machine_periodicity == 0 and save_fpl2pdf == 1:
  #svg_file = 'project_sorspm_semfem/drawing'
  ##os.system("fpl2svg %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  #os.system("inkscape -A %s %s"%(svg_file+"_plain"+".pdf",svg_file+".svg"))
  
#if exit_before_solving == 1:  
  #if show_fpl_before_band_solver == 1:
    #os.system("fpl2 project_sorspm_semfem/drawing.poly")
  #conn.close()
  #sys.exit()  

if coil_shape == RADIALLY_STRAIGHT:
  #rotorDAxisOffset=0.5*kmr*radially_magnet_pitch
  #phase_A_offset=0.5*ks_pitch
  rotorDAxisOffset=0
  phase_A_offset=0
elif coil_shape == SQUARE_SLOTS:
  rotorDAxisOffset=0.5*kmr*radially_magnet_pitch
  phase_A_offset=0

area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)
coil_side_radians = 0.5*kc*coil_pitch #single side radian angle width
coil_core_radians = ks*coil_pitch #stator teeth radian angle width
coil_center_radius = 0.5*(stator_yoke_outer_radius + stator_coil_radius)
end_winding_radius = ((coil_core_radians+coil_side_radians)*coil_center_radius)/2   #theta*r = s (arc length)    divide by 2 to get radius, instead of diameter
ewf = (np.pi*end_winding_radius)/l		#end winding factor
if enable_l_series_connections:
  l_series_connections = coils_in_series_per_phase*(3*coil_pitch*(coil_center_radius)) #technically should be (q-1) but over compensate for the lengths
else:
  l_series_connections = 0

if input_is_Jrms0_Pcu1_slotcurrent2==1:
  if coil_shape == SQUARE_SLOTS and number_of_copper_bars_per_coil == 2:
    
    turnsPerCoil_temp = turnsPerCoil
    N_temp = N
    
    turnsPerCoil = number_of_copper_bars_per_coil
    N = turnsPerCoil
    
    l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil
    l_total = (coils_in_series_per_phase*l_coil)+l_series_connections
    
    Ra_analytical = resistance_analytical(l_total)
    
    #predicted_phase_current_rms = np.sqrt(copper_losses_allowed/((3/2)*Ra_analytical[enable_ac_resistance]))/np.sqrt(2)

    predicted_phase_current_rms = np.sqrt(copper_losses_allowed/(3*Ra_analytical[enable_ac_resistance]))
    predicted_current_density_rms = predicted_phase_current_rms/(bar_side_width*bar_side_height*1000**2)
    ra_analytical_predicted_use = Ra_analytical[enable_ac_resistance]

    J = predicted_current_density_rms
    id = J*np.sqrt(2)*sin((np.pi/180)*current_angle_degrees)
    iq = J*np.sqrt(2)*cos((np.pi/180)*current_angle_degrees)   #current density[A/mm2] should now be given as PEAK values (J is a RMS value)    
    
    turnsPerCoil = turnsPerCoil_temp
    N = N_temp

#######################################################
### PREDETERMINE SUFFICIENT CURRENT AND TURN VALUES ###
#######################################################
#if this is activated, then we need to use the Ls and flm values determined in a previous simulation of this exact machine
if enable_target_mech_power == 1:
  p_conductive_analytical_dq = 0
  flux_weakening_was_done = 0
  
  #synchronous_inductance_single_turn = db_input_select(output_table_name,"synchronous_inductance_single_turn",primary_key)
  #flm_single_turn = db_input_select(output_table_name,"PM_flux_linkage_single_turn",primary_key)

  synchronous_inductance_single_turn = db_input_select(input_table_name,"synchronous_inductance_single_turn",primary_key)
  flm_single_turn = db_input_select(input_table_name,"PM_flux_linkage_single_turn",primary_key)

  #determine torque needed to reach target power
  torque_target = (target_mech_power_kW*1000*30/np.pi)/(n_rpm)
  
  #determine single turn currents
  single_turn_iq_peak = (torque_target*(2.0/3.0)*(2.0/poles))/flm_single_turn
  single_turn_id_peak = 0
  turnsPerCoil_dq = 1
  safe_turnsPerCoil = 1
  
  #determine phase voltage as a result of this
  l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil_dq
  l_total = (coils_in_series_per_phase*l_coil)+l_series_connections
  if conductors_round0_bars1 == 0:
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil_dq
  else:
    area_available_per_turn = (bar_side_area*1000**2*fill_factor*1000**2)/turnsPerCoil_dq
    
  single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)  
  Ra_analytical_dq = resistance_analytical(l_total)
  
  #determine new iq current in order to stay within copper loss limit
  if priority_torque_target1_copper_loss_limit0 == 0:
    #determine copper losses for this single turn scenario
    p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2) #id current is ommited since flux weakening did not occur yet
    
    #print("Before current correction")
    #print("Ra_analytical_AC (turns = %d) = %f mOhm"%(turnsPerCoil_dq,Ra_analytical_dq[enable_ac_resistance]*1000))
    #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
    #print("single_turn_iq_peak = %f"%(single_turn_iq_peak))
    #print("torque_target = %f"%(torque_target))
    
    #if the copper losses are more than allowed, then we need to decrease the current (at this stage iq current) in order to stay under the copper losses limit, even if we lose torque output
    if p_conductive_analytical_dq > copper_losses_allowed:
      single_turn_iq_peak = np.sqrt((copper_losses_allowed*2*turnsPerCoil**2)/(3*Ra_analytical_dq[enable_ac_resistance]))
      predicted_torque = (3.0/2.0)*(poles/2.0)*flm_single_turn*single_turn_iq_peak
      p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2) #id current is ommited since flux weakening did not occur yet
      #print("\nAfter current correction")
      #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
      #print("single_turn_iq_peak = %f"%(single_turn_iq_peak))
      #print("predicted_torque = %f\n"%(predicted_torque))
      #print("-------------------------------------")
      
  vd_temp = Ra_analytical_dq[enable_ac_resistance]*single_turn_id_peak - w_e*synchronous_inductance_single_turn*single_turn_iq_peak
  vq_temp = Ra_analytical_dq[enable_ac_resistance]*single_turn_iq_peak + w_e*(synchronous_inductance_single_turn*single_turn_id_peak + flm_single_turn)  
  va_temp = np.sqrt(vd_temp**2 + vq_temp**2) #this is a peak value
  
  #this section should only be enabled when the ideal number of turns at top speed has not been discovered yet
  #we assume that a very small portion of Pcu allowance is towards generating torque, which will be true at top speed. So most of the Pcu allowance can be spent towards flux weakening.
  if enable_turns_calculation == 1:
    #while the terminal phase voltage is lower than the allowed system voltage, and the copper losses are below the limit, we can increase the number of turns
    #while va_temp < peak_terminal_phase_voltage_limit and p_conductive_analytical_dq < copper_losses_allowed:
    #while p_conductive_analytical_dq < copper_losses_allowed and flux_weakening_was_done == 0:# and turnsPerCoil_dq<10: #let's assume va_temp < peak_terminal_phase_voltage_limit will be more or less true if flux weakening occurs
    while va_temp < peak_terminal_phase_voltage_limit or (flux_weakening_was_done == 1 and p_conductive_analytical_dq < copper_losses_allowed):# and turnsPerCoil_dq<10:
      #save the number of turns which caused this while loop to be true
      safe_turnsPerCoil = turnsPerCoil_dq
      turnsPerCoil_dq = turnsPerCoil_dq + 1
      
      #determine new resistance, as a result of the increased turns
      l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil_dq
      l_total = (coils_in_series_per_phase*l_coil)+l_series_connections 
      if conductors_round0_bars1 == 0:
	area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil_dq
      else:
	area_available_per_turn = (bar_side_area*1000**2*fill_factor*1000**2)/turnsPerCoil_dq      
      single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)  
      Ra_analytical_dq = resistance_analytical(l_total)
    
      #determine new phase voltages, as a result of the increased turns
      flq_temp = synchronous_inductance_single_turn*single_turn_iq_peak*turnsPerCoil_dq
      vd_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/turnsPerCoil_dq) - w_e*flq_temp
      vq_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/turnsPerCoil_dq) + w_e*(synchronous_inductance_single_turn*single_turn_id_peak + flm_single_turn)*turnsPerCoil_dq  
      va_temp = np.sqrt(vd_temp**2 + vq_temp**2) #this is a peak value      

      #determine copper losses
      p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/(np.sqrt(2)*turnsPerCoil_dq))**2 + Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2) 
      
      #print "\nBefore flux weakening. Turns %d"%(turnsPerCoil_dq)
      #print("va_temp = %f"%(va_temp))
      #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
      
      #if the new phase voltage is too high, calculate an adequate -id value (if the copper losses allows it too)
      if va_temp > peak_terminal_phase_voltage_limit and enable_automatic_flux_weakening and p_conductive_analytical_dq < copper_losses_allowed:
	#print "phase voltage too high"
	flux_weakening_was_done = 1
	sqrt_calc = (peak_terminal_phase_voltage_limit**2/w_e**2)-flq_temp**2
	if sqrt_calc>0:
	  single_turn_id_peak = ((np.sqrt(sqrt_calc) - flm_single_turn*turnsPerCoil_dq)/(synchronous_inductance_single_turn*turnsPerCoil_dq**2))*turnsPerCoil_dq	
	else:
	  break
	
	#print("Id = %f"%(single_turn_id_peak))
	#determine new phase voltages, as a result of the introduced -id value
	vd_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/turnsPerCoil_dq) - w_e*flq_temp
	vq_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/turnsPerCoil_dq) + w_e*(synchronous_inductance_single_turn*single_turn_id_peak + flm_single_turn)*turnsPerCoil_dq  
	va_temp = np.sqrt(vd_temp**2 + vq_temp**2) #this is a peak value 
      
      #print "After"
      #print("va_temp = %f"%(va_temp))
      
      #determine copper losses
      p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/(np.sqrt(2)*turnsPerCoil_dq))**2 + Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2)  
      #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
  
  #print "\nWhile loop finished"
  #print "Forced number of turns section" 
  #recalculate everything with the last known number of turns which caused the while loop to be true
  ################
  if enable_turns_calculation == 1:
    turnsPerCoil_dq = safe_turnsPerCoil
  else:
    turnsPerCoil_dq = N #use number of turns forced by input db

  #determine new resistance, as a result of the best turn number
  l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil_dq
  l_total = (coils_in_series_per_phase*l_coil)+l_series_connections 
  if conductors_round0_bars1 == 0:
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil_dq
  else:
    area_available_per_turn = (bar_side_area*1000**2*fill_factor*1000**2)/turnsPerCoil_dq  
  single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)  
  Ra_analytical_dq = resistance_analytical(l_total)  
  #print "Final number of turns selected %d"%(turnsPerCoil_dq)
  
  #reset iq current in order to achieve torque target
  single_turn_iq_peak = (torque_target*(2.0/3.0)*(2.0/poles))/(flm_single_turn)
  predicted_torque = (3.0/2.0)*(poles/2.0)*flm_single_turn*single_turn_iq_peak
  
  #reset this current, as a new flux weakening current will be calculated shortly
  single_turn_id_peak = 0

  #determine copper losses
  p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/(np.sqrt(2)*turnsPerCoil_dq))**2 + Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2)
  #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
  
  #if copper loss limit enjoys priority over torque target, then we need to ensure copper losses are below the limit
  if priority_torque_target1_copper_loss_limit0 == 0:
    #determine copper losses for this single turn scenario
    p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2) #id current is ommited since flux weakening did not occur yet
    
    #print("Before current correction")
    #print("Ra_analytical_AC (turns = %d) = %f mOhm"%(turnsPerCoil_dq,Ra_analytical_dq[enable_ac_resistance]*1000))
    #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
    #print("single_turn_iq_peak = %f"%(single_turn_iq_peak))
    #print("torque_target = %f"%(torque_target))
    
    #if the copper losses are more than allowed, then we need to decrease the current (at this stage iq current) in order to stay under the copper losses limit, even if we lose torque output
    if p_conductive_analytical_dq > copper_losses_allowed:
      single_turn_iq_peak = np.sqrt((copper_losses_allowed*2*turnsPerCoil_dq**2)/(3*Ra_analytical_dq[enable_ac_resistance]))
      predicted_torque = (3.0/2.0)*(poles/2.0)*flm_single_turn*single_turn_iq_peak
      p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2) #id current is ommited since flux weakening did not occur yet
      #print("\nAfter current correction (to get copper losses within limit)")
      #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
      #print("single_turn_iq_peak = %f"%(single_turn_iq_peak))
      #print("predicted_torque = %f\n"%(predicted_torque))
    
    #if the copper losses are now below the limit, so we still have room to get closer to the target torque
    #if predicted_torque<torque_target:
      #print("\nAfter current increase (to get closer to torque target)")
      #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
      #print("single_turn_iq_peak = %f"%(single_turn_iq_peak))
      #print("predicted_torque = %f\n"%(predicted_torque))  
  
  #determine new phase voltages, as a result of the best turn number
  flq_temp = synchronous_inductance_single_turn*single_turn_iq_peak*turnsPerCoil_dq
  vd_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/turnsPerCoil_dq) - w_e*flq_temp
  vq_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/turnsPerCoil_dq) + w_e*(synchronous_inductance_single_turn*single_turn_id_peak + flm_single_turn)*turnsPerCoil_dq  
  va_temp = np.sqrt(vd_temp**2 + vq_temp**2) #this is a peak value  
  
  #print("va_temp before = %f\n"%(va_temp))  
  
  #at this point, we assume that torque_target is not competing with copper losses, WHEN we need to do flux weakening
  if va_temp > peak_terminal_phase_voltage_limit and enable_automatic_flux_weakening and p_conductive_analytical_dq < copper_losses_allowed:
    #print "Flux weakening was implemented"
    sqrt_calc = (peak_terminal_phase_voltage_limit**2/w_e**2)-flq_temp**2
    if sqrt_calc>0:
      single_turn_id_peak = ((np.sqrt(sqrt_calc) - flm_single_turn*turnsPerCoil_dq)/(synchronous_inductance_single_turn*turnsPerCoil_dq**2))*turnsPerCoil_dq    
    #else: print "value to be squarerooted was less than 0"

    #determine new va_temp after flux weakening is done
    vd_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/turnsPerCoil_dq) - w_e*flq_temp
    vq_temp = Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/turnsPerCoil_dq) + w_e*(synchronous_inductance_single_turn*single_turn_id_peak + flm_single_turn)*turnsPerCoil_dq  
    va_temp = np.sqrt(vd_temp**2 + vq_temp**2) #this is a peak value  

    #print("va_temp after = %f\n"%(va_temp))  

  #determine copper losses
  p_conductive_analytical_dq = 3*(Ra_analytical_dq[enable_ac_resistance]*(single_turn_id_peak/(np.sqrt(2)*turnsPerCoil_dq))**2 + Ra_analytical_dq[enable_ac_resistance]*(single_turn_iq_peak/(np.sqrt(2)*turnsPerCoil_dq))**2)
  #print("p_conductive_analytical_dq = %f"%(p_conductive_analytical_dq))
  #print("iq = %f"%(single_turn_iq_peak/turnsPerCoil_dq))
  #print("id = %f"%(single_turn_id_peak/turnsPerCoil_dq))  
  
  #print("ia_peak = %f"%(np.sqrt((single_turn_iq_peak/turnsPerCoil_dq)**2 + (single_turn_id_peak/turnsPerCoil_dq)**2)))

  #print("iddd = %f"%(id))
  #print("iqqq = %f"%(iq))
  
  #final iq and id that is sent for simulation
  if conductors_round0_bars1 == 0:
    iq = single_turn_iq_peak/(coil_side_area_in_mm*fill_factor) #convert iq into current density
    id = single_turn_id_peak/(coil_side_area_in_mm*fill_factor) #convert id into current density
  elif conductors_round0_bars1 == 1:
    iq = single_turn_iq_peak/(bar_side_area*1000**2*fill_factor) #convert iq into current density
    id = single_turn_id_peak/(bar_side_area*1000**2*fill_factor) #convert id into current density
  
  J = np.sqrt(id**2 + iq**2)/np.sqrt(2)
  if (np.isclose(id,0,rtol=1e-04, atol=1e-04, equal_nan=False) and np.isclose(iq,0,rtol=1e-04, atol=1e-04, equal_nan=False)) or np.isclose(torque_target,0,rtol=1e-04, atol=1e-04, equal_nan=False):
    current_angle_degrees = 0
  else:
    current_angle_degrees = np.arctan(id/iq)*180/np.pi
  predicted_torque = (3.0/2.0)*(poles/2.0)*flm_single_turn*single_turn_iq_peak
  
  #print("iddd = %f"%(id))
  #print("iqqq = %f"%(iq))
  #print("predicted_torque = %f\n"%(predicted_torque))
  
  #The number of turns calculated in this section will be FORCED in the post-analysis. The results should be similar to the post-analysis, which makes use of phase quantities instead of dq quantities.
  N = turnsPerCoil_dq
#################################################


if args.plot == 0:
  #############################
  # Final simulation settings #
  #############################
  if use_airgap_layers == 0:
    sf.semfem_set_cvw_gaps(1, [stator_coil_radius], [stator_coil_radius+g])
  elif use_airgap_layers == 1:
    #if number_of_layers_airgap%2 == 1:
    #gap_center = stator_coil_radius+airgap_radii[int(len(airgap_radii)/2)]
    
    sf.semfem_set_cvw_gaps(1, [airgap_radii[int(len(airgap_radii)/2)-1]], [airgap_radii[int(len(airgap_radii)/2)]])

  ###FROM OE_MAC.py periodic example

  if use_machine_periodicity == 1:
    sf.semfem_set_left_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the left boundary) (use this when simulating only one electrical period (partial machine))
    sf.semfem_set_right_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the right boundary) (use this when simulating only one electrical period (partial machine))  
  else:
    sf.semfem_set_left_boundary(sf.NEUMANN)		#because this section repeats itself by an even number (on the left boundary) (use this when simulating entire 360 degree machine)
    sf.semfem_set_right_boundary(sf.NEUMANN)		#because this section repeats itself by an even number (on the right boundary) (use this when simulating entire 360 degree machine)  

  sf.semfem_set_top_boundary(sf.DIRICHLET)		#top section Dirichlet boundary (determines in which directions the flux can be solved)
  sf.semfem_set_bottom_boundary(sf.DIRICHLET)		#bottom section Dirichlet boundary (determines in which directions the flux can be solved)
  ###

  #according to semfem manual:
  #sf.semfem_coil_turns[:] controls the number of turns of each COIL (not PHASE as stated in manual)
  #sf.semfem_coil_sides[:] This parameter must be set equal to the number of areas in the model representing the same phase. For this parameter the polarity of the phase is irrelevant. The default value is 1.

  sf.semfem_coil_turns[:] = turnsPerCoil								#define number of turns for each coil (22) (this line not needed if using sf.PHASE_CURRENT_DENSITY)

  #print "semfem_coil_sides"
  if use_machine_periodicity == 1:
    sf.semfem_coil_sides[:] = (int((N_L*q)/periodicity_in_one_full_rotation))	   			#define number of coils SIDES for each phase in the model being simulated (if full model is used, then total of all coil sides in a single phase must be used)
												  #if only a periodic portion of model is used, then only that number of coil sides per phase in the periodic portion of the model must be used
  else:
    sf.semfem_coil_sides[:] = (int(N_L*q))

  #specify fill factor(added this on 2016/10/25)
  for i in range(np.size(sf.semfem_ff_vec)):			#Although it is only necessary to assign values to the first 3 elements of the array (3 phase machine) the other elements are assigned these values just for contingency.
      sf.semfem_ff_vec[i] = fill_factor

  #rotorDAxisOffset=0.5*radially_magnet_pitch
  #rotorDAxisOffset = 0.5*magnet_pitch		#this angle is used to move rotor d-axis inline with the x-axis (assuming the transformation angle = 0 because we assume the phase-A axis also to be there)
						  #then, the phase_A_offset variable will then be used to move the rotor (d-axis) inline with the actual position of the phase-A axis (so that the transformation angle is indeed 0)

  #phase_A_offset=0

  if input_is_Jrms0_Pcu1_slotcurrent2 == 2:
    id = J*np.sqrt(2)*sin((np.pi/180)*current_angle_degrees)
    iq = J*np.sqrt(2)*cos((np.pi/180)*current_angle_degrees)   #current density[A/mm2] should now be given as PEAK values (J is a RMS value)   

  for i in range(steps):					#simulates an entire electrical period in the allocated number of steps
    if use_working_harmonic_step_size_scale == 0:
      sf.semfem_time_vec[i] = ((i+1)/steps)/(f_e/rotor_movement_multiple)	#this is necessary to setup the full period time span for the simulation of the motor THE CONVENTION FOR SEMFEM IS THAT time_vec SHOULD CONTAIN END-TIME OF EACH TIME STEP, THUS FIRST TIME ELEMENT WILL NOT BE 0
							#it also adjusts how fast (number of steps) the simulation runs through a single electrical period
							#Error: Invalid time vector, t_vec should contain end-time of each time-step. Simulation starts at t=0.
    
    else:
      sf.semfem_time_vec[i] = ((i+1)/steps)/(f_e/rotor_movement_multiple)

    #Controls the position of different components at each step. Components are seperated by air-gaps and are numbered from the inside out.
    #sf.semfem_p_vec[i,0] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset		#the second array index indicates rotation of the inner rotor [0]

    if steps == 1:
      sf.semfem_p_vec[i,1] = -rotorDAxisOffset + phase_A_offset + debug_offset
      #sf.semfem_p_vec[i,1] = debug_offset
    else:
      if locked_rotor_test == 0:
	if use_working_harmonic_step_size_scale == 1:
	  sf.semfem_p_vec[i,1] = direction*(i/(steps))*(mechRotation/(working_harmonic)) + debug_offset		
	else:  
	  sf.semfem_p_vec[i,1] = direction*(i/(steps))*(mechRotation*rotor_movement_multiple/2) + debug_offset		
      else:
	sf.semfem_p_vec[i,1] = 0

    #print("sf.semfem_p_vec[%d,1] = %f [deg]"%(i,sf.semfem_p_vec[i,1]*(180/np.pi)))

    [ia, ib, ic] = [0, 0, 0]
    #ia, ib, ic = sf.dq_abc(id, iq, (i*2.*np.pi)/(steps-1))	#3rd function argument is the transformation angle of the dq transformation
    if steps == 1:
      ia, ib, ic = sf.dq_abc(id, iq, 0)	#3rd function argument is the transformation angle of the dq transformation
    else:
      ia, ib, ic = sf.dq_abc(id, iq, (i*2.*np.pi*rotor_movement_multiple)/(steps)-(np.pi/2)-shift_current_electrical_angle*(np.pi/180))
      #ia, ib, ic = sf.dq_abc(id, iq, (i*2.*np.pi*working_harmonic)/(steps)-(np.pi/2)-shift_current_electrical_angle*(np.pi/180))	#3rd function argument is the transformation angle of the dq transformation *** -(np.pi/2) is inserted to make cos waves instead of sine waves
    
    #use the following three lines for phase current density quantities (these values specified are peak values)
    sf.semfem_idens_vec[i,0] = ia
    sf.semfem_idens_vec[i,1] = ib
    sf.semfem_idens_vec[i,2] = ic 
    #print ia
    #print sf.semfem_idens_vec[i,2]

  #symmetry settings
  #print "symmetry multipliers"
  if use_machine_periodicity == 1:
    sf.semfem_set_symmetry_multiplier(int(periodicity_in_one_full_rotation))		#number of periodic occurances of machine structure
    sf.semfem_set_parallel_circuits(int(number_of_parallel_circuits)) 			#because all phase coils will be connected in series
  else:
    sf.semfem_set_symmetry_multiplier(1)						#number of periodic occurances of machine structure
    sf.semfem_set_parallel_circuits(1) 							#because all phase coils will be connected in series  
  #print "end of symmetry multipliers"

if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["-----------TOPOLOGY ---------------------------------------------", "-----------------------------", "-----------------------------"]) 
  
  if coil_shape == SQUARE_SLOTS:
    pretty_terminal_table.add_row(["coil_shape","SQUARE","shape"])  
  elif coil_shape == RADIALLY_STRAIGHT:
    pretty_terminal_table.add_row(["coil_shape","RADIAL","shape"])
  
  if conductors_round0_bars1:
    pretty_terminal_table.add_row(["windings_type","SOLID BARS","used"]) 
    pretty_terminal_table.add_row(["copper_bars_per_coil_side(turns_per_coil)", number_of_copper_bars_per_coil, "[p.u.]"])
  else:
    pretty_terminal_table.add_row(["windings_type","ROUND WIRES","used"])
  
  if insert_lamination_hole == 1:
    pretty_terminal_table.add_row(["stator_tooth_rod_material","ALLUMINIUM","used"])
    pretty_terminal_table.add_row(["stator_tooth_hole_diameter",stator_rod_hole_diameter*1000,"[mm]"])
    pretty_terminal_table.add_row(["stator_rod_distance_from_tooth_tip",stator_rod_distance_from_tooth_tip*1000,"[mm]"])
  
  pretty_terminal_table.add_row(["winding_material","COPPER","used"])
  #pretty_terminal_table.add_row(["copper_grade","C101000 (ETP)",""])
  #pretty_terminal_table.add_row(["copper_grade","standard used",""])
  pretty_terminal_table.add_row(["copper_grade","",""])

  pretty_terminal_table.add_row(["---UNS","C110000",""])
  pretty_terminal_table.add_row(["---ISO","Cu ETP",""])
  pretty_terminal_table.add_row(["---BS UN","CW004 (formally C101)",""])
  
  #pretty_terminal_table.add_row(["","UNS","C110000"])
  #pretty_terminal_table.add_row(["","ISO","Cu ETP"])
  #pretty_terminal_table.add_row(["","BS EN","CW004 (formally C101)"])
  
  #pretty_terminal_table.add_row(["","UNS","?"])
  #pretty_terminal_table.add_row(["","ISO","Cu ETP1"])
  #pretty_terminal_table.add_row(["","BS EN","CW003 (formally C100)"])  
  
  pretty_terminal_table.add_row(["C110000_resistivity @ 20 deg. C", c110000_resistivity_20, "[Ohm.m]"])
  
  if square_magnets == 0:
    pretty_terminal_table.add_row(["magnet_shape","CURVED","shape"])  
  elif square_magnets == 1:
    pretty_terminal_table.add_row(["magnet_shape","SQUARE","shape"])  

  if interior_magnets == 0:
    pretty_terminal_table.add_row(["magnet_mounting","SURFACE","mount"]) 
  elif interior_magnets == 1:
    pretty_terminal_table.add_row(["magnet_mounting","INTERIOR","mount"])

  if magnetisation_radial0_normal1 == 0:
    pretty_terminal_table.add_row(["magnetisation","RADIAL","direction"])  
  elif magnetisation_radial0_normal1 == 1:
    pretty_terminal_table.add_row(["magnetisation","NORMAL","direction"])   
  
  if three_phase_delta0_wye1:
    pretty_terminal_table.add_row(["three phase connection","WYE","used"])  
  else:
    pretty_terminal_table.add_row(["three phase connection","DELTA","used"])  
  
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----------MACHINE SIMULATION SETUP -----------------------------", "-----------------------------", "-----------------------------"]) 
  pretty_terminal_table.add_row(["symmetry_multiplier",periodicity_in_one_full_rotation,"[p.u.]"])
  pretty_terminal_table.add_row(["Na",number_of_parallel_circuits,"parallel circuits"])
  pretty_terminal_table.add_row(["q",q,"coils (slots) per phase"])  
  pretty_terminal_table.add_row(["Q",Q,"total coils (slots)"])
  pretty_terminal_table.add_row(["poles",poles,"total poles"])
  #pretty_terminal_table.add_row(["slots_per_phase_per_pole",slots_per_phase_per_pole,"ratio"])
  #pretty_terminal_table.add_row(["pole_pairs_per_slot_per_phase",pole_pairs_per_slot_per_phase,"ratio"])
  
  pretty_terminal_table.add_row(["kc",kc,"ratio of stator coil side width (max size 1)"])
  pretty_terminal_table.add_row(["ks",ks,"ratio of coil core, i.e. stator teeth width (max size 1)"])
  #pretty_terminal_table.add_row(["kq",kq,"coils pole ratio"])
  pretty_terminal_table.add_row(["f_e",f_e,"[Hz]"])
  pretty_terminal_table.add_row(["steps_simulated",steps,"[p.u.]"])
  pretty_terminal_table.add_row(["machine_speed",n_rpm,"[rpm]"])
  #pretty_terminal_table.add_row(["simulated_time",(1/f_e)*1000,"[ms]"])
  #pretty_terminal_table.add_row(["time_step_size",((1/f_e)/steps)*1000,"[ms]"])
  #pretty_terminal_table.add_row(["time_at_specific_step",time_at_specific_step*1000,"[ms]"])
  #pretty_terminal_table.add_row(["electrical_rotation_at_specific_step",electrical_rotation_at_specific_step,"[e.deg]"])
  pretty_terminal_table.add_row(["J_rms",J,"[A/mm^2]"])
  pretty_terminal_table.add_row(["id_peak",id,"[A/mm^2]"])
  pretty_terminal_table.add_row(["iq_peak",iq,"[A/mm^2]"])
  pretty_terminal_table.add_row(["Br_PM",Brem,"[T]"])
  #pretty_terminal_table.add_row(["phase_A_offset",phase_A_offset,"[rad]"])
  #pretty_terminal_table.add_row(["rotorDAxisOffset", rotorDAxisOffset,"[rad]"])
  #pretty_terminal_table.add_row(["phase_A_offset",phase_A_offset*180/np.pi,"[deg]"])
  #pretty_terminal_table.add_row(["rotorDAxisOffset", rotorDAxisOffset*180/np.pi,"[deg]"])
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----------DIMENSIONS -------------------------------------------", "-----------------------------", "-----------------------------"])
  pretty_terminal_table.add_row(["active_stack_length", l*1000, "[mm]"])
  pretty_terminal_table.add_row(["stator_yoke_inner_radius", stator_yoke_inner_radius*1000, "[mm]"])
  pretty_terminal_table.add_row(["stator_yoke_height", (stator_yoke_outer_radius-stator_yoke_inner_radius)*1000, "[mm]"])
  #pretty_terminal_table.add_row(["stator_teeth(coil)_height", (stator_coil_radius-stator_yoke_outer_radius)*1000, "[mm]"])
  pretty_terminal_table.add_row(["airgap", g*1000, "[mm]"])
  #pretty_terminal_table.add_row(["magnet_height", (magnet_radially_outer_radius-magnet_radially_inner_radius)*1000, "[mm]"])
  #pretty_terminal_table.add_row(["magnet_inner_radius", magnet_radially_inner_radius*1000, "[mm]"])
  #pretty_terminal_table.add_row(["magnet_outer_radius", magnet_radially_outer_radius*1000, "[mm]"])
  pretty_terminal_table.add_row(["rotor_yoke_height", (ryo-magnet_radially_outer_radius)*1000, "[mm]"])
  pretty_terminal_table.add_row(["rotor_yoke_outer_radius", ryo*1000, "[mm]"]) 
  
  if coil_shape == SQUARE_SLOTS:
    if flare_teeth == 0:
      pretty_terminal_table.add_row(["stator_teeth_width", teeth_width*1000, "[mm]"])
    elif flare_teeth == 1:
      pretty_terminal_table.add_row(["stator_teeth_BOTTOM_width (width measured where tooth meets yoke)", teeth_bottom_width*1000, "[mm]"])
      pretty_terminal_table.add_row(["stator_teeth_TOP_width (width just below shoe)", teeth_top_width*1000, "[mm]"])
  
  if flare_teeth == 0:
    pretty_terminal_table.add_row(["stator_teeth_height", (stator_coil_radius-stator_yoke_outer_radius)*1000, "[mm]"])
  else:
    pretty_terminal_table.add_row(["stator_teeth_height (along center of tooth ending at stator-rotor airgap)", (stator_coil_radius-stator_yoke_outer_radius)*1000, "[mm]"])
    pretty_terminal_table.add_row(["stator_teeth_height (measured alongside tooth until just before shoe)", (stator_teeth_height_excl_shoe)*1000, "[mm]"])
    pretty_terminal_table.add_row(["stator_shoe_height_EXCLUDING_weakness_compensation", (stator_shoe_height)*1000, "[mm]"])
    pretty_terminal_table.add_row(["stator_shoe_height_INCLUDING_weakness_compensation (total)", (stator_shoe_height+shoe_weakpoint_thickness)*1000, "[mm]"])
    pretty_terminal_table.add_row(["shoe_tip_height", (shoe_tip_height)*1000, "[mm]"])    
    pretty_terminal_table.add_row(["shoe_tip_width", (shoe_tip_width)*1000, "[mm]"])
    
  if coil_shape == SQUARE_SLOTS:
    pretty_terminal_table.add_row(["coil_side_width", coil_side_width*1000, "[mm]"])
    pretty_terminal_table.add_row(["coil_side_height", coil_side_height*1000, "[mm]"])
    if conductors_round0_bars1 and number_of_copper_bars_per_coil == 2:
      pretty_terminal_table.add_row(["bar_width", bar_side_width*1000, "[mm]"])
      pretty_terminal_table.add_row(["bar_height", bar_side_height*1000, "[mm]"])    
  else:
    pretty_terminal_table.add_row(["coil_side_height", (stator_coil_radius-stator_yoke_outer_radius-coil_yoke_spacing)*1000, "[mm]"])
  
  if flare_teeth == 1:
    pretty_terminal_table.add_row(["slot_width", 2*half_slot_width*1000, "[mm]"])
  
  if conductors_round0_bars1 == 1 and flare_teeth == 1:
    pretty_terminal_table.add_row(["coil_to_teeth_spacing", coil_teeth_spacing*1000, "[mm]"])
    pretty_terminal_table.add_row(["coil_to_yoke_spacing", coil_yoke_spacing*1000, "[mm]"])
    pretty_terminal_table.add_row(["coil_to_coil_spacing", coil_opposing_spacing*1000, "[mm]"])   
    pretty_terminal_table.add_row(["coil_to_shoe_spacing", coil_shoe_spacing*1000, "[mm]"])   
  
  pretty_terminal_table.add_row(["coil_side_area",coil_side_area_in_mm,"[mm^2]"])
  if conductors_round0_bars1 and number_of_copper_bars_per_coil == 2:
    pretty_terminal_table.add_row(["per_bar_area",(bar_side_area/number_of_copper_bars_per_coil)*1000**2,"[mm^2]"])
  
  if square_magnets == 1:
    pretty_terminal_table.add_row(["magnet_width", magnet_side_length*1000, "[mm]"])
  pretty_terminal_table.add_row(["magnet_height", (magnet_radially_outer_radius-magnet_radially_inner_radius)*1000, "[mm]"])    
  
  if interior_magnets == 1:
    pretty_terminal_table.add_row(["magnet_sleeve_thickness", magnet_sleeve_height*1000, "[mm]"])
    pretty_terminal_table.add_row(["magnet_sleeve_tolerance_spacing", magnet_sleeve_tolerance*1000, "[mm]"])
  
  if conductors_round0_bars1 == 1 and flare_teeth == 1:
    pretty_terminal_table.add_row(["effective_fill_factor", effective_fill_factor, "[p.u.]"])
  
  #pretty_terminal_table.add_row(["magnet_tangentially_outer_radius", magnet_tangentially_outer_radius*1000, "[mm]"])
#pretty_terminal_table.add_row(["------------------end of drawing commands------------------", "-----------", "----------"])
#pretty_terminal_table.add_row(["", "", ""])

if args.plot == 0:
  if use_machine_periodicity == 1:
    m = sf.py_make_mesh(sf.NO_FIX)
  else:
    m = sf.py_make_mesh(sf.FIX)

if use_machine_periodicity == 0 and save_fpl2pdf == 1:
  svg_file = 'project_sorspm_semfem/drawing'
  #os.system("fpl2svg %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(svg_file+"_plain"+".pdf",svg_file+".svg"))

if exit_before_solving == 1:  
  if show_fpl_before_band_solver == 1:
    os.system("fpl2 project_sorspm_semfem/drawing.poly")
  conn.close()
  sys.exit() 

if show_fpl_before_band_solver == 1:
  os.system("fpl2 project_sorspm_semfem/drawing.poly")
  #os.system("fpl2 project_sorspm_semfem/drawing2.poly")
  os.system("fpl2 project_sorspm_semfem/drawing.fpl")

##########
# Solver #
##########

#as of semfem 3.8.1 this is the new function call:
#def band_solver(self, m, eq_solver, order, problem)

if run_band_solver == 1:
  if SOLVER == 1:
    m = sf.band_solver(m, sf.GAUSS, 1,sf.NONLINEAR_PROBLEM)				#Stiaan het gese ek moet NONLINEAR_PROBLEM spesifiseer
  elif SOLVER == 2:
    m = sf.age_solver(m, sf.ICCG)				#takes much longer to simulate

if show_fpl_after_band_solver == 1:
  os.system("fpl2 project_sorspm_semfem/results/fieldplots/band_machine_field001.fpl")

#os.system("post.py project_sorspm_semfem/results/post.res")			#brings up thousands of plots

#os.system("post.py --voltage project_sorspm_semfem/results/post.res -f %f" % (f_e))	#brings up all the voltage plots, for more usage and options to use this command, see
											    #SEMFEM_git/SEMFEM/core/plotter/post.py

if plot_all == 1:								
  os.system("post.py project_sorspm_semfem/results/post.res -f %f" % (f_e/rotor_movement_multiple))

#sys.exit()

#print sf.semfem_idens_vec[:,0]
#print sf.semfem_idens_vec[:,1]
#print sf.semfem_idens_vec[:,2]

#determine mass
if args.plot == 0:
  if stator_yoke_iron_N_al == 1:
    stator_yoke_mass = sf.get_mass(m, stator_iron, sf.STEEL_DENSITY) #FIXME: Should this be IRON or STEEL? sf.STEEL_DENSITY
  else:
    stator_yoke_mass = sf.get_mass(m, stator_iron, sf.ALUM_DENSITY)
    
  if rotor_yoke_iron_N_al == 1:  
    rotor_yoke_mass = sf.get_mass(m, rotor_iron, sf.STEEL_DENSITY)
  else:
    rotor_yoke_mass = sf.get_mass(m, rotor_iron, sf.ALUM_DENSITY)
    
  magnet_mass = sf.get_magnet_mass(m, sf.NDFEB_DENSITY)
  copper_mass = 0.
  aluminium_mass = 0.
  for i in range(1, 4):
    if conductor_cu0_al1 == 0:
      coil_mass = sf.get_mass(m, i, sf.COPPER_DENSITY)
      copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]
      coil_mass = sf.get_mass(m, -i, sf.COPPER_DENSITY)
      copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]
    elif conductor_cu0_al1 == 1:
      coil_mass = sf.get_mass(m, i, sf.ALUM_DENSITY)
      aluminium_mass = aluminium_mass + coil_mass*sf.semfem_ff_vec[i-1]
      coil_mass = sf.get_mass(m, -i, sf.ALUM_DENSITY)
      aluminium_mass = aluminium_mass + coil_mass*sf.semfem_ff_vec[i-1]      

  if enable_shell_mass == 1:
    rotor_shell_mass = np.pi*((rotor_shell_height+ryo)**2 - ryo**2)*l*sf.ALUM_DENSITY
    stator_shell_mass = np.pi*(stator_yoke_inner_radius**2 - (stator_yoke_inner_radius-stator_shell_height)**2)*l*sf.ALUM_DENSITY
    aluminium_mass = aluminium_mass + rotor_shell_mass + stator_shell_mass

  total_mass = stator_yoke_mass+rotor_yoke_mass+magnet_mass+copper_mass+aluminium_mass
    
else:
  stator_yoke_mass = db_input_select(output_table_name,"stator_yoke_mass",primary_key)
  rotor_yoke_mass = db_input_select(output_table_name,"rotor_yoke_mass",primary_key)
  magnet_mass = db_input_select(output_table_name,"magnet_mass",primary_key)
  copper_mass = db_input_select(output_table_name,"copper_mass",primary_key)
  aluminium_mass = db_input_select(output_table_name,"aluminium_mass",primary_key)
  total_mass = db_input_select(output_table_name,"total_mass",primary_key)

if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["steel_density",sf.STEEL_DENSITY,"[kg/m^3]"])
  pretty_terminal_table.add_row(["copper_density",sf.COPPER_DENSITY,"[kg/m^3]"])
  if enable_shell_mass == 1:
    pretty_terminal_table.add_row(["aluminium_density",sf.ALUM_DENSITY,"[kg/m^3]"])
  pretty_terminal_table.add_row(["magnet_density",sf.NDFEB_DENSITY,"[kg/m^3]"])
  pretty_terminal_table.add_row(["stator_yoke_mass",stator_yoke_mass,"[kg]"])
  pretty_terminal_table.add_row(["rotor_yoke_mass",rotor_yoke_mass,"[kg]"])
  pretty_terminal_table.add_row(["copper_mass (stator_coils)",copper_mass,"[kg]"])
  pretty_terminal_table.add_row(["magnet_mass",magnet_mass,"[kg]"])
  if enable_shell_mass == 1:
    pretty_terminal_table.add_row(["aluminium_mass",aluminium_mass,"[kg]"])
    pretty_terminal_table.add_row(["---rotor_shell_mass",rotor_shell_mass,"[kg]"])
    pretty_terminal_table.add_row(["---stator_shell_mass",stator_shell_mass,"[kg]"])
  pretty_terminal_table.add_row(["total_mass",total_mass,"[kg]"])

#use end winding factor
coil_side_radians = 0.5*kc*coil_pitch #single side radian angle width
coil_core_radians = ks*coil_pitch #stator teeth radian angle width
coil_center_radius = 0.5*(stator_yoke_outer_radius + stator_coil_radius)
end_winding_radius = ((coil_core_radians+coil_side_radians)*coil_center_radius)/2   #theta*r = s (arc length)    divide by 2 to get radius, instead of diameter
ewf = (np.pi*end_winding_radius)/l		#end winding factor
#pretty_terminal_table.add_row(["stacklength", l, "[m]"])
#pretty_terminal_table.add_row(["end winding factor", ewf, "[p.u.]"])
#pretty_terminal_table.add_row(["end_winding_radius", end_winding_radius, "[m]"])
#pretty_terminal_table.add_row(["end_winding_single_end_length", np.pi*end_winding_radius, "[m]"])
#pretty_terminal_table.add_row(["steps", steps, "[p.u.]"])
#pretty_terminal_table.add_row(["direction", direction, "[p.u.]"])

##CALCULATE DQ TORQUE
#fld = np.zeros(steps)
#flq = np.zeros(steps)
#torque_dq = np.zeros(steps)

#for i in range(steps):
  #fld[i], flq[i] = sf.abc_dq(sf.semfem_flink_vec[i,0], sf.semfem_flink_vec[i,1], sf.semfem_flink_vec[i,2], i/steps*2.*np.pi)
  #torque_dq[i] = -(3/2)*(poles/2)*(fld[i]*iq - flq[i]*id)

#average_torque_dq = average(torque_dq)

#if np.isclose(0,average_torque_dq,rtol=1e-04, atol=1e-04, equal_nan=False):
  #torque_ripple_dq = 1000
#else:
  #torque_ripple_dq = ((max(torque_dq)-min(torque_dq))/average_torque_dq)*100


#pretty_terminal_table.add_row(["0 peak_fld", max(fld), "[Wb]"])
#pretty_terminal_table.add_row(["0 peak_flq", max(flq), "[Wb]"])
#pretty_terminal_table.add_row(["0 average_torque_dq", average_torque_dq, "[Nm]"])
#pretty_terminal_table.add_row(["0 torque_ripple_dq", torque_ripple_dq, "[%]"])

##################################
#skin_depth = np.sqrt((2*copper_resistivity)/(2*np.pi*f_e*4*np.pi*10**(-7)))
if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----------DETERMINE NUMBER OF TURNS ----------------------------", "-----------------------------", "-----------------------------"])
  pretty_terminal_table.add_row(["number_of_parallel_circuits", number_of_parallel_circuits, "[p.u.]"])
  pretty_terminal_table.add_row(["fill_factor_assumption", fill_factor, "[p.u.]"])
  pretty_terminal_table.add_row(["current_through_slot (rms)", current_through_slot, "[A]"])
  #pretty_terminal_table.add_row(["conductor_length_between_phase_coils", l_series_connections, "[m]"])
  pretty_terminal_table.add_row(["DC Bus voltage (system voltage)", system_voltage, "[V]"])
  if inverter_squarewave0_sinusoidalPWM1 == 0: pretty_terminal_table.add_row(["inverter_topology", "SQUARE WAVE (factor 1.103)", ""])
  elif inverter_squarewave0_sinusoidalPWM1 == 1: pretty_terminal_table.add_row(["inverter_topology", "SINUSOIDAL PWM (factor 0.866)", ""])
  elif inverter_squarewave0_sinusoidalPWM1 == 2: pretty_terminal_table.add_row(["inverter_topology", "FLAT (factor 1.0)", ""])
  #pretty_terminal_table.add_row(["inverter_voltage_drop_factor", 0.95, "[p.u.]"])
  pretty_terminal_table.add_row(["peak_terminal_line_voltage_limit", peak_terminal_line_voltage_limit, "[V]"])
  pretty_terminal_table.add_row(["peak_terminal_phase_voltage_limit", peak_terminal_phase_voltage_limit, "[V]"])
  pretty_terminal_table.add_row(["RMS_terminal_line_voltage_limit", RMS_terminal_line_voltage_limit, "[V]"])
  pretty_terminal_table.add_row(["RMS_terminal_phase_voltage_limit", RMS_terminal_phase_voltage_limit, "[V]"])
  
  pretty_terminal_table.add_row(["coil_temperature_assumption",coil_temperature, "[deg. C]"])
  pretty_terminal_table.add_row(["copper_resistivity @ %d deg. C"%(coil_temperature), copper_resistivity, "[Ohm.m]"])
  pretty_terminal_table.add_row(["magnet_resistivity @ %d deg. C"%(coil_temperature), magnet_resistivity, "[Ohm.m]"])
  pretty_terminal_table.add_row(["skin_depth @ %d [Hz]"%(f_e), skin_depth*1000, "[mm]"])

if enable_target_mech_power == 0:
  synchronous_inductance = -1
  flm = -1

  synchronous_inductance_single_turn = -1
  flm_single_turn = -1

#USE SEMFEM VALUES TO OBTAIN (LINE CURRENT, PHASE VOLTAGE)
if steps != 1 and os.path.isfile('project_sorspm_semfem/results/post.res'):
  #CALCULATE DQ VOLTAGES (USING SEMFEM VOLTAES)
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["----------------------STAGE 1 ---------------------------------", "## using MORE than 1 step ##", "## obtain single turn voltage and current ##"])
  #sfm_post = SEMFEM_postfile('project_sorspm_semfem/results/post.res', 1.0/f_e)
  sfm_post = SEMFEM_postfile('project_sorspm_semfem/results/post.res', 1.0/f_e)
  vd = np.zeros(steps)
  vq = np.zeros(steps)
  
  for i in range(steps):
    vq[i],vd[i] = sf.abc_dq(sfm_post.voltage[0,i], sfm_post.voltage[1,i], sfm_post.voltage[2,i], i/steps*2.*np.pi)    

  id_new = np.zeros(steps)
  iq_new = np.zeros(steps)
  fld = np.zeros(steps)
  flq = np.zeros(steps)
  torque_dq = np.zeros(steps)

  for i in range(steps):
    if steps == 1:
      iq_new[i],id_new[i] = sf.abc_dq(sf.semfem_i_vec[i,0], sf.semfem_i_vec[i,1], sf.semfem_i_vec[i,2], 0)
      flq[i],fld[i] = sf.abc_dq(sf.semfem_flink_vec[i,0], sf.semfem_flink_vec[i,1], sf.semfem_flink_vec[i,2], 0)    
    else:
      iq_new[i],id_new[i] = sf.abc_dq(sf.semfem_i_vec[i,0], sf.semfem_i_vec[i,1], sf.semfem_i_vec[i,2], i/steps*2.*np.pi)
      flq[i],fld[i] = sf.abc_dq(sf.semfem_flink_vec[i,0], sf.semfem_flink_vec[i,1], sf.semfem_flink_vec[i,2], i/steps*2.*np.pi)
    
    torque_dq[i] = -(3/2)*(poles/2)*(fld[i]*iq_new[i] - flq[i]*id_new[i])

  average_torque_dq = abs(average(torque_dq))
  torque_density = average_torque_dq/total_mass

  #calculate vd and vq here as the values seem more constant as opposed to determining vd and vq from sf.abd_dq 
  vd_calculated = np.zeros(steps)
  vq_calculated = np.zeros(steps)
  for i in range(0,steps):
    vd_calculated[i] = -w_e*flq[i]#/turnsPerCoil #minus is present with vd, see Umans p578
    vq_calculated[i] = w_e*fld[i]#/turnsPerCoil

  #determine synchronous inductance Ls, and magnet flux linkage flm if a pure q-axis current is injected, or if we are not yet at stage in which the target kW should be attepted (we first need the Ls and flm quantities)
  if locked_rotor_test == 0 and enable_target_mech_power == 0:
    if np.isclose(current_angle_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      synchronous_inductance_single_turn = abs(average(vd_calculated)/(np.sqrt(2)*w_e*iq_new[0])) #I think the current used here is an rms value
      flm_single_turn = (abs(average(vq_calculated))-w_e*synchronous_inductance_single_turn*id_new[0])/w_e

  #DETERMINE MAX INPUT CURRENT (PEAK VALUE)
  if args.plot == 0:
    max_input_phase_current = max(sf.semfem_i_vec[:,0])
  else:
    max_input_phase_current = db_input_select(output_table_name,"phase_current_peak",primary_key)*db_input_select(output_table_name,"turnsPerCoil",primary_key)
    
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["1 peak_input_phase_current (single_turn)", max_input_phase_current, "[A]"])
  
  Ia_instantaneous = []
  for i in range (len(sf.semfem_i_vec[:,0])):
    Ia_instantaneous.append(sf.semfem_i_vec[i,0])

  #print(Ia_instantaneous)

  #DETERMINE PHASE VOLTAGES (USING SEMFEM)
  max_induced_phase_voltage = max(sfm_post.voltage[0])

  Vab = np.zeros(len(sfm_post.voltage[0]))
  for i in range(0,len(sfm_post.voltage[0])):
    Vab[i] = sfm_post.voltage[0][i]-sfm_post.voltage[1][i] #VLL = Va-Vb
  
  peak_single_turn_phase_current = max_input_phase_current
  RMS_single_turn_phase_current = max_input_phase_current/np.sqrt(2)
  
  peak_single_turn_phase_voltage = max_induced_phase_voltage
  RMS_single_turn_phase_voltage = np.sqrt(np.mean(sfm_post.voltage[0]**2))
  
  if three_phase_delta0_wye1 == 1:
    RMS_single_turn_line_current = RMS_single_turn_phase_current
    RMS_single_turn_line_voltage = np.sqrt(np.mean(Vab**2))  
    peak_single_turn_line_voltage = max(abs(max(Vab)),abs(min(Vab)))
  else:
    RMS_single_turn_line_current = np.sqrt(3)*RMS_single_turn_phase_current
    RMS_single_turn_line_voltage = RMS_single_turn_phase_voltage
    peak_single_turn_line_voltage = max(abs(max(sfm_post.voltage[0])),abs(min(sfm_post.voltage[0])))  
  
  peak_single_turn_line_current = RMS_single_turn_line_current*np.sqrt(2)

  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["1 peak_induced_phase_voltage (single_turn)", peak_single_turn_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["1 peak_induced_line_voltage (single_turn)", peak_single_turn_line_voltage, "[V]"])

  if locked_rotor_test == 1:
    if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      synchronous_inductance_single_turn = peak_single_turn_phase_voltage/(peak_single_turn_phase_current*w_e)
      synchronous_inductance = synchronous_inductance_single_turn   
    
  pretty_terminal_table.add_row(["1 synchronous inductance (Ls)", synchronous_inductance_single_turn*1000, "[mH]"])
  pretty_terminal_table.add_row(["1 PM flux linkage (flm)", flm_single_turn*1000, "[mWb]"])
  
  single_turn_induced_phase_voltage_peak = peak_single_turn_phase_voltage
  single_turn_line_current_peak = peak_single_turn_line_current
  single_turn_phase_current_peak = peak_single_turn_phase_current

#USE DQ VALUES TO OBTAIN (LINE CURRENT, PHASE VOLTAGE)
elif steps == 1:
  #DETERMINE MAX INPUT CURRENT (PEAK VALUE)
  max_input_phase_current = current_through_slot
  #max_input_phase_current = iq

  #NB remember at this stage turnsPerCoil is still 1
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["----------------------STAGE 1 ---------------------------------", "## using ONLY 1 step ##", "## obtain single turn voltage and current ##"])

  flq,fld = sf.abc_dq(sf.semfem_flink_vec[0,0], sf.semfem_flink_vec[0,1], sf.semfem_flink_vec[0,2], 0)
  
  #DETERMINE PHASE VOLTAGES (USING DQ VALUES)
  vd_manual = -2*np.pi*f_e*flq
  vq_manual = -2*np.pi*f_e*fld 
  
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["1 vd_manual", vd_manual, "[V]"])
    pretty_terminal_table.add_row(["1 vq_manual", vq_manual, "[V]"])
 
  max_induced_phase_voltage = np.sqrt(vd_manual**2 + vq_manual**2)		#that means that this Va_rms, would need to be slightly higher if for instance eddy current losses were included (however eddy current losses are present even when Ia = 0, meaning it should rather be modeled 
  
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["1 peak_input_phase_current (single_turn)", max_input_phase_current, "[A]"])
    pretty_terminal_table.add_row(["1 peak_induced_phase_voltage (single_turn)", max_induced_phase_voltage, "[V]"])
  
  if three_phase_delta0_wye1 == 1:
    single_turn_induced_phase_voltage_peak = max_induced_phase_voltage
    single_turn_phase_current_peak = max_input_phase_current
    single_turn_line_current_peak = max_input_phase_current*np.sqrt(3)   


terminal_phase_voltage_A = np.zeros(len(sfm_post.voltage[0]))
terminal_phase_voltage_B = np.zeros(len(sfm_post.voltage[0]))
terminal_voltage_AB = np.zeros(len(sfm_post.voltage[0]))

#NOW WE KNOW THE LINE CURRENT AND PHASE VOLTAGE VALUE DUE TO A SINGLE TURN, GET FIRST ESTIMATE OF NUMBER OF TURNS
#DO this only if we are not going to force number of turns
if N < 1 or conductors_round0_bars1 == 1:
  #turnsPerCoil = int(peak_voltage_allowed/single_turn_induced_phase_voltage_peak)	#get integer number which is rounded down so as to not exceed voltage design limit, THIS IS THE FIRST ESTIMATE NUMBER OF TURNS
  #turnsPerCoil = int(VLL_pk_max_allowed/peak_induced_line_voltage)	#get integer number which is rounded down so as to not exceed voltage design limit, THIS IS THE FIRST ESTIMATE NUMBER OF TURNS
  turnsPerCoil = int(RMS_terminal_line_voltage_limit/RMS_single_turn_line_voltage)

  if conductors_round0_bars1 == 1:
    if turnsPerCoil < 1 and conductors_round0_bars1 == 0:
      turnsPerCoil = 1
    elif N > 0:
      turnsPerCoil = number_of_copper_bars_per_coil
      N = turnsPerCoil #force number of turns

  max_induced_phase_voltage = turnsPerCoil*single_turn_induced_phase_voltage_peak
  area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil #area available in turn in MM!
  max_input_phase_current = single_turn_phase_current_peak/turnsPerCoil	#CURRENT VALUE ALSO GETS UPDATED
  single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)
  if use_parallel_strands:
    parallel_stranded_conductors = int(area_available_per_turn/(np.pi*(strand_diameter/2)**2))

  if conductors_round0_bars1:   
    Ra_analytical_per_bar = resistance_analytical(l)*number_of_parallel_circuits
    
  #NOW THAT WE HAVE NUMBER OF TURNS, CAN NOW CALCULATE RA RESISTANCE
  #l_coil = (2*l + 2*np.pi*end_winding_radius)*turnsPerCoil
  l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil
  l_total = (coils_in_series_per_phase*l_coil)+l_series_connections
  if conductor_cu0_al1 == 0: 
    Ra_analytical = resistance_analytical(l_total)
      
  elif conductor_cu0_al1 == 1:
    enable_ac_resistance = 0
    Ra_analytical = (aluminium_resistivity*l_total)/(area_available_per_turn/(1000**2)) #l_total is given in [m] and area_available_per_turn in [mm^2]. Copper resistivity is given in [m]

  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical[enable_ac_resistance]

  for i in range(0,len(sfm_post.voltage[0])):
    terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+(sf.semfem_i_vec[i,0]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]
    terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+(sf.semfem_i_vec[i,1]/turnsPerCoil)*Ra_analytical[enable_ac_resistance] 
    terminal_voltage_AB[i] = terminal_phase_voltage_A[i]-terminal_phase_voltage_B[i]
    
  RMS_terminal_phase_voltage = np.sqrt(np.mean(terminal_phase_voltage_A**2))
  RMS_input_phase_current = max_input_phase_current/np.sqrt(2) #a perfect sine wave is injected

  if three_phase_delta0_wye1 == 1:
    RMS_input_line_current = RMS_input_phase_current
    RMS_terminal_line_voltage = np.sqrt(np.mean(terminal_voltage_AB**2))  
    peak_terminal_line_voltage = max(abs(max(terminal_voltage_AB)),abs(min(terminal_voltage_AB)))
  else:
    RMS_input_line_current = np.sqrt(3)*RMS_input_phase_current
    RMS_terminal_line_voltage = RMS_terminal_phase_voltage
    peak_terminal_line_voltage = max(abs(max(terminal_phase_voltage_A)),abs(min(terminal_phase_voltage_A)))

  Ra_analytical_per_m = [0,0]
  Ra_analytical_per_m = resistance_analytical(1)*number_of_parallel_circuits

  if locked_rotor_test == 1:
    if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      synchronous_inductance = max_induced_phase_voltage/(max_input_phase_current*w_e)
  else:
    synchronous_inductance = synchronous_inductance_single_turn*turnsPerCoil**2
    flm = flm_single_turn*turnsPerCoil

if enable_terminal_printout == 1:
  if (number_of_copper_bars_per_coil == 0 or conductors_round0_bars1 == 0) and N < 1:
    pretty_terminal_table.add_row(["----------------------STAGE 2 ---------------------------------", "## applying voltage limit ##", "## part A ##"])
    pretty_terminal_table.add_row(["2 preliminary_number_of_turns_per_coil", turnsPerCoil, "[turns]"])
    pretty_terminal_table.add_row(["2 area_available_per_turn_THEORETICAL_IDEAL", area_available_per_turn/fill_factor, "[mm^2]"])
    pretty_terminal_table.add_row(["2 area_available_per_turn_FILL_FACTOR_CONSIDERED", area_available_per_turn, "[mm^2]"])
    pretty_terminal_table.add_row(["2 plausible_diameter_of_insulated_conductor", single_turn_conductor_radius*2, "[mm]"])
    if use_parallel_strands:
      pretty_terminal_table.add_row(["2 number_of_parallel_stranded_conductors",parallel_stranded_conductors, "[p.u.]"])
    pretty_terminal_table.add_row(["2 single_coil_wire_length", l_coil, "[m]"])
    pretty_terminal_table.add_row(["2 total_phase_wire_length_devided by_number_of_parallel_circuits", l_total, "[m]"])
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ %d [Hz]"%(f_e), Ra_analytical_per_m[1]*1000, "[mOhm/m]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ DC", Ra_analytical_per_m[0]*1000, "[mOhm/m]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical @ %d [Hz]"%(f_e), Ra_analytical[1]*1000, "[mOhm]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical @ DC", Ra_analytical[0]*1000, "[mOhm]"])

  if number_of_copper_bars_per_coil > 0 and conductors_round0_bars1 == 1:
    pretty_terminal_table.add_row(["----------------------STAGE 2 ---------------------------------", "## using FORCED number of turns ##", "-----------"])
    pretty_terminal_table.add_row(["2 number_of_turns_per_coil", turnsPerCoil, "[turns]"])

    pretty_terminal_table.add_row(["2 area_available_per_turn_THEORETICAL_IDEAL", area_available_per_turn/fill_factor, "[mm^2]"])
    #pretty_terminal_table.add_row(["2 area_available_per_turn_FILL_FACTOR_CONSIDERED", area_available_per_turn, "[mm^2]"])
    pretty_terminal_table.add_row(["2 single_coil_wire_length", l_coil, "[m]"])
    pretty_terminal_table.add_row(["2 total_phase_wire_length_devided by_number_of_parallel_circuits", l_total, "[m]"])
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ %d [Hz]"%(f_e), Ra_analytical_per_m[1]*1000, "[mOhm/m]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ DC", Ra_analytical_per_m[0]*1000, "[mOhm/m]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical @ %d [Hz]"%(f_e), Ra_analytical[1]*1000, "[mOhm]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical @ DC", Ra_analytical[0]*1000, "[mOhm]"])
    pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ %d [Hz]"%(f_e), Ra_analytical_per_bar[1]*1000, "[mOhm]"])
    pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ DC", Ra_analytical_per_bar[0]*1000, "[mOhm]"])

  #this website gives very similar answer to Ra:
  #http://chemandy.com/calculators/round-wire-resistance-calculator.htm
  #peak_voltage_allowed = (VLL_pk_max_allowed*0.95*1.15)/np.sqrt(3)
  if N < 1 or conductors_round0_bars1 == 1:
    #pretty_terminal_table.add_row(["2 V_LL_peak_input", (max_input_phase_voltage*np.sqrt(3))/(0.95*1.15), "[V]"])
    pretty_terminal_table.add_row(["2 -----PHASE QUANTITIES-----","-----" , "-----"])
    pretty_terminal_table.add_row(["2 peak_induced_phase_voltage", max_induced_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 peak_terminal_phase_voltage", max_input_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 peak_input_phase_current", max_input_phase_current, "[A]"])
    pretty_terminal_table.add_row(["2 RMS_terminal_phase_voltage", RMS_terminal_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_input_phase_current", RMS_input_phase_current, "[A]"])
    pretty_terminal_table.add_row(["2 synchronous inductance (Ls)", synchronous_inductance*1000, "[mH]"])
    pretty_terminal_table.add_row(["2 PM flux linkage (flm)", flm*1000, "[mWb]"])
    pretty_terminal_table.add_row(["2 -----LINE QUANTITIES-----","-----" , "-----"])
    pretty_terminal_table.add_row(["2 peak_terminal_line_voltage", peak_terminal_line_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_terminal_line_voltage", RMS_terminal_line_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_input_line_current", RMS_input_line_current, "[A]"])
  


if number_of_copper_bars_per_coil > 0 and conductors_round0_bars1 == 1:
  pass
elif N >= 1: #if we force number of turns
  turnsPerCoil = N
  area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
  single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)
  if use_parallel_strands:
    parallel_stranded_conductors = int(area_available_per_turn/(np.pi*(strand_diameter/2)**2))
  max_induced_phase_voltage = turnsPerCoil*single_turn_induced_phase_voltage_peak  
  max_input_phase_current = single_turn_phase_current_peak/turnsPerCoil
  RMS_input_phase_current = max_input_phase_current/np.sqrt(2) #a perfect sine wave is injected
  l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil
  l_total = (coils_in_series_per_phase*l_coil)+l_series_connections
  if conductor_cu0_al1 == 0:
    Ra_analytical = resistance_analytical(l_total)
    if conductors_round0_bars1:
      Ra_analytical_per_bar = resistance_analytical(l)*number_of_parallel_circuits
 
  elif conductor_cu0_al1 == 1:
    enable_ac_resistance = 0
    #Ra_analytical_DC = (Ra_analytical_per_bar_DC/l)*l_total
    Ra_analytical = (aluminium_resistivity*l_total)/(area_available_per_turn/(1000**2)) #l_total is given in [m] and area_available_per_turn in [mm^2]. Copper resistivity is given in [m]

  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical[enable_ac_resistance]

  for i in range(0,len(sfm_post.voltage[0])):
    terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+(sf.semfem_i_vec[i,0]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]
    terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+(sf.semfem_i_vec[i,1]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]    
    terminal_voltage_AB[i] = terminal_phase_voltage_A[i]-terminal_phase_voltage_B[i]  

  RMS_terminal_phase_voltage = np.sqrt(np.mean(terminal_phase_voltage_A**2))

  if three_phase_delta0_wye1 == 1:
    RMS_input_line_current = RMS_input_phase_current
    RMS_terminal_line_voltage = np.sqrt(np.mean(terminal_voltage_AB**2))  
    peak_terminal_line_voltage = max(abs(max(terminal_voltage_AB)),abs(min(terminal_voltage_AB)))
  else:
    RMS_input_line_current = np.sqrt(3)*RMS_input_phase_current
    RMS_terminal_line_voltage = RMS_terminal_phase_voltage
    peak_terminal_line_voltage = max(abs(max(terminal_phase_voltage_A)),abs(min(terminal_phase_voltage_A)))
  
  Ra_analytical_per_m = resistance_analytical(1)*number_of_parallel_circuits

  if locked_rotor_test == 1:
    if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      synchronous_inductance = max_induced_phase_voltage/(max_input_phase_current*w_e)
  else:
    synchronous_inductance = synchronous_inductance_single_turn*turnsPerCoil**2
    flm = flm_single_turn*turnsPerCoil
  
  if enable_terminal_printout == 1:
    pretty_terminal_table.add_row(["----------------------STAGE 2 ---------------------------------", "## using FORCED number of turns ##", "-----------"])  
    pretty_terminal_table.add_row(["2 FORCED_number_of_turns_per_coil", turnsPerCoil, "[turns]"])
    pretty_terminal_table.add_row(["2 area_available_per_turn_THEORETICAL_IDEAL", area_available_per_turn/fill_factor, "[mm^2]"])
    pretty_terminal_table.add_row(["2 area_available_per_turn_FILL_FACTOR_CONSIDERED", area_available_per_turn, "[mm^2]"])
    pretty_terminal_table.add_row(["2 plausible_diameter_of_insulated_conductor", single_turn_conductor_radius*2, "[mm]"])
    if use_parallel_strands:
      pretty_terminal_table.add_row(["2 number_of_parallel_stranded_conductors",parallel_stranded_conductors, "[p.u.]"])    
    pretty_terminal_table.add_row(["2 single_coil_wire_length", l_coil, "[m]"])
    pretty_terminal_table.add_row(["2 total_phase_wire_length_devided by_number_of_parallel_circuits", l_total, "[m]"])
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ %d [Hz]"%(f_e), Ra_analytical_per_m[1]*1000, "[mOhm/m]"])    
    pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ DC", Ra_analytical_per_m[0]*1000, "[mOhm/m]"])    
    if number_of_copper_bars_per_coil > 0 and conductors_round0_bars1:
      pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ %d [Hz]"%(f_e), Ra_analytical_per_bar[1]*1000, "[mOhm]"])    
      pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ DC"%(f_e), Ra_analytical_per_bar[0]*1000, "[mOhm]"])
    pretty_terminal_table.add_row(["2 Ra_analytical @ %d [Hz]"%(f_e), Ra_analytical[1]*1000, "[mOhm]"]) 
    pretty_terminal_table.add_row(["2 Ra_analytical @ DC", Ra_analytical[0]*1000, "[mOhm]"])
    #pretty_terminal_table.add_row(["2 V_LL_peak_input", (max_input_phase_voltage*np.sqrt(3))/(0.95*1.15), "[V]"])
    pretty_terminal_table.add_row(["2 -----PHASE QUANTITIES-----","-----" , "-----"])
    pretty_terminal_table.add_row(["2 peak_induced_phase_voltage", max_induced_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 peak_terminal_phase_voltage", max_input_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 peak_input_phase_current", max_input_phase_current, "[A]"])
    pretty_terminal_table.add_row(["2 RMS_terminal_phase_voltage", RMS_terminal_phase_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_input_phase_current", RMS_input_phase_current, "[A]"])
    pretty_terminal_table.add_row(["2 synchronous inductance (Ls)", synchronous_inductance*1000, "[mH]"])
    pretty_terminal_table.add_row(["2 PM flux linkage (flm)", flm*1000, "[mWb]"])
    pretty_terminal_table.add_row(["2 -----LINE QUANTITIES-----","-----" , "-----"])
    pretty_terminal_table.add_row(["2 peak_terminal_line_voltage", peak_terminal_line_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_terminal_line_voltage", RMS_terminal_line_voltage, "[V]"])
    pretty_terminal_table.add_row(["2 RMS_input_line_current", RMS_input_line_current, "[A]"])

else:
  #DOES THE ACTUAL TERMINAL PHASE VOLTAGE SATISFY VOLTAGE CONSTRAINT?
  #decrease number of turns until below allowed voltage
  if turnsPerCoil > 1:
    #while peak_terminal_line_voltage > VLL_pk_max_allowed:
    while peak_terminal_line_voltage > peak_terminal_line_voltage_limit:
    #while RMS_terminal_line_voltage > RMS_terminal_line_voltage_limit:
      turnsPerCoil = turnsPerCoil - 1
      
      area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
      single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)
      if use_parallel_strands:
	parallel_stranded_conductors = int(area_available_per_turn/(np.pi*(strand_diameter/2)**2))
      max_induced_phase_voltage = turnsPerCoil*single_turn_induced_phase_voltage_peak
      max_input_phase_current = single_turn_phase_current_peak/turnsPerCoil
      RMS_input_phase_current = max_input_phase_current/np.sqrt(2) #a perfect sine wave is injected

      l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil
      l_total = (coils_in_series_per_phase*l_coil)+l_series_connections

      if conductor_cu0_al1 == 0:
	Ra_analytical = resistance_analytical(l_total)
      elif conductor_cu0_al1 == 1:
	enable_ac_resistance = 0
	#Ra_analytical_DC = (Ra_analytical_per_bar_DC/l)*l_total
	Ra_analytical_DC = (aluminium_resistivity*l_total)/(area_available_per_turn/(1000**2)) #l_total is given in [m] and area_available_per_turn in [mm^2]. Copper resistivity is given in [m]
      
      #max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical_DC
      max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical[enable_ac_resistance]

      for i in range(0,len(sfm_post.voltage[0])):
	#terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+sf.semfem_i_vec[i,0]*Ra_analytical_DC
	#terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+sf.semfem_i_vec[i,1]*Ra_analytical_DC
	terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+(sf.semfem_i_vec[i,0]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]
	terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+(sf.semfem_i_vec[i,1]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]
	
	terminal_voltage_AB[i] = terminal_phase_voltage_A[i]-terminal_phase_voltage_B[i] 

      RMS_terminal_phase_voltage = np.sqrt(np.mean(terminal_phase_voltage_A**2))

      if three_phase_delta0_wye1 == 1:
	RMS_input_line_current = RMS_input_phase_current
	RMS_terminal_line_voltage = np.sqrt(np.mean(terminal_voltage_AB**2))  
	peak_terminal_line_voltage = max(abs(max(terminal_voltage_AB)),abs(min(terminal_voltage_AB)))
      else:
	RMS_input_line_current = np.sqrt(3)*RMS_input_phase_current
	RMS_terminal_line_voltage = RMS_terminal_phase_voltage
	peak_terminal_line_voltage = max(abs(max(terminal_phase_voltage_A)),abs(min(terminal_phase_voltage_A)))

      if turnsPerCoil == 1: break

    Ra_analytical_per_m = resistance_analytical(1)*number_of_parallel_circuits

    if locked_rotor_test == 1:
      if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
	synchronous_inductance = max_induced_phase_voltage/(max_input_phase_current*w_e)
    else:
      synchronous_inductance = synchronous_inductance_single_turn*turnsPerCoil**2
      flm = flm_single_turn*turnsPerCoil

    if enable_terminal_printout == 1:
      pretty_terminal_table.add_row(["----------------------STAGE 2 ---------------------------------", "## applying voltage limit ##", "## part B ##"])
      if enable_voltage_leeway == 1:
	pretty_terminal_table.add_row(["2 preliminary_number_of_turns_per_coil", turnsPerCoil, "[turns]"])
      else:
	pretty_terminal_table.add_row(["2 FINAL_number_of_turns_per_coil", turnsPerCoil, "[turns]"])
	
      pretty_terminal_table.add_row(["2 area_available_per_turn_THEORETICAL_IDEAL", area_available_per_turn/fill_factor, "[mm^2]"])
      pretty_terminal_table.add_row(["2 area_available_per_turn_FILL_FACTOR_CONSIDERED", area_available_per_turn, "[mm^2]"])
      pretty_terminal_table.add_row(["2 plausible_diameter_of_insulated_conductor", single_turn_conductor_radius*2, "[mm]"])
      if use_parallel_strands:
	pretty_terminal_table.add_row(["2 number_of_parallel_stranded_conductors",parallel_stranded_conductors, "[p.u.]"])      
      pretty_terminal_table.add_row(["2 single_coil_wire_length", l_coil, "[m]"])
      pretty_terminal_table.add_row(["2 total_phase_wire_length_devided by_number_of_parallel_circuits", l_total, "[m]"])
      pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ %d [Hz]"%(f_e), Ra_analytical_per_m[1]*1000, "[mOhm/m]"])    
      pretty_terminal_table.add_row(["2 Ra_analytical_per_m @ DC", Ra_analytical_per_m[0]*1000, "[mOhm/m]"])      
      if conductors_round0_bars1 and turnsPerCoil < 2 and number_of_copper_bars_per_coil < 2:
	pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ %d [Hz]"%(f_e), Ra_analytical_per_bar[1]*1000, "[mOhm]"])      
	pretty_terminal_table.add_row(["2 Ra_analytical_per_bar @ DC", Ra_analytical_per_bar[0]*1000, "[mOhm]"])
      pretty_terminal_table.add_row(["2 Ra_analytical @ %d [Hz]"%(f_e), Ra_analytical[1]*1000, "[mOhm]"]) 
      pretty_terminal_table.add_row(["2 Ra_analytical @ DC", Ra_analytical[0]*1000, "[mOhm]"])
      #pretty_terminal_table.add_row(["2 V_LL_peak_input", (max_input_phase_voltage*np.sqrt(3))/(0.95*1.15), "[V]"])
      pretty_terminal_table.add_row(["2 -----PHASE QUANTITIES-----","-----" , "-----"])
      pretty_terminal_table.add_row(["2 peak_induced_phase_voltage", max_induced_phase_voltage, "[V]"])
      pretty_terminal_table.add_row(["2 peak_terminal_phase_voltage", max_input_phase_voltage, "[V]"])
      pretty_terminal_table.add_row(["2 peak_input_phase_current", max_input_phase_current, "[A]"])
      pretty_terminal_table.add_row(["2 RMS_terminal_phase_voltage", RMS_terminal_phase_voltage, "[V]"])
      pretty_terminal_table.add_row(["2 RMS_input_phase_current", RMS_input_phase_current, "[A]"])
      pretty_terminal_table.add_row(["2 synchronous inductance (Ls)", synchronous_inductance*1000, "[mH]"])
      pretty_terminal_table.add_row(["2 PM flux linkage (flm)", flm*1000, "[mWb]"])
      pretty_terminal_table.add_row(["2 -----LINE QUANTITIES-----","-----" , "-----"])
      pretty_terminal_table.add_row(["2 peak_terminal_line_voltage", peak_terminal_line_voltage, "[V]"])
      pretty_terminal_table.add_row(["2 RMS_terminal_line_voltage", RMS_terminal_line_voltage, "[V]"])
      pretty_terminal_table.add_row(["2 RMS_input_line_current", RMS_input_line_current, "[A]"])

    if enable_voltage_leeway == 1:
      turnsPerCoil = turnsPerCoil - 1

      area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
      single_turn_conductor_radius = np.sqrt(area_available_per_turn/np.pi)
      if use_parallel_strands:
	parallel_stranded_conductors = int(area_available_per_turn/(np.pi*(strand_diameter/2)**2))
      max_induced_phase_voltage = turnsPerCoil*single_turn_induced_phase_voltage_peak
      max_input_phase_current = single_turn_phase_current_peak/turnsPerCoil
      RMS_input_phase_current = max_input_phase_current/np.sqrt(2) #a perfect sine wave is injected

      l_coil = 2*(l+(np.pi*end_winding_radius))*turnsPerCoil
      l_total = (coils_in_series_per_phase*l_coil)+l_series_connections

      if conductor_cu0_al1 == 0:
	Ra_analytical = resistance_analytical(l_total)
	if conductors_round0_bars1:
	  Ra_analytical_per_bar = resistance_analytical(l)*number_of_parallel_circuits
	    
      elif conductor_cu0_al1 == 1:
	enable_ac_resistance = 0
	Ra_analytical = (aluminium_resistivity*l_total)/(area_available_per_turn/(1000**2)) #l_total is given in [m] and area_available_per_turn in [mm^2]. Copper resistivity is given in [m]
      #max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical_DC	
      max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical[enable_ac_resistance]	

      for i in range(0,len(sfm_post.voltage[0])):
	#terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+sf.semfem_i_vec[i,0]*Ra_analytical_DC
	#terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+sf.semfem_i_vec[i,1]*Ra_analytical_DC
	terminal_phase_voltage_A[i] = (sfm_post.voltage[0][i]*turnsPerCoil)+(sf.semfem_i_vec[i,0]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]
	terminal_phase_voltage_B[i] = (sfm_post.voltage[1][i]*turnsPerCoil)+(sf.semfem_i_vec[i,1]/turnsPerCoil)*Ra_analytical[enable_ac_resistance]	
	terminal_voltage_AB[i] = terminal_phase_voltage_A[i]-terminal_phase_voltage_B[i]

      RMS_terminal_phase_voltage = np.sqrt(np.mean(terminal_phase_voltage_A**2))

      if three_phase_delta0_wye1 == 1:
	RMS_input_line_current = RMS_input_phase_current
	RMS_terminal_line_voltage = np.sqrt(np.mean(terminal_voltage_AB**2))  
	peak_terminal_line_voltage = max(abs(max(terminal_voltage_AB)),abs(min(terminal_voltage_AB)))
      else:
	RMS_input_line_current = np.sqrt(3)*RMS_input_phase_current
	RMS_terminal_line_voltage = RMS_terminal_phase_voltage
	peak_terminal_line_voltage = max(abs(max(terminal_phase_voltage_A)),abs(min(terminal_phase_voltage_A)))

      Ra_analytical_per_m = resistance_analytical(1)*number_of_parallel_circuits

      if locked_rotor_test == 1:
	if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
	  synchronous_inductance = max_induced_phase_voltage/(max_input_phase_current*w_e)
      else:
	synchronous_inductance = synchronous_inductance_single_turn*turnsPerCoil**2
	flm = flm_single_turn*turnsPerCoil	  
      
      if enable_terminal_printout == 1:
	pretty_terminal_table.add_row(["----------------------STAGE 3 ---------------------------------", "## adding voltage leeway ##", "-----------"])
	pretty_terminal_table.add_row(["5 FINAL_number_of_turns_per_coil", turnsPerCoil, "[turns]"])
	pretty_terminal_table.add_row(["5 area_available_per_turn_THEORETICAL_IDEAL", area_available_per_turn/fill_factor, "[mm^2]"])
	pretty_terminal_table.add_row(["5 area_available_per_turn_FILL_FACTOR_CONSIDERED", area_available_per_turn, "[mm^2]"])
	pretty_terminal_table.add_row(["5 plausible_diameter_of_insulated_conductor", single_turn_conductor_radius*2, "[mm]"])
	if use_parallel_strands:
	  pretty_terminal_table.add_row(["5 number_of_parallel_stranded_conductors",parallel_stranded_conductors, "[p.u.]"])	
	pretty_terminal_table.add_row(["5 single_coil_wire_length", l_coil, "[m]"])
	pretty_terminal_table.add_row(["5 total_phase_wire_length_devided by_number_of_parallel_circuits", l_total, "[m]"])
	pretty_terminal_table.add_row(["5 Ra_analytical_per_m @ %d [Hz]"%(f_e), Ra_analytical_per_m[1]*1000, "[mOhm/m]"])    
	pretty_terminal_table.add_row(["5 Ra_analytical_per_m @ DC", Ra_analytical_per_m[0]*1000, "[mOhm/m]"])	
	if turnsPerCoil < 2 and number_of_copper_bars_per_coil < 2 and conductors_round0_bars1:
	  pretty_terminal_table.add_row(["5 Ra_analytical_per_bar @ %d [Hz]"%(f_e), Ra_analytical_per_bar[1]*1000, "[mOhm]"])	
	  pretty_terminal_table.add_row(["5 Ra_analytical_per_bar @ DC", Ra_analytical_per_bar[0]*1000, "[mOhm]"])
	pretty_terminal_table.add_row(["5 Ra_analytical @ %d [Hz]"%(f_e), Ra_analytical[1]*1000, "[mOhm]"])
	pretty_terminal_table.add_row(["5 Ra_analytical @ DC", Ra_analytical[0]*1000, "[mOhm]"])
	#pretty_terminal_table.add_row(["5 V_LL_peak_input", (max_input_phase_voltage*np.sqrt(3))/(0.95*1.15), "[V]"])
	pretty_terminal_table.add_row(["5 -----PHASE QUANTITIES-----","-----" , "-----"])
	pretty_terminal_table.add_row(["5 peak_induced_phase_voltage", max_induced_phase_voltage, "[V]"])
	pretty_terminal_table.add_row(["5 peak_terminal_phase_voltage", max_input_phase_voltage, "[V]"])
	pretty_terminal_table.add_row(["5 peak_input_phase_current", max_input_phase_current, "[A]"])
	pretty_terminal_table.add_row(["5 RMS_terminal_phase_voltage", RMS_terminal_phase_voltage, "[V]"])
	pretty_terminal_table.add_row(["5 RMS_input_phase_current", RMS_input_phase_current, "[A]"])
	pretty_terminal_table.add_row(["5 synchronous inductance (Ls)", synchronous_inductance*1000, "[mH]"])
	pretty_terminal_table.add_row(["5 PM flux linkage (flm)", flm*1000, "[mWb]"])
	pretty_terminal_table.add_row(["5 -----LINE QUANTITIES-----","-----" , "-----"])
	pretty_terminal_table.add_row(["5 peak_terminal_line_voltage", peak_terminal_line_voltage, "[V]"])
	pretty_terminal_table.add_row(["5 RMS_terminal_line_voltage", RMS_terminal_line_voltage, "[V]"])
	pretty_terminal_table.add_row(["5 RMS_input_line_current", RMS_input_line_current, "[A]"])

if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----------POST ANALYSIS ----------------------------------------", "-----------------------------", "-----------------------------"])
#NOW THAT NUMBER OF TURNS HAS BEEN DETERMINED
########################


#CALCULATE SEMFEM TORQUE
if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["----------------------SEMFEM TORQUE", "-----------", "-----------"])

if args.plot == 0:
  average_torque_semfem = abs(average(sf.semfem_torque_vec[:,1]))
  
  if np.isclose(0,average_torque_semfem,rtol=1e-04, atol=1e-04, equal_nan=False):
    torque_ripple_semfem = 1000
  else:
    torque_ripple_semfem = ((max(sf.semfem_torque_vec[:,1])-min(sf.semfem_torque_vec[:,1]))/average_torque_semfem)*100    
  
  if enable_target_mech_power:
    predicted_torque_difference = predicted_torque-average_torque_dq
    if predicted_torque>0 and torque_target>0:
      torque_prediction_error_percentage = (predicted_torque_difference/predicted_torque)*100
      torque_target_shortfall_due_to_copper_losses_limit_percentage = ((torque_target-predicted_torque)/torque_target)*100
      total_torque_shortfall_percentage = ((torque_target-average_torque_dq)/torque_target)*100
    else:
      torque_prediction_error_percentage = 0
      torque_target_shortfall_due_to_copper_losses_limit_percentage = 0
      total_torque_shortfall_percentage = 0      
  else:
    predicted_torque = 0
    torque_target = 0
    predicted_torque_difference = 0
    torque_prediction_error_percentage = 0
    torque_target_shortfall_due_to_copper_losses_limit_percentage = 0
    total_torque_shortfall_percentage = 0
  
else:
  average_torque_semfem = db_input_select(output_table_name,"average_torque_semfem",primary_key)
  torque_ripple_semfem = db_input_select(output_table_name,"torque_ripple_semfem",primary_key)
  

if enable_terminal_printout == 1:
  if enable_target_mech_power:
    pretty_terminal_table.add_row(["torque_prediction_error_percentage", torque_prediction_error_percentage, "[%]"])
    pretty_terminal_table.add_row(["torque_target_shortfall_due_to_copper_losses_limit_percentage", torque_target_shortfall_due_to_copper_losses_limit_percentage, "[%]"])
    pretty_terminal_table.add_row(["total_torque_shortfall_percentage", total_torque_shortfall_percentage, "[%]"])
    
    pretty_terminal_table.add_row(["torque_prediction_error", predicted_torque_difference, "[Nm]"])
    pretty_terminal_table.add_row(["predicted_torque", predicted_torque, "[Nm]"])
    pretty_terminal_table.add_row(["torque_target", torque_target, "[Nm]"])
    
  pretty_terminal_table.add_row(["average_torque_semfem", average_torque_semfem, "[Nm]"])
  pretty_terminal_table.add_row(["torque_ripple_semfem", torque_ripple_semfem, "[%]"])
  pretty_terminal_table.add_row(["dominant_torque_ripple_harmonic", int(cogging_torque_harmonic), "harmonic number"])
  pretty_terminal_table.add_row(["torque_pk_pk_semfem", max(sf.semfem_torque_vec[:,1])-min(sf.semfem_torque_vec[:,1]), "[Nm]"])
  pretty_terminal_table.add_row(["max_torque_semfem", max(sf.semfem_torque_vec[:,1]), "[Nm]"])
  pretty_terminal_table.add_row(["min_torque_semfem", min(sf.semfem_torque_vec[:,1]), "[Nm]"])

#CALCULATE DQ TORQUE
if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["----------------------DQ QUANTITIES", "-----------", "-----------"])
#calculate new dq current according to new number of turnsPerCoil
#id_new[i], iq_new[i] = sf.abc_dq(sfm_post.voltage[0,i], sfm_post.voltage[1,i], sfm_post.voltage[2,i], i/steps*2.*np.pi)
if args.plot == 0:
  input_phase_A_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,0]]
  input_phase_B_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,1]]
  input_phase_C_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,2]]

  fla = [x*turnsPerCoil for x in sf.semfem_flink_vec[:,0]]
  flb = [x*turnsPerCoil for x in sf.semfem_flink_vec[:,1]]
  flc = [x*turnsPerCoil for x in sf.semfem_flink_vec[:,2]]

  #print(input_phase_A_current)
  #print(input_phase_B_current)
  #print(input_phase_C_current)

  if np.isclose(0,average_torque_dq,rtol=1e-04, atol=1e-04, equal_nan=False):
    torque_ripple_dq = 1000
  else:
    torque_ripple_dq = ((max(torque_dq)-min(torque_dq))/average_torque_dq)*100
    
else:
  average_torque_dq = db_input_select(output_table_name,"average_torque_dq",primary_key)
  torque_ripple_dq = db_input_select(output_table_name,"torque_ripple_dq",primary_key)  
  torque_density = db_input_select(output_table_name,"torque_density",primary_key)  

if plot_input_current:
  input_phase_A_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,0]]
  input_phase_B_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,1]]
  input_phase_C_current = [x/turnsPerCoil for x in sf.semfem_i_vec[:,2]]

  time_range = np.linspace(0,(1/f_e)*1000,steps)

  fig = pl.figure(0)

  #ax=pl.gca()
  #ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.title('Input current')
  pl.plot(time_range,input_phase_A_current,linewidth=LINEWIDTH,label=r'Current A')
  pl.plot(time_range,input_phase_B_current,linewidth=LINEWIDTH,label=r'Current B')
  pl.plot(time_range,input_phase_C_current,linewidth=LINEWIDTH,label=r'Current C')

  pl.xlabel(r'Time [ms]',fontsize=FONTSIZE)
  pl.ylabel(r'Current [A]',fontsize=FONTSIZE)  
  pl.legend(loc='upper right',fontsize=FONTSIZE)  
  pl.grid(True)  

if plot_dq_voltage:
  time_range = np.linspace(0,(1/f_e)*1000,steps)
  va = np.zeros(steps)
  va_peak_detect = np.zeros(steps)
  #vd_calculated = np.zeros(steps)
  #vq_calculated = np.zeros(steps)
  for i in range(0,steps):
    va[i] = np.sqrt(vd[i]**2+vq[i]**2) #this will be a peak phase value, see Umans p579
    va_peak_detect[i] = max([abs(sfm_post.voltage[0][i]),abs(sfm_post.voltage[1][i]),abs(sfm_post.voltage[2][i])])
    #vd_calculated[i] = -w_e*flq[i]#/turnsPerCoil #minus is present with vd, see Umans p578
    #vq_calculated[i] = w_e*fld[i]#/turnsPerCoil
  
  fig = pl.figure(1)
  #pl.plot(time_range,va,linewidth=2,label=r'Peak Phase Voltage derived from dq voltages')
  #pl.plot(time_range,va_peak_detect,linewidth=2,label=r'Peak Detect from various Phase Voltages')
  #pl.plot(time_range,sfm_post.voltage[0],linewidth=2,label=r'Phase Voltage A')
  #pl.plot(time_range,sfm_post.voltage[1],linewidth=2,label=r'Phase Voltage B')
  #pl.plot(time_range,sfm_post.voltage[2],linewidth=2,label=r'Phase Voltage C')

  pl.plot(time_range,vd_calculated,linewidth=2,label=r'Vd derived from dq flux linkages')
  pl.plot(time_range,vq_calculated,linewidth=2,label=r'Vq derived from dq flux linkages')
  pl.plot(time_range,-vd,linewidth=2,label=r'Vd from SEMFEM')
  pl.plot(time_range,-vq,linewidth=2,label=r'Vq from SEMFEM')

  pl.xlabel(r'Time [ms]',fontsize=FONTSIZE)
  pl.ylabel(r'Voltage [V]',fontsize=FONTSIZE)  
  pl.legend(loc='upper right',fontsize=FONTSIZE)

if plot_dq_torque:
  fig = pl.figure(4)
  ax1 = fig.add_subplot(211)
  ax2 = ax1.twiny()
  ax3 = ax1.twinx()  
  #ax=pl.gca()
  #ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.title('DQ Torque vs DQ Flux linkage',fontsize=16,y=1.08)
  #pl.axis(xmin=-180/q,xmax=180/q)

  line1 = ax1.plot(np.arange(steps),-torque_dq,linewidth=LINEWIDTH,label='DQ Torque',c=colors[0])
  #line2 = ax3.plot(np.arange(steps),fld,linewidth=LINEWIDTH,label='fld',c=colors[1])
  #line3 = ax3.plot(np.arange(steps),flq,linewidth=LINEWIDTH,label='flq',c=colors[2])
  line4 = ax1.plot(np.arange(steps),sf.semfem_torque_vec[:,1],linewidth=LINEWIDTH,label='SEMFEM Torque',c=colors[3])

  #ax1.set_xlabel('Step number',fontsize=FONTSIZE)
  ax1.set_ylabel('Torque [Nm]',fontsize=FONTSIZE)
  ax3.set_ylabel('Flux Linkage [Wb]',fontsize=FONTSIZE)
  
  ax3.set_ylim(min([min(fld),min(flq)])*1.1,max([max(fld),max(flq)])*1.1)

  #lines = line1+line2+line3+line4
  lines = line1+line4
  labels = [lyn.get_label() for lyn in lines]
  ax1.legend(lines, labels, loc='upper right',fontsize=FONTSIZE)
  #pl.legend(loc='upper right',fontsize=14)  
  #ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=FONTSIZE, verticalalignment='top', bbox=props)
  ax1.grid(True)
  
  
  ax4 = fig.add_subplot(212)
  ax5 = ax4.twiny()
  ax6 = ax4.twinx()  
  #ax=pl.gca()
  #ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.title('DQ Torque vs DQ Current',fontsize=16,y=1.08)
  #pl.axis(xmin=-180/q,xmax=180/q)

  line1 = ax4.plot(np.arange(steps),torque_dq,linewidth=LINEWIDTH,label='DQ Torque',c='b')
  line2 = ax6.plot(np.arange(steps),id_new,linewidth=LINEWIDTH,label='id',c='g')
  line3 = ax6.plot(np.arange(steps),iq_new,linewidth=LINEWIDTH,label='iq',c='r')

  ax4.set_xlabel('Step number',fontsize=FONTSIZE)
  ax4.set_ylabel('Torque [Nm]',fontsize=FONTSIZE)
  ax6.set_ylabel('Current [A]',fontsize=FONTSIZE)
  
  ax6.set_ylim(min([min(id_new),min(iq_new)])*1.1,max([max(id_new),max(iq_new)])*1.1)

  lines = line1+line2+line3
  labels = [lyn.get_label() for lyn in lines]
  ax4.legend(lines, labels, loc='upper right',fontsize=FONTSIZE)
  #pl.legend(loc='upper right',fontsize=14)  
  #ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=FONTSIZE, verticalalignment='top', bbox=props)
  ax4.grid(True)  
  

if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["average_torque_dq", average_torque_dq, "[Nm]"])
  pretty_terminal_table.add_row(["torque_ripple_dq", torque_ripple_dq, "[%]"])
  pretty_terminal_table.add_row(["torque_density_(total_mass)", torque_density, "[Nm/kg]"])
  #if args.plot == 0:
    #pretty_terminal_table.add_row(["average_fld", np.average(fld), "[Wb]"])
    #pretty_terminal_table.add_row(["average_flq", np.average(flq), "[Wb]"])
    #pretty_terminal_table.add_row(["fld_ripple", ((max(fld)-min(fld))/np.average(fld))*100, "[%]"])
    #pretty_terminal_table.add_row(["flq_ripple", ((max(flq)-min(flq))/np.average(flq))*100, "[%]"])
    #pretty_terminal_table.add_row(["id (constant)", id_new[0], "[A]"])
    #pretty_terminal_table.add_row(["iq (constant)", iq_new[0], "[A]"])

simulation_time_end = time.time()

fplfiles = []
for i in range(0,steps):  
  fplfiles.append("project_sorspm_semfem/results/fieldplots/band_machine_field%003d.fpl"%(i+1))

results_dir = "project_sorspm_semfem/results/fieldplots/"

################################
#CALCULATE EDDY CURRENT LOSSES #
################################

p_eddy = 0

if enable_eddy_current_losses:
  eddy_time_start = time.time()
  calculate_other_phases = 0
  calculate_other_coil_sides = 1
  #number_of_layers_eddy = 6 #fine for mesh=2.0, speed_up_air_regions = 1.0
  #number_of_layers_eddy = 5 #fine for mesh=3.0, speed_up_air_regions = 1.0
  number_of_layers_eddy = 4 #also fine for mesh=3.0, speed_up_air_regions = 1.0
  eddy_bar_method = 0
  use_single_B_value = 0
  max_harmonics_eddy = 15	#this number is not used when flared teeth and copper bars are implemented
  corrective_factor = 2
  #M = steps
  #M = max_harmonics_eddy
  M = 31
  plot_eddy_current_graphs = 0
  #fft_size = int(M/2)+1
  
  #samples = steps #used to be steps but took to long to solve for large number of steps
  samples = M
  skip_ratio = int(steps/samples)
  samples = int(steps/skip_ratio)
  
  skip_fplfiles = []
  j = 0
  for i in range(0,samples):
    skip_fplfiles.append(fplfiles[j])
    j = j + skip_ratio
  
  if conductors_round0_bars1 == 1:
    max_harmonics_eddy = samples
  
  if use_parallel_strands:
    if parallel_stranded_conductors>0:
      wire_diameter = strand_diameter/1000
  else:
    wire_diameter = (2*single_turn_conductor_radius)/1000
  
  if conductors_round0_bars1 == 0:
    Br_phase_1_side_1 = np.zeros([number_of_layers_eddy,samples])
    Br_phase_1_side_2 = np.zeros([number_of_layers_eddy,samples])
    if calculate_other_phases:
      Br_phase_2_side_1 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_2_side_2 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_3_side_1 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_3_side_2 = np.zeros([number_of_layers_eddy,samples])  
    
    Bt_phase_1_side_1 = np.zeros([number_of_layers_eddy,samples])
    Bt_phase_1_side_2 = np.zeros([number_of_layers_eddy,samples])
    if calculate_other_phases:
      Bt_phase_2_side_1 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_2_side_2 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_3_side_1 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_3_side_2 = np.zeros([number_of_layers_eddy,samples])  
  
  elif conductors_round0_bars1 == 1:
    Br_phase_1_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
    Br_phase_1_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples])
    Bt_phase_1_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
    Bt_phase_1_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples])

    if calculate_other_phases:
      Br_phase_2_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_2_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_3_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_3_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples])   

      Bt_phase_2_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_2_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_3_side_1_bar_1 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_3_side_2_bar_1 = np.zeros([number_of_layers_eddy,samples]) 
    
    if number_of_copper_bars_per_coil == 2:
      Br_phase_1_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
      Br_phase_1_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])    
      Bt_phase_1_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
      Bt_phase_1_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])
      if calculate_other_phases:
	Br_phase_2_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Br_phase_2_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Br_phase_3_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Br_phase_3_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])	
	
	Bt_phase_2_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Bt_phase_2_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Bt_phase_3_side_1_bar_2 = np.zeros([number_of_layers_eddy,samples])
	Bt_phase_3_side_2_bar_2 = np.zeros([number_of_layers_eddy,samples])   
  
  for i in range(0,samples):  
    conductors_per_layer_per_coil_side = (turnsPerCoil*parallel_stranded_conductors)/number_of_layers_eddy  
    
    if flare_teeth:
      if conductors_round0_bars1 == 0:
	line_through_phase_1_coil_side = []
	line_through_phase_2_coil_side = []
	line_through_phase_3_coil_side = []
	
	#2 coil sides to analyse for each phase
	#phase 1 coil side 1
	tmp_bot = coil_side_bot_top_edges_cplx_pnt[0][0]
	tmp_top = coil_side_bot_top_edges_cplx_pnt[0][-1]
	line_through_phase_1_coil_side.append(np.linspace(tmp_bot,tmp_top,number_of_layers_eddy+1, endpoint=False))

	if calculate_other_phases:
	  #phase 2 coil side 1
	  tmp_bot_2 = tmp_bot*np.exp(1j*coil_pitch)
	  tmp_top_2 = tmp_top*np.exp(1j*coil_pitch)
	  line_through_phase_2_coil_side.append(np.linspace(tmp_bot_2,tmp_top_2,number_of_layers_eddy+1, endpoint=False))
	  
	  #phase 3 coil side 1
	  tmp_bot_3 = tmp_bot_2*np.exp(1j*coil_pitch)
	  tmp_top_3 = tmp_top_2*np.exp(1j*coil_pitch)
	  line_through_phase_3_coil_side.append(np.linspace(tmp_bot_3,tmp_top_3,number_of_layers_eddy+1, endpoint=False)) 

	#phase 1 coil side 2
	tmp_bot = np.conj(coil_side_bot_top_edges_cplx_pnt[0][0])*np.exp(1j*(coil_pitch))
	tmp_top = np.conj(coil_side_bot_top_edges_cplx_pnt[0][-1])*np.exp(1j*(coil_pitch))
	line_through_phase_1_coil_side.append(np.linspace(tmp_bot,tmp_top,number_of_layers_eddy+1, endpoint=False))

	if calculate_other_phases:
	  #phase 2 coil side 2
	  tmp_bot_2 = tmp_bot*np.exp(1j*coil_pitch)
	  tmp_top_2 = tmp_top*np.exp(1j*coil_pitch)
	  line_through_phase_2_coil_side.append(np.linspace(tmp_bot_2,tmp_top_2,number_of_layers_eddy+1, endpoint=False))
	  
	  #phase 3 coil side 2
	  tmp_bot_3 = tmp_bot_2*np.exp(1j*coil_pitch)
	  tmp_top_3 = tmp_top_2*np.exp(1j*coil_pitch)
	  line_through_phase_3_coil_side.append(np.linspace(tmp_bot_3,tmp_top_3,number_of_layers_eddy+1, endpoint=False))             
	  
      elif conductors_round0_bars1 == 1:
	line_through_phase_1_coil_side_bar_1 = []
	line_through_phase_1_coil_side_bar_2 = []
	line_through_phase_2_coil_side_bar_1 = []
	line_through_phase_2_coil_side_bar_2 = []
	line_through_phase_3_coil_side_bar_1 = []      
	line_through_phase_3_coil_side_bar_2 = []  
	
	#2 coil sides to analyse for each phase
	#phase 1 coil side 1
	tmp_bot_1_bar_1 = coil_side_bot_top_edges_cplx_pnt[0][0]
	tmp_top_1_bar_1 = coil_side_bot_top_edges_cplx_pnt[0][1]
	line_through_phase_1_coil_side_bar_1.append(np.linspace(tmp_bot_1_bar_1,tmp_top_1_bar_1,number_of_layers_eddy+1, endpoint=False))	

	if number_of_copper_bars_per_coil == 2:
	  tmp_bot_1_bar_2 = coil_side_bot_top_edges_cplx_pnt[0][2]
	  tmp_top_1_bar_2 = coil_side_bot_top_edges_cplx_pnt[0][-1]
	  line_through_phase_1_coil_side_bar_2.append(np.linspace(tmp_bot_1_bar_2,tmp_top_1_bar_2,number_of_layers_eddy+1, endpoint=False))

	if calculate_other_phases:
	  tmp_bot_2_bar_1 = tmp_bot_1_bar_1*np.exp(1j*coil_pitch)
	  tmp_top_2_bar_1 = tmp_top_1_bar_1*np.exp(1j*coil_pitch)
	  line_through_phase_2_coil_side_bar_1.append(np.linspace(tmp_bot_2_bar_1,tmp_top_2_bar_1,number_of_layers_eddy+1, endpoint=False))

	  if number_of_copper_bars_per_coil == 2:
	    tmp_bot_2_bar_2 = tmp_bot_1_bar_2*np.exp(1j*coil_pitch)
	    tmp_top_2_bar_2 = tmp_top_1_bar_2*np.exp(1j*coil_pitch)
	    line_through_phase_2_coil_side_bar_2.append(np.linspace(tmp_bot_2_bar_2,tmp_top_2_bar_2,number_of_layers_eddy+1, endpoint=False))	    
    
	  tmp_bot_3_bar_1 = tmp_bot_2_bar_1*np.exp(1j*coil_pitch)
	  tmp_top_3_bar_1 = tmp_top_2_bar_1*np.exp(1j*coil_pitch)
	  line_through_phase_3_coil_side_bar_1.append(np.linspace(tmp_bot_3_bar_1,tmp_top_3_bar_1,number_of_layers_eddy+1, endpoint=False))

	  if number_of_copper_bars_per_coil == 2:
	    tmp_bot_3_bar_2 = tmp_bot_2_bar_2*np.exp(1j*coil_pitch)
	    tmp_top_3_bar_2 = tmp_top_2_bar_2*np.exp(1j*coil_pitch)
	    line_through_phase_3_coil_side_bar_2.append(np.linspace(tmp_bot_3_bar_2,tmp_top_3_bar_2,number_of_layers_eddy+1, endpoint=False))    
    
    
	#phase 1 coil side 2
	tmp_bot_1_bar_1 = np.conj(coil_side_bot_top_edges_cplx_pnt[0][0])*np.exp(1j*(coil_pitch))
	tmp_top_1_bar_1 = np.conj(coil_side_bot_top_edges_cplx_pnt[0][1])*np.exp(1j*(coil_pitch))
	line_through_phase_1_coil_side_bar_1.append(np.linspace(tmp_bot_1_bar_1,tmp_top_1_bar_1,number_of_layers_eddy+1, endpoint=False))    

	if number_of_copper_bars_per_coil == 2:
	  tmp_bot_1_bar_2  = np.conj(coil_side_bot_top_edges_cplx_pnt[0][2])*np.exp(1j*(coil_pitch))
	  tmp_top_1_bar_2 = np.conj(coil_side_bot_top_edges_cplx_pnt[0][-1])*np.exp(1j*(coil_pitch))
	  line_through_phase_1_coil_side_bar_2.append(np.linspace(tmp_bot_1_bar_2 ,tmp_top_1_bar_2,number_of_layers_eddy+1, endpoint=False)) 
    
	if calculate_other_phases:
	  tmp_bot_2_bar_1 = tmp_bot_1_bar_1*np.exp(1j*coil_pitch)
	  tmp_top_2_bar_1 = tmp_top_1_bar_1*np.exp(1j*coil_pitch)
	  line_through_phase_2_coil_side_bar_1.append(np.linspace(tmp_bot_2_bar_1,tmp_top_2_bar_1,number_of_layers_eddy+1, endpoint=False))

	  if number_of_copper_bars_per_coil == 2:
	    tmp_bot_2_bar_2 = tmp_bot_1_bar_2*np.exp(1j*coil_pitch)
	    tmp_top_2_bar_2 = tmp_top_1_bar_2*np.exp(1j*coil_pitch)
	    line_through_phase_2_coil_side_bar_2.append(np.linspace(tmp_bot_2_bar_2,tmp_top_2_bar_2,number_of_layers_eddy+1, endpoint=False))	    
    
	  tmp_bot_3_bar_1 = tmp_bot_2_bar_1*np.exp(1j*coil_pitch)
	  tmp_top_3_bar_1 = tmp_top_2_bar_1*np.exp(1j*coil_pitch)
	  line_through_phase_3_coil_side_bar_1.append(np.linspace(tmp_bot_3_bar_1,tmp_top_3_bar_1,number_of_layers_eddy+1, endpoint=False))

	  if number_of_copper_bars_per_coil == 2:
	    tmp_bot_3_bar_2 = tmp_bot_2_bar_2*np.exp(1j*coil_pitch)
	    tmp_top_3_bar_2 = tmp_top_2_bar_2*np.exp(1j*coil_pitch)
	    line_through_phase_3_coil_side_bar_2.append(np.linspace(tmp_bot_3_bar_2,tmp_top_3_bar_2,number_of_layers_eddy+1, endpoint=False))    
    
    
    
    #open slots (non flared teeth)
    else:
      line_through_phase_1_coil_side = []
      line_through_phase_2_coil_side = []
      line_through_phase_3_coil_side = []
      
      #2 coil sides to analyse for each phase
      #phase 1 coil side 1
      tmp_bot = np.conj(coil_side_bot_top_edges_cplx_pnt[0][0])*np.exp(1j*(0.5*coil_pitch))
      tmp_top = np.conj(coil_side_bot_top_edges_cplx_pnt[0][-1])*np.exp(1j*(0.5*coil_pitch))
      line_through_phase_1_coil_side.append(np.linspace(tmp_bot,tmp_top,number_of_layers_eddy+1, endpoint=False))
      
      if calculate_other_phases:
	#phase 2 coil side 1
	tmp_bot_2 = tmp_bot*np.exp(1j*coil_pitch)
	tmp_top_2 = tmp_top*np.exp(1j*coil_pitch)
	line_through_phase_2_coil_side.append(np.linspace(tmp_bot_2,tmp_top_2,number_of_layers_eddy+1, endpoint=False))
	
	#phase 3 coil side 1
	tmp_bot_3 = tmp_bot_2*np.exp(1j*coil_pitch)
	tmp_top_3 = tmp_top_2*np.exp(1j*coil_pitch)
	line_through_phase_3_coil_side.append(np.linspace(tmp_bot_3,tmp_top_3,number_of_layers_eddy+1, endpoint=False)) 

      #phase 1 coil side 2
      tmp_bot = coil_side_bot_top_edges_cplx_pnt[0][0]*np.exp(1j*(0.5*coil_pitch))
      tmp_top = coil_side_bot_top_edges_cplx_pnt[0][-1]*np.exp(1j*(0.5*coil_pitch))
      line_through_phase_1_coil_side.append(np.linspace(tmp_bot,tmp_top,number_of_layers_eddy+1, endpoint=False))

      if calculate_other_phases:
	#phase 2 coil side 2
	tmp_bot_2 = tmp_bot*np.exp(1j*coil_pitch)
	tmp_top_2 = tmp_top*np.exp(1j*coil_pitch)
	line_through_phase_2_coil_side.append(np.linspace(tmp_bot_2,tmp_top_2,number_of_layers_eddy+1, endpoint=False))
	
	#phase 3 coil side 2
	tmp_bot_3 = tmp_bot_2*np.exp(1j*coil_pitch)
	tmp_top_3 = tmp_top_2*np.exp(1j*coil_pitch)
	line_through_phase_3_coil_side.append(np.linspace(tmp_bot_3,tmp_top_3,number_of_layers_eddy+1, endpoint=False))     
     	  
    #now we have determined the coordinates of the layer positions regardless of the stator topology
    for k in range (0, number_of_layers_eddy): #for each layer
      #each layer's start and end angle will be sligthly different since the coils are not perfectly radially aligned
      
      if conductors_round0_bars1 == 0:
	theta_start=np.angle(line_through_phase_1_coil_side[0][k+1])
	theta_end=np.angle(line_through_phase_1_coil_side[1][k+1])
	
	Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start,theta_end, np.absolute(line_through_phase_1_coil_side[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)
	Br_phase_1_side_1[k][i] = Br_tmp[0]
	Br_phase_1_side_2[k][i] = Br_tmp[1]
	Bt_phase_1_side_1[k][i] = Bt_tmp[0]
	Bt_phase_1_side_2[k][i] = Bt_tmp[1]	

	if calculate_other_phases:
	  theta_start=np.angle(line_through_phase_2_coil_side[0][k+1])
	  theta_end=np.angle(line_through_phase_2_coil_side[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start,theta_end, np.absolute(line_through_phase_2_coil_side[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)
	  Br_phase_2_side_1[k][i] = Br_tmp[0]
	  Br_phase_2_side_2[k][i] = Br_tmp[1]
	  Bt_phase_2_side_1[k][i] = Bt_tmp[0]
	  Bt_phase_2_side_2[k][i] = Bt_tmp[1]

	  theta_start=np.angle(line_through_phase_3_coil_side[0][k+1])
	  theta_end=np.angle(line_through_phase_3_coil_side[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start,theta_end, np.absolute(line_through_phase_3_coil_side[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)
	  Br_phase_3_side_1[k][i] = Br_tmp[0]
	  Br_phase_3_side_2[k][i] = Br_tmp[1]
	  Bt_phase_3_side_1[k][i] = Bt_tmp[0]
	  Bt_phase_3_side_2[k][i] = Bt_tmp[1]
      
      elif conductors_round0_bars1 == 1:
	if flare_teeth == 1:
	  theta_start_bar_1=np.angle(line_through_phase_1_coil_side_bar_1[0][k+1])
	  theta_end_bar_1=np.angle(line_through_phase_1_coil_side_bar_1[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_1,theta_end_bar_1, np.absolute(line_through_phase_1_coil_side_bar_1[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)
	else:
	  theta_start_bar_1=np.angle(line_through_phase_1_coil_side[0][k+1])
	  theta_end_bar_1=np.angle(line_through_phase_1_coil_side[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_1,theta_end_bar_1, np.absolute(line_through_phase_1_coil_side[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)	  
	
	Br_phase_1_side_1_bar_1[k][i] = Br_tmp[0]
	Br_phase_1_side_2_bar_1[k][i] = Br_tmp[1]
	Bt_phase_1_side_1_bar_1[k][i] = Bt_tmp[0]
	Bt_phase_1_side_2_bar_1[k][i] = Bt_tmp[1]
	
	if calculate_other_phases:
	  theta_start_bar_1=np.angle(line_through_phase_2_coil_side_bar_1[0][k+1])
	  theta_end_bar_1=np.angle(line_through_phase_2_coil_side_bar_1[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_1,theta_end_bar_1, np.absolute(line_through_phase_2_coil_side_bar_1[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)	  
	
	  Br_phase_2_side_1_bar_1[k][i] = Br_tmp[0]
	  Br_phase_2_side_2_bar_1[k][i] = Br_tmp[1]
	  Bt_phase_2_side_1_bar_1[k][i] = Bt_tmp[0]
	  Bt_phase_2_side_2_bar_1[k][i] = Bt_tmp[1]	

	  theta_start_bar_1=np.angle(line_through_phase_3_coil_side_bar_1[0][k+1])
	  theta_end_bar_1=np.angle(line_through_phase_3_coil_side_bar_1[1][k+1])
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_1,theta_end_bar_1, np.absolute(line_through_phase_3_coil_side_bar_1[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)	  
	
	  Br_phase_3_side_1_bar_1[k][i] = Br_tmp[0]
	  Br_phase_3_side_2_bar_1[k][i] = Br_tmp[1]
	  Bt_phase_3_side_1_bar_1[k][i] = Bt_tmp[0]
	  Bt_phase_3_side_2_bar_1[k][i] = Bt_tmp[1]	
	
	
	if number_of_copper_bars_per_coil == 2:
	  theta_start_bar_2=np.angle(line_through_phase_1_coil_side_bar_2[0][k+1])
	  theta_end_bar_2=np.angle(line_through_phase_1_coil_side_bar_2[1][k+1])
	  
	  Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_2,theta_end_bar_2, np.absolute(line_through_phase_1_coil_side_bar_2[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)
	  Br_phase_1_side_1_bar_2[k][i] = Br_tmp[0]
	  Br_phase_1_side_2_bar_2[k][i] = Br_tmp[1]
	  Bt_phase_1_side_1_bar_2[k][i] = Bt_tmp[0]
	  Bt_phase_1_side_2_bar_2[k][i] = Bt_tmp[1]	
	  
	  if calculate_other_phases:
	    theta_start_bar_2=np.angle(line_through_phase_2_coil_side_bar_2[0][k+1])
	    theta_end_bar_2=np.angle(line_through_phase_2_coil_side_bar_2[1][k+1])
	    Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_2,theta_end_bar_2, np.absolute(line_through_phase_2_coil_side_bar_2[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)	  
	  
	    Br_phase_2_side_1_bar_2[k][i] = Br_tmp[0]
	    Br_phase_2_side_2_bar_2[k][i] = Br_tmp[1]
	    Bt_phase_2_side_1_bar_2[k][i] = Bt_tmp[0]
	    Bt_phase_2_side_2_bar_2[k][i] = Bt_tmp[1]	

	    theta_start_bar_2=np.angle(line_through_phase_3_coil_side_bar_2[0][k+1])
	    theta_end_bar_2=np.angle(line_through_phase_3_coil_side_bar_2[1][k+1])
	    Bm, Br_tmp,Bt_tmp = sf.get_arc_B(skip_fplfiles[i], theta_start_bar_2,theta_end_bar_2, np.absolute(line_through_phase_3_coil_side_bar_2[0][k+1]), 2) #returns 2 B values (one for each coil side, within the specified layer)	  
	  
	    Br_phase_3_side_1_bar_2[k][i] = Br_tmp[0]
	    Br_phase_3_side_2_bar_2[k][i] = Br_tmp[1]
	    Bt_phase_3_side_1_bar_2[k][i] = Bt_tmp[0]
	    Bt_phase_3_side_2_bar_2[k][i] = Bt_tmp[1]	  
	  
	  
  
  pretty_eddy = PrettyTable(['Phase','Coil Side','P_eddy'])
  pretty_eddy.align = 'l'
  pretty_eddy.border= True    

  pretty_eddy_bar = PrettyTable(['Phase','Coil Side','Bar','P_eddy'])
  pretty_eddy_bar.align = 'l'
  pretty_eddy_bar.border= True 

  pretty_eddy_bar_extra = PrettyTable(['Bar','P_eddy per bar', 'P_eddy'])
  pretty_eddy_bar_extra.align = 'l'
  pretty_eddy_bar_extra.border= True  
  
  #the GOAL here is to determine the eddy current losses from only one coil side
  hos_phase_1_side_1 = 0
  hos_phase_1_side_2 = 0
  hos_phase_2_side_1 = 0
  hos_phase_2_side_2 = 0
  hos_phase_3_side_1 = 0
  hos_phase_3_side_2 = 0
  aux_constant = (np.pi * l * (wire_diameter**4) * w_e**2)/(64*copper_resistivity*corrective_factor)


  avg_Br_phase_1_side_1_bar_1 = np.zeros(samples)
  avg_Bt_phase_1_side_1_bar_1 = np.zeros(samples)
  avg_Br_phase_1_side_1_bar_2 = np.zeros(samples)
  avg_Bt_phase_1_side_1_bar_2 = np.zeros(samples)
  
  if calculate_other_coil_sides:
    avg_Br_phase_1_side_2_bar_1 = np.zeros(samples)
    avg_Bt_phase_1_side_2_bar_1 = np.zeros(samples)
    avg_Br_phase_1_side_2_bar_2 = np.zeros(samples)
    avg_Bt_phase_1_side_2_bar_2 = np.zeros(samples)
  
  if calculate_other_phases:
  #phase2  if calculate_other_coil_sides:
    avg_Br_phase_1_side_2_bar_1 = np.zeros(samples)
    avg_Bt_phase_1_side_2_bar_1 = np.zeros(samples)
    avg_Br_phase_1_side_2_bar_2 = np.zeros(samples)
    avg_Bt_phase_1_side_2_bar_2 = np.zeros(samples)
  
  if calculate_other_phases:
  #phase2
    avg_Br_phase_2_side_1_bar_1 = np.zeros(samples)
    avg_Bt_phase_2_side_1_bar_1 = np.zeros(samples)
    avg_Br_phase_2_side_1_bar_2 = np.zeros(samples)
    avg_Bt_phase_2_side_1_bar_2 = np.zeros(samples)
    
    if calculate_other_coil_sides:
      avg_Br_phase_2_side_2_bar_1 = np.zeros(samples)
      avg_Bt_phase_2_side_2_bar_1 = np.zeros(samples)
      avg_Br_phase_2_side_2_bar_2 = np.zeros(samples)
      avg_Bt_phase_2_side_2_bar_2 = np.zeros(samples)  
  
  #phase3
    avg_Br_phase_3_side_1_bar_1 = np.zeros(samples)
    avg_Bt_phase_3_side_1_bar_1 = np.zeros(samples)
    avg_Br_phase_3_side_1_bar_2 = np.zeros(samples)
    avg_Bt_phase_3_side_1_bar_2 = np.zeros(samples)
    
    if calculate_other_coil_sides:
      avg_Br_phase_3_side_2_bar_1 = np.zeros(samples)
      avg_Bt_phase_3_side_2_bar_1 = np.zeros(samples)
      avg_Br_phase_3_side_2_bar_2 = np.zeros(samples)
      avg_Bt_phase_3_side_2_bar_2 = np.zeros(samples)
    avg_Br_phase_2_side_1_bar_1 = np.zeros(samples)
    avg_Bt_phase_2_side_1_bar_1 = np.zeros(samples)
    avg_Br_phase_2_side_1_bar_2 = np.zeros(samples)
    avg_Bt_phase_2_side_1_bar_2 = np.zeros(samples)
    
    if calculate_other_coil_sides:
      avg_Br_phase_2_side_2_bar_1 = np.zeros(samples)
      avg_Bt_phase_2_side_2_bar_1 = np.zeros(samples)
      avg_Br_phase_2_side_2_bar_2 = np.zeros(samples)
      avg_Bt_phase_2_side_2_bar_2 = np.zeros(samples)  
  
  #phase3
    avg_Br_phase_3_side_1_bar_1 = np.zeros(samples)
    avg_Bt_phase_3_side_1_bar_1 = np.zeros(samples)
    avg_Br_phase_3_side_1_bar_2 = np.zeros(samples)
    avg_Bt_phase_3_side_1_bar_2 = np.zeros(samples)
    
    if calculate_other_coil_sides:
      avg_Br_phase_3_side_2_bar_1 = np.zeros(samples)
      avg_Bt_phase_3_side_2_bar_1 = np.zeros(samples)
      avg_Br_phase_3_side_2_bar_2 = np.zeros(samples)
      avg_Bt_phase_3_side_2_bar_2 = np.zeros(samples)  
  
  for i in range(0,samples):
    for k in range(0,number_of_layers_eddy):
      avg_Br_phase_1_side_1_bar_1[i] = avg_Br_phase_1_side_1_bar_1[i] + Br_phase_1_side_1_bar_1[k][i]
      avg_Bt_phase_1_side_1_bar_1[i] = avg_Bt_phase_1_side_1_bar_1[i] + Bt_phase_1_side_1_bar_1[k][i]      
      avg_Br_phase_1_side_1_bar_2[i] = avg_Br_phase_1_side_1_bar_2[i] + Br_phase_1_side_1_bar_2[k][i]
      avg_Bt_phase_1_side_1_bar_2[i] = avg_Bt_phase_1_side_1_bar_2[i] + Bt_phase_1_side_1_bar_2[k][i]
      
      if calculate_other_coil_sides:
	avg_Br_phase_1_side_2_bar_1[i] = avg_Br_phase_1_side_2_bar_1[i] + Br_phase_1_side_2_bar_1[k][i]
	avg_Bt_phase_1_side_2_bar_1[i] = avg_Bt_phase_1_side_2_bar_1[i] + Bt_phase_1_side_2_bar_1[k][i]      
	avg_Br_phase_1_side_2_bar_2[i] = avg_Br_phase_1_side_2_bar_2[i] + Br_phase_1_side_2_bar_2[k][i]
	avg_Bt_phase_1_side_2_bar_2[i] = avg_Bt_phase_1_side_2_bar_2[i] + Bt_phase_1_side_2_bar_2[k][i]	
    
      if calculate_other_phases:
      #phase2
	avg_Br_phase_2_side_1_bar_1[i] = avg_Br_phase_2_side_1_bar_1[i] + Br_phase_2_side_1_bar_1[k][i]
	avg_Bt_phase_2_side_1_bar_1[i] = avg_Bt_phase_2_side_1_bar_1[i] + Bt_phase_2_side_1_bar_1[k][i]      
	avg_Br_phase_2_side_1_bar_2[i] = avg_Br_phase_2_side_1_bar_2[i] + Br_phase_2_side_1_bar_2[k][i]
	avg_Bt_phase_2_side_1_bar_2[i] = avg_Bt_phase_2_side_1_bar_2[i] + Bt_phase_2_side_1_bar_2[k][i]
	
	if calculate_other_coil_sides:
	  avg_Br_phase_2_side_2_bar_1[i] = avg_Br_phase_2_side_2_bar_1[i] + Br_phase_2_side_2_bar_1[k][i]
	  avg_Bt_phase_2_side_2_bar_1[i] = avg_Bt_phase_2_side_2_bar_1[i] + Bt_phase_2_side_2_bar_1[k][i]      
	  avg_Br_phase_2_side_2_bar_2[i] = avg_Br_phase_2_side_2_bar_2[i] + Br_phase_2_side_2_bar_2[k][i]
	  avg_Bt_phase_2_side_2_bar_2[i] = avg_Bt_phase_2_side_2_bar_2[i] + Bt_phase_2_side_2_bar_2[k][i]	
	
      #phase3
	avg_Br_phase_3_side_1_bar_1[i] = avg_Br_phase_3_side_1_bar_1[i] + Br_phase_3_side_1_bar_1[k][i]
	avg_Bt_phase_3_side_1_bar_1[i] = avg_Bt_phase_3_side_1_bar_1[i] + Bt_phase_3_side_1_bar_1[k][i]      
	avg_Br_phase_3_side_1_bar_2[i] = avg_Br_phase_3_side_1_bar_2[i] + Br_phase_3_side_1_bar_2[k][i]
	avg_Bt_phase_3_side_1_bar_2[i] = avg_Bt_phase_3_side_1_bar_2[i] + Bt_phase_3_side_1_bar_2[k][i]
	
	if calculate_other_coil_sides:
	  avg_Br_phase_3_side_2_bar_1[i] = avg_Br_phase_3_side_2_bar_1[i] + Br_phase_3_side_2_bar_1[k][i]
	  avg_Bt_phase_3_side_2_bar_1[i] = avg_Bt_phase_3_side_2_bar_1[i] + Bt_phase_3_side_2_bar_1[k][i]      
	  avg_Br_phase_3_side_2_bar_2[i] = avg_Br_phase_3_side_2_bar_2[i] + Br_phase_3_side_2_bar_2[k][i]
	  avg_Bt_phase_3_side_2_bar_2[i] = avg_Bt_phase_3_side_2_bar_2[i] + Bt_phase_3_side_2_bar_2[k][i]
	
	
	
    
    #this is now a single average value obtained by various points/layers within each bar
    avg_Br_phase_1_side_1_bar_1[i] = avg_Br_phase_1_side_1_bar_1[i]/number_of_layers_eddy
    avg_Bt_phase_1_side_1_bar_1[i] = avg_Bt_phase_1_side_1_bar_1[i]/number_of_layers_eddy
    avg_Br_phase_1_side_1_bar_2[i] = avg_Br_phase_1_side_1_bar_2[i]/number_of_layers_eddy
    avg_Bt_phase_1_side_1_bar_2[i] = avg_Bt_phase_1_side_1_bar_2[i]/number_of_layers_eddy
    if calculate_other_coil_sides:
      avg_Br_phase_1_side_2_bar_1[i] = avg_Br_phase_1_side_2_bar_1[i]/number_of_layers_eddy
      avg_Bt_phase_1_side_2_bar_1[i] = avg_Bt_phase_1_side_2_bar_1[i]/number_of_layers_eddy
      avg_Br_phase_1_side_2_bar_2[i] = avg_Br_phase_1_side_2_bar_2[i]/number_of_layers_eddy
      avg_Bt_phase_1_side_2_bar_2[i] = avg_Bt_phase_1_side_2_bar_2[i]/number_of_layers_eddy  
      
    if calculate_other_phases:
    #phase2
      avg_Br_phase_2_side_1_bar_1[i] = avg_Br_phase_2_side_1_bar_1[i]/number_of_layers_eddy
      avg_Bt_phase_2_side_1_bar_1[i] = avg_Bt_phase_2_side_1_bar_1[i]/number_of_layers_eddy
      avg_Br_phase_2_side_1_bar_2[i] = avg_Br_phase_2_side_1_bar_2[i]/number_of_layers_eddy
      avg_Bt_phase_2_side_1_bar_2[i] = avg_Bt_phase_2_side_1_bar_2[i]/number_of_layers_eddy
      if calculate_other_coil_sides:
	avg_Br_phase_2_side_2_bar_1[i] = avg_Br_phase_2_side_2_bar_1[i]/number_of_layers_eddy
	avg_Bt_phase_2_side_2_bar_1[i] = avg_Bt_phase_2_side_2_bar_1[i]/number_of_layers_eddy
	avg_Br_phase_2_side_2_bar_2[i] = avg_Br_phase_2_side_2_bar_2[i]/number_of_layers_eddy
	avg_Bt_phase_2_side_2_bar_2[i] = avg_Bt_phase_2_side_2_bar_2[i]/number_of_layers_eddy    
    
    #phase3
      avg_Br_phase_3_side_1_bar_1[i] = avg_Br_phase_3_side_1_bar_1[i]/number_of_layers_eddy
      avg_Bt_phase_3_side_1_bar_1[i] = avg_Bt_phase_3_side_1_bar_1[i]/number_of_layers_eddy
      avg_Br_phase_3_side_1_bar_2[i] = avg_Br_phase_3_side_1_bar_2[i]/number_of_layers_eddy
      avg_Bt_phase_3_side_1_bar_2[i] = avg_Bt_phase_3_side_1_bar_2[i]/number_of_layers_eddy
      if calculate_other_coil_sides:
	avg_Br_phase_3_side_2_bar_1[i] = avg_Br_phase_3_side_2_bar_1[i]/number_of_layers_eddy
	avg_Bt_phase_3_side_2_bar_1[i] = avg_Bt_phase_3_side_2_bar_1[i]/number_of_layers_eddy
	avg_Br_phase_3_side_2_bar_2[i] = avg_Br_phase_3_side_2_bar_2[i]/number_of_layers_eddy
	avg_Bt_phase_3_side_2_bar_2[i] = avg_Bt_phase_3_side_2_bar_2[i]/number_of_layers_eddy 

  
  #get the rms value of all the values obtained at each step
  rms_Br_phase_1_side_1_bar_1 = np.sqrt(np.mean(avg_Br_phase_1_side_1_bar_1**2))
  rms_Bt_phase_1_side_1_bar_1 = np.sqrt(np.mean(avg_Bt_phase_1_side_1_bar_1**2))
  rms_Br_phase_1_side_1_bar_2 = np.sqrt(np.mean(avg_Br_phase_1_side_1_bar_2**2))
  rms_Bt_phase_1_side_1_bar_2 = np.sqrt(np.mean(avg_Bt_phase_1_side_1_bar_2**2))
  if calculate_other_coil_sides:
    rms_Br_phase_1_side_2_bar_1 = np.sqrt(np.mean(avg_Br_phase_1_side_2_bar_1**2))
    rms_Bt_phase_1_side_2_bar_1 = np.sqrt(np.mean(avg_Bt_phase_1_side_2_bar_1**2))
    rms_Br_phase_1_side_2_bar_2 = np.sqrt(np.mean(avg_Br_phase_1_side_2_bar_2**2))
    rms_Bt_phase_1_side_2_bar_2 = np.sqrt(np.mean(avg_Bt_phase_1_side_2_bar_2**2))

    if calculate_other_phases:
    #phase2
      rms_Br_phase_2_side_1_bar_1 = np.sqrt(np.mean(avg_Br_phase_2_side_1_bar_1**2))
      rms_Bt_phase_2_side_1_bar_1 = np.sqrt(np.mean(avg_Bt_phase_2_side_1_bar_1**2))
      rms_Br_phase_2_side_1_bar_2 = np.sqrt(np.mean(avg_Br_phase_2_side_1_bar_2**2))
      rms_Bt_phase_2_side_1_bar_2 = np.sqrt(np.mean(avg_Bt_phase_2_side_1_bar_2**2))
      if calculate_other_coil_sides:
	rms_Br_phase_2_side_2_bar_1 = np.sqrt(np.mean(avg_Br_phase_2_side_2_bar_1**2))
	rms_Bt_phase_2_side_2_bar_1 = np.sqrt(np.mean(avg_Bt_phase_2_side_2_bar_1**2))
	rms_Br_phase_2_side_2_bar_2 = np.sqrt(np.mean(avg_Br_phase_2_side_2_bar_2**2))
	rms_Bt_phase_2_side_2_bar_2 = np.sqrt(np.mean(avg_Bt_phase_2_side_2_bar_2**2))
    
    #phase3
      rms_Br_phase_3_side_1_bar_1 = np.sqrt(np.mean(avg_Br_phase_3_side_1_bar_1**2))
      rms_Bt_phase_3_side_1_bar_1 = np.sqrt(np.mean(avg_Bt_phase_3_side_1_bar_1**2))
      rms_Br_phase_3_side_1_bar_2 = np.sqrt(np.mean(avg_Br_phase_3_side_1_bar_2**2))
      rms_Bt_phase_3_side_1_bar_2 = np.sqrt(np.mean(avg_Bt_phase_3_side_1_bar_2**2))
      if calculate_other_coil_sides:
	rms_Br_phase_3_side_2_bar_1 = np.sqrt(np.mean(avg_Br_phase_3_side_2_bar_1**2))
	rms_Bt_phase_3_side_2_bar_1 = np.sqrt(np.mean(avg_Bt_phase_3_side_2_bar_1**2))
	rms_Br_phase_3_side_2_bar_2 = np.sqrt(np.mean(avg_Br_phase_3_side_2_bar_2**2))
	rms_Bt_phase_3_side_2_bar_2 = np.sqrt(np.mean(avg_Bt_phase_3_side_2_bar_2**2))

  #get the fft value of all the values obtained at each step
  fft_Br_phase_1_side_1_bar_1 = abs(np.fft.rfft(avg_Br_phase_1_side_1_bar_1,M)*2/M)
  fft_Bt_phase_1_side_1_bar_1 = abs(np.fft.rfft(avg_Bt_phase_1_side_1_bar_1,M)*2/M)
  fft_Br_phase_1_side_1_bar_2 = abs(np.fft.rfft(avg_Br_phase_1_side_1_bar_2,M)*2/M)
  fft_Bt_phase_1_side_1_bar_2 = abs(np.fft.rfft(avg_Bt_phase_1_side_1_bar_2,M)*2/M)
  if calculate_other_coil_sides:
    fft_Br_phase_1_side_2_bar_1 = abs(np.fft.rfft(avg_Br_phase_1_side_2_bar_1,M)*2/M)
    fft_Bt_phase_1_side_2_bar_1 = abs(np.fft.rfft(avg_Bt_phase_1_side_2_bar_1,M)*2/M)
    fft_Br_phase_1_side_2_bar_2 = abs(np.fft.rfft(avg_Br_phase_1_side_2_bar_2,M)*2/M)
    fft_Bt_phase_1_side_2_bar_2 = abs(np.fft.rfft(avg_Bt_phase_1_side_2_bar_2,M)*2/M)

  if calculate_other_phases:
  #phase2
    fft_Br_phase_2_side_1_bar_1 = abs(np.fft.rfft(avg_Br_phase_2_side_1_bar_1,M)*2/M)
    fft_Bt_phase_2_side_1_bar_1 = abs(np.fft.rfft(avg_Bt_phase_2_side_1_bar_1,M)*2/M)
    fft_Br_phase_2_side_1_bar_2 = abs(np.fft.rfft(avg_Br_phase_2_side_1_bar_2,M)*2/M)
    fft_Bt_phase_2_side_1_bar_2 = abs(np.fft.rfft(avg_Bt_phase_2_side_1_bar_2,M)*2/M)
    if calculate_other_coil_sides:
      fft_Br_phase_2_side_2_bar_1 = abs(np.fft.rfft(avg_Br_phase_2_side_2_bar_1,M)*2/M)
      fft_Bt_phase_2_side_2_bar_1 = abs(np.fft.rfft(avg_Bt_phase_2_side_2_bar_1,M)*2/M)
      fft_Br_phase_2_side_2_bar_2 = abs(np.fft.rfft(avg_Br_phase_2_side_2_bar_2,M)*2/M)
      fft_Bt_phase_2_side_2_bar_2 = abs(np.fft.rfft(avg_Bt_phase_2_side_2_bar_2,M)*2/M)    

  #phase3
    fft_Br_phase_3_side_1_bar_1 = abs(np.fft.rfft(avg_Br_phase_3_side_1_bar_1,M)*2/M)
    fft_Bt_phase_3_side_1_bar_1 = abs(np.fft.rfft(avg_Bt_phase_3_side_1_bar_1,M)*2/M)
    fft_Br_phase_3_side_1_bar_2 = abs(np.fft.rfft(avg_Br_phase_3_side_1_bar_2,M)*2/M)
    fft_Bt_phase_3_side_1_bar_2 = abs(np.fft.rfft(avg_Bt_phase_3_side_1_bar_2,M)*2/M)
    if calculate_other_coil_sides:
      fft_Br_phase_3_side_2_bar_1 = abs(np.fft.rfft(avg_Br_phase_3_side_2_bar_1,M)*2/M)
      fft_Bt_phase_3_side_2_bar_1 = abs(np.fft.rfft(avg_Bt_phase_3_side_2_bar_1,M)*2/M)
      fft_Br_phase_3_side_2_bar_2 = abs(np.fft.rfft(avg_Br_phase_3_side_2_bar_2,M)*2/M)
      fft_Bt_phase_3_side_2_bar_2 = abs(np.fft.rfft(avg_Bt_phase_3_side_2_bar_2,M)*2/M)
  
  

    
  if conductors_round0_bars1 == 1:
    ##get average tangential and normal components  
    p_eddy_phase_1_side_1_bar_1 = 0
    p_eddy_phase_1_side_1_bar_2 = 0
    p_eddy_phase_1_side_2_bar_1 = 0
    p_eddy_phase_1_side_2_bar_2 = 0
    p_eddy_phase_2_side_1_bar_1 = 0
    p_eddy_phase_2_side_1_bar_2 = 0    
    p_eddy_phase_2_side_2_bar_1 = 0
    p_eddy_phase_2_side_2_bar_2 = 0
    p_eddy_phase_3_side_1_bar_1 = 0
    p_eddy_phase_3_side_1_bar_2 = 0
    p_eddy_phase_3_side_2_bar_1 = 0
    p_eddy_phase_3_side_2_bar_2 = 0
    
    p_eddy_bar_1_avg = []
    p_eddy_bar_2_avg = []
    
    #if bar_side_height>bar_side_width: #which usually is the case, bar_side_height is the longer side
    a = bar_side_height
    b = bar_side_width
    #else:
      #a = bar_side_width
      #b = bar_side_height    
    
    if eddy_bar_method == 0: #Murgatroyd1979 (corrective factor is needed to achieve the exact same result as Namjoshi1988, when skin_depth with fundamental frequency is used)
      br_aux = ((1/24.0)*(1/copper_resistivity)*((w_e)**2)*(a*b**3)*l)/corrective_factor
      bt_aux = ((1/24.0)*(1/copper_resistivity)*((w_e)**2)*(b*a**3)*l)/corrective_factor
    else: #Namjoshi1988
      a=a/2
      b=b/2
      #skin_depth_second = np.sqrt((2*copper_resistivity)/(w_e*mu)) #we should actually use skin depth due to second (working) harmonic, but the result seems to big
      br_aux = 0.5*((1/skin_depth)**3)*((2*b)**3)*(a/(3*(a+b)))*(copper_resistivity/(skin_depth*(mu**2)))*(a+b)*4*l
      bt_aux = 0.5*((1/skin_depth)**3)*((2*a)**3)*(b/(3*(a+b)))*(copper_resistivity/(skin_depth*(mu**2)))*(a+b)*4*l	#correct
    
    if use_single_B_value == 0:
      for i in range(0, len(fft_Br_phase_1_side_1_bar_1)): #for each harmonic
	fft_Br_phase_1_side_1_bar_1[i] = fft_Br_phase_1_side_1_bar_1[i]/np.sqrt(2)
	fft_Bt_phase_1_side_1_bar_1[i] = fft_Bt_phase_1_side_1_bar_1[i]/np.sqrt(2)       
	if number_of_copper_bars_per_coil == 2:
	  fft_Br_phase_1_side_1_bar_2[i] = fft_Br_phase_1_side_1_bar_2[i]/np.sqrt(2)
	  fft_Bt_phase_1_side_1_bar_2[i] = fft_Bt_phase_1_side_1_bar_2[i]/np.sqrt(2)    

	if calculate_other_coil_sides:
	  fft_Br_phase_1_side_2_bar_1[i] = fft_Br_phase_1_side_2_bar_1[i]/np.sqrt(2)
	  fft_Bt_phase_1_side_2_bar_1[i] = fft_Bt_phase_1_side_2_bar_1[i]/np.sqrt(2)  
	  if number_of_copper_bars_per_coil == 2:
	    fft_Br_phase_1_side_2_bar_2[i] = fft_Br_phase_1_side_2_bar_2[i]/np.sqrt(2)
	    fft_Bt_phase_1_side_2_bar_2[i] = fft_Bt_phase_1_side_2_bar_2[i]/np.sqrt(2)

	if calculate_other_phases:
	#phase2
	  fft_Br_phase_2_side_1_bar_1[i] = fft_Br_phase_2_side_1_bar_1[i]/np.sqrt(2)
	  fft_Bt_phase_2_side_1_bar_1[i] = fft_Bt_phase_2_side_1_bar_1[i]/np.sqrt(2)       
	  if number_of_copper_bars_per_coil == 2:
	    fft_Br_phase_2_side_1_bar_2[i] = fft_Br_phase_2_side_1_bar_2[i]/np.sqrt(2)
	    fft_Bt_phase_2_side_1_bar_2[i] = fft_Bt_phase_2_side_1_bar_2[i]/np.sqrt(2)    

	  if calculate_other_coil_sides:
	    fft_Br_phase_2_side_2_bar_1[i] = fft_Br_phase_2_side_2_bar_1[i]/np.sqrt(2)
	    fft_Bt_phase_2_side_2_bar_1[i] = fft_Bt_phase_2_side_2_bar_1[i]/np.sqrt(2)  
	    if number_of_copper_bars_per_coil == 2:
	      fft_Br_phase_2_side_2_bar_2[i] = fft_Br_phase_2_side_2_bar_2[i]/np.sqrt(2)
	      fft_Bt_phase_2_side_2_bar_2[i] = fft_Bt_phase_2_side_2_bar_2[i]/np.sqrt(2)      

	#phase3
	  fft_Br_phase_3_side_1_bar_1[i] = fft_Br_phase_3_side_1_bar_1[i]/np.sqrt(2)
	  fft_Bt_phase_3_side_1_bar_1[i] = fft_Bt_phase_3_side_1_bar_1[i]/np.sqrt(2)       
	  if number_of_copper_bars_per_coil == 2:
	    fft_Br_phase_3_side_1_bar_2[i] = fft_Br_phase_3_side_1_bar_2[i]/np.sqrt(2)
	    fft_Bt_phase_3_side_1_bar_2[i] = fft_Bt_phase_3_side_1_bar_2[i]/np.sqrt(2)    

	  if calculate_other_coil_sides:
	    fft_Br_phase_3_side_2_bar_1[i] = fft_Br_phase_3_side_2_bar_1[i]/np.sqrt(2)
	    fft_Bt_phase_3_side_2_bar_1[i] = fft_Bt_phase_3_side_2_bar_1[i]/np.sqrt(2)  
	    if number_of_copper_bars_per_coil == 2:
	      fft_Br_phase_3_side_2_bar_2[i] = fft_Br_phase_3_side_2_bar_2[i]/np.sqrt(2)
	      fft_Bt_phase_3_side_2_bar_2[i] = fft_Bt_phase_3_side_2_bar_2[i]/np.sqrt(2)      

    
      for i in range(1, len(fft_Br_phase_1_side_1_bar_1)): #for each harmonic
	#now calculate eddy losses
	p_eddy_phase_1_side_1_bar_1 = p_eddy_phase_1_side_1_bar_1 + br_aux*(i**2)*(fft_Br_phase_1_side_1_bar_1[i]**2)
	p_eddy_phase_1_side_1_bar_1 = p_eddy_phase_1_side_1_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_1_side_1_bar_1[i]**2)
	if number_of_copper_bars_per_coil == 2:
	  p_eddy_phase_1_side_1_bar_2 = p_eddy_phase_1_side_1_bar_2 + br_aux*(i**2)*(fft_Br_phase_1_side_1_bar_2[i]**2)
	  p_eddy_phase_1_side_1_bar_2 = p_eddy_phase_1_side_1_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_1_side_1_bar_2[i]**2)      
	if calculate_other_coil_sides:
	  p_eddy_phase_1_side_2_bar_1 = p_eddy_phase_1_side_2_bar_1 + br_aux*(i**2)*(fft_Br_phase_1_side_2_bar_1[i]**2)
	  p_eddy_phase_1_side_2_bar_1 = p_eddy_phase_1_side_2_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_1_side_2_bar_1[i]**2)
	  if number_of_copper_bars_per_coil == 2:
	    p_eddy_phase_1_side_2_bar_2 = p_eddy_phase_1_side_2_bar_2 + br_aux*(i**2)*(fft_Br_phase_1_side_2_bar_2[i]**2)
	    p_eddy_phase_1_side_2_bar_2 = p_eddy_phase_1_side_2_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_1_side_2_bar_2[i]**2)	

	if calculate_other_phases:
	#phase2
	  p_eddy_phase_2_side_1_bar_1 = p_eddy_phase_2_side_1_bar_1 + br_aux*(i**2)*(fft_Br_phase_2_side_1_bar_1[i]**2)
	  p_eddy_phase_2_side_1_bar_1 = p_eddy_phase_2_side_1_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_2_side_1_bar_1[i]**2)
	  if number_of_copper_bars_per_coil == 2:
	    p_eddy_phase_2_side_1_bar_2 = p_eddy_phase_2_side_1_bar_2 + br_aux*(i**2)*(fft_Br_phase_2_side_1_bar_2[i]**2)
	    p_eddy_phase_2_side_1_bar_2 = p_eddy_phase_2_side_1_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_2_side_1_bar_2[i]**2)      
	  if calculate_other_coil_sides:
	    p_eddy_phase_2_side_2_bar_1 = p_eddy_phase_2_side_2_bar_1 + br_aux*(i**2)*(fft_Br_phase_2_side_2_bar_1[i]**2)
	    p_eddy_phase_2_side_2_bar_1 = p_eddy_phase_2_side_2_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_2_side_2_bar_1[i]**2)
	    if number_of_copper_bars_per_coil == 2:
	      p_eddy_phase_2_side_2_bar_2 = p_eddy_phase_2_side_2_bar_2 + br_aux*(i**2)*(fft_Br_phase_2_side_2_bar_2[i]**2)
	      p_eddy_phase_2_side_2_bar_2 = p_eddy_phase_2_side_2_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_2_side_2_bar_2[i]**2)

	#phase3
	  p_eddy_phase_3_side_1_bar_1 = p_eddy_phase_3_side_1_bar_1 + br_aux*(i**2)*(fft_Br_phase_3_side_1_bar_1[i]**2)
	  p_eddy_phase_3_side_1_bar_1 = p_eddy_phase_3_side_1_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_3_side_1_bar_1[i]**2)
	  if number_of_copper_bars_per_coil == 2:
	    p_eddy_phase_3_side_1_bar_2 = p_eddy_phase_3_side_1_bar_2 + br_aux*(i**2)*(fft_Br_phase_3_side_1_bar_2[i]**2)
	    p_eddy_phase_3_side_1_bar_2 = p_eddy_phase_3_side_1_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_3_side_1_bar_2[i]**2)      
	  if calculate_other_coil_sides:
	    p_eddy_phase_3_side_2_bar_1 = p_eddy_phase_3_side_2_bar_1 + br_aux*(i**2)*(fft_Br_phase_3_side_2_bar_1[i]**2)
	    p_eddy_phase_3_side_2_bar_1 = p_eddy_phase_3_side_2_bar_1 + bt_aux*(i**2)*(fft_Bt_phase_3_side_2_bar_1[i]**2)
	    if number_of_copper_bars_per_coil == 2:
	      p_eddy_phase_3_side_2_bar_2 = p_eddy_phase_3_side_2_bar_2 + br_aux*(i**2)*(fft_Br_phase_3_side_2_bar_2[i]**2)
	      p_eddy_phase_3_side_2_bar_2 = p_eddy_phase_3_side_2_bar_2 + bt_aux*(i**2)*(fft_Bt_phase_3_side_2_bar_2[i]**2)

    if use_single_B_value == 1:
      #assumong rms Bt and Br quantities has the second (working) frequency
      p_eddy_phase_1_side_1_bar_1 = br_aux*(rms_Br_phase_1_side_1_bar_1**2) + bt_aux*(rms_Bt_phase_1_side_1_bar_1**2)
      p_eddy_phase_1_side_1_bar_2 = br_aux*(rms_Br_phase_1_side_1_bar_2**2) + bt_aux*(rms_Bt_phase_1_side_1_bar_2**2)
      if calculate_other_coil_sides:
	p_eddy_phase_1_side_2_bar_1 = br_aux*(rms_Br_phase_1_side_2_bar_1**2) + bt_aux*(rms_Bt_phase_1_side_2_bar_1**2)
	p_eddy_phase_1_side_2_bar_2 = br_aux*(rms_Br_phase_1_side_2_bar_2**2) + bt_aux*(rms_Bt_phase_1_side_2_bar_2**2)

      if calculate_other_phases:
      #phase2
	p_eddy_phase_2_side_1_bar_1 = br_aux*(rms_Br_phase_2_side_1_bar_1**2) + bt_aux*(rms_Bt_phase_2_side_1_bar_1**2)
	p_eddy_phase_2_side_1_bar_2 = br_aux*(rms_Br_phase_2_side_1_bar_2**2) + bt_aux*(rms_Bt_phase_2_side_1_bar_2**2)
	if calculate_other_coil_sides:
	  p_eddy_phase_2_side_2_bar_1 = br_aux*(rms_Br_phase_2_side_2_bar_1**2) + bt_aux*(rms_Bt_phase_2_side_2_bar_1**2)
	  p_eddy_phase_2_side_2_bar_2 = br_aux*(rms_Br_phase_2_side_2_bar_2**2) + bt_aux*(rms_Bt_phase_2_side_2_bar_2**2)

      #phase3
	p_eddy_phase_3_side_1_bar_1 = br_aux*(rms_Br_phase_3_side_1_bar_1**2) + bt_aux*(rms_Bt_phase_3_side_1_bar_1**2)
	p_eddy_phase_3_side_1_bar_2 = br_aux*(rms_Br_phase_3_side_1_bar_2**2) + bt_aux*(rms_Bt_phase_3_side_1_bar_2**2)
	if calculate_other_coil_sides:
	  p_eddy_phase_3_side_2_bar_1 = br_aux*(rms_Br_phase_3_side_2_bar_1**2) + bt_aux*(rms_Bt_phase_3_side_2_bar_1**2)
	  p_eddy_phase_3_side_2_bar_2 = br_aux*(rms_Br_phase_3_side_2_bar_2**2) + bt_aux*(rms_Bt_phase_3_side_2_bar_2**2)    
 
 
 
    #determine average losses in bars
    #p_eddy_bar_1_avg
 
    #calculate eddy losses in each coil side
    p_eddy_phase_1_side_1 = p_eddy_phase_1_side_1_bar_1+p_eddy_phase_1_side_1_bar_2
    if calculate_other_coil_sides:
      p_eddy_phase_1_side_2 = p_eddy_phase_1_side_2_bar_1+p_eddy_phase_1_side_2_bar_2
    
    if calculate_other_phases:
    #phase2
      p_eddy_phase_2_side_1 = p_eddy_phase_2_side_1_bar_1+p_eddy_phase_2_side_1_bar_2
      if calculate_other_coil_sides:
	p_eddy_phase_2_side_2 = p_eddy_phase_2_side_2_bar_1+p_eddy_phase_2_side_2_bar_2
    #phase3
      p_eddy_phase_3_side_1 = p_eddy_phase_3_side_1_bar_1+p_eddy_phase_3_side_1_bar_2
      if calculate_other_coil_sides:
	p_eddy_phase_3_side_2 = p_eddy_phase_3_side_2_bar_1+p_eddy_phase_3_side_2_bar_2
      
      
    #calculate eddy losses for each phase
    if calculate_other_coil_sides:  
      p_eddy_phase_1 = p_eddy_phase_1_side_1+p_eddy_phase_1_side_2
    else:
      p_eddy_phase_1 = 2*p_eddy_phase_1_side_1
    
    if calculate_other_phases:
    #phase2
      if calculate_other_coil_sides:  
	p_eddy_phase_2 = p_eddy_phase_2_side_1+p_eddy_phase_2_side_2
      else:
	p_eddy_phase_2 = 2*p_eddy_phase_2_side_1
    #phase3
      if calculate_other_coil_sides:  
	p_eddy_phase_3 = p_eddy_phase_3_side_1+p_eddy_phase_3_side_2
      else:
	p_eddy_phase_3 = 2*p_eddy_phase_3_side_1
    
    
  elif conductors_round0_bars1 == 0:
    #second harmonic is used here, because the frequency of the magnet poles is double the electrical frequency we inject into the machine
    #the conductors are being cut by flux fields created (mostly) by the magnet poles, which has the frequency of the working (second) harmonic
    p_eddy_phase_1_side_1 = aux_constant*hos_phase_1_side_1 	#this is then the eddy current losses for a single coil side of only one phase
    if calculate_other_phases:
      p_eddy_phase_2_side_1 = aux_constant*hos_phase_2_side_1 	#this is then the eddy current losses for a single coil side of only one phase
      p_eddy_phase_3_side_1 = aux_constant*hos_phase_3_side_1 	#this is then the eddy current losses for a single coil side of only one phase
    
    if calculate_other_coil_sides:
      p_eddy_phase_1_side_2 = aux_constant*hos_phase_1_side_2 	#this is then the eddy current losses for a single coil side of only one phase
      p_eddy_phase_1 = p_eddy_phase_1_side_1 + p_eddy_phase_1_side_2
      if calculate_other_phases:
	p_eddy_phase_2_side_2 = aux_constant*hos_phase_2_side_2 	#this is then the eddy current losses for a single coil side of only one phase
	p_eddy_phase_3_side_2 = aux_constant*hos_phase_3_side_2 	#this is then the eddy current losses for a single coil side of only one phase
	p_eddy_phase_2 = p_eddy_phase_2_side_1 + p_eddy_phase_2_side_2
	p_eddy_phase_3 = p_eddy_phase_3_side_1 + p_eddy_phase_3_side_2    
    else:
      p_eddy_phase_1 = 2*p_eddy_phase_1_side_1
      if calculate_other_phases:
	p_eddy_phase_2 = 2*p_eddy_phase_2_side_1
	p_eddy_phase_3 = 2*p_eddy_phase_3_side_1
  
  #print("aux constant = %f"%(aux_constant))
  #print("hos_phase_1_side_1 = %f"%(hos_phase_1_side_1))
  #print("conductors_per_layer_per_coil_side = %f"%(conductors_per_layer_per_coil_side))
  #print("B_component_phase_1_side_1 = %f"%(B_component_phase_1_side_1))
  #print("max(Br_layer_fft_phase_1_side_1) = %f"%(max(Br_layer_fft_phase_1_side_1)))
  
  #if conductors_round0_bars1 == 1:
    #print("max(avg_Br_fft_phase_1_side_1_bar_1) = %f"%(max(avg_Br_fft_phase_1_side_1_bar_1)))
    #print("max(avg_Bt_fft_phase_1_side_1_bar_1) = %f"%(max(avg_Bt_fft_phase_1_side_1_bar_1)))
    #if number_of_copper_bars_per_coil == 2:
      #print("max(avg_Br_fft_phase_1_side_1_bar_2) = %f"%(max(avg_Br_fft_phase_1_side_1_bar_2)))
      #print("max(avg_Bt_fft_phase_1_side_1_bar_2) = %f"%(max(avg_Bt_fft_phase_1_side_1_bar_2)))      
    
    #if calculate_other_coil_sides:
      #print("max(avg_Br_fft_phase_1_side_2_bar_1) = %f"%(max(avg_Br_fft_phase_1_side_2_bar_1)))
      #print("max(avg_Bt_fft_phase_1_side_2_bar_1) = %f"%(max(avg_Bt_fft_phase_1_side_2_bar_1)))
      #if number_of_copper_bars_per_coil == 2:
	#print("max(avg_Br_fft_phase_1_side_2_bar_2) = %f"%(max(avg_Br_fft_phase_1_side_2_bar_2)))
	#print("max(avg_Bt_fft_phase_1_side_2_bar_2) = %f"%(max(avg_Bt_fft_phase_1_side_2_bar_2)))     
   
    #if calculate_other_phases:
    ##phase2
      #print("max(avg_Br_fft_phase_2_side_1_bar_1) = %f"%(max(avg_Br_fft_phase_2_side_1_bar_1)))
      #print("max(avg_Bt_fft_phase_2_side_1_bar_1) = %f"%(max(avg_Bt_fft_phase_2_side_1_bar_1)))
      #if number_of_copper_bars_per_coil == 2:
	#print("max(avg_Br_fft_phase_2_side_1_bar_2) = %f"%(max(avg_Br_fft_phase_2_side_1_bar_2)))
	#print("max(avg_Bt_fft_phase_2_side_1_bar_2) = %f"%(max(avg_Bt_fft_phase_2_side_1_bar_2)))      
      
      #if calculate_other_coil_sides:
	#print("max(avg_Br_fft_phase_2_side_2_bar_1) = %f"%(max(avg_Br_fft_phase_2_side_2_bar_1)))
	#print("max(avg_Bt_fft_phase_2_side_2_bar_1) = %f"%(max(avg_Bt_fft_phase_2_side_2_bar_1)))
	#if number_of_copper_bars_per_coil == 2:
	  #print("max(avg_Br_fft_phase_2_side_2_bar_2) = %f"%(max(avg_Br_fft_phase_2_side_2_bar_2)))
	  #print("max(avg_Bt_fft_phase_2_side_2_bar_2) = %f"%(max(avg_Bt_fft_phase_2_side_2_bar_2)))   
   
    ##phase3
      #print("max(avg_Br_fft_phase_3_side_1_bar_1) = %f"%(max(avg_Br_fft_phase_3_side_1_bar_1)))
      #print("max(avg_Bt_fft_phase_3_side_1_bar_1) = %f"%(max(avg_Bt_fft_phase_3_side_1_bar_1)))
      #if number_of_copper_bars_per_coil == 2:
	#print("max(avg_Br_fft_phase_3_side_1_bar_2) = %f"%(max(avg_Br_fft_phase_3_side_1_bar_2)))
	#print("max(avg_Bt_fft_phase_3_side_1_bar_2) = %f"%(max(avg_Bt_fft_phase_3_side_1_bar_2)))      
      
      #if calculate_other_coil_sides:
	#print("max(avg_Br_fft_phase_3_side_2_bar_1) = %f"%(max(avg_Br_fft_phase_3_side_2_bar_1)))
	#print("max(avg_Bt_fft_phase_3_side_2_bar_1) = %f"%(max(avg_Bt_fft_phase_3_side_2_bar_1)))
	#if number_of_copper_bars_per_coil == 2:
	  #print("max(avg_Br_fft_phase_3_side_2_bar_2) = %f"%(max(avg_Br_fft_phase_3_side_2_bar_2)))
	  #print("max(avg_Bt_fft_phase_3_side_2_bar_2) = %f"%(max(avg_Bt_fft_phase_3_side_2_bar_2)))    
    
    
    
    #print("(a*b**3*1e9) = %f"%(a*b**3*1e9))
    #print("(b*a**3*1e9) = %f"%(b*a**3*1e9))
  
  if calculate_other_phases:
    p_eddy = q*(p_eddy_phase_1+p_eddy_phase_2+p_eddy_phase_3)
  else:
    p_eddy = p_eddy_phase_1*Q

  pretty_eddy.add_row(["A","Side 1",p_eddy_phase_1_side_1*q])
  if calculate_other_coil_sides: pretty_eddy.add_row(["A","Side 2",p_eddy_phase_1_side_2*q])
  if calculate_other_phases:
    pretty_eddy.add_row(["B","Side 1",p_eddy_phase_2_side_1*q])
    if calculate_other_coil_sides: pretty_eddy.add_row(["B","Side 2",p_eddy_phase_2_side_2*q])
    pretty_eddy.add_row(["C","Side 1",p_eddy_phase_3_side_1*q])
    if calculate_other_coil_sides: pretty_eddy.add_row(["C","Side 2",p_eddy_phase_3_side_2*q])
  pretty_eddy.add_row(["Total","",p_eddy])

  

  if conductors_round0_bars1 == 1:
    pretty_eddy_bar.add_row(["A","Side 1","Bar 1",p_eddy_phase_1_side_1_bar_1])
    p_eddy_bar_1_avg.append(p_eddy_phase_1_side_1_bar_1)
    if number_of_copper_bars_per_coil == 2:
      pretty_eddy_bar.add_row(["A","Side 1","Bar 2",p_eddy_phase_1_side_1_bar_2])
      p_eddy_bar_2_avg.append(p_eddy_phase_1_side_1_bar_2)
    if calculate_other_coil_sides:
      pretty_eddy_bar.add_row(["A","Side 2","Bar 1",p_eddy_phase_1_side_2_bar_1])
      p_eddy_bar_1_avg.append(p_eddy_phase_1_side_2_bar_1)
      if number_of_copper_bars_per_coil == 2:
	pretty_eddy_bar.add_row(["A","Side 2","Bar 2",p_eddy_phase_1_side_2_bar_2])
	p_eddy_bar_2_avg.append(p_eddy_phase_1_side_2_bar_2)

    if calculate_other_phases:
    #phase2
      pretty_eddy_bar.add_row(["B","Side 1","Bar 1",p_eddy_phase_2_side_1_bar_1])
      p_eddy_bar_1_avg.append(p_eddy_phase_2_side_1_bar_1)
      if number_of_copper_bars_per_coil == 2:
	pretty_eddy_bar.add_row(["B","Side 1","Bar 2",p_eddy_phase_2_side_1_bar_2])
	p_eddy_bar_2_avg.append(p_eddy_phase_2_side_1_bar_2)
      if calculate_other_coil_sides:
	pretty_eddy_bar.add_row(["B","Side 2","Bar 1",p_eddy_phase_2_side_2_bar_1])
	p_eddy_bar_1_avg.append(p_eddy_phase_2_side_2_bar_1)
	if number_of_copper_bars_per_coil == 2:
	  pretty_eddy_bar.add_row(["B","Side 2","Bar 2",p_eddy_phase_2_side_2_bar_2])   
	  p_eddy_bar_2_avg.append(p_eddy_phase_2_side_2_bar_2)
    
    #phase3
      pretty_eddy_bar.add_row(["C","Side 1","Bar 1",p_eddy_phase_3_side_1_bar_1])
      p_eddy_bar_1_avg.append(p_eddy_phase_3_side_1_bar_1)
      if number_of_copper_bars_per_coil == 2:
	pretty_eddy_bar.add_row(["C","Side 1","Bar 2",p_eddy_phase_3_side_1_bar_2])
	p_eddy_bar_2_avg.append(p_eddy_phase_3_side_1_bar_2)
      if calculate_other_coil_sides:
	pretty_eddy_bar.add_row(["C","Side 2","Bar 1",p_eddy_phase_3_side_2_bar_1])
	p_eddy_bar_1_avg.append(p_eddy_phase_3_side_2_bar_1)
	if number_of_copper_bars_per_coil == 2:
	  pretty_eddy_bar.add_row(["C","Side 2","Bar 2",p_eddy_phase_3_side_2_bar_2])
	  p_eddy_bar_2_avg.append(p_eddy_phase_3_side_2_bar_2)
  
  pretty_eddy_bar_extra.add_row(["Bar 1",average(p_eddy_bar_1_avg),average(p_eddy_bar_1_avg)*Q*2])
  pretty_eddy_bar_extra.add_row(["Bar 2",average(p_eddy_bar_2_avg),average(p_eddy_bar_2_avg)*Q*2])
  pretty_eddy_bar_extra.add_row(["Total",average(p_eddy_bar_1_avg)+average(p_eddy_bar_2_avg),(average(p_eddy_bar_1_avg)+average(p_eddy_bar_2_avg))*Q*2])
  
  if plot_eddy_current_graphs:
    #phi_range=np.linspace(-np.pi/q,np.pi/q,samples)
    phi_range=np.linspace(0,2*magnet_radially_pitch,samples)
    if flare_teeth == 0:
      pl.figure(1)
      for i in range(0,number_of_layers_eddy):
	pl.plot(np.degrees(phi_range),Br_phase_1_side_1[i])
	pl.plot(np.degrees(phi_range),Bt_phase_1_side_1[i],'--')

      pl.figure(2)
      for i in range(0,number_of_layers_eddy):
	pl.plot(np.degrees(phi_range),Br_phase_1_side_2[i])
	pl.plot(np.degrees(phi_range),Bt_phase_1_side_2[i],'--')
	
      if calculate_other_phases:  
	pl.figure(3)
	for i in range(0,number_of_layers_eddy):
	  pl.plot(np.degrees(phi_range),Br_phase_2_side_1[i])
	  pl.plot(np.degrees(phi_range),Bt_phase_2_side_1[i],'--')
	  
	pl.figure(4)
	for i in range(0,number_of_layers_eddy):
	  pl.plot(np.degrees(phi_range),Br_phase_2_side_2[i])
	  pl.plot(np.degrees(phi_range),Bt_phase_2_side_2[i],'--')
	  
	pl.figure(5)
	for i in range(0,number_of_layers_eddy):
	  pl.plot(np.degrees(phi_range),Br_phase_3_side_1[i])
	  pl.plot(np.degrees(phi_range),Bt_phase_3_side_1[i],'--')
	  
	pl.figure(6)
	for i in range(0,number_of_layers_eddy):
	  pl.plot(np.degrees(phi_range),Br_phase_3_side_2[i])  
	  pl.plot(np.degrees(phi_range),Bt_phase_3_side_2[i],'--')
    
    elif flare_teeth == 1:
      # row and column sharing
      fig, ((ax1, ax2), (ax3, ax4)) = pl.subplots(2, 2, sharex='col', sharey='row')
      ax1.set_title('$B_r(\phi)$ - Inner Bar')
      ax1.plot(np.degrees(phi_range),avg_Br_phase_1_side_1_bar_1)
      for i in range(0,number_of_layers_eddy):
	ax1.plot(np.degrees(phi_range),Br_phase_1_side_1_bar_1[i],'--') 
	
      ax2.set_title('$B_t(\phi)$ - Inner Bar')
      ax2.plot(np.degrees(phi_range),avg_Bt_phase_1_side_1_bar_1)
      for i in range(0,number_of_layers_eddy):
	ax2.plot(np.degrees(phi_range),Bt_phase_1_side_1_bar_1[i],'--')

      ax3.set_title('$B_r(\phi)$ - Outer Bar')
      ax3.plot(np.degrees(phi_range),avg_Br_phase_1_side_1_bar_2)
      for i in range(0,number_of_layers_eddy):
	ax3.plot(np.degrees(phi_range),Br_phase_1_side_1_bar_2[i],'--')

      ax4.set_title('$B_t(\phi)$ - Outer Bar')
      ax4.plot(np.degrees(phi_range),avg_Bt_phase_1_side_1_bar_2)
      for i in range(0,number_of_layers_eddy):
	ax4.plot(np.degrees(phi_range),Bt_phase_1_side_1_bar_2[i],'--')

      #ax1.set_xticks(np.linspace(-180/q,180/q,7))
      #ax2.set_xticks(np.linspace(-180/q,180/q,7))
      #ax3.set_xticks(np.linspace(-180/q,180/q,7))
      #ax4.set_xticks(np.linspace(-180/q,180/q,7))

      ax1.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      ax2.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      ax3.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      ax4.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      
      #fig.text(0.5, 0.04, r'Mechanical Angle, $\phi$ [$^\circ$]', ha='center')
      #fig.text(0.04, 0.5, r'Radial Flux Density, $B_{r}$ [T]', va='center', rotation='vertical')
      
      #pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
      #pl.ylabel(r'Radial Flux Density, $B_{r}$ [T]')
      #pl.axis(xmin=-180/q,xmax=180/q)
      
      ax3.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
      ax4.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
      
      ax1.set_ylabel(r'Flux Density, $B$ [T]')
      ax3.set_ylabel(r'Flux Density, $B$ [T]')
      
      #ax1.set_xlim(-180/q,xmax=180/q)
      #ax2.set_xlim(-180/q,xmax=180/q)
      #ax3.set_xlim(-180/q,xmax=180/q)
      #ax4.set_xlim(-180/q,xmax=180/q)

      ax1.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi)
      ax2.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi) 
      ax3.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi)
      ax4.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi)
      
      ax1.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
      ax2.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
      ax3.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
      ax4.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
      
      fig.savefig("Br_Bt_bars.pdf",bbox_inches='tight')
      
      




  #print pretty_layer
  print pretty_eddy
  print pretty_eddy_bar
  print pretty_eddy_bar_extra
  eddy_time_end = time.time()

#MAGNET LOSSES
######################
p_magnet_total_losses = 0
if enable_magnet_losses:
  magnet_time_start = time.time()
  use_analytical_magnet_flux_density = 1
  #number_of_magnets_to_analyse = 4 #all magnets
  number_of_magnets_to_analyse = 2 #get_arc_B function can't gather less than two points
  number_of_magnets_to_use_results_of = 1
  plot_magnet_graphs = 0
  corrective_factor = 1
  fft_range_len_graph = 16 #number of harmonics to output on graph
  
  M = steps
  fft_size = int(M/2)+1  
  
  if rotor_movement_multiple == 1:
    #fft_range=np.arange(0,fft_size*1.5,1.5)
    fft_range=np.arange(0,fft_size)
    w_slots_fundamental = w_e/2
  elif rotor_movement_multiple == 2:
    fft_range=np.arange(0,fft_size)
    w_slots_fundamental = w_e/2
  
  theta_start = np.angle(mag_cplx_5[0])
  theta_end = np.angle(mag_cplx_5[3]) #always the last or next magnet, no matter how many magnets we analyse  
  
  a = magnet_side_length
  b = magnet_radially_outer_radius-magnet_radially_inner_radius

  #b = magnet_side_length
  #a = magnet_radially_outer_radius-magnet_radially_inner_radius
  
  #bossche method
  if magnet_loss_approximation_method == 0:
    br_aux = ((1/24.0)*(1/magnet_resistivity)*(w_slots_fundamental**2)*(a*b**3)*l)/corrective_factor
    bt_aux = ((1/24.0)*(1/magnet_resistivity)*(w_slots_fundamental**2)*(b*a**3)*l)/corrective_factor
  
  #rix's approximation
  elif magnet_loss_approximation_method == 1:
    br_aux = ((b**2)*a*b*l)/(12*magnet_resistivity)
    bt_aux = 0
  
  #Bm_magnets_tmp = []
  Br_magnets_tmp = []
  Bt_magnets_tmp = []
  
  #Bm_magnets = np.zeros([number_of_magnets_to_use_results_of,steps])
  Br_magnets = np.zeros([number_of_magnets_to_use_results_of,steps])
  Bt_magnets = np.zeros([number_of_magnets_to_use_results_of,steps])
  fft_Br_magnets = []
  fft_Bt_magnets = []
  p_eddy_magnets = np.zeros(number_of_magnets_to_use_results_of)  

  pretty_magnets = PrettyTable(['','[W]'])
  pretty_magnets.align = 'l'
  pretty_magnets.border= True 
  
  if use_analytical_magnet_flux_density == 0:
    for i in range(0,steps):  
      angle_tmp = direction*(i/(steps))*(mechRotation*rotor_movement_multiple/2) + debug_offset
      #if j == 0:
	#print("r = %.2f   angle = %.2f"%(absolute(mag_cplx_5[j])*1000, (theta_start+angle_tmp)*180/np.pi))
      Bm, Br_tmp,Bt_tmp = sf.get_arc_B(fplfiles[i], theta_start+angle_tmp,theta_end+angle_tmp, absolute(mag_cplx_5[0]), number_of_magnets_to_analyse) #returns 4 B values (one for each magnet pole)
      #print(Bm[0])
      #Bm_magnets_tmp.append(Bm)
      Br_magnets_tmp.append(Br_tmp)
      Bt_magnets_tmp.append(Bt_tmp)

    for j in range(0,number_of_magnets_to_use_results_of):
      for i in range(0,steps):
	#Bm_magnets[j][i] = Bm_magnets_tmp[i][j]
	Br_magnets[j][i] = Br_magnets_tmp[i][j]
	Bt_magnets[j][i] = Bt_magnets_tmp[i][j]
    
    #for i in range(0,steps):
      #angle_tmp = direction*(i/(steps))*(mechRotation*rotor_movement_multiple/2) + debug_offset
      #print("r = %.2f   angle = %.2f    B_m = %.2f [T]  B_r = %.2f [T]"%(absolute(mag_cplx_5[0])*1000, (theta_start+angle_tmp)*180/np.pi,Bm_magnets[1][i],Br_magnets[1][i]))
    if magnet_loss_approximation_method < 2 or plot_magnet_graphs:
      for j in range(0,number_of_magnets_to_use_results_of):
	#get the fft value of all the values obtained at each step
	fft_Br_magnets.append(abs(np.fft.rfft(Br_magnets[j],M)*2/M))
	fft_Bt_magnets.append(abs(np.fft.rfft(Bt_magnets[j],M)*2/M))
      
      for j in range(0,number_of_magnets_to_use_results_of):
	for i in range(0, fft_size): #for each harmonic
	  #convert to rms values
	  fft_Br_magnets[j][i] = fft_Br_magnets[j][i]
	  fft_Bt_magnets[j][i] = fft_Bt_magnets[j][i]	
	  #fft_Br_magnets[j][i] = (fft_Br_magnets[j][i]/2)
	  #fft_Bt_magnets[j][i] = (fft_Bt_magnets[j][i]/2)      
	  #fft_Br_magnets[j][i] = (fft_Br_magnets[j][i]/2)/np.sqrt(2) #not sure if peak or rms values should be used
	  #fft_Bt_magnets[j][i] = (fft_Bt_magnets[j][i]/2)/np.sqrt(2)

      if magnet_loss_approximation_method < 2:
	for j in range(0,number_of_magnets_to_use_results_of):
	  for i in range(1, fft_size): #for each harmonic (start from 1 so DC component is ignored)
	    #now calculate eddy losses
	    p_eddy_magnets[j] = p_eddy_magnets[j] + br_aux*(fft_range[i]**2)*(fft_Br_magnets[j][i]**2)
	    p_eddy_magnets[j] = p_eddy_magnets[j] + bt_aux*(fft_range[i]**2)*(fft_Bt_magnets[j][i]**2)

	  pretty_magnets.add_row(["Magnet %d"%(j+1),p_eddy_magnets[j]])
	  p_magnet_total_losses = p_magnet_total_losses + p_eddy_magnets[j]

	p_magnet_total_losses = p_magnet_total_losses*(poles/number_of_magnets_to_use_results_of)

  #Fang2012 method  
  if magnet_loss_approximation_method == 2:
    w_mech = n_rpm*(np.pi/30)
    vr = w_mech*stator_coil_radius
    br_aux = (l*a*b*(vr**2))/(2*magnet_resistivity)
    bt_aux = br_aux
    b0 = slot_opening_arc_length #slot opening width
    #b0 = 0.010634 #slot opening width (hardcoded)
    beta = 0.5*(1-(1/np.sqrt(1+(b0/2/g))))
    tau_t = coil_pitch #slot/coil pitch
    
    sum_of_term = 0
    for n in range (1,20):
      Kn = beta*(4/(np.pi*n))*np.sin(1.6*np.pi*n*b0/tau_t)*(0.5+((((n*b0)/tau_t)**2)/(0.7815-2*((n*b0)/tau_t)**2)))
      term_temp = (Kn**2)*(1-((tau_t/(np.pi*n*magnet_side_length))*np.sin((np.pi*n*magnet_side_length)/tau_t))**2)
      sum_of_term = sum_of_term + term_temp
  
    index = int(np.round(((0.5*coil_pitch-0.5*magnet_radially_pitch)/(rotor_movement_multiple*mechRotation/2))*steps,0))
    
    if use_analytical_magnet_flux_density == 0:
      Br_magnet_stator_tooth_aligned = Br_magnets[0][index]
    else:
      Br_magnet_stator_tooth_aligned = (Brem*b)/(muR*g + b)
  
    p_pm = br_aux*sum_of_term*Br_magnet_stator_tooth_aligned**2 #only Br is considered in this approximation
    pretty_magnets.add_row(["Loss per Magnet",p_pm])
    p_pmt = 2*poles*p_pm
    p_magnet_total_losses = p_pmt


  pretty_magnets.add_row(["Total Magnet Losses",p_magnet_total_losses])
  pretty_magnets.add_row(["Approximation Method",magnet_loss_approximation_method])
  
  #print pretty_magnets

  if plot_magnet_graphs and use_analytical_magnet_flux_density == 0:
    bar_width=0.25
    #return fft values back to original peak values
    for j in range(0,number_of_magnets_to_use_results_of):
      for i in range(0, fft_size): #for each harmonic
	#convert to rms values
	#fft_Br_magnets[j][i] = fft_Br_magnets[j][i]
	#fft_Bt_magnets[j][i] = fft_Bt_magnets[j][i]
	fft_Br_magnets[j][i] = fft_Br_magnets[j][i]/2
	fft_Bt_magnets[j][i] = fft_Bt_magnets[j][i]/2	
	#fft_Br_magnets[j][i] = fft_Br_magnets[j][i]*np.sqrt(2)
	#fft_Bt_magnets[j][i] = fft_Bt_magnets[j][i]*np.sqrt(2)     
    
    if rotor_movement_multiple == 1:
      fft_range=np.arange(0,fft_size*1.5,1.5)
      phi_range=np.linspace(0,2*magnet_radially_pitch,steps)
    elif rotor_movement_multiple == 2:
      fft_range=np.arange(0,fft_size)
      phi_range=np.linspace(0,3*coil_pitch,steps)
    
    # row and column sharing
    #fig, ((ax1, ax2), (ax3, ax4)) = pl.subplots(2, 2, sharex='col', sharey='row')
    #fig, (ax1, ax2) = pl.subplots(2, 1, sharex='col', sharey='row')
    #fig, (ax1, ax2, ax3) = pl.subplots(3, 1, sharex='col', sharey='row')
    fig, (ax1, ax2, ax3) = pl.subplots(3, 1)
    #ax1.set_title('$B_r(\phi)$ - Magnet')
    ax1.plot(np.degrees(phi_range),Br_magnets[0],color='b',label='$B_r$')
      
    #ax2.set_title('$B_t(\phi)$ - Magnet')
    ax2.plot(np.degrees(phi_range),Bt_magnets[0],color='r',label='$B_t$')

    ax3.bar(fft_range[:fft_range_len_graph],fft_Br_magnets[0][:fft_range_len_graph],bar_width,color='b',label='$B_r$')
    ax3.bar(fft_range[:fft_range_len_graph]+bar_width,fft_Bt_magnets[0][:fft_range_len_graph],bar_width,color='r',label='$B_t$')

    if rotor_movement_multiple == 1:
      ax1.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      ax2.set_xticks(np.linspace(0,180*2*magnet_radially_pitch/np.pi,7))
      
      ax1.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi)
      ax2.set_xlim(0,xmax=180*2*magnet_radially_pitch/np.pi)         
    else:
      ax1.set_xticks(np.linspace(0,3*coil_pitch*180/np.pi,7))
      ax2.set_xticks(np.linspace(0,3*coil_pitch*180/np.pi,7)) 
      
      ax1.set_xlim(0,xmax=3*coil_pitch*180/np.pi)
      ax2.set_xlim(0,xmax=3*coil_pitch*180/np.pi)      
    
    ax3.set_xticks(fft_range[:fft_range_len_graph])
    ax3.set_yticks(np.linspace(0,1.0,5))
    ax3.set_xlim(fft_range[:fft_range_len_graph][0],fft_range[:fft_range_len_graph][-1])    
    
    ax1.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
    ax2.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
    ax3.set_xlabel(r'Harmonic Number')
    
    #ax1.set_ylabel(r'Flux Density, $B$ [T]')
    #ax2.set_ylabel(r'Flux Density, $B$ [T]')
    #ax3.set_ylabel(r'Flux Density, $B$ [T]')
    
    ax1.legend(loc='upper right')
    ax2.legend(loc='upper right')
    ax3.legend(loc='upper right')
    
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax2.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax3.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    
    F = plt.gcf()
    DefaultSize = F.get_size_inches()
    #print(DefaultSize) #default was (8,6)
    fig.set_size_inches(DefaultSize[0], 8)
    
    fig.subplots_adjust(hspace=.5)
    
    #fig.text(0.5, 0.04, r'Mechanical Angle, $\phi$ [$^\circ$]', ha='center')
    
    fig.text(0.04, 0.2, r'Flux Density, $B$ [T]', va='center', rotation='vertical')
    fig.text(0.04, 0.5, r'Flux Density, $B_t$ [T]', va='center', rotation='vertical')
    fig.text(0.04, 0.8, r'Flux Density, $B_r$ [T]', va='center', rotation='vertical')

    fig.savefig("Br_Bt_magnets.pdf",bbox_inches='tight')

  magnet_time_end = time.time()

#SAVE PDF FILE OF FPL FILES
######################
  #os.system("fpl2svg --help")
  #fpl2svg -- convert SEMFEM fpl files to svg

    #-d, --fluxdensity          Show flux density colormap
    #-l, --fluxlines            Show flux lines
    #-m, --mesh                 Show the mesh
    #-n, --nlines=<value>       Specify the number of flux lines
    #-s, --strokewidth=<value>  Specify the line width [mm] used for drawing triangles
    #-w, --fluxwidth=<value>    Specify the line width [mm] used for drawing flux lines
    #-?, --help                 Give this help list
    #    --usage                Give a short usage message
if use_machine_periodicity == 1 and save_fpl2pdf == 1:
  fpl2pdf_time_start = time.time()
  use_this_fpl_number = 0
  #use_this_fpl_number = steps-1
  
  os.system("fpl2svg %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_plain"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -e --export-dpi=400 %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_plain"+".png",fplfiles[use_this_fpl_number][:-3]+"svg"))

  #os.system("fpl2svg 0 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("fpl2svg -m -s 0.01 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_mesh"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))

  os.system("fpl2svg -ld -n 25 -w 0.15 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_flux"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))

  fpl2pdf_time_end = time.time()

######################
#CALCULATE POWER, POWER FACTOR AND EFFICIENCY
if args.plot == 0:
  if steps != 1:
    #CALCULATE POWER, POWER FACTOR AND EFFICIENCY
    Ea_instantaneous = turnsPerCoil*sfm_post.voltage[0]
    
    p_in_instantanious = np.zeros(steps)
    Va_instantaneous = np.zeros(steps)
    #p_electromagnetic = np.zeros(steps)
    
    for i in range(steps):
      Ia_instantaneous[i] = Ia_instantaneous[i]/turnsPerCoil
      Va_instantaneous[i] = Ea_instantaneous[i] + Ia_instantaneous[i]*Ra_analytical[enable_ac_resistance]
      p_in_instantanious[i] = Va_instantaneous[i]*Ia_instantaneous[i]
      #p_electromagnetic[i] = (3/2)*(vd[i]*turnsPerCoil*id_new[i] + vq[i]*turnsPerCoil*iq_new[i])
	
    p_in_electrical = abs(np.mean(3*p_in_instantanious))
    #p_in_electrical = 3*np.sqrt(np.mean(p_in_instantanious**2))
    p_in_electrical_old = p_in_electrical
    
    V_rms=np.sqrt(np.mean(Va_instantaneous**2)) #np.mean is the same as np.average (here the goal is to calculate true RMS value, not just the estimate by dividing the peak and supposedly "pure" sinusoidal by sqrt(2) )
    #I_rms=np.sqrt(np.mean(Ia_instantaneous**2)) #not really necessary to compute rms value of current, because we know that we are injecting a pure sinusoidal current wave
    I_rms=max(Ia_instantaneous)/sqrt(2) #can do this because it is a pure sine wave
    #fl_rms=sqrt(mean(fla**2))
    #S= 3*(V_rms*I_rms)   
    

  p_rotor_core_losses = sf.semfem_avg_core_loss_vec[rotor_iron-200]
  p_stator_core_losses = sf.semfem_avg_core_loss_vec[stator_iron-200]
  p_conductive_analytical = 3*(RMS_input_phase_current**2)*Ra_analytical[enable_ac_resistance] #I^2R losses of all three phases combined
  p_total_losses = p_rotor_core_losses + p_stator_core_losses + p_conductive_analytical + p_eddy + p_magnet_total_losses

  #p_in_electrical = p_in_electrical + p_rotor_core_losses + p_stator_core_losses + p_eddy
  p_in_electrical = p_in_electrical + p_total_losses

  S_in_apparant = 3*RMS_input_phase_current*RMS_terminal_phase_voltage
  power_factor_tmp = p_in_electrical_old/S_in_apparant
  S_in_apparant = p_in_electrical/power_factor_tmp

  #THIS METHOD OF OBTAINING p_out_semfem_torque IS ALSO CORRECT, BUT OMITTED BECAUSE IT IS UNECCESARY
  #p_out_instantanious = np.zeros(steps)
  #for i in range(steps):
    #p_out_instantanious[i] = (n_rpm*(np.pi/30)) * sf.semfem_torque_vec[i,1]

  #p_out_semfem_torque = abs(np.mean(p_out_instantanious))

  p_out_semfem_torque = (n_rpm*(np.pi/30)) * average_torque_semfem
  p_out_dq_torque = (n_rpm*(np.pi/30)) * average_torque_dq

  p_in_indirect = p_total_losses + p_out_semfem_torque
  p_in_indirect_dq = p_total_losses + p_out_dq_torque

  if steps == 1: p_in_electrical = p_in_indirect

  if np.isclose(0,p_in_electrical,rtol=1e-04, atol=1e-04, equal_nan=False):
    efficiency_semfem = 0
    efficiency_dq = 0
    efficiency_indirect = 0
    efficiency_indirect_dq = 0
  else:
    efficiency_semfem = (p_out_semfem_torque/p_in_electrical)*100
    efficiency_dq = (p_out_dq_torque/p_in_electrical)*100
    efficiency_indirect = (p_out_semfem_torque/p_in_indirect)*100
    efficiency_indirect_dq = (p_out_dq_torque/p_in_indirect_dq)*100

  #notice np.mean and np.average are the same!

  if steps != 1:
    if np.isclose(0,S_in_apparant,rtol=1e-04, atol=1e-04, equal_nan=False):
      power_factor = 0
    else:
      #power_factor = p_in_electrical/S #p_in_electrical not that accurate
      power_factor = p_in_indirect_dq/S_in_apparant
  else:
    power_factor = -1
else:
  power_factor = db_input_select(output_table_name,"power_factor",primary_key)
  efficiency_semfem = db_input_select(output_table_name,"efficiency_semfem",primary_key)
  efficiency_indirect = db_input_select(output_table_name,"efficiency_indirect",primary_key)
  p_rotor_core_losses = db_input_select(output_table_name,"p_rotor_core_losses",primary_key)
  p_stator_core_losses = db_input_select(output_table_name,"p_stator_core_losses",primary_key)
  p_conductive_analytical = db_input_select(output_table_name,"p_conductive_analytical",primary_key)
  p_eddy = db_input_select(output_table_name,"p_eddy",primary_key)
  p_magnet_total_losses = db_input_select(output_table_name,"p_magnet_losses",primary_key)
  p_total_losses = db_input_select(output_table_name,"total_losses",primary_key)
  p_in_electrical = db_input_select(output_table_name,"p_in_electrical",primary_key)
  p_in_indirect = db_input_select(output_table_name,"p_in_indirect",primary_key)
  p_in_indirect_dq = db_input_select(output_table_name,"p_in_indirect_dq",primary_key)
  p_out_semfem_torque = db_input_select(output_table_name,"p_out_semfem_torque",primary_key)
  p_out_dq_torque = db_input_select(output_table_name,"p_out_dq_torque",primary_key)
  efficiency_dq = db_input_select(output_table_name,"efficiency_dq",primary_key)
  efficiency_indirect_dq = db_input_select(output_table_name,"efficiency_indirect_dq",primary_key) #use this as ultimate efficiency value


if enable_terminal_printout == 1:
  #if args.plot == 0:
    #if steps != 1:  
      #pretty_terminal_table.add_row(["----------------------INPUT QUANTITIES", "-----------", "-----------"])
      #pretty_terminal_table.add_row(["V_rms", V_rms, "[V]"])
      #pretty_terminal_table.add_row(["I_rms", I_rms, "[A]"])
      #pretty_terminal_table.add_row(["S", S, "[VA]"])
  pretty_terminal_table.add_row(["----------------------LOSSES", "-----------", "-----------"])
  pretty_terminal_table.add_row(["p_rotor_core_losses(reported by SEMFEM)", p_rotor_core_losses, "[W]"])
  pretty_terminal_table.add_row(["p_stator_core_losses(reported by SEMFEM)", p_stator_core_losses, "[W]"])
  pretty_terminal_table.add_row(["p_conductive_losses(calculated analytically)", p_conductive_analytical, "[W]"])
  if enable_eddy_current_losses:
    pretty_terminal_table.add_row(["p_eddy_losses (%d harmonics, %d layers)"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy,"[W]"])
  if enable_magnet_losses:
    pretty_terminal_table.add_row(["p_magnet_losses (method %d)"%(magnet_loss_approximation_method),p_magnet_total_losses,"[W]"])
  
  pretty_terminal_table.add_row(["p_total_losses", p_total_losses, "[W]"])
  #pretty_terminal_table.add_row(["p_electromagnetic", np.average(p_electromagnetic), "[W]"])
  pretty_terminal_table.add_row(["----------------------INPUT POWER", "-----------", "-----------"])
  pretty_terminal_table.add_row(["S_in_apparant", S_in_apparant/1000, "[kVA]"])
  pretty_terminal_table.add_row(["p_in_electrical", p_in_electrical/1000, "[kW]"])
  pretty_terminal_table.add_row(["p_in_indirect", p_in_indirect/1000, "[kW]"])
  pretty_terminal_table.add_row(["p_in_indirect_dq", p_in_indirect_dq/1000, "[kW]"])
  pretty_terminal_table.add_row(["----------------------OUTPUT POWER", "-----------", "-----------"])
  pretty_terminal_table.add_row(["p_out_semfem_torque", p_out_semfem_torque/1000, "[kW]"])
  pretty_terminal_table.add_row(["p_out_dq_torque", p_out_dq_torque/1000, "[kW]"])
  pretty_terminal_table.add_row(["----------------------EFFICIENCY AND PF", "-----------", "-----------"])
  pretty_terminal_table.add_row(["efficiency_semfem (not that reliable)", efficiency_semfem, "[%]"])
  pretty_terminal_table.add_row(["efficiency_dq (not that reliable)", efficiency_dq, "[%]"])
  pretty_terminal_table.add_row(["efficiency_indirect", efficiency_indirect, "[%]"])
  pretty_terminal_table.add_row(["efficiency_indirect_dq", efficiency_indirect_dq, "[%]"])
  pretty_terminal_table.add_row(["power_factor", power_factor, "[p.u.]"])
  
  pretty_terminal_table.add_row(["----------------------WARNING REPORT", "-----------", "-----------"])
  if conductors_round0_bars1:
    largest_dimension = max([bar_side_height,bar_side_width])
    approximation_value = largest_dimension/skin_depth    
    if approximation_value > 1.6:
      pretty_terminal_table.add_row(["approximation_value", approximation_value, "not a valid low frequency approximation"])
  
  if conductors_round0_bars1 == 0:
    if input_is_Jrms0_Pcu1_slotcurrent2 == 1:
      pretty_terminal_table.add_row(["Jrms", J, "is used as input despite database setting to use Pcu"])

  if use_parallel_strands:
    if np.pi*single_turn_conductor_radius**2 < parallel_stranded_conductors * np.pi*(strand_diameter/2)**2:
      pretty_terminal_table.add_row(["Di", strand_diameter, "[mm] is not viable for the copper area that is available"])
    
    #if the available area per turn is not adequate to use the finest type of Litz wire (0.2mm with 70 strands), then the turns per coil need to be reduced. If the available area is more than required by this Litz wire, then we can use thicker wire, or use more parallel strands, or insert another turn during manufacturing
    #if enable_litz_wire_selection == 1:
      #while area_available_per_turn < single_turn_area*1e6:
	#turnsPerCoil = turnsPerCoil - 1
      
	#area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	#max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	#max_input_phase_current = iq/turnsPerCoil	

	#l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	#l_total = (coilsTotal/3)*l_coil

	#Ra_analytical_DC = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	#max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical_DC
     
  
  #current_through_slot = max_input_phase_current*turnsPerCoil
  #P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical_DC
  
  #pretty_terminal_table.add_row(["P_conductive_analytical", P_conductive_analytical, "[W]"])
  
  ##if debug == 1:
    ##print("(3) Total analytical conductive (copper) losses P_cu = %f [W]\n"%(P_conductive_analytical)) 
  
  ##S = 3*Va_rms*Ia_rms
  #S = (3/2)*max_input_phase_voltage*max_input_phase_current
  #P_electromechanical = (3/2)*(vd*id + vq*iq)
  #P = P_electromechanical  + P_conductive_analytical
  ##P = (3/2)*(vd*id + vq*iq) 		#inlcudes P_out and P_conductive (does not include other losses)
  #Q= np.sqrt(S**2 - P**2)
  #if S !=0:
      #power_factor=(P/S)*100
  #else:
      #power_factor=0
      
      
  #pretty_terminal_table.add_row(["6 P", P, "[W]"])
  #pretty_terminal_table.add_row(["6 S", S, "[VA]"])
  #pretty_terminal_table.add_row(["6 p.f.", power_factor, "[p.u.]"])
  #####################   

#terminal_peak_LL_voltage = (max_input_phase_voltage*np.sqrt(3))/(0.95*1.15)

db_output_insert(output_table_name,"magnet_mass",primary_key,magnet_mass)
db_output_insert(output_table_name,"copper_mass",primary_key,copper_mass)
db_output_insert(output_table_name,"stator_yoke_mass",primary_key,stator_yoke_mass)
db_output_insert(output_table_name,"rotor_yoke_mass",primary_key,rotor_yoke_mass)
db_output_insert(output_table_name,"aluminium_mass",primary_key,aluminium_mass)
db_output_insert(output_table_name,"total_mass",primary_key,total_mass)
db_output_insert(output_table_name,"turnsPerCoil",primary_key,turnsPerCoil)
db_output_insert(output_table_name,"average_torque_dq",primary_key,average_torque_dq)
db_output_insert(output_table_name,"average_torque_semfem",primary_key,average_torque_semfem)
db_output_insert(output_table_name,"torque_ripple_dq",primary_key,torque_ripple_dq)
db_output_insert(output_table_name,"torque_ripple_semfem",primary_key,torque_ripple_semfem)
db_output_insert(output_table_name,"terminal_LL_peak_voltage",primary_key,peak_terminal_line_voltage)
db_output_insert(output_table_name,"phase_current_peak",primary_key,max_input_phase_current)
db_output_insert(output_table_name,"p_in_electrical",primary_key,p_in_electrical)
db_output_insert(output_table_name,"p_out_semfem_torque",primary_key,p_out_semfem_torque)
db_output_insert(output_table_name,"p_out_dq_torque",primary_key,p_out_dq_torque)
db_output_insert(output_table_name,"efficiency_dq",primary_key,efficiency_dq)
db_output_insert(output_table_name,"efficiency_semfem",primary_key,efficiency_semfem)
db_output_insert(output_table_name,"efficiency_indirect",primary_key,efficiency_indirect)
db_output_insert(output_table_name,"efficiency_indirect_dq",primary_key,efficiency_indirect_dq)
db_output_insert(output_table_name,"power_factor",primary_key,power_factor)
db_output_insert(output_table_name,"p_rotor_core_losses",primary_key,p_rotor_core_losses)
db_output_insert(output_table_name,"p_stator_core_losses",primary_key,p_stator_core_losses)
db_output_insert(output_table_name,"p_conductive_analytical",primary_key,p_conductive_analytical)
db_output_insert(output_table_name,"p_eddy",primary_key,p_eddy)
db_output_insert(output_table_name,"p_magnet_losses",primary_key,p_magnet_total_losses)
db_output_insert(output_table_name,"total_losses",primary_key,p_total_losses)
#db_output_insert(output_table_name,"p_electromagnetic",primary_key,np.average(p_electromagnetic))
db_output_insert(output_table_name,"periodicity_used",primary_key,use_machine_periodicity)
db_output_insert(output_table_name,"Ra_analytical_DC",primary_key,Ra_analytical[0])
db_output_insert(output_table_name,"Ra_analytical_AC",primary_key,Ra_analytical[1])
db_output_insert(output_table_name,"p_in_indirect",primary_key,p_in_indirect)
db_output_insert(output_table_name,"p_in_indirect_dq",primary_key,p_in_indirect_dq)
db_output_insert(output_table_name,"torque_density",primary_key,torque_density)
db_output_insert(output_table_name,"id",primary_key,id)
db_output_insert(output_table_name,"iq",primary_key,iq)
db_output_insert(output_table_name,"phase_voltage_rms",primary_key,RMS_terminal_phase_voltage)
db_output_insert(output_table_name,"line_current_rms",primary_key,RMS_input_line_current)
db_output_insert(output_table_name,"phase_voltage_peak",primary_key,max_input_phase_voltage)
db_output_insert(output_table_name,"l_series_connections",primary_key,l_series_connections)
db_output_insert(output_table_name,"l_coil",primary_key,l_coil)
db_output_insert(output_table_name,"l_total",primary_key,l_total)
db_output_insert(output_table_name,"Ra_analytical_DC_l_series",primary_key,resistance_analytical(l_series_connections)[0])
db_output_insert(output_table_name,"effective_fill_factor",primary_key,effective_fill_factor)

db_output_insert(output_table_name,"predicted_torque",primary_key,predicted_torque)
db_output_insert(output_table_name,"torque_target",primary_key,torque_target)
db_output_insert(output_table_name,"predicted_torque_difference",primary_key,predicted_torque_difference)
db_output_insert(output_table_name,"torque_prediction_error_percentage",primary_key,torque_prediction_error_percentage)
db_output_insert(output_table_name,"torque_target_shortfall_due_to_copper_losses_limit_percentage",primary_key,torque_target_shortfall_due_to_copper_losses_limit_percentage)
db_output_insert(output_table_name,"total_torque_shortfall_percentage",primary_key,total_torque_shortfall_percentage)

if enable_turns_calculation:
  db_output_insert(input_table_name,"force_this_number_of_turns",primary_key,turnsPerCoil_dq)
  db_output_insert(output_table_name,"turnsPerCoil",primary_key,turnsPerCoil_dq)

if synchronous_inductance>0 and np.isclose(current_angle_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
  #db_output_insert(output_table_name,"synchronous_inductance_single_turn",primary_key,synchronous_inductance_single_turn)
  #db_output_insert(output_table_name,"PM_flux_linkage_single_turn",primary_key,flm_single_turn)

  db_output_insert(input_table_name,"synchronous_inductance_single_turn",primary_key,synchronous_inductance_single_turn)
  db_output_insert(input_table_name,"PM_flux_linkage_single_turn",primary_key,flm_single_turn)

if input_is_Jrms0_Pcu1_slotcurrent2 == 1 or enable_target_mech_power:
  db_output_insert(output_table_name,"current_density_rms",primary_key,J)
  db_output_insert(input_table_name,"current_density_rms",primary_key,J)
  db_output_insert(output_table_name,"current_angle_degrees",primary_key,current_angle_degrees)
  db_output_insert(input_table_name,"current_angle_degrees",primary_key,current_angle_degrees)

#pretty_terminal_table.add_row(["turnsPerCoil",turnsPerCoil,"[turns]"])

now = datetime.datetime.now()
#c.execute("INSERT INTO machine_input_parameters(date_of_result) values (?)", (now))
#c.execute("INSERT INTO machine_semfem_output(date_of_result) values (?)", (now))
db_output_insert(output_table_name,"date_of_result",primary_key,now)
db_output_insert(input_table_name,"date_of_result",primary_key,now)


####################################
# Calculate flux density harmonics #
####################################
if enable_flux_density_harmonic_calc == 1:
  M = 1024					#M should be even number to optimally use the FFT algorithm
  #phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/kq))
  phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/(slots_per_phase_per_pole*4)))

  gap_inner = stator_coil_radius
  gap_center = stator_coil_radius + 0.5*g
  gap_outer = stator_coil_radius + g

  one_electrical_period = (2*np.pi)/periodicity_in_one_full_rotation

  for p in range(0,len(analyse_specific_step)):
    if analyse_specific_step[p]+1 < 10:
      band_fpl_file = 'project_sorspm_semfem/results/fieldplots/band_machine_field00%d.fpl'%(analyse_specific_step[p]+1)
    elif analyse_specific_step[p]+1 <100:
      band_fpl_file = 'project_sorspm_semfem/results/fieldplots/band_machine_field0%d.fpl'%(analyse_specific_step[p]+1)
    elif analyse_specific_step[p]+1 <1000:
      band_fpl_file = 'project_sorspm_semfem/results/fieldplots/band_machine_field%d.fpl'%(analyse_specific_step[p]+1)
      
    print "band_fpl_file being used = %s"%band_fpl_file 
    ###############################################
    #AR
    if fft_AR == 1:
      #dir_parent="old_simulations/pk_0008/AR/"
      dir_parent=""
      fplfile_AR = dir_parent+band_fpl_file #notice that only the first step is being used to obtain the flux density pattern at that instance
      
      Br_AR = []
      Br_AR.append([])
      Br_AR.append([])
      Br_AR.append([])

      #Bm, Br_AR[0], Bt = sf.get_arc_B(fplfile_AR, 0,one_electrical_period-phase_A_offset, gap_inner, M)		#the last argument specifies the number of data points one desires
      #Bm, Br_AR[1], Bt = sf.get_arc_B(fplfile_AR, mechanical_rotation_at_specific_step*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step*(np.pi/180), gap_center, M)
      #Bm, Br_AR[2], Bt = sf.get_arc_B(fplfile_AR, 0,one_electrical_period-phase_A_offset, gap_outer, M)

      Bm, Br_AR[1], Bt = sf.get_arc_B(fplfile_AR, 0,one_electrical_period-phase_A_offset, gap_center, M)
      Bm, Br_AR[2], Bt = sf.get_arc_B(fplfile_AR, mechanical_rotation_at_specific_step*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step*(np.pi/180), gap_center, M)

      array_size = np.size(Br_AR[1])
      array_step_size = array_size/steps
      shift_size = int(round(array_step_size*(analyse_specific_step[p]-1),0))
      new_Br_AR_array = np.zeros(array_size)

      if dont_shift_br_array == 0:
	for i in range(0,shift_size):
	  new_Br_AR_array[i] = Br_AR[2][np.size(Br_AR[2])-shift_size+i]

	for i in range(shift_size,np.size(Br_AR[1])):
	  new_Br_AR_array[i] = Br_AR[1][i]
      else:
	for i in range(0,np.size(Br_AR[1])):
	  new_Br_AR_array[i] = Br_AR[1][i]	

      Br_AR_fft = np.fft.rfft(new_Br_AR_array[1],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
      #Br_AR_fft = np.fft.rfft(Br_AR[1],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
      #Br_AR_fft = abs(np.fft.rfft(Br_AR[1],M)*2/M)	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)
      Br1_AR = abs(Br_AR_fft[1])
      max_Br_1_AR = max(Br_AR[1])

      Br_THD_AR = sqrt(sum(Br_AR_fft[2:int((M-1)/2)]**2))/Br1_AR

    ###############################################
    #PM
    if fft_PM == 1:
      #dir_parent="old_simulations/pk_0008/PM/"
      dir_parent=""
      fplfile_PM = dir_parent+band_fpl_file #notice that only the first step is being used to obtain the flux density pattern at that instance
      
      Br_PM = []
      Br_PM.append([])
      Br_PM.append([])
      Br_PM.append([])

      #Bm, Br_PM[0], Bt = sf.get_arc_B(fplfile_PM, 0,one_electrical_period-phase_A_offset, gap_inner, M)		#the last argument specifies the number of data points one desires
      #Bm, Br_PM[1], Bt = sf.get_arc_B(fplfile_PM, mechanical_rotation_at_specific_step*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step*(np.pi/180), gap_center, M)
      #Bm, Br_PM[2], Bt = sf.get_arc_B(fplfile_PM, 0,one_electrical_period-phase_A_offset, gap_outer, M)


      Bm, Br_PM[1], Bt = sf.get_arc_B(fplfile_PM, 0,one_electrical_period-phase_A_offset, gap_center, M)
      Bm, Br_PM[2], Bt = sf.get_arc_B(fplfile_PM, mechanical_rotation_at_specific_step*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step*(np.pi/180), gap_center, M)

      array_size = np.size(Br_PM[1])
      array_step_size = array_size/steps
      shift_size = int(round(array_step_size*(analyse_specific_step[p]-1),0))
      new_Br_PM_array = np.zeros(array_size)

      if dont_shift_br_array == 0:
	for i in range(0,shift_size):
	  new_Br_PM_array[i] = Br_PM[2][np.size(Br_PM[2])-shift_size+i]

	for i in range(shift_size,np.size(Br_PM[1])):
	  new_Br_PM_array[i] = Br_PM[1][i]
      else:
	for i in range(0,np.size(Br_PM[1])):
	  new_Br_PM_array[i] = Br_PM[1][i]	

      Br_PM_fft = np.fft.rfft(new_Br_PM_array[1],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
      #Br_PM_fft = np.fft.rfft(Br_PM[1],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
      #Br_PM_fft = abs(np.fft.rfft(Br_PM[1],M)*2/M)	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken) 
      Br1_PM = abs(Br_PM_fft[1])
      max_Br_1_PM = max(Br_PM[1])

      Br_THD_PM = sqrt(sum(Br_PM_fft[2:int((M-1)/2)]**2))/Br1_PM  

    ###############################################
    #ALL
    if fft_ALL == 1:
      #dir_parent="old_simulations/pk_0008/ALL/"
      dir_parent=""
      fplfile_ALL = dir_parent+band_fpl_file #notice that only the first step is being used to obtain the flux density pattern at that instance
      
      Br_ALL = []
      Br_ALL.append([])
      Br_ALL.append([])
      Br_ALL.append([])

      #Bm, Br_ALL[0], Bt = sf.get_arc_B(fplfile_ALL, 0,one_electrical_period-phase_A_offset, gap_inner, M)		#the last argument specifies the number of data points one desires
      
      
      
      Bm, Br_ALL[1], Bt = sf.get_arc_B(fplfile_ALL, 0,one_electrical_period-phase_A_offset, gap_center, M)
      Bm, Br_ALL[2], Bt = sf.get_arc_B(fplfile_ALL, mechanical_rotation_at_specific_step[p]*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step[p]*(np.pi/180), gap_center, M)
      #Bm, Br_ALL[1], Bt = sf.get_arc_B(fplfile_ALL, mechanical_rotation_at_specific_step*(np.pi/180),one_electrical_period-phase_A_offset+mechanical_rotation_at_specific_step*(np.pi/180), gap_center, M)
      
      array_size = np.size(Br_ALL[1])
      array_step_size = array_size/steps
      shift_size = int(round(array_step_size*(analyse_specific_step[p]-1),0))
      new_Br_ALL_array = np.zeros(array_size)

      if dont_shift_br_array == 0:
	for i in range(0,shift_size):
	  new_Br_ALL_array[i] = Br_ALL[2][np.size(Br_ALL[2])-shift_size+i]

	for i in range(shift_size,np.size(Br_ALL[1])):
	  #print i
	  new_Br_ALL_array[i] = Br_ALL[1][i]
      else:
	for i in range(0,np.size(Br_ALL[1])):
	  new_Br_ALL_array[i] = Br_ALL[1][i]	


      #Br_ALL_fft = np.fft.rfft(Br_ALL[1],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken) 
      #Br_ALL_fft = np.fft.rfft(Br_ALL[2],M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken) 
      Br_ALL_fft = np.fft.rfft(new_Br_ALL_array,M)*2/M	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken) 
      Br1_ALL = abs(Br_ALL_fft[1])
      max_Br_1_ALL = max(Br_ALL[1])

      Br_THD_ALL = sqrt(sum(Br_ALL_fft[2:int((M-1)/2)]**2))/Br1_ALL  


    pl.figure(2*p+1)
    ax=pl.gca()
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    pl.title(r'$B_{r}(\phi)$ Spacial distribution, time %.4f [ms], rotation of %.2f [e.deg] or %.2f [m.deg]'%(time_at_specific_step[p]*1000,electrical_rotation_at_specific_step[p],mechanical_rotation_at_specific_step[p]))
    pl.axis(xmin=-180/q,xmax=180/q)
    #pl.plot(np.degrees(phi_range),Br_AR[0],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} - h/2}$')
    #pl.plot(np.degrees(phi_range),Br_AR[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$')
    #pl.plot(np.degrees(phi_range),Br_AR[2],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} + h/2}$')

    if fft_PM == 1:
      pl.plot(np.degrees(phi_range),new_Br_PM_array,linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ PMs only')
    if fft_AR == 1:
      pl.plot(np.degrees(phi_range),new_Br_AR_array,linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ Windings only')
    if fft_ALL == 1:
      #pl.plot(np.degrees(phi_range),Br_ALL[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ All')
      pl.plot(np.degrees(phi_range),new_Br_ALL_array,linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ All')
    #pl.plot(np.degrees(phi_range),Br_PM[1]+Br_AR[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ Manually added') #results in exactly the same graph as "All"

    pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]',fontsize=FONTSIZE)
    pl.ylabel(r'Radial flux density, $B_{r}$ [T]',fontsize=FONTSIZE)  
    pl.legend(loc='upper right',fontsize=FONTSIZE)  
    pl.grid(True)
    
    
    #plot harmonics
    if plot_harmonics == 1:
      pl.figure(2*p+2)
      pl.title(r'|$B_{r}(\phi)$| Harmonic distribution magntitude, time %.4f [ms], rotation of %.2f [e.deg] or %.2f [m.deg]'%(time_at_specific_step[p]*1000,electrical_rotation_at_specific_step[p],mechanical_rotation_at_specific_step[p]))
      my_range = 20
      k = np.arange(my_range)			#maak aanname dat elektriese periode van die sein 1 Hz is (dan sal 1st harmoniek by k = 1 le, en dus verteenwoordig k ook dan sommer die frekwensie in Hz)
      #pl.plot(k,abs(Br_PM_fft[:my_range]),label=r'$B_{r|r_{n}}$ PMs only')
      #pl.plot(k,abs(Br_AR_fft[:my_range]),label=r'$B_{r|r_{n}}$ Windings only')
      #pl.plot(k,abs(Br_ALL_fft[:my_range]),label=r'$B_{r|r_{n}}$ All')
      
      
      index = k
      bar_width = 0.35
      opacity = 0.4
      
      if fft_PM == 1:
	pl.bar(index, abs(Br_PM_fft[:my_range]), bar_width, alpha=opacity, color='r', label=r'$B_{r|r_{n}}$ PMs only')
      if fft_AR == 1:
	pl.bar(index+bar_width, abs(Br_AR_fft[:my_range]), bar_width, alpha=opacity, color='b', label=r'$B_{r|r_{n}}$ Windings only')
      if fft_ALL == 1:
	pl.bar(index, abs(Br_ALL_fft[:my_range]), bar_width, alpha=opacity, color='g', label=r'$B_{r|r_{n}}$ All')
      
      pl.xticks(index)
      
      pl.legend(loc='upper right',fontsize=FONTSIZE)  
      pl.xlabel('Frequency [Hz]',fontsize=FONTSIZE)
      pl.ylabel('|$B_{r|r_{n}}$| [T]',fontsize=FONTSIZE)
      pl.tight_layout()
    
    
    
    
    
    
    #pl.figure(3)
    #pl.title(r'$B_{r}(\phi)$ Harmonic distribution')
    #my_range = 20
    #k = np.arange(my_range)			#maak aanname dat elektriese periode van die sein 1 Hz is (dan sal 1st harmoniek by k = 1 le, en dus verteenwoordig k ook dan sommer die frekwensie in Hz)
    ##pl.plot(k,abs(Br_PM_fft[:my_range]),label=r'$B_{r|r_{n}}$ PMs only')
    ##pl.plot(k,abs(Br_AR_fft[:my_range]),label=r'$B_{r|r_{n}}$ Windings only')
    ##pl.plot(k,abs(Br_ALL_fft[:my_range]),label=r'$B_{r|r_{n}}$ All')
    
    
    #index = k
    #bar_width = 0.35
    #opacity = 0.4
    
    #if fft_PM == 1:
      #pl.bar(index, Br_PM_fft[:my_range], bar_width, alpha=opacity, color='r', label=r'$B_{r|r_{n}}$ PMs only')
    #if fft_AR == 1:
      #pl.bar(index+bar_width, abs(Br_AR_fft[:my_range]), bar_width, alpha=opacity, color='b', label=r'$B_{r|r_{n}}$ Windings only')
    #if fft_ALL == 1:
      #pl.bar(index, abs(Br_ALL_fft[:my_range]), bar_width, alpha=opacity, color='g', label=r'$B_{r|r_{n}}$ All')
    
    #pl.xticks(index)
    
    #pl.legend(loc='upper right',fontsize=FONTSIZE)  
    #pl.xlabel('Frequency [Hz]',fontsize=FONTSIZE)
    #pl.ylabel('$B_{r|r_{n}}$ [T]',fontsize=FONTSIZE)
    #pl.tight_layout()  
    
    #print(Br_PM_fft[:my_range])


    #db_output_insert("flux_density_output","date_of_result",primary_key,now)

    #np.savetxt(dir_parent+'script_results/idrfpm/br.csv', transpose([Br[0],Br[1],Br[2]]), delimiter = ',',newline='\n',header='Br @(rn-h/2), Br @(rn), Br @(rn+h/2)',fmt='%f')



if store_csv == 1:
  equivelent_electrical_angle_position = np.zeros(len(sf.semfem_torque_vec[:,1]))
  ia = np.zeros(len(sf.semfem_torque_vec[:,1]))
  ib = np.zeros(len(sf.semfem_torque_vec[:,1]))
  ic = np.zeros(len(sf.semfem_torque_vec[:,1]))
  
  for i in range(0,len(sf.semfem_torque_vec[:,1])):
    equivelent_electrical_angle_position[i] = i*(360/len(sf.semfem_torque_vec[:,1]))
    ia[i] = input_phase_A_current[i]
    ib[i] = input_phase_B_current[i]
    ic[i] = input_phase_C_current[i]
  
  #np.savetxt('semfem_script_output.csv', [sf.semfem_time_vec,equivelent_electrical_angle_position,sf.semfem_torque_vec[:,1],input_phase_A_current,input_phase_B_current,input_phase_C_current],delimiter = ',',newline='\n',header='time_stamp,equivelent_electrical_angle_position,semfem_torque,ia,ib,ic',fmt='%f')
  #np.savetxt('semfem_script_output.csv', [sf.semfem_time_vec,sf.semfem_torque_vec[:,1]],delimiter = ',',newline='\n',header='time_stamp,semfem_torque',fmt='%f')
  np.savetxt('semfem_script_output.csv', transpose([sf.semfem_time_vec,equivelent_electrical_angle_position,sf.semfem_torque_vec[:,1],ia,ib,ic]),delimiter = ',',newline='\n',header='time_stamp,equivelent_electrical_angle_position,semfem_torque,ia,ib,ic',fmt='%8f')

####################################
# Calculate flux density harmonics #
####################################
if store_Br_airgap == 1:
  M = 1024					#M should be even number to optimally use the FFT algorithm
  fplfile = "project_sorspm_semfem/results/fieldplots/band_machine_field001.fpl"

  #get_arc_B(self, fpl_file, theta1, theta2, r, npoints) #function prototype from semfem.py
  #Bm, Br[0], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn - hc/2, 2*M)		#the last argument specifies the number of data points one desires
  #Bm, Br[1], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn, 2*M)
  #Bm, Br[2], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn + hc/2, 2*M)

  Bm, Br, Bt = sf.get_arc_B(fplfile, 0,(mechRotation/2), stator_coil_radius+0.5*g, 2*M)

  ##get first harmonic of Br @ rn
  #pl.figure(3)

  #Br_fft = abs(np.fft.rfft(Br[1],M)*2/M)	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
  #Br1 = Br_fft[1]
  #max_Br_1 = max(Br[1])

  #my_range = 20
  #k = np.arange(my_range)			#maak aanname dat elektriese periode van die sein 1 Hz is (dan sal 1st harmoniek by k = 1 le, en dus verteenwoordig k ook dan sommer die frekwensie in Hz)
  #pl.plot(k,abs(Br_fft[:my_range]))
  #pl.xlabel('Freq (Hz)')
  #pl.ylabel('|$B_{r|r_{n}}$|')
  #pl.show()
  
  np.savetxt('B_airgap.csv', transpose([np.linspace(0,mechRotation,2*M),Br]),delimiter = ',',newline='\n',header='position,Br',fmt='%8f')
  
  #Br_THD = sqrt(sum(Br_fft[2:int((M-1)/2)]**2))/Br1 

if store_results_in_database == 1 and overwrite_previous_result == 1:
  #print "SORSPM_semfem.py -- going to commit"
  conn.commit()

sf.semfem_end()

if enable_terminal_printout == 1:
  print pretty_terminal_table

  text_file = open("semfem_terminal_output.txt", "w")
  text_file.write(pretty_terminal_table.get_string())
  text_file.close()   
  print("Time used by simulation = %.2f"%(simulation_time_end-simulation_time_start))
  if enable_eddy_current_losses:
    print("Time used by eddy current calculations = %.2f"%(eddy_time_end-eddy_time_start))
  
  if enable_magnet_losses:
    print("Time used by magnet loss calculations = %.2f"%(magnet_time_end-magnet_time_start))
    
  if save_fpl2pdf:
    print("Time used by fpl to pdf conversions = %.2f"%(fpl2pdf_time_end-fpl2pdf_time_start))
    
  print("Total time elapsed = %.2f"%(time.time()-simulation_time_start))

#conn.commit()
conn.close()
#print "SORSPM_semfem.py -- finished"


pl.show()