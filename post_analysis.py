#!/usr/bin/python
from __future__ import division
import argparse
import os,sys
import pylab as pl
import numpy as np
from prettytable import PrettyTable
#import cairosvg
#from scitools.std import movie
from datetime import datetime
import shutil
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from scipy.interpolate import griddata 
import sqlite3

#spline libraries
from scipy.interpolate import Rbf
from matplotlib import cm 
from scipy.stats import gaussian_kde
from scipy import stats
from scipy.interpolate import interp1d
import matplotlib.mlab as mlab
from itertools import chain

###PARSER
##########################################################################################
parser = argparse.ArgumentParser(description="Draw graphs",formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("option", help="Select which graph to produce:\n"+
		    "1- main.py          -- Plot 3D pareto surface.\n"+
		    "2- main.py          -- Plot 2D scatter plots.\n"+
		    "3- main.py          -- Plot 2D contour plot.\n"+
		    "4- main.py          -- Plot elite outline plot.\n"+
		    "5- main.py          -- Plot elite outline plot.\n"+
		    "6- main.py          -- Re-simulate ALL solutions elite with better mesh and more steps in new subdirectory.\n"+
		    "7- main.py          -- Re-simulate FILTERED solutions elite with better mesh and more steps in new subdirectory.\n"+
		    "8- main.py          -- Plot 3D pareto surface using solutions tested at different current densities.\n"+
		    "9- main.py          -- Plot 3D pareto surface using optimisations completed at different current densities, and then afterwards tested at other current densities.\n"+
		    "10-main.py		 -- Plot torque graphs\n"
		    ,type=int, choices=[1,2,3,4,5,6,7,8,9,10])

args = parser.parse_args()									#store the argument parsed into the variable "args"

##########################################################################################

#dir = ''
#dir = 'optimisation/2016-06-22(backup)/'
#dir = 'optimisation/2016-06-22_tor_ripp_mass/'
#dir = 'optimisation/2016-07-12/'
#dir = 'optimisation/2016-07-15(kq=0.5)/'
#dir = 'optimisation/2016-07-18/'
#dir = 'optimisation/2017-01-19/J=2.5A/'
#dir = 'optimisation/2017-01-19/J=4.0A/'
#dir = 'optimisation/2017-04-09/J=1.0A/'
dir = 'optimisation/2017-07-09/optimise_torque_density/'
#dir = 'optimisation/2017-04-09/J=4.0A/'

#dir = 'optimisation/J=3.0_ff=0.4_p=40/'
#dir = 'optimisation/J=5.0_ff=0.4_p=40/'
#dir = 'optimisation/J=7.0_ff=0.4_p=40/'

#dir = 'optimisation/J=5.0_ff=0.8_p=40/'

#dir = 'optimisation/J=3.0_ff=0.4_p=36/'
#dir = 'optimisation/J=5.0_ff=0.4_p=36/'
#dir = 'optimisation/J=7.0_ff=0.4_p=36/'

#dir = 'optimisation/solid_copper_conductors/J=5.0_ff=1.0_p=40/'

#subdirectory = ''
subdirectory = 'best_with_improved_mesh_and_steps_J=1.0/'
#subdirectory = 'best_with_improved_mesh_and_steps_J=3.0_ff=0.4/'
#subdirectory = 'best_with_improved_mesh_and_steps_J=7.0_ff=0.4/'
have_already_resimulated = 0


if args.option == (8 or 9):
  dir_sol = []
  dir_sol.append([dir+'best_with_improved_mesh_and_steps_J=1.0/',0.4])
  dir_sol.append([dir+'best_with_improved_mesh_and_steps_J=2.5/',0.3])
  dir_sol.append([dir+'best_with_improved_mesh_and_steps_J=4.0/',0.3])
  number_of_operating_points = len(dir_sol)
  
  min_torque_ripple_pu = 0
  max_torque_ripple_pu = 1.25
  #max_torque_ripple_pu = -1
  
  min_torque_density_pu = 0
  #min_torque_density_pu = 0.95
  max_torque_density_pu = -1
  
  min_eff_pu = 0
  max_eff_pu = -1
  
  min_mass_pu = 0
  max_mass_pu = -1
  
  min_avg_torque_pu = 0
  max_avg_torque_pu = -1
  
  min_pf_pu = 0
  max_pf_pu = -1
  
  min_pm_mass_pu = 0
  max_pm_mass_pu = -1
  

if have_already_resimulated == 1:
  dir = dir+subdirectory

#show solutions within these limits
#MAXIMUMS
#max_avg_torque = 260
max_avg_torque = -1
max_mass = -1
#max_torque_ripple = 80
#max_torque_ripple = 15 #J=3 or 7
#max_torque_ripple = 10 #J=5
#max_torque_ripple = 5.5 #J=5 with ff=1
#max_torque_ripple = 3.8 #J=5 with ff=0.8
max_torque_ripple = -1
max_eff = -1
max_pf = -1
max_torque_density = -1
#max_pm_mass = 1.1 #J=5 with ff=1
max_pm_mass = -1

#MINIMUMS
min_avg_torque = 0
min_mass = 0
min_torque_ripple = 0
min_eff = 0
#min_pf = 0
min_pf = 0
#min_torque_density = 6 #J=3
#min_torque_density = 10.8 #J=5
#min_torque_density = 13.5 #J=2.5
#min_torque_density = 13.6 #J=4 with ff=1
#min_torque_density = 17 #J=5 with ff=1
#min_torque_density = 13.8 #J=7
#min_torque_density = 16.44 #J=5 ff=0.8
min_torque_density = 0
min_pm_mass = 0

markersize=12
LINEWIDTH=1
#use latex font
#pl.rc('text', usetex=True)
#pl.rc('font', family='serif')
font_size=18
#font_size=24
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']


pretty_terminal_table = PrettyTable(['Variable','Value'])
#pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

if not args.option == (8 or 9):
  pretty_best_table = PrettyTable(['Variable','Value'])
else:
  headers = []
  headers.append('Variable')
  for i in range(0,len(dir_sol)):
    headers.append('J = '+dir_sol[i][0][-4:-1]+' A/mm2')
  
  pretty_best_table = PrettyTable(headers)

pretty_best_table.align = 'l'
pretty_best_table.border= True 

#setup databse connection
conn = sqlite3.connect(dir+'optimise.db')
c = conn.cursor()
best_dir = dir + 'best.db'
c.execute("ATTACH DATABASE '%s' as best"%(best_dir))
#c.execute("ATTACH DATABASE 'best.db' as best")

ideal_optimization_sequence = []  

axis_titles = []
axis_titles.append(r"Efficiency ($\eta$) [\%]")
axis_titles.append(r"Torque Ripple ($\tau_{ripple}$) [\%]")
axis_titles.append(r"Total Mass ($m$) [kg]")
axis_titles.append(r"Torque ($\tau$) [Nm]")
axis_titles.append(r"Power Factor ($p.f.$) [p.u.]")
axis_titles.append(r"Torque Density ($\tau_{density}$) [Nm/kg]")
axis_titles.append(r"PM Mass ($m_{PM}$) [kg]")

filters = []

#efficiency filter
filters.append([])
filters[0].append([88,90,[]])
filters[0].append([84,86,[]])
filters[0].append([80,82,[]])
filters[0].append([76,78,[]])
filters[0].append([72,74,[]])

#torque ripple filter
filters.append([])
#filters[1].append([0,4,[]])
#filters[1].append([4,8,[]])
#filters[1].append([8,12,[]])
#filters[1].append([16,20,[]])
#filters[1].append([20,24,[]])

filters[1].append([8,12,[]])
filters[1].append([12,16,[]])
filters[1].append([16,20,[]])
filters[1].append([20,24,[]])
filters[1].append([24,28,[]])

#total mass filter
filters.append([])
filters[2].append([30,40,[]])
filters[2].append([50,60,[]])
filters[2].append([70,80,[]])
filters[2].append([90,100,[]])
filters[2].append([110,120,[]])

#torque average filter
filters.append([])
filters[3].append([30,40,[]])
filters[3].append([50,60,[]])
filters[3].append([70,80,[]])
filters[3].append([90,100,[]])
filters[3].append([110,120,[]])

#power factor filter
filters.append([])
filters[4].append([1.00,0.95,[]])
filters[4].append([0.95,0.90,[]])
filters[4].append([0.90,0.85,[]])
filters[4].append([0.85,0.80,[]])
filters[4].append([0.80,0.75,[]])

#torque density filter
filters.append([])
filters[5].append([3.5,3.3,[]])
filters[5].append([3.3,3.1,[]])
filters[5].append([3.1,2.9,[]])
filters[5].append([2.9,2.7,[]])
filters[5].append([2.7,2.5,[]])

#pm mass filter
filters.append([])
#filters[6].append([30,25,[]])
#filters[6].append([25,20,[]])
#filters[6].append([15,10,[]])
#filters[6].append([10,5,[]])
#filters[6].append([5,0,[]])

filters[6].append([1.0,1.5,[]])
filters[6].append([1.5,2.0,[]])
filters[6].append([2.0,2.5,[]])
filters[6].append([2.5,3.0,[]])
filters[6].append([3.0,3.5,[]])

if args.option == (8 or 9): 
  max_torque_density = max_torque_density_pu
  max_torque_ripple = max_torque_ripple_pu
  max_eff = max_eff_pu
  max_mass = max_mass_pu 
  max_avg_torque = max_avg_torque_pu
  max_pf = max_pf_pu
  max_pm_mass = max_pm_mass_pu
  
  min_torque_density = min_torque_density_pu
  min_torque_ripple = min_torque_ripple_pu
  min_eff = min_eff_pu
  min_mass = min_mass_pu
  min_avg_torque = min_avg_torque_pu
  min_pf = min_pf_pu
  min_pm_mass = min_pm_mass_pu

c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_eff',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[0],'efficiency_indirect_dq',max_eff,min_eff,1,filters[0]])
c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_ripple',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[1],'torque_ripple_semfem',max_torque_ripple,min_torque_ripple,-1,filters[1]])
c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_mass_total',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[2],'total_mass',max_mass,min_mass,-1,filters[2]])
c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_average',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[3],'average_torque_dq',max_avg_torque,min_avg_torque,1,filters[3]])
c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pf',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[4],'power_factor',max_pf,min_pf,1,filters[4]])  
c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_density',))
ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[5],'torque_density',max_torque_density,min_torque_density,1,filters[5]])  
try:
  c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pm_mass',))
  ideal_optimization_sequence.append([c.fetchone()[0],axis_titles[6],'magnet_mass',max_pm_mass,min_pm_mass,-1,filters[6]])  
except:
  print "This dataset did not have PM_mass as a possible objective."

#sort the weight and title sequence
sorted_sequence = sorted(ideal_optimization_sequence)
sorted_titles = []

for i in range (0,len(sorted_sequence)):
  if sorted_sequence[i][0]>=1:
    for j in range (0,len(sorted_sequence)):
      if sorted_sequence[i][0]>0:
	if sorted_sequence[i] == ideal_optimization_sequence[j]:
	  sorted_titles.append(ideal_optimization_sequence[j][1])
    
x_axis_title = sorted_titles[0]
y_axis_title = sorted_titles[1]
z_axis_title = sorted_titles[2]

#get ideal values
#try to get the primary_key ideal value outputs

for i in range(0,len(ideal_optimization_sequence)):
  try:
    c.execute("SELECT %s FROM best.machine_semfem_output WHERE primary_key = ?"%(ideal_optimization_sequence[i][2]),(ideal_optimization_sequence[i][0],))
    ideal_optimization_sequence[i].append(c.fetchone()[0])
  except:
    ideal_optimization_sequence[i].append(1)

  pretty_terminal_table.add_row(["ideal_"+ideal_optimization_sequence[i][1],ideal_optimization_sequence[i][7]])


#get max number of primary_keys in database
max_keys = 0
for i in range(0,len(ideal_optimization_sequence)):
  if ideal_optimization_sequence[i][0] > max_keys:
    max_keys = ideal_optimization_sequence[i][0]

if not args.option == (8 or 9): 
  for j in range(0,len(ideal_optimization_sequence)):
    best_temp = 0
    value_temp = 0
    #print(ideal_optimization_sequence[j][1])
    #print(ideal_optimization_sequence[j][5])
    for i in range(1,max_keys+1):
      try:
	c.execute("SELECT %s FROM best.machine_semfem_output WHERE primary_key = ?"%(ideal_optimization_sequence[j][2]),(i,))
	value_temp = c.fetchone()[0]
	#print value_temp
	if i == 1:
	  best_temp = value_temp
	else:
	  if ideal_optimization_sequence[j][5] == 1:
	    if best_temp < value_temp:
	      best_temp = value_tem
	  elif ideal_optimization_sequence[j][5] == -1:
	    if best_temp > value_temp:
	      best_temp = value_temp
	
	#ideal_optimization_sequence[i].append(c.fetchone()[0])
      except:
	pass
	#print "Entry for ."
    
    pretty_best_table.add_row(["best_"+ideal_optimization_sequence[j][1],best_temp])


#get X,Y,Z values for the graph
X = []
Y = []
Z = []

pretty_weighted_single_pu_objective_score_table = PrettyTable(['primary_key',sorted_sequence[0][2],sorted_sequence[1][2],sorted_sequence[2][2],sorted_sequence[3][2],sorted_sequence[4][2],sorted_sequence[5][2],sorted_sequence[6][2]])
pretty_weighted_single_pu_objective_score_table.align = 'l'
pretty_weighted_single_pu_objective_score_table.border = True  

if not args.option == (8 or 9): 
  pretty_filter_table = PrettyTable(['primary_key',sorted_sequence[0][2],sorted_sequence[1][2],sorted_sequence[2][2],sorted_sequence[3][2],sorted_sequence[4][2],sorted_sequence[5][2],sorted_sequence[6][2]])  
else:
  headers = []
  headers.append('primary_key')
  headers.append('J [A/mm2]')
  
  for i in range(0,len(sorted_sequence)):
    headers.append(sorted_sequence[i][2])
    #pretty_best_table = PrettyTable(headers)  
  
  pretty_filter_table = PrettyTable(headers)  

pretty_dimension_table = PrettyTable(['primary_key','hyo','hyi','hc','hm','kc','kmr','krod'])  
elite_pk = []

i = 0
c.execute("SELECT rotor_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyo = c.fetchone()[0]
c.execute("SELECT stator_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyi = c.fetchone()[0]
c.execute("SELECT coil_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hm = c.fetchone()[0]
c.execute("SELECT coil_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kmr = c.fetchone()[0]
c.execute("SELECT insert_lamination_hole_support_rod FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
insert_rod = c.fetchone()[0]
if insert_rod == 1:
  c.execute("SELECT stator_rod_distance_from_tooth_tip FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
  krod = c.fetchone()[0]
  pretty_dimension_table.add_row(['start',hyo,hyi,hc,hm,kc,kmr,krod])
else:  
  pretty_dimension_table.add_row(['start',hyo,hyi,hc,hm,kc,kmr])
i = -1
c.execute("SELECT rotor_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyo = c.fetchone()[0]
c.execute("SELECT stator_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyi = c.fetchone()[0]
c.execute("SELECT coil_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hm = c.fetchone()[0]
c.execute("SELECT coil_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kmr = c.fetchone()[0]
if insert_rod == 1:
  krod=''
  pretty_dimension_table.add_row(['min',hyo,hyi,hc,hm,kc,kmr,krod])
else:  
  pretty_dimension_table.add_row(['min',hyo,hyi,hc,hm,kc,kmr])
i = -2
c.execute("SELECT rotor_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyo = c.fetchone()[0]
c.execute("SELECT stator_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hyi = c.fetchone()[0]
c.execute("SELECT coil_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
hm = c.fetchone()[0]
c.execute("SELECT coil_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kc = c.fetchone()[0]
c.execute("SELECT radially_magnetized_PM_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
kmr = c.fetchone()[0]
c.execute("SELECT insert_lamination_hole_support_rod FROM best.machine_input_parameters WHERE primary_key = ?",(i,))

if insert_rod==1:
  krod = ''
  pretty_dimension_table.add_row(['max',hyo,hyi,hc,hm,kc,kmr,krod])
  pretty_dimension_table.add_row(['------','----','----','----','----','----','----','----'])
else:
  pretty_dimension_table.add_row(['max',hyo,hyi,hc,hm,kc,kmr])
  pretty_dimension_table.add_row(['------','----','----','----','----','----','----'])
    
temp = []

#number_of_solutions_displayed = 0
for i in range (1,max(sorted_sequence)[0]):
  try:
    c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(i,))
    already_optimised = c.fetchone()[0]  
  except:
    already_optimised = 0
  
  bib = np.zeros(len(sorted_titles))
  objectives_to_be_constrained = np.zeros(len(sorted_sequence))
  if already_optimised == 1:
    k=0
    
    for j in range(0,len(sorted_sequence)):
      try:
	c.execute("SELECT %s FROM best.machine_semfem_output WHERE primary_key = ?"%(sorted_sequence[j][2]),(i,))
	
	objectives_to_be_constrained[j] = c.fetchone()[0] 
	
	if sorted_sequence[j][0] >= 1:
	  bib[k] = objectives_to_be_constrained[j]
	  k = k+1 
      except:
	print "primary_key result %d not found."%(i)     
    
    
    satisfies_max_constraints = 0
    for j in range (0,len(sorted_sequence)): #check every possible constraint
      #print objectives_to_be_constrained[j]
      if sorted_sequence[j][3] < 0 and sorted_sequence[j][4] == 0: #if there is no maximum or minimum criteria, then solution automatically passes
	satisfies_max_constraints = satisfies_max_constraints + 1
	#print "ja bal"
	#print "sorted_sequence[j][3] = %g   and   sorted_sequence[j][4] = %g"%(sorted_sequence[j][3],sorted_sequence[j][4])
      elif sorted_sequence[j][3] < 0 and objectives_to_be_constrained[j] >= sorted_sequence[j][4]:
	satisfies_max_constraints = satisfies_max_constraints + 1
	
      elif sorted_sequence[j][4] == 0 and objectives_to_be_constrained[j] <= sorted_sequence[j][3]:
	satisfies_max_constraints = satisfies_max_constraints + 1
	
      elif objectives_to_be_constrained[j] <= sorted_sequence[j][3] and objectives_to_be_constrained[j] >= sorted_sequence[j][4]: #if solution is below the maximum critera and if solution is above the minimum critera
	satisfies_max_constraints = satisfies_max_constraints + 1
	#print "whoopsie"
      #elif objectives_to_be_constrained[j] >= sorted_sequence[j][4]: #if solution is above the minimum critera
	##print "yipee"
	#satisfies_max_constraints = satisfies_max_constraints + 1

      

  
    #if satisfies_max_constraints == 2:
    #print(satisfies_max_constraints)
    if satisfies_max_constraints == len(sorted_sequence):
      temp.append([bib[0],bib[1],bib[2]])
      if not args.option == (8 or 9):
	pretty_filter_table.add_row([i,objectives_to_be_constrained[0],objectives_to_be_constrained[1],objectives_to_be_constrained[2],objectives_to_be_constrained[3],objectives_to_be_constrained[4],objectives_to_be_constrained[5],objectives_to_be_constrained[6]])
      elite_pk.append(i)
      
      try:
	c.execute("SELECT rotor_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	hyo = c.fetchone()[0]
	c.execute("SELECT stator_yoke_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	hyi = c.fetchone()[0]
	c.execute("SELECT coil_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	hc = c.fetchone()[0]
	c.execute("SELECT radially_magnetized_PM_height_in_mm FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	hm = c.fetchone()[0]
	c.execute("SELECT coil_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	kc = c.fetchone()[0]
	c.execute("SELECT radially_magnetized_PM_width_ratio FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	kmr = c.fetchone()[0]
	if insert_rod==1:
	  c.execute("SELECT stator_rod_distance_from_tooth_tip FROM best.machine_input_parameters WHERE primary_key = ?",(i,))
	  krod = c.fetchone()[0]	
	
      except:
	print "Could not read dimensions"

      if insert_rod==1:
	pretty_dimension_table.add_row([i,hyo,hyi,hc,hm,kc,kmr,krod])
      else:
	pretty_dimension_table.add_row([i,hyo,hyi,hc,hm,kc,kmr])

if not args.option == (8 or 9):    
  #print(pretty_filter_table)
  print(pretty_dimension_table)

for i in range (0,len(temp)):
  X.append(temp[i][0])
  Y.append(temp[i][1])
  Z.append(temp[i][2])
  
  
lim = np.zeros(3)
for i in range(0,3):
  nip = 0
  for j in range(0,len(temp)):
    if temp[j][i]>nip:
      nip = temp[j][i]
  lim[i]=nip  

scatter = []
text = []
handles_list = []

if not args.option == (8 or 9): 
  print pretty_terminal_table    
  print pretty_best_table    

##3D plot of pareto surface of efficiency, torque and PM mass
if args.option == 1:

  fig = pl.figure()
  
  #ax = fig.add_subplot(111)
  ax = fig.add_subplot(111, projection='3d')
  #ax = fig.gca(projection='3d')
  
  ax.set_xlabel(x_axis_title,fontsize=font_size)
  ax.set_ylabel(y_axis_title,fontsize=font_size)
  ax.set_zlabel(z_axis_title,fontsize=font_size)
  
  ax.tick_params(axis='x', labelsize=font_size)
  ax.tick_params(axis='y', labelsize=font_size)
  ax.tick_params(axis='z', labelsize=font_size)  
  
  #ax.set_xlim3d(sorted_sequence[len(sorted_sequence)-3][4],1.1*lim[0])
  #ax.set_ylim3d(sorted_sequence[len(sorted_sequence)-2][4],1.1*lim[1])
  #ax.set_zlim3d(sorted_sequence[len(sorted_sequence)-1][4],1.1*lim[2])
  
  #ax.plot_wireframe(X, Y, Z,rstride=10, cstride=10)
  ax.scatter(X, Y, Z, s=10, color='b', marker='s', label='all')
  #ax.scatter(x_elite, y_elite, z_elite, s=10, color='r', marker='o', label='Elite solutions')
  #ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
  #ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=10, color='b', marker='o', label='Remaining solutions')
  #ax.scatter(prototype[2], prototype[1], prototype[0], s=50, color='m', marker='o', label='Prototype')
  #pl.legend(loc='upper right', fontsize = font_size)
  pl.legend(loc='upper center', fontsize = font_size)
  
  #pl.savefig('3d_pareto.pdf',transparent=True, bbox_inches='tight')

#plot all 2D scatter plots
if args.option == 2:
  markersize=150
  
  f = interp1d(X, Y, kind='linear',assume_sorted=False) 
  xnew = np.linspace(min(X), max(X), num=len(X), endpoint=True)  

  #plot x against y
  fig = pl.figure(1)
  pl.grid(True)
  pl.xlabel(sorted_titles[0],fontsize=font_size)
  pl.ylabel(sorted_titles[1],fontsize=font_size)
  pl.scatter(X,Y,c=colors[0])
  
  #plot x against z
  fig = pl.figure(2)
  pl.grid(True)
  pl.xlabel(sorted_titles[0],fontsize=font_size)
  pl.ylabel(sorted_titles[2],fontsize=font_size)
  pl.scatter(X,Z,c=colors[0])  

  #plot y against z
  fig = pl.figure(3)
  pl.grid(True)
  pl.xlabel(sorted_titles[1],fontsize=font_size)
  pl.ylabel(sorted_titles[2],fontsize=font_size)
  pl.scatter(Y,Z,c=colors[0])  

#plot 2D contour plot
if args.option == 3:
  markersize=150
  
  #plot x against y
  fig = pl.figure(1)
  for i in range(0,len(X)):
    for n in range(0,len(sorted_sequence[len(sorted_sequence)-1][6])):
      if Z[i]>sorted_sequence[len(sorted_sequence)-1][6][n][0] and Z[i]<sorted_sequence[len(sorted_sequence)-1][6][n][1]:
	sorted_sequence[len(sorted_sequence)-1][6][n][2].append([X[i],Y[i]])  
  
  scatter = []
  text = []
  handles_list = []  
  
  for n in range (0,len(sorted_sequence[len(sorted_sequence)-1][6])): #used to be: for n in range (0,len(filter_torque_ripple)):
    x = np.zeros(len(sorted_sequence[len(sorted_sequence)-1][6][n][2]))
    y = np.zeros(len(sorted_sequence[len(sorted_sequence)-1][6][n][2]))    
    character = sorted_sequence[len(sorted_sequence)-1][1].split(" ")[1].translate(None,'(').translate(None,')') #NB, if axes and legends seem wrong, its because I should maybe use sorted_sequence[len(sorted_sequence)-1][1] instead of sorted_sequence[len(sorted_sequence)-2][1]
    text.append(r'$%d<%s<%d$'%(sorted_sequence[len(sorted_sequence)-1][6][n][0],character,sorted_sequence[len(sorted_sequence)-1][6][n][1]))
    for p in range (0,len(sorted_sequence[len(sorted_sequence)-1][6][n][2])):
      x[p] = sorted_sequence[len(sorted_sequence)-1][6][n][2][p][0]
      y[p] = sorted_sequence[len(sorted_sequence)-1][6][n][2][p][1]
    
    #print(len(x))
    #print(len(y))
    #print(np.shape(x))
    #print(np.shape(y))
    
    f = interp1d(x, y, kind='linear',assume_sorted=False) 
    xnew = np.linspace(min(x), max(x), num=len(x), endpoint=True)
    handles_list.append(pl.plot(xnew,f(xnew),c=colors[n]))
    scatter.append(pl.scatter(x,y,c=colors[n]))

  tuple_text = tuple(text)
  tuple_scatter = tuple(scatter)

  pl.legend(tuple_scatter,tuple_text,scatterpoints=3,markerscale=1.5 ,loc='best',title=r'%s Range'%(sorted_sequence[len(sorted_sequence)-1][1]))

  pl.xlabel(sorted_titles[0],fontsize=font_size)
  pl.ylabel(sorted_titles[1],fontsize=font_size)
  pl.grid(True)
  pl.title(r"Pareto Contours of %s against %s (given a certain %s)"%(sorted_sequence[len(sorted_sequence)-3][1],sorted_sequence[len(sorted_sequence)-2][1],sorted_sequence[len(sorted_sequence)-1][1]),fontsize=font_size)



  #plot x against z
  fig = pl.figure(2)
  for i in range(0,len(X)):
    for n in range(0,len(sorted_sequence[len(sorted_sequence)-1][6])):
      if Y[i]>sorted_sequence[len(sorted_sequence)-2][6][n][0] and Y[i]<sorted_sequence[len(sorted_sequence)-2][6][n][1]:
	sorted_sequence[len(sorted_sequence)-2][6][n][2].append([X[i],Z[i]])  

  scatter = []
  text = []
  handles_list = []  
  
  for n in range (0,len(sorted_sequence[len(sorted_sequence)-2][6])): #used to be: for n in range (0,len(filter_torque_ripple)):
    x = np.zeros(len(sorted_sequence[len(sorted_sequence)-2][6][n][2]))
    y = np.zeros(len(sorted_sequence[len(sorted_sequence)-2][6][n][2]))    
    character = sorted_sequence[len(sorted_sequence)-2][1].split(" ")[1].translate(None,'(').translate(None,')')
    #text.append(r'$%d<%s<%d$'%(sorted_sequence[len(sorted_sequence)-2][6][n][0],character,sorted_sequence[len(sorted_sequence)-2][6][n][1]))
    text.append(r'$%.1f<%s<%.1f$'%(sorted_sequence[len(sorted_sequence)-2][6][n][0],character,sorted_sequence[len(sorted_sequence)-2][6][n][1]))
    for p in range (0,len(sorted_sequence[len(sorted_sequence)-2][6][n][2])):
      x[p] = sorted_sequence[len(sorted_sequence)-2][6][n][2][p][0]
      y[p] = sorted_sequence[len(sorted_sequence)-2][6][n][2][p][1]
      
    f = interp1d(x, y, kind='linear',assume_sorted=False) 
    xnew = np.linspace(min(x), max(x), num=len(x), endpoint=True)
    handles_list.append(pl.plot(xnew,f(xnew),c=colors[n]))
    scatter.append(pl.scatter(x,y,c=colors[n]))

  tuple_text = tuple(text)
  tuple_scatter = tuple(scatter)

  pl.legend(tuple_scatter,tuple_text,scatterpoints=3,markerscale=1.5 ,loc='best',title=r'%s Range'%(sorted_sequence[len(sorted_sequence)-2][1]))

  pl.xlabel(sorted_titles[0],fontsize=font_size)
  pl.ylabel(sorted_titles[2],fontsize=font_size)
  pl.grid(True)
  pl.title(r"Pareto Contours of %s against %s (given a certain %s)"%(sorted_sequence[len(sorted_sequence)-3][1],sorted_sequence[len(sorted_sequence)-1][1],sorted_sequence[len(sorted_sequence)-2][1]),fontsize=font_size)

#plot outline plot
if args.option == 4:
  markersize=150
  
  #plot x against y
  fig = pl.figure(1)
  
  com = np.zeros([len(X),2])
  for i in range(0,len(X)):
    com[i][0] = X[i]
    com[i][1] = Z[i]
    
  sorted_data = com[np.argsort(com[:,0])]
 
  sorted_list = []
  for i in range(0,len(sorted_data)):
    sorted_list.append([sorted_data[i][0],sorted_data[i][1]])
 
  for i in range(0,len(sorted_list)):
    number_of_items_to_be_deleted = 0
    for j in range(0,len(sorted_list)-1):
      if sorted_list[j][1] > sorted_list[j+1][1]:
	number_of_items_to_be_deleted = number_of_items_to_be_deleted+1    
    
    for k in range(0,len(sorted_list)-number_of_items_to_be_deleted-1):
      if sorted_list[k][1] > sorted_list[k+1][1]:
	sorted_list.remove(sorted_list[k])

  
  print(len(sorted_data))
  print(len(sorted_list))

  x = np.zeros(len(sorted_list))
  y = np.zeros(len(sorted_list))

  for i in range(len(sorted_list)):
    x[i] = sorted_list[i][0]
    y[i] = sorted_list[i][1]

  f = interp1d(x, y, kind='linear',assume_sorted=False) 
  xnew = np.linspace(min(x), max(x), num=len(x), endpoint=True)
  pl.plot(xnew,f(xnew),c=colors[0])
  pl.scatter(x,y)

#plot outline plot
if args.option == 5:
  markersize=150
  
  x = np.linspace(min(X), max(X), num=80, endpoint=True)
  inc_size = (max(X)-min(X))/len(x)
  y_min = np.zeros(len(x))
  y_max = np.zeros(len(x))
  
  for i in range(0,len(X)):
  #i=243
    pos = int((X[i]-min(X))/inc_size)
    
    if pos >= len(x):
      print pos
      pos = len(x)-1
    
    if y_min[pos] == 0: #initialise minimum at this position
      y_min[pos] = Y[i]
      
    if y_max[pos] == 0: #initialise maximum at this position
      y_max[pos] = Y[i]

    if Y[i] < y_min[pos]: #set new minimum found
      y_min[pos] = Y[i]  
      
    if Y[i] > y_max[pos]: #set new maximum found
      y_max[pos] = Y[i]


  #create list from array
  x_list = []
  y_min_list = []
  y_max_list = []
  
  for i in range(0,len(x)):
    if y_min[i] != 0: #y_max[i] will also be 0 if y_min[i] is 0 (because of initialisation process)
      if i > 1 and i < len(x)-1:
	if y_min[i]<1.3*y_min[i-1] and y_min[i]<1.3*y_min[i+1]:
	  x_list.append(x[i])
	  y_min_list.append(y_min[i])
	  y_max_list.append(y_max[i])

  #remove all zeros
  
  #create new array from shortened list
  x_arr = np.array(x_list)
  y_min_arr = np.array(y_min_list)
  y_max_arr = np.array(y_max_list)


  f = interp1d(x_arr, y_min_arr, kind='linear',assume_sorted=False) 
  xnew = np.linspace(min(x_arr), max(x_arr), num=len(x_arr), endpoint=True)
  pl.plot(xnew,f(xnew),c=colors[0])
  pl.scatter(x_arr,y_min_arr)
  
  
if args.option == 6:
  dir_temp = dir+subdirectory
  if not os.path.isdir(dir_temp): os.makedirs(dir_temp)
  shutil.copy(dir+'best.db',dir+subdirectory+'best.db')
  shutil.copy(dir+'optimise.db',dir+subdirectory+'optimise.db')
  
  c.execute("ATTACH DATABASE '%s' as best_resimulate"%(dir+subdirectory+'best.db'))
  
  for i in range(0,max(sorted_sequence)[0]+1):
    c.execute("UPDATE best_resimulate.machine_input_parameters SET mesh_scale = ? WHERE primary_key = ?",(2.0,i))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET steps = ? WHERE primary_key = ?",(120,i))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET current_density_rms = ? WHERE primary_key = ?",(4.0,i))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET fill_factor = ? WHERE primary_key = ?",(1.0,i))
  
  conn.commit()
  
  for i in range(0,max(sorted_sequence)[0]+1):
    c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(i,))
    already_optimised = c.fetchone()[0]      
    if not already_optimised == 0:
      print "Re-simulating %d"%(i)
      os.system("./SORSPM_semfem.py -pk %d -db %s"%(i,dir_temp+'best.db'))
  
if args.option == 7:
  dir_temp = dir+subdirectory
  if not os.path.isdir(dir_temp): os.makedirs(dir_temp)
  shutil.copy(dir+'best.db',dir+subdirectory+'best.db')
  shutil.copy(dir+'optimise.db',dir+subdirectory+'optimise.db')
  
  c.execute("ATTACH DATABASE '%s' as best_resimulate"%(dir+subdirectory+'best.db'))
  
  for i in range(0,len(elite_pk)):
    pk = elite_pk[i]
    c.execute("UPDATE best_resimulate.machine_input_parameters SET mesh_scale = ? WHERE primary_key = ?",(2.0,pk))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET steps = ? WHERE primary_key = ?",(120,pk))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET current_density_rms = ? WHERE primary_key = ?",(2.5,pk))
    c.execute("UPDATE best_resimulate.machine_input_parameters SET fill_factor = ? WHERE primary_key = ?",(1.0,pk))
  
  conn.commit()
  
  for i in range(0,len(elite_pk)):
    pk = elite_pk[i]
    c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(pk,))
    already_optimised = c.fetchone()[0]      
    if already_optimised == 1:
      print "Re-simulating %d"%(pk)
      os.system("./SORSPM_semfem.py -pk %d -db %s"%(pk,dir_temp+'best.db'))
  
if args.option == 8:
  pretty_terminal_table = PrettyTable(['Variable','Value'])
  #pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
  pretty_terminal_table.align = 'l'
  pretty_terminal_table.border= True  

  #pretty_best_table = PrettyTable(['Variable','Value'])
  #pretty_best_table.align = 'l'
  #pretty_best_table.border= True   
  
  #setup databse connection
  conn = sqlite3.connect(dir+'optimise.db')
  c = conn.cursor()
  best_db_name = []
  for i in range(0,len(dir_sol)):
    print(dir_sol[i][0])
    best_dir = dir_sol[i][0] + 'best.db'
    best_db_name.append('best_db_%d'%i)
    c.execute("ATTACH DATABASE '%s' as '%s'"%(best_dir,best_db_name[i]))  

  best_table_data = []
  #best_table_data.append([])

  best_temp = np.zeros([len(sorted_sequence),len(dir_sol)])
  value_temp = np.zeros([len(sorted_sequence),len(dir_sol)])
  for m in range(0,len(dir_sol)):
    for j in range(0,len(sorted_sequence)):
      for i in range(1,max_keys+1):
	try:
	  c.execute("SELECT %s FROM %s.machine_semfem_output WHERE primary_key = ?"%(sorted_sequence[j][2],best_db_name[m]),(i,))
	  value_temp[j][m] = c.fetchone()[0]
	  if i == 1:
	    best_temp[j][m] = value_temp[j][m]
	  else:
	    if sorted_sequence[j][5] == 1:
	      if best_temp[j][m] < value_temp[j][m]:
		best_temp[j][m] = value_temp[j][m]
	    elif sorted_sequence[j][5] == -1:
	      if best_temp[j][m] > value_temp[j][m]:
		best_temp[j][m] = value_temp[j][m]
	  
	except:
	  pass
	  #print "Entry for ."
  
  for j in range(0,len(sorted_sequence)):
    best_table_data.append([])
    best_table_data[j].append("best_"+sorted_sequence[j][1])
    for m in range(0,len(dir_sol)):
      best_table_data[j].append(best_temp[j][m])
    
    pretty_best_table.add_row(best_table_data[j])


  #for i in range (1,max(sorted_sequence)[0]):
    #try:
      #c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(i,))
      #already_optimised = c.fetchone()[0]  
    #except:
      #already_optimised = 0   

  ##max_torque_ripple_pu = 2
  ##max_torque_density_pu = -1
  
  ##min_torque_ripple_pu = 0
  ##min_torque_density_pu = 0.8

    #for m in range(0,len(dir_sol)):
      #bib = np.zeros(len(sorted_titles))
      #objectives_to_be_constrained = np.zeros(len(sorted_sequence))     
      #if not already_optimised == 0:
	#k=0	
	#for j in range(0,len(sorted_sequence)):
	  #try:
	    #c.execute("SELECT %s FROM %s.machine_semfem_output WHERE primary_key = ?"%(sorted_sequence[j][2],best_db_name[m]),(i,))
	    
	    #objectives_to_be_constrained[j] = c.fetchone()[0] 
	    
	    #if sorted_sequence[j][0] >= 1:
	      #bib[k] = objectives_to_be_constrained[j]
	      #k = k+1 
	  #except:
	    #print "primary_key result %d not found."%(i)     
	  
	#temp.append([bib[0],bib[1],bib[2]])
	#if m == 0:
	  #tmp_i=i
	#else:
	  #tmp_i=''
	#pretty_filter_table.add_row([tmp_i,dir_sol[m][0][-4:-1],objectives_to_be_constrained[0],objectives_to_be_constrained[1],objectives_to_be_constrained[2],objectives_to_be_constrained[3],objectives_to_be_constrained[4],objectives_to_be_constrained[5],objectives_to_be_constrained[6]])
	#elite_pk.append(i)
  filter_data = []  
  objectives_to_be_constrained = np.zeros([len(dir_sol),max(sorted_sequence)[0],len(sorted_sequence)])
  X = []
  Y = []
  Z = []
  for i in range (0,max(sorted_sequence)[0]):
    try:
      c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(i,))
      already_optimised = c.fetchone()[0]  
    except:
      already_optimised = 0
      
    bib = np.zeros(len(sorted_titles))
    
    for m in range(0,len(dir_sol)):
    #m=0
      if already_optimised == 1:
	k=0
	
	for j in range(0,len(sorted_sequence)):
	  try:
	    c.execute("SELECT %s FROM %s.machine_semfem_output WHERE primary_key = ?"%(sorted_sequence[j][2],best_db_name[m]),(i,))
	    objectives_to_be_constrained[m][i][j] = c.fetchone()[0] 
	    
	    if sorted_sequence[j][0] >= 1:
	      bib[k] = objectives_to_be_constrained[m][i][j]
	      k = k+1 
	  except:
	    print "primary_key result %d not found."%(i) 
	    
	if satisfies_max_constraints == len(sorted_sequence):
	  temp.append([bib[0],bib[1],bib[2]])
	  if m == 0:
	    tmp_i=i
	  else:
	    tmp_i=''
	  
	  filter_data.append([])
	  filter_data[len(filter_data)-1].append(tmp_i)
	  filter_data[len(filter_data)-1].append(dir_sol[m][0][-4:-1])
	  for n in range(0,len(sorted_sequence)):
	    filter_data[len(filter_data)-1].append(objectives_to_be_constrained[m][i][n])
	  
	  pretty_filter_table.add_row(filter_data[len(filter_data)-1])
	  #pretty_filter_table.add_row([tmp_i,dir_sol[m][0][-4:-1],objectives_to_be_constrained[0],objectives_to_be_constrained[1],objectives_to_be_constrained[2],objectives_to_be_constrained[3],objectives_to_be_constrained[4],objectives_to_be_constrained[5],objectives_to_be_constrained[6]])
	  elite_pk.append(i)
	  try:
	    c.execute("SELECT rotor_yoke_height_in_mm FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    hyo = c.fetchone()[0]
	    c.execute("SELECT stator_yoke_height_in_mm FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    hyi = c.fetchone()[0]
	    c.execute("SELECT coil_height_in_mm FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    hc = c.fetchone()[0]
	    c.execute("SELECT radially_magnetized_PM_height_in_mm FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    hm = c.fetchone()[0]
	    c.execute("SELECT coil_width_ratio FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    kc = c.fetchone()[0]
	    c.execute("SELECT radially_magnetized_PM_width_ratio FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	    kmr = c.fetchone()[0]
	    if insert_rod==1:
	      c.execute("SELECT stator_rod_distance_from_tooth_tip FROM %s.machine_input_parameters WHERE primary_key = ?"%(best_db_name[m]),(i,))
	      krod = c.fetchone()[0]	
	    
	  except:
	    print "Could not read dimensions of pk %d"%(i)

	  if insert_rod==1:
	    pretty_dimension_table.add_row([i,hyo,hyi,hc,hm,kc,kmr,krod])
	  else:
	    pretty_dimension_table.add_row([i,hyo,hyi,hc,hm,kc,kmr])
  
  pu_objective_score = np.zeros([len(dir_sol),max(sorted_sequence)[0],len(sorted_sequence)])
  for m in range(0,len(dir_sol)):  
    for i in range(0,max(sorted_sequence)[0]):
      for j in range (0,len(sorted_sequence)):
	pu_objective_score[m][i][j] = objectives_to_be_constrained[m][i][j]/best_table_data[j][m+1]

  weighted_single_pu_objective_score = np.zeros([max(sorted_sequence)[0],len(sorted_sequence)])
  for i in range(0,max(sorted_sequence)[0]):
    for j in range (0,len(sorted_sequence)):
      for m in range(0,len(dir_sol)):
	weighted_single_pu_objective_score[i][j] = weighted_single_pu_objective_score[i][j] + dir_sol[m][1]*pu_objective_score[m][i][j]

  for i in range(0,max(sorted_sequence)[0]):
    satisfies_max_constraints = 0
    for j in range (0,len(sorted_sequence)): #check every possible constraint
      #if j == 4 or j == 6:#objectives which are being valued according to pu.
      if sorted_sequence[j][3] < 0 and sorted_sequence[j][4] == 0: #if there is no maximum or minimum criteria, then solution automatically passes
	satisfies_max_constraints = satisfies_max_constraints + 1
      elif sorted_sequence[j][3] < 0 and weighted_single_pu_objective_score[i][j] >= sorted_sequence[j][4]:
	satisfies_max_constraints = satisfies_max_constraints + 1
      elif sorted_sequence[j][4] == 0 and weighted_single_pu_objective_score[i][j] <= sorted_sequence[j][3]:
	satisfies_max_constraints = satisfies_max_constraints + 1
      elif weighted_single_pu_objective_score[i][j] <= sorted_sequence[j][3] and weighted_single_pu_objective_score[i][j] >= sorted_sequence[j][4]: #if solution is below the maximum critera and if solution is above the minimum critera
	satisfies_max_constraints = satisfies_max_constraints + 1
	
      #else:
	#if sorted_sequence[j][3] < 0 and sorted_sequence[j][4] == 0: #if there is no maximum or minimum criteria, then solution automatically passes
	  #satisfies_max_constraints = satisfies_max_constraints + 1
	#elif sorted_sequence[j][3] < 0 and objectives_to_be_constrained[m][i][j] >= sorted_sequence[j][4]:
	  #satisfies_max_constraints = satisfies_max_constraints + 1
	#elif sorted_sequence[j][4] == 0 and objectives_to_be_constrained[m][i][j] <= sorted_sequence[j][3]:
	  #satisfies_max_constraints = satisfies_max_constraints + 1
	#elif objectives_to_be_constrained[m][i][j] <= sorted_sequence[j][3] and objectives_to_be_constrained[m][i][j] >= sorted_sequence[j][4]: #if solution is below the maximum critera and if solution is above the minimum critera
	  #satisfies_max_constraints = satisfies_max_constraints + 1	       
    
    if satisfies_max_constraints == len(sorted_sequence):
      c.execute("SELECT completed_optimisation FROM machine_input_parameters WHERE primary_key = ?",(i,))
      already_optimised = c.fetchone()[0]
      if already_optimised == 1:
	pretty_weighted_single_pu_objective_score_table.add_row([i,weighted_single_pu_objective_score[i][0],weighted_single_pu_objective_score[i][1],weighted_single_pu_objective_score[i][2],weighted_single_pu_objective_score[i][3],weighted_single_pu_objective_score[i][4],weighted_single_pu_objective_score[i][5],weighted_single_pu_objective_score[i][6]])
	X.append(weighted_single_pu_objective_score[i][4])
	Y.append(weighted_single_pu_objective_score[i][5])
	Z.append(weighted_single_pu_objective_score[i][6])
      
  print(pretty_filter_table)
  print(pretty_best_table)
  print(pretty_dimension_table)
  print(pretty_weighted_single_pu_objective_score_table)
 
  fig = pl.figure()
  
  ax = fig.add_subplot(111, projection='3d')
  
  #ax.set_xlabel(sorted_sequence[4][1],fontsize=font_size)
  #ax.set_ylabel(sorted_sequence[5][1],fontsize=font_size)
  #ax.set_zlabel(sorted_sequence[6][1],fontsize=font_size)

  ax.set_xlabel('Torque Density Score',fontsize=font_size)
  ax.set_ylabel('PM Mass Score',fontsize=font_size)
  ax.set_zlabel('Torque Ripple Score',fontsize=font_size)
  
  ax.tick_params(axis='x', labelsize=font_size)
  ax.tick_params(axis='y', labelsize=font_size)
  ax.tick_params(axis='z', labelsize=font_size)  
  
  #ax.set_xlim3d(sorted_sequence[len(sorted_sequence)-3][4],1.1*lim[0])
  #ax.set_ylim3d(sorted_sequence[len(sorted_sequence)-2][4],1.1*lim[1])
  #ax.set_zlim3d(sorted_sequence[len(sorted_sequence)-1][4],1.1*lim[2])
  
  #ax.plot_wireframe(X, Y, Z,rstride=10, cstride=10)
  ax.scatter(X, Y, Z, s=10, color='b', marker='s', label='all')

  pl.legend(loc='upper center', fontsize = font_size)

if args.option == 9:
  print('hallo')

if args.option == 10:
  
  fig_x_cm = 8
  fig_y_cm = 5
  fig_x_inch_pdf = 18
  fig_y_inch_pdf = 8

  margin_left = 0.05
  margin_right = 0.74
  margin_bottom = 0.08
  margin_top = 0.92
  
  file_names = []
  
  file_names.append('semfem_script_output_steps521.csv')
  file_names.append('semfem_script_output_steps120.csv')
  file_names.append('semfem_script_output_steps30.csv')

  fig = pl.figure(1, figsize=(fig_x_cm,fig_y_cm))
  ax1 = fig.add_subplot(111)
  line = []
  
  for i in range(0, len(file_names)):
    time_stamp, electrical_angle, semfem_torque = np.loadtxt(file_names[i],skip_header=1,usecols=(0,1,2),unpack=True)  
    line.append(ax1.plot(time_stamp,semfem_torque,label=r'$\tau$, steps = %d'%(len(time_stamp))))
  
  ax1.set_xlabel(r'Time [ms]')#,fontsize=axis_label_font_size)
  ax1.set_ylabel(r'Torque [N.m]')#,fontsize=axis_label_font_size)
  
  ax1.set_position([box.x0, box.y0, box.width, box.height * 0.75])
  labels = [l.get_label() for l in lines]
  #lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.325), fontsize=legend_font_size, title='Torque')
  lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.325), title='Torque')
  
  ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
  art.append(lgd)
  ax1.grid()
  
  fig.set_size_inches(fig_x_inch_pdf, fig_y_inch_pdf)
  pl.subplots_adjust(left=margin_left, right=margin_right, top=margin_top, bottom=margin_bottom)
  fig.savefig("torque_vs_steps_comparison.pdf", additional_artists=art)

conn.close()
pl.show()
    
