#!/usr/bin/python
from __future__ import division

import pylab as pl
import numpy as np

#fig_x_cm = 4
#fig_y_cm = 5
#fig_x_inch_pdf = 18
#fig_y_inch_pdf = 8

#margin_left = 0.05
#margin_right = 0.79
#margin_bottom = 0.09
#margin_top = 0.90

margin_left = 0.06
margin_right = 0.98
margin_bottom = 1.0
margin_top = 1.0

#LINEWIDTH=2
#font_size=14
axis_label_font_size=20
tick_label_font_size=18
legend_font_size=18
legend_title_fontsize=20
marker_size = 20

#old setup
#axis_label_font_size=18
#tick_label_font_size=16
#legend_font_size=18
#legend_title_fontsize=20

pl.rc('text', usetex=True)
pl.rc('font', family='serif')

colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

file_names = []
art = []

dir = 'torque_vs_steps_csv/new/'

file_names.append(dir+'semfem_script_output_steps521.csv')
#file_names.append(dir+'semfem_script_output_steps120.csv')
#file_names.append(dir+'semfem_script_output_steps60.csv')
#file_names.append(dir+'semfem_script_output_steps31.csv')
#file_names.append(dir+'semfem_script_output_steps30.csv')
#file_names.append(dir+'semfem_script_output_steps24.csv')
#file_names.append(dir+'semfem_script_output_steps17.csv')
#file_names.append(dir+'semfem_script_output_steps15.csv')
file_names.append(dir+'semfem_script_output_steps13.csv')
file_names.append(dir+'semfem_script_output_steps12.csv')
#file_names.append(dir+'semfem_script_output_steps11.csv')
#file_names.append(dir+'semfem_script_output_steps8.csv')
#file_names.append(dir+'semfem_script_output.csv')

fig = pl.figure(1,)# figsize=(fig_x_cm,fig_y_cm))
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
line = []
lines = None
dummy_line = []
dummy_lines = None
max_datapoints=0

for i in range(0, len(file_names)):
  #time_stamp, electrical_angle, semfem_torque = np.loadtxt(file_names[i],skip_header=1,usecols=(0,1,2),unpack=True)  
  time_stamp, electrical_angle, semfem_torque = np.genfromtxt(file_names[i],skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
  
  if(len(time_stamp)>max_datapoints): max_datapoints = len(time_stamp)
  
  actual_time_stamp = np.zeros(len(time_stamp))
  for j in range(1,len(time_stamp)):
    actual_time_stamp[j] = time_stamp[j-1]*1000 #wil be showing in ms
  
  line.append(ax1.plot(actual_time_stamp,semfem_torque,label=r'%d'%(len(actual_time_stamp)), color = colors[i]))#linewidth=LINEWIDTH
  
  torque_ripple = (max(semfem_torque)-min(semfem_torque))/np.average(semfem_torque)*100
  text = '%.2f'%torque_ripple
  text = text + r'$\%$'
  dummy_line.append(ax1.plot(actual_time_stamp,semfem_torque,label=text, color = colors[i]))#linewidth=LINEWIDTH
  #ax1.scatter(actual_time_stamp,semfem_torque,s=marker_size, color = colors[i])
  ax2.scatter(electrical_angle,semfem_torque,s=marker_size, color = colors[i])

end_time = time_stamp[-1]*1000
#theta_e = np.linspace(0,360,max_datapoints)

lines = line[0]
for i in range(1,len(line)):
  lines = lines + line[i]

dummy_lines = dummy_line[0]
for i in range(1,len(dummy_line)):
  dummy_lines = dummy_lines + dummy_line[i]

ax1.set_xlabel(r'Time [ms]',fontsize=axis_label_font_size)
ax1.set_ylabel(r'Torque [N.m]',fontsize=axis_label_font_size)
ax2.set_xlabel(r'\textbf{\theta_{e} [^{\circ}]}',fontsize=axis_label_font_size)

ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

box = ax1.get_position()

#ax1.set_position([box.x0, box.y0, box.width, box.height * 0.75])
#ax2.set_position([box.x0, box.y0, box.width, box.height * 0.75])

labels = [l.get_label() for l in lines]
dummy_labels = [l.get_label() for l in dummy_lines]

#lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(1.05, 0.5), fontsize=legend_font_size, title='Number of Steps')

lgd = ax1.legend(lines, labels, loc='center left', bbox_to_anchor=(0.001, 0.85), fontsize=legend_font_size, title='Number of Steps', borderpad=1.0)
#lgd2 = pl.legend(lines, dummy_labels, loc='center left', bbox_to_anchor=(1.05, 0.8), fontsize=legend_font_size, title='Torque Ripple')
lgd2 = ax2.legend(lines, dummy_labels, loc='center left', bbox_to_anchor=(0.25, 0.85), fontsize=legend_font_size, title='Torque Ripple', borderpad=1.0)#borderpad=1.66


#lgd2 = ax1.legend(dummy_lines, dummy_labels, loc='center left', bbox_to_anchor=(1.05, 0.8), fontsize=legend_font_size, title='Torque Ripple')

ax1.set_xlim(0,end_time)
#ax1.set_ylim(0,1.1*max(semfem_torque))
#ax1.set_ylim(0,100)
#ax1.set_ylim(60,95) #for pk16
#ax1.set_ylim(180,205)
ax1.set_ylim(185,205)
ax2.set_xlim(0,360)

pl.xticks(np.arange(0, 420, 60))
##pl.yticks(np.arange(0, 110, 10))#for pk16
#pl.yticks(np.arange(0, 210, 5))

ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
art.append(lgd)
ax1.grid()

fig.set_size_inches(12, 8) #default is (8,6)
pl.subplots_adjust(left=margin_left, right=margin_right)#, top=margin_top, bottom=margin_bottom)
fig.savefig("torque_vs_steps_comparison.pdf", additional_artists=art)

pl.show() 
