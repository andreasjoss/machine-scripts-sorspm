#!/usr/bin/python

import sqlite3
import os, sys
import numpy as np
import pylab as pl
import matplotlib.transforms as mtransforms

###SETTINGS
##########################################
db_folder = 'sweep_turns/'

#new db that I will extract data from the sweep
db_list_method_1 = []
#db_list_method_1.append(db_folder+'J=3.2_lseries_in.db')
db_list_method_1.append(db_folder+'J=3.2_lseries_out.db')

#save_extension = '_lseries_in'
save_extension = '_lseries_out'
##########################################


conn = sqlite3.connect(db_list_method_1[0])
c = conn.cursor()

#get speed values
c.execute("SELECT force_this_number_of_turns FROM machine_input_parameters")
tmp = c.fetchall()

turns = np.zeros(len(tmp))
for i in range(0,len(tmp)):
  turns[i] = tmp[i][0]

min_turns = min(turns)
max_turns = max(turns)

number_of_graph_entries = len(turns)

#comment out the lines/graphs that are not needed
graph_1_axis_3 = []
#graph_1_axis_3.append([r'$P_{copper}$ [W]',"p_conductive_analytical",np.zeros(number_of_graph_entries)])
graph_1_axis_3.append([r'$R_{a}$ [m$\Omega$]',"Ra_analytical_DC",np.zeros(number_of_graph_entries)])

graph_1_axis_1 = []
#graph_1_axis_1.append([r'$R_{a}$ [m$\Omega$]',"Ra_analytical_DC",np.zeros(number_of_graph_entries)])
graph_1_axis_1.append([r'$P_{copper}$ [W]',"p_conductive_analytical",np.zeros(number_of_graph_entries)])

#graph_2_axis_3 = []
#graph_2_axis_3.append([r'$P_{copper}$',"p_conductive_analytical",np.zeros(number_of_graph_entries)])
#graph_2_axis_3.append([r'$P_{rotor}$',"p_rotor_core_losses",np.zeros(number_of_graph_entries)])
#graph_2_axis_3.append([r'$P_{stator}$',"p_stator_core_losses",np.zeros(number_of_graph_entries)])
#graph_2_axis_3.append([r'$P_{magnet}$',"p_magnet_losses",np.zeros(number_of_graph_entries)])
#graph_2_axis_3.append([r'$P_{eddy}$',"p_eddy",np.zeros(number_of_graph_entries)])
#graph_2_axis_3.append([r'$P_{losses}$',"total_losses",np.zeros(number_of_graph_entries)])


#get graph_1_axis_3 values
for k in range(0,len(graph_1_axis_3)):
  c.execute("SELECT %s FROM machine_semfem_output"%(graph_1_axis_3[k][1]))
  tmp = c.fetchall()

  for i in range(0,len(tmp)):
    #graph_1_axis_3[k][2][i] = tmp[i][0]
    graph_1_axis_3[k][2][i] = tmp[i][0]*1000 #obtain milli Ohm

#get graph_1_axis_1 values
for k in range(0,len(graph_1_axis_1)):
  c.execute("SELECT %s FROM machine_semfem_output"%(graph_1_axis_1[k][1]))
  tmp = c.fetchall()

  for i in range(0,len(tmp)):
    #graph_1_axis_1[k][2][i] = tmp[i][0]*1000 #obtain milli Ohm
    graph_1_axis_1[k][2][i] = tmp[i][0]
    
##get graph_2_axis_3 values
#for k in range(0,len(graph_2_axis_3)):
  #c.execute("SELECT %s FROM machine_semfem_output"%(graph_2_axis_3[k][1]))
  #tmp = c.fetchall()

  #for i in range(0,len(tmp)):
    #graph_2_axis_3[k][2][i] = tmp[i][0]

##get current_angles to determine flux weakening region
#current_angle_degrees = np.zeros(number_of_graph_entries)
#c.execute("SELECT current_angle_degrees FROM machine_semfem_output")
#tmp = c.fetchall()

#for i in range(0,len(tmp)):
  #current_angle_degrees[i] = tmp[i][0]

conn.commit()
conn.close()

#flux_weakening_rpm = max_turns
#flux_weakening_index = number_of_graph_entries-1
#flux_weakening_position_determined = 0
##determine speed at which flux weakening occurs (if any)
#for i in range(0,number_of_graph_entries):
  #if not np.isclose(0,current_angle_degrees[i],rtol=1e-02, atol=1e-02, equal_nan=False) and flux_weakening_position_determined == 0:
    #flux_weakening_rpm = turns[i]
    #flux_weakening_index = i
    #flux_weakening_position_determined = 1

#constant_torque_rpm = min_turns
#constant_torque_index = 0
##determine speed at which flux weakening occurs (if any)
#for i in range(0,number_of_graph_entries):
  #if graph_1_axis_3[0][2][i] < 2000:
    #constant_torque_rpm = turns[i]
    #constant_torque_index = i

###GRAPHS
##################################
line_graph_1 = []
scat_graph_1 = []
line_graph_2 = []
scat_graph_2 = []

art = []

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
#LINEWIDTH=2
#font_size=14
#axis_label_font_size=22
#tick_label_font_size=22
legend_font_size=10
legend_title_fontsize=12
marker_size = 20
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki','g','y','k']

graph_1_colours = ['b','r','black']

margin_left = 0.0
margin_right = 1.0
margin_bottom = 0.09
margin_top = 0.90

#######################################
#GRAPH 1
legend_pos_x = 0.388
legend_pos_y = 0.9

fig1, ax1 = pl.subplots(1, 1)# ,figsize=(fig_x_cm,fig_y_cm))
ax2 = ax1.twiny()
ax3 = ax1.twinx()
j=0
#plot graph_1_axis_3
for k in range(0,len(graph_1_axis_3)):
  line_graph_1.append(ax3.plot(turns[:number_of_graph_entries],graph_1_axis_3[k][2][:number_of_graph_entries],'-o',alpha = 1.0,markersize=4.5,fillstyle='full',markeredgewidth=0.0,c=graph_1_colours[j],label=graph_1_axis_3[k][0]))#,linewidth=LINEWIDTH))
  j=j+1

#plot graph_1_axis_1
for k in range(0,len(graph_1_axis_1)):
  line_graph_1.append(ax1.plot(turns[:number_of_graph_entries],graph_1_axis_1[k][2][:number_of_graph_entries],'-o',alpha = 1.0,markersize=4.5,fillstyle='full',markeredgewidth=0.0,c=graph_1_colours[j],label=graph_1_axis_1[k][0]))#,linewidth=LINEWIDTH))
  j=j+1
  
lines_graph_1 = line_graph_1[0]
for k in range(1,len(graph_1_axis_3)+len(graph_1_axis_1)):
  lines_graph_1 = lines_graph_1 + line_graph_1[k]
labels_collection_graph_1 = [l.get_label() for l in lines_graph_1]
#lgd_graph_1 = ax1.legend(lines_graph_1, labels_collection_graph_1, loc='center left', bbox_to_anchor=(legend_pos_x, legend_pos_y))#, title='Method 1',fontsize=legend_font_size)
lgd_graph_1 = ax1.legend(lines_graph_1, labels_collection_graph_1, loc='upper center')#, title='Method 1',fontsize=legend_font_size)
ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
art.append(lgd_graph_1)

ax1.set_xticks(np.linspace(min_turns,max_turns,max_turns))
ax2.xaxis.set_visible(False)
#ax1.set_yticks(np.linspace(0,225,10))
ax3.set_yticks(np.linspace(0,280,8))

#ax3.set_yticks(np.linspace(175,250,10))
ax1.set_yticks(np.linspace(190,260,8))


ax1.set_xlabel('Number of Turns')#,fontsize=axis_label_font_size)

#ax1.set_ylabel('Phase Resistance [m$\Omega$]')#,fontsize=axis_label_font_size)
ax3.set_ylabel('Phase Resistance [m$\Omega$]')#,fontsize=axis_label_font_size)

#ax3.set_ylabel('Copper Losses [W]')#,fontsize=axis_label_font_size)
ax1.set_ylabel('Copper Losses [W]')#,fontsize=axis_label_font_size)

ax1.set_xlim(min_turns,max_turns)
#ax3.set_ylim(175,250)
ax3.set_ylim(0,280)

ax2.set_xlim(min_turns,max_turns)
#ax1.set_ylim(0,225)
ax1.set_ylim(190,260)

#trans = mtransforms.blended_transform_factory(ax3.transData, ax3.transAxes)
#ax3.fill_between(turns, min_turns, max_turns, where=graph_1_axis_3[0][2] < graph_1_axis_3[0][2][constant_torque_index], facecolor='green', alpha=0.1, transform=trans)	#contant torque region
#ax3.fill_between(turns, min_turns, max_turns, where=graph_1_axis_3[0][2] > graph_1_axis_3[0][2][flux_weakening_index-1], facecolor='red', alpha=0.1, transform=trans) #flux weakening region

ax1.grid()
fig1.set_size_inches(8, 4) #default is (8,6)

fig1.savefig(db_folder+'turns_pcu_ohm%s.pdf'%(save_extension), bbox_inches='tight', additional_artists=art)

#######################################
##GRAPH 2
##legend_pos_x = 0.0
#legend_pos_x = 0.38
#legend_pos_y = 0.8

#fig1, ax1 = pl.subplots(1, 1)# ,figsize=(fig_x_cm,fig_y_cm))
#ax2 = ax1.twiny()
#ax3 = ax1.twinx()
#j=0
##plot graph_2_axis_3
#for k in range(0,len(graph_2_axis_3)):
  #line_graph_2.append(ax3.plot(turns[:number_of_graph_entries],graph_2_axis_3[k][2][:number_of_graph_entries],'-o',alpha = 1.0,markersize=4.5,fillstyle='full',markeredgewidth=0.0,c=colors[j],label=graph_2_axis_3[k][0]))#,linewidth=LINEWIDTH))
  ##line_graph_2.append(ax3.plot(turns[:number_of_graph_entries],graph_2_axis_3[k][2][:number_of_graph_entries],c=colors[j],label=graph_2_axis_3[k][0]))#,linewidth=LINEWIDTH))
  ##ax3.scatter(turns[:number_of_graph_entries],graph_2_axis_3[k][2][:number_of_graph_entries], alpha = 1.0, c=colors[j],label=graph_2_axis_3[k][0],lw = 0,s=marker_size)
  #j=j+1
  
#lines_graph_2 = line_graph_2[0]
#for k in range(1,len(graph_2_axis_3)):
  #lines_graph_2 = lines_graph_2 + line_graph_2[k]
#labels_collection_graph_2 = [l.get_label() for l in lines_graph_2]
#lgd_graph_2 = ax1.legend(lines_graph_2, labels_collection_graph_2, loc='center left', bbox_to_anchor=(legend_pos_x, legend_pos_y))#, title='Method 1',fontsize=legend_font_size)
#ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
#art.append(lgd_graph_2)

#ax1.set_xticks(np.linspace(min_turns,max_turns,7))
#ax2.xaxis.set_visible(False)
##ax1.yaxis.set_visible(False)
#ax1.set_yticks(np.linspace(0,450,10))
#ax3.set_yticks(np.linspace(0,450,10))

#ax1.set_xlabel('Mechanical Speed [rpm]')#,fontsize=axis_label_font_size)
##ax1.set_ylabel('Torque [Nm]')#,fontsize=axis_label_font_size)
#ax1.set_ylabel('Losses [W]')#,fontsize=axis_label_font_size)
#ax3.set_ylabel('Losses [W]')#,fontsize=axis_label_font_size)

#ax1.set_xlim(min_turns,max_turns)
#ax3.set_ylim(0,450)

#ax2.set_xlim(min_turns,max_turns)
#ax1.set_ylim(0,450)

#trans = mtransforms.blended_transform_factory(ax3.transData, ax3.transAxes)
##ax3.fill_between(turns, min_turns, max_turns, where=graph_1_axis_3[0][2] < 2000, facecolor='green', alpha=0.1, transform=trans)	#contant torque region
#ax3.fill_between(turns, min_turns, max_turns, where=graph_1_axis_3[0][2] < graph_1_axis_3[0][2][constant_torque_index], facecolor='green', alpha=0.1, transform=trans)	#contant torque region
#ax3.fill_between(turns, min_turns, max_turns, where=graph_1_axis_3[0][2] > graph_1_axis_3[0][2][flux_weakening_index-1], facecolor='red', alpha=0.1, transform=trans) #flux weakening region

#ax1.grid()
#ax3.tick_params(axis='y', which='major', pad=10) #spacing of 10 makes that graph1 (which has a 4 digit y-axis on RHS) ligns up nicely with 3 digit y-axis of graph2 in my latex report
##pl.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
##fig1.savefig(db_folder+'constant_torque_power_losses%s.pdf'%(save_extension), additional_artists=art)
#fig1.savefig(db_folder+'constant_torque_power_losses%s.pdf'%(save_extension), bbox_inches='tight', additional_artists=art)

pl.show() 


#right
#center left
#upper right
#lower right
#best
#center
#lower left
#center right
#upper left
#upper center
#lower center