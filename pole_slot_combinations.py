#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: script to determine all suitable pole/slot combinations for outrunner PM machine with non-overlapping concentrated double-layer coils, utilising the 2nd working harmonic of the stator coils
#Author : Andreas Joss

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)

import pylab as pl
import numpy as np
from prettytable import PrettyTable
import os, sys
import sqlite3
import datetime
import argparse
from fractions import gcd 

parser = argparse.ArgumentParser(description="SORSPM semfem simulation script",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=15, help='Specify the primary_key of the input row of the database that should be used')
parser.add_argument('-db','--database', dest='database', default='SORSPM.db', help='Specify the database name which should be used')

args = parser.parse_args()
primary_key = args.primary_key
db_name = args.database

conn = sqlite3.connect(db_name)
c = conn.cursor()

pretty_terminal_table = PrettyTable(['poles','slots','GCD','LCM','Periodicity','slots_per_pole_per_phase (q)', 'pole_pairs_per_slot_per_phase','cogging_torque_harmonic','M_s','W_s','comment'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

#def lcm(*numbers):
    #"""Return lowest common multiple."""    
    #def lcm(a, b):
        #return (a * b) // gcd(a, b)
    #return reduce(lcm, numbers, 1)

def lcm(x, y):
   """This function takes two
   integers and returns the L.C.M."""

   # choose the greater number
   if x > y:
       greater = x
   else:
       greater = y

   while(True):
       if((greater % x == 0) and (greater % y == 0)):
           lcm = greater
           break
       greater += 1

   return lcm

poles = db_input_select("machine_input_parameters","poles",primary_key)
slots = db_input_select("machine_input_parameters","slots",primary_key)

#print poles
#print slots

#max_poles = 40
max_poles = 60

#slots per pole per phase should be 0.25 in order to use 2nd working harmonic
max_slots = (max_poles*3)/4

print("gcd(30,27)")
print(gcd(30,27))
print("lcm(30,27)")
print(lcm(30,27))

#print "max poles = %d"%(max_poles)
#print "max_slots = %d"%(max_slots)
print ""
print "slots_per_pole_per_phase (q) < 1 always for concentrated windings. Thus q will always be fractional, and they follow the same set of rules as fractional slot windings (Skaar2006)."
print "The proposed feasible region from Skaar2006 is 0.25<q<0.50"

print ""
print "A higher winding factor is traditionally seen as superior, since it normally delivers better machine efficiency (Rix p.50-51). If a sinusoidal back-EMF is desired, then winding factor harmonics should be as close to 0 as possible, with the fundamental close to 1."

print ""
print "According to dr. Rix thesis (p.56), there are 3 factors which normally determine pole/slot combinations:"
print "1. winding factor"
print "2. GCD - gives number of symmetrical portions in machine - LOWER GCD value is better (but should be atleast 1)"
print "3. LCM - affects torque ripple in machine - HIGHER LCM value is better, because it results in reduced torque ripple (Rix p.?, "

#iterate through all slot numbers until maximum is reached
slots_i = 3
while slots_i <= max_slots:
  comment = ""
  poles_i = int((slots_i*4)/3)
  
  if poles_i == slots_i:
    comment = "number of poles cannot be equal to number of slots, leads to cogging torque according to Skaar2006 p.2"
    #print "number of poles cannot be equal to number of slots, leads to cogging torque according to Skaar2006 p.2"
  
  if abs(poles_i-slots_i)==1:
    comment = "unbalanced radial forces will exist according to Skaar2006"
  
  #temp = []
  #temp.append(poles_i)
  #temp.append(slots_i)
  #LCM = lcm(temp)
  LCM = lcm(poles_i,slots_i)
  GCD = gcd(poles_i,slots_i)
  
  periodicity_in_one_full_rotation = gcd(int(slots_i/3),int(poles_i/2))
  pole_pairs_per_slot_per_phase = int(poles_i/2)/int(slots_i/3)
  slots_per_pole_per_phase = slots_i/(poles_i*3)
  cogging_torque_harmonic = (slots/gcd(slots,poles))*(poles/gcd(slots,poles)) #works well so far for integral-slot machines
  
  #equations used by dr. Rix chapter 2
  M_s = gcd(int(poles_i/2), slots_i) #dr Rix p.11 says M_s is number of machine sections which are periodic in machine
  W_s = gcd(poles_i,slots_i) #dr Rix p.12 says W_s is number of winding sections which repeats itself (with negative or positive periodicity
  tau = int(W_s/M_s) #answer for this machine is 1, thus positive periodicity, and the following assumptions are valid
  
  pretty_terminal_table.add_row([poles_i, slots_i, GCD, LCM, periodicity_in_one_full_rotation, slots_per_pole_per_phase, pole_pairs_per_slot_per_phase,cogging_torque_harmonic,M_s,W_s,comment])
  slots_i = slots_i + 3

print pretty_terminal_table  