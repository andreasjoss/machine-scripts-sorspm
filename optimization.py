#!/usr/bin/python

#Title : This script takes care of a single optimization convergence process
#Author: Andreas Joss

from __future__ import division
#import pyOpt
import nlopt
import numpy as np
import os, sys
import shutil
from prettytable import PrettyTable
#import threading
import argparse
import time
import sqlite3
import pylab as pl
import smtplib
import datetime

enable_multiple_operating_points = 1
#operating_point = 1 #ROUND LITZ
operating_point = 0 #SOLID BARS
enable_state_machine_operating_points = 1
enable_objective_constraints = 1
single_weight_optimization = 1
use_pyOpt = 0
use_nlopt = 1
start_time = time.time()
primary_key_start = 1
primary_key_upper_limit = -2
primary_key_lower_limit = -1
debug = 1
debug_f = 0
has_entered_opt_function = False
display_poly_from_this_iteration = -1
#display_poly_from_this_iteration = 125
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki','g','y','k']

#epsilon1 = 0.5e-3 #hierdie waarde vat so 15min vir 'n sekere weight om geoptimeer te wees
epsilon1 = 3e-2

start_time = time.time()

parser = argparse.ArgumentParser(description="Optimize weighted sum",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=1,help='primary_key to be optimized')
args = parser.parse_args()
primary_key = args.primary_key

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  #try:
  c.execute(string)
  return c.fetchone()[0]
  #except:
    #return 0

pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

pretty_variable_table = PrettyTable(['Variable','Value [p.u.]','Value','Unit'])
pretty_variable_table.align = 'l'
pretty_variable_table.border= True  

pretty_output_table = PrettyTable(['Variable','Value','Unit'])
pretty_output_table.align = 'l'
pretty_output_table.border= True  

#setup databse connection
conn = sqlite3.connect('SORSPM.db')
c = conn.cursor()

#c.execute("ATTACH DATABASE 'SORSPM.db' as SORSPM")
pk=1

#ROUND LITZ
#p_mech_constraint = [[0,1,0],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
#torque_ripple_constraint = [[0,0,0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
#torque_constraint = [[0,0,0],db_input_select("output_constraints","average_torque_dq",pk)]
#power_factor_constraint = [[0,0,0],db_input_select("output_constraints","power_factor",pk)]
#total_mass_constraint = [[0,0,0],db_input_select("output_constraints","total_mass",pk)]
#magnet_mass_constraint = [[0,0,0],db_input_select("output_constraints","magnet_mass",pk)]
#efficiency_constraint = [[0,0,0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
#total_losses_constraint = [[0,0,0],db_input_select("output_constraints","total_losses",pk)]
#current_density_rms_constraint = [[0,0,0],db_input_select("output_constraints","current_density_rms",pk)]
#torque_density_constraint = [[0,0,0],db_input_select("output_constraints","torque_density",pk)] 

#SOLID BARS
#p_mech_constraint = [[1,0],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
#torque_ripple_constraint = [[0,0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
#torque_constraint = [[0,0],db_input_select("output_constraints","average_torque_dq",pk)]
#power_factor_constraint = [[0,0],db_input_select("output_constraints","power_factor",pk)]
#total_mass_constraint = [[0,0],db_input_select("output_constraints","total_mass",pk)]
#magnet_mass_constraint = [[0,0],db_input_select("output_constraints","magnet_mass",pk)]
#efficiency_constraint = [[0,0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
#total_losses_constraint = [[0,0],db_input_select("output_constraints","total_losses",pk)]
#current_density_rms_constraint = [[0,0],db_input_select("output_constraints","current_density_rms",pk)]
#torque_density_constraint = [[0,0],db_input_select("output_constraints","torque_density",pk)]

p_mech_constraint = [[1],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
torque_ripple_constraint = [[0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
torque_constraint = [[0],db_input_select("output_constraints","average_torque_dq",pk)]
power_factor_constraint = [[0],db_input_select("output_constraints","power_factor",pk)]
total_mass_constraint = [[0],db_input_select("output_constraints","total_mass",pk)]
magnet_mass_constraint = [[0],db_input_select("output_constraints","magnet_mass",pk)]
efficiency_constraint = [[0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
total_losses_constraint = [[0],db_input_select("output_constraints","total_losses",pk)]
current_density_rms_constraint = [[0],db_input_select("output_constraints","current_density_rms",pk)]
torque_density_constraint = [[0],db_input_select("output_constraints","torque_density",pk)]

#setup databse connection
conn.close()
conn = sqlite3.connect('optimise.db')
c = conn.cursor()

#obtain all the working points
operating_speeds = []
if enable_state_machine_operating_points:
  res = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
  for name in res:
    if name[0].startswith("machine_input_parameters_"):
      print name[0]
      operating_speeds.append(int(name[0][-3:]))
    
input_table_name = []
output_table_name = []

for i in range(0,len(operating_speeds)):
  input_table_name.append('machine_input_parameters' + '_rpm_%03d'%(operating_speeds[i]))
  output_table_name.append('machine_semfem_output' + '_rpm_%03d'%(operating_speeds[i]))

if single_weight_optimization == 1:
  iteration_number = 1
else:
  iteration_number = 0

c.execute("UPDATE machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))

#c.execute("ATTACH DATABASE 'best.db' as best")
#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(0,))
#c.execute("INSERT INTO best.info (variable_name,value_int) VALUES (?,?)",('have_already_commited_best_solution',0))
conn.commit()

#GLOBALS USED IN OBJ FUNC()
weight_eff = []
weight_pf = []
weight_mass_total = []
weight_torque_average = []
weight_torque_ripple = []
weight_torque_density = []
weight_pm_mass = []
weight_pmech = []
weight_total_losses = []
weight_J_rms = []
#read weights
for i in range(0,len(operating_speeds)):
  weight_eff.append(db_input_select(input_table_name[i],"weight_eff",primary_key))
  weight_pf.append(db_input_select(input_table_name[i],"weight_pf",primary_key))
  weight_mass_total.append(db_input_select(input_table_name[i],"weight_mass_total",primary_key))
  weight_torque_average.append(db_input_select(input_table_name[i],"weight_torque_average",primary_key))
  weight_torque_ripple.append(db_input_select(input_table_name[i],"weight_torque_ripple",primary_key))
  weight_torque_density.append(db_input_select(input_table_name[i],"weight_torque_density",primary_key))
  weight_pm_mass.append(db_input_select(input_table_name[i],"weight_pm_mass",primary_key))
  weight_pmech.append(db_input_select(input_table_name[i],"weight_pmech",primary_key))
  weight_total_losses.append(0)
  weight_J_rms.append(0)

#weight_eff = db_input_select(input_table_name[0],"weight_eff",primary_key)
#weight_eff = 0

#weight_pf = db_input_select(input_table_name[0],"weight_pf",primary_key)
#weight_pf = 0

#weight_mass_total = db_input_select(input_table_name[0],"weight_mass_total",primary_key)
#weight_mass_total = 0

#weight_torque_average = db_input_select(input_table_name[0],"weight_torque_average",primary_key)
#weight_torque_average = 0

#weight_torque_ripple = db_input_select(input_table_name[0],"weight_torque_ripple",primary_key)
#weight_torque_ripple = 0.2

#weight_torque_density = db_input_select(input_table_name[0],"weight_torque_density",primary_key)
#weight_torque_density = 0.2

#weight_pm_mass = db_input_select(input_table_name[0],"weight_pm_mass",primary_key)
#weight_pm_mass = 0

#weight_pmech = db_input_select(input_table_name[0],"weight_pmech",primary_key)
#weight_pmech = 0.6

#weight_total_losses = db_input_select(input_table_name[0],"weight_total_losses",primary_key)
#weight_total_losses = 0

#weight_J_rms = db_input_select(input_table_name[0],"weight_J_rms",primary_key)
#weight_J_rms = 0

#p_mech_penalty_constant = 1.7
p_mech_penalty_constant = 1.0

#get primary_keys where ideal values are stored, see if their outputs exist
#if their outputs dont exist, then we only devide by 1
#ideal_value_keys = []
#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_eff',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_mass_total',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_average',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_ripple',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pf',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_torque_density',))
#ideal_value_keys.append(c.fetchone()[0])

#c.execute("SELECT primary_key FROM ideal_value_keys WHERE weight_name = ?",('weight_pm_mass',))
#ideal_value_keys.append(c.fetchone()[0])

if enable_objective_constraints == 0:
  #try to get the primary_key ideal value outputs
  try:
    c.execute("SELECT efficiency_indirect_dq FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[0]),))
    ideal_eff = c.fetchone()[0]
  except:
    ideal_eff = 1
    
  try:  
    c.execute("SELECT total_mass FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[1]),))
    ideal_mass_total = c.fetchone()[0]
  except:
    ideal_mass_total = 1

  try:
    c.execute("SELECT average_torque_dq FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[2]),))
    ideal_torque_average = c.fetchone()[0]
  except:
    ideal_torque_average = 1
    
  try:
    c.execute("SELECT torque_ripple_semfem FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[3]),))
    ideal_torque_ripple = c.fetchone()[0]
  except:
    ideal_torque_ripple = 1
    
  try:
    c.execute("SELECT power_factor FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[4]),))
    ideal_pf = c.fetchone()[0]
  except:
    ideal_pf = 1
    
  try:
    c.execute("SELECT torque_density FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[5]),))
    ideal_torque_density = c.fetchone()[0]
  except:
    ideal_torque_density = 1    

  try:
    c.execute("SELECT magnet_mass FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[5]),))
    ideal_pm_mass = c.fetchone()[0]
  except:
    ideal_pm_mass = 1    
else:
  ideal_eff = efficiency_constraint[1]
  ideal_mass_total = total_mass_constraint[1]
  ideal_torque_average = torque_constraint[1]
  ideal_torque_ripple = torque_ripple_constraint[1]
  ideal_pf = power_factor_constraint[1]
  ideal_torque_density = torque_density_constraint[1]
  ideal_pm_mass = magnet_mass_constraint[1]
  ideal_pmech_out = p_mech_constraint[1]
  ideal_J_rms = current_density_rms_constraint[1]
  ideal_total_losses = total_losses_constraint[1]

pretty_terminal_table.add_row(["ideal_eff",ideal_eff,"[%]"])
pretty_terminal_table.add_row(["ideal_mass_total",ideal_mass_total,"[kg]"])
pretty_terminal_table.add_row(["ideal_torque_average",ideal_torque_average,"[N.m]"])
pretty_terminal_table.add_row(["ideal_torque_ripple",ideal_torque_ripple,"[N.m]"])
pretty_terminal_table.add_row(["ideal_pf",ideal_pf,"[%]"])
pretty_terminal_table.add_row(["ideal_torque_density",ideal_torque_density,"[N.m/kg]"])
pretty_terminal_table.add_row(["ideal_pm_mass",ideal_pm_mass,"[N.m/kg]"])
if enable_objective_constraints == 1:
  pretty_terminal_table.add_row(["ideal_pmech_out",ideal_pmech_out,"[W]"])
  pretty_terminal_table.add_row(["ideal_J_rms",ideal_J_rms,"[A/mm2]"])
  pretty_terminal_table.add_row(["ideal_total_losses",ideal_total_losses,"[W]"])

#read starting values
#pole_pairs = db_input_select(input_table_name[0],"poles",primary_key)/2
#J = db_input_select(input_table_name[0],"current_density_rms",primary_key)
#VLL_pk_max_allowed = db_input_select(input_table_name[0],"allowed_peak_line_to_line_terminal_voltage",primary_key)
#r -- radius, h -- height, m -- magnet, o -- outer, i -- inner, k -- ratio, r -- radially, t -- tangentially

#ryo = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key)
hyo = db_input_select(input_table_name[0],"rotor_yoke_height_in_mm",primary_key)
hyi = db_input_select(input_table_name[0],"stator_yoke_height_in_mm",primary_key)
hc  = db_input_select(input_table_name[0],"coil_height_in_mm",primary_key)
hmr = db_input_select(input_table_name[0],"radially_magnetized_PM_height_in_mm",primary_key)
kc  = db_input_select(input_table_name[0],"coil_width_ratio",primary_key)
kmr = db_input_select(input_table_name[0],"radially_magnetized_PM_width_ratio",primary_key)
ryo = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key)
stacklength = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key)

hs = db_input_select(input_table_name[0],"stator_shoe_height_in_mm",primary_key)
ks = db_input_select(input_table_name[0],"stator_shoe_width_in_mm",primary_key)
shoe_theta = db_input_select(input_table_name[0],"stator_shoe_wedge_degrees",primary_key)

insert_lamination_hole_support_rod = db_input_select(input_table_name[0],"insert_lamination_hole_support_rod",primary_key)
if insert_lamination_hole_support_rod == 1:
  hrod = db_input_select(input_table_name[0],"stator_rod_distance_from_tooth_tip",primary_key)
  stator_rod_hole_diameter = db_input_select(input_table_name[0],"stator_rod_hole_diameter",primary_key)
  #stator_rod_minimum_distance_from_shoe_tip = db_input_select(input_table_name[0],"stator_rod_minimum_distance_from_shoe_tip",primary_key)
  stator_rod_minimum_distance_from_shoe_tip = 1.1*0.5*stator_rod_hole_diameter
  #stator_rod_minimum_ratio_of_teeth_height = db_input_select(input_table_name[0],"stator_rod_minimum_ratio_of_teeth_height",primary_key)
  stator_rod_minimum_ratio_of_teeth_height = 0.7
  magnet_sleeve_tolerance = db_input_select(input_table_name[0],"magnet_sleeve_tolerance_spacing_in_mm",primary_key)
  shoe_weakpoint_thickness = db_input_select(input_table_name[0],"shoe_weakpoint_thickness",primary_key)
  stator_shoe_height = db_input_select(input_table_name[0],"stator_shoe_height_in_mm",primary_key)
  magnet_radially_inner_radius = ryo-hyo-hmr-magnet_sleeve_tolerance
  g = db_input_select(input_table_name[0],"airgap_in_mm",primary_key)
  stator_coil_radius = magnet_radially_inner_radius - g - magnet_sleeve_tolerance
  stator_teeth_inner_radius = magnet_radially_inner_radius - g - hc - magnet_sleeve_tolerance - shoe_weakpoint_thickness - stator_shoe_height
  
  hrod_max_var = stator_coil_radius+stator_rod_minimum_ratio_of_teeth_height*(stator_coil_radius-stator_teeth_inner_radius)
  hrod_max = hrod_max_var-stator_coil_radius
  hrod_min = stator_rod_minimum_distance_from_shoe_tip
  
  #print("hrod = %g"%hrod)
  #print("hrod_min = %g"%hrod_min)
  #print("hrod_max = %g"%hrod_max)
  if hrod_max < hrod_min: hrod_max=hrod_min

  
#kc == ratio of stator coil side width out of maximum size 1
#ks == ratio of coil core, i.e. stator teeth width 



#kmt = db_input_select(input_table_name[0],"tangentially_magnetized_PM_width_ratio",primary_key)
#hs_ratio= db_input_select(input_table_name[0],"stator_teeth_to_coil_height_ratio",primary_key)
#kq  = db_input_select(input_table_name[0],"coils_per_phase_vs_rotor_pole_pair_ratio",primary_key)
#l = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key)
#g = db_input_select(input_table_name[0],"airgap_in_mm",primary_key)
#muR_yo = db_input_select(input_table_name[0],"rotor_yoke_relative_permeability",primary_key)
#muR_yi = db_input_select(input_table_name[0],"stator_yoke_relative_permeability",primary_key)
#fill_factor = db_input_select(input_table_name[0],"fill_factor",primary_key)
#n_rpm = db_input_select(input_table_name[0],"n_rpm",primary_key)
#Brem= db_input_select(input_table_name[0],"B_rem",primary_key)						#remnant flux density of NdBFe N48 PMs        [T]
#ryoke_minimum_ratio= db_input_select(input_table_name[0],"rotor_yoke_height_minimum_ratio",primary_key)
#minimum_rotor_yoke_thickness = ryoke_minimum_ratio*hyo
#km_gap_teeth = db_input_select(input_table_name[0],"rotor_gap_teeth_height_ratio",primary_key)
#rotor_yoke_iron_N_al= db_input_select(input_table_name[0],"rotor_yoke_iron_N_al",primary_key)
#stator_yoke_iron_N_al= db_input_select(input_table_name[0],"stator_yoke_iron_N_al",primary_key)
#N = db_input_select(input_table_name[0],"force_this_number_of_turns",primary_key)
#coil_temperature = db_input_select(input_table_name[0],"coil_temperature",primary_key)
#hmt_slot_depth_ratio = db_input_select(input_table_name[0],"rotor_slot_depth_ratio",primary_key)
#hmt_magnet_height_ratio_of_slot = db_input_select(input_table_name[0],"tangentially_magnetised_PM_height_ratio_of_slot",primary_key)

#km_gap = 1 - kmr - kmt
#ks = 1-kc
if debug == 1:
  #ri = ryo-hyo-hyi-g-hc-hmr
  #pretty_terminal_table.add_row(["-------------","------------","------------"])
  #pretty_terminal_table.add_row(["ryo",ryo,"[mm]"])
  #pretty_terminal_table.add_row(["g",g,"[mm]"])
  #pretty_terminal_table.add_row(["ri",ri,"[mm]"])
  pretty_terminal_table.add_row(["hyo",hyo,"[mm]"])
  pretty_terminal_table.add_row(["hyi",hyi,"[mm]"])
  pretty_terminal_table.add_row(["hc",hc,"[mm]"])
  pretty_terminal_table.add_row(["hmr",hmr,"[mm]"])
  #pretty_terminal_table.add_row(["hrod",hrod,"[mm]"])



#read minimum values
#pole_pairs_min = db_input_select(input_table_name[0],"poles",primary_key_lower_limit)/2
#ryo_min = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key_lower_limit)
#ri_min = db_input_select(input_table_name[0],"stator_yoke_inner_radius_in_mm",primary_key_lower_limit)
hyo_min = db_input_select(input_table_name[0],"rotor_yoke_height_in_mm",primary_key_lower_limit)
hyi_min = db_input_select(input_table_name[0],"stator_yoke_height_in_mm",primary_key_lower_limit)
hc_min  = db_input_select(input_table_name[0],"coil_height_in_mm",primary_key_lower_limit)
#hs_ratio_min= db_input_select(input_table_name[0],"stator_teeth_to_coil_height_ratio",primary_key_lower_limit)
hmr_min = db_input_select(input_table_name[0],"radially_magnetized_PM_height_in_mm",primary_key_lower_limit)
kmr_min = db_input_select(input_table_name[0],"radially_magnetized_PM_width_ratio",primary_key_lower_limit)
#kmt_min = db_input_select(input_table_name[0],"tangentially_magnetized_PM_width_ratio",primary_key_lower_limit)
kc_min  = db_input_select(input_table_name[0],"coil_width_ratio",primary_key_lower_limit)
ryo_min  = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key_lower_limit)
stacklength_min  = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key_lower_limit)
#l_min = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key_lower_limit)
#km_gap_teeth_min = db_input_select(input_table_name[0],"rotor_gap_teeth_height_ratio",primary_key_lower_limit)
#hmt_slot_depth_ratio_min = db_input_select(input_table_name[0],"rotor_slot_depth_ratio",primary_key_lower_limit)
#hmt_magnet_height_ratio_of_slot_min = db_input_select(input_table_name[0],"tangentially_magnetised_PM_height_ratio_of_slot",primary_key_lower_limit)

hs_min = db_input_select(input_table_name[0],"stator_shoe_height_in_mm",primary_key_lower_limit)
ks_min = db_input_select(input_table_name[0],"stator_shoe_width_in_mm",primary_key_lower_limit)
shoe_theta_min = db_input_select(input_table_name[0],"stator_shoe_wedge_degrees",primary_key_lower_limit)

#read maximum values (only those necessary)
#l_max   = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key_upper_limit)
#ryo_max = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key_upper_limit)
#pole_pairs_max = db_input_select(input_table_name[0],"poles",primary_key_upper_limit)/2
hyo_max = db_input_select(input_table_name[0],"rotor_yoke_height_in_mm",primary_key_upper_limit)
hyi_max = db_input_select(input_table_name[0],"stator_yoke_height_in_mm",primary_key_upper_limit)
hc_max = db_input_select(input_table_name[0],"coil_height_in_mm",primary_key_upper_limit)
hmr_max = db_input_select(input_table_name[0],"radially_magnetized_PM_height_in_mm",primary_key_upper_limit)
kmr_max = db_input_select(input_table_name[0],"radially_magnetized_PM_width_ratio",primary_key_upper_limit)
#kmt_max = db_input_select(input_table_name[0],"tangentially_magnetized_PM_width_ratio",primary_key_upper_limit)
kc_max  = db_input_select(input_table_name[0],"coil_width_ratio",primary_key_upper_limit)
ryo_max  = db_input_select(input_table_name[0],"rotor_yoke_outer_radius_in_mm",primary_key_upper_limit)
stacklength_max  = db_input_select(input_table_name[0],"stacklength_in_mm",primary_key_upper_limit)
#hmt_slot_depth_ratio_max  = db_input_select(input_table_name[0],"rotor_slot_depth_ratio",primary_key_upper_limit)

hs_max = db_input_select(input_table_name[0],"stator_shoe_height_in_mm",primary_key_upper_limit)
ks_max = db_input_select(input_table_name[0],"stator_shoe_width_in_mm",primary_key_upper_limit)
shoe_theta_max = db_input_select(input_table_name[0],"stator_shoe_wedge_degrees",primary_key_upper_limit)

hyo_pu = (hyo - hyo_min)/(hyo_max-hyo_min)
hyi_pu = (hyi - hyi_min)/(hyi_max-hyi_min)
hc_pu = (hc - hc_min)/(hc_max-hc_min)
hmr_pu = (hmr - hmr_min)/(hmr_max-hmr_min)
kmr_pu = (kmr - kmr_min)/(kmr_max-kmr_min)
kc_pu = (kc - kc_min)/(kc_max-kc_min)
hrod_pu = (hrod - hrod_min)/(hrod_max-hrod_min)
ryo_pu = (ryo - ryo_min)/(ryo_max-ryo_min)
stacklength_pu = (stacklength - stacklength_min)/(stacklength_max-stacklength_min)

hs_pu = (hs - hs_min)/(hs_max-hs_min)
ks_pu = (ks - ks_min)/(ks_max-ks_min)
shoe_theta_pu = (shoe_theta - shoe_theta_min)/(shoe_theta_max-shoe_theta_min)

print pretty_terminal_table

##determine initial values to replicate starting dimensions (all these values should have a value between 0 and 1)
#x_ryo_init = (ryo-ryo_min)/(ryo_max-ryo_min)
#ri = ryo-hyo-hyi-g-hc-hmr
#x_ri_init  = (ri-ri_min)/(ryo-ri_min-hyo_min-hyi_min-hc_min-g-hmr_min)
#x_hyo_init = (hyo-hyo_min)/(ryo-ri-hyo_min-hyi_min-hc_min-g-hmr_min)
#x_hyi_init = (hyihrod_min-hyi_min)/(ryo-ri-hyo-hyi_min-hc_min-g-hmr_min)
#x_hc_init  = (hc-hc_min)/(ryo-ri-hyo-hyi-hc_min-g-hmr_min)
#x_hmr_init = (hmr-hmr_min)/(ryo-ri-hyo-hyi-hc-g-hmr_min)
#x_l_init = (l-l_min)/(l_max-l_min)

#if debug == 1:
  #pretty_terminal_table.add_row(["-------------","------------","------------"])
  #pretty_terminal_table.add_row(["x_ryo_init",x_ryo_init,"[p.u.]"])
  #pretty_terminal_table.add_row(["x_ri_init",x_ri_init,"[p.u.]"])
  #pretty_terminal_table.add_row(["x_hyo_init",x_hyo_init,"[p.u.]"])
  #pretty_terminal_table.add_row(["x_hyi_init",x_hyi_init,"[p.u.]"])
  #pretty_terminal_table.add_row(["x_hc_init",x_hc_init,"[p.u.]"])
  #pretty_terminal_table.add_row(["x_hmr_init",x_hmr_init,"[p.u.]"])

  ##just for debugging
  #ryo = (ryo_max - ryo_min)*x_ryo_init + ryo_min
  #ri  = (ryo-ri_min-hyo_min-hyi_min-hc_min-g-hmr_min)*x_ri_init + ri_min
  #hyo = (ryo-ri-hyo_min-hyi_min-hc_min-g-hmr_min)*x_hyo_init + hyo_min
  #hyi = (ryo-ri-hyo-hyi_min-hc_min-g-hmr_min)*x_hyi_init + hyi_min
  #hc  = (ryo-ri-hyo-hyi-hc_min-g-hmr_min)*x_hc_init + hc_min
  #hmr = (ryo-ri-hyo-hyi-hc-g-hmr_min)*x_hmr_init + hmr_min

  #pretty_terminal_table.add_row(["-------------","------------","------------"])
  #pretty_terminal_table.add_row(["ryo",ryo,"[mm]"])
  #pretty_terminal_table.add_row(["ri",ri,"[mm]"])
  #pretty_terminal_table.add_row(["hyo",hyo,"[mm]"])
  #pretty_terminal_table.add_row(["hyi",hyi,"[mm]"])
  #pretty_terminal_table.add_row(["hc",hc,"[mm]"])
  #pretty_terminal_table.add_row(["hmr",hmr,"[mm]"])

  #print pretty_terminal_table

f_pmech_normalised_array = []
f_total_mass_normalised_array = []
f_torque_ripple_semfem_normalised_array = []
f_efficiency_indirect_dq_normalised_array = []
f_torque_density_normalised_array = []

f_array = []

hyo_array = []
hyi_array = []
hc_array = []
hmr_array = []
kc_array = []
kmr_array = []
hrod_array = []
stacklength_array = []
ryo_array = []

hyo_pu_array = []
hyi_pu_array = []
hc_pu_array = []
hmr_pu_array = []
kc_pu_array = []
kmr_pu_array = []
hrod_pu_array = []
stacklength_pu_array = []
ryo_pu_array = []

line_input_variables = []
scat_input_variables = []

f_power_factor = np.zeros(len(operating_speeds))
f_average_torque_dq = np.zeros(len(operating_speeds))
f_torque_ripple_semfem = np.zeros(len(operating_speeds))
f_efficiency_indirect_dq = np.zeros(len(operating_speeds))
f_total_mass = np.zeros(len(operating_speeds))
f_torque_density = np.zeros(len(operating_speeds))
f_pm_mass = np.zeros(len(operating_speeds))
f_pmech = np.zeros(len(operating_speeds))
f_current_density_rms = np.zeros(len(operating_speeds))
f_total_losses = np.zeros(len(operating_speeds))

f_power_factor_normalised = np.zeros(len(operating_speeds))
f_average_torque_dq_normalised = np.zeros(len(operating_speeds))
f_torque_ripple_semfem_normalised = np.zeros(len(operating_speeds))
f_efficiency_indirect_dq_normalised = np.zeros(len(operating_speeds))
f_total_mass_normalised = np.zeros(len(operating_speeds))
f_torque_density_normalised = np.zeros(len(operating_speeds))
f_pm_mass_normalised = np.zeros(len(operating_speeds))
f_pmech_normalised = np.zeros(len(operating_speeds))
f_current_density_rms_normalised = np.zeros(len(operating_speeds))
f_total_losses_normalised = np.zeros(len(operating_speeds))

#define objective function to be optimized
def weighted_objective_function(x,grad):
  global c
  global primary_key
  global iteration_number
  global conn
  global operating_point
  #raw_input("Press Enter to continue...")
  print("NEW ITERATION")
  print("iteration number = %d"%(iteration_number))
  #print "optimization.py -- starting weighted_objective_function"
  if single_weight_optimization == 0:
    iteration_number = db_input_select(input_table_name[0],"iteration_number",primary_key)
  else:
    #read last entry in database
    #SELECT * FROM TABLE WHERE ID = (SELECT MAX(ID) FROM TABLE);
    c.execute("SELECT * FROM %s WHERE primary_key = (SELECT MAX(primary_key) FROM %s)"%(input_table_name[0],input_table_name[0]))
    #primary_key = c.fetchone()[0]
    #primary_key = 1
    #iteration_number = primary_key  
  
  #hyo_pu = np.round(x[0],3)
  #hyi_pu = np.round(x[1],3)
  #hc_pu  = np.round(x[2],3)
  #hmr_pu = np.round(x[3],3)
  #kc_pu  = np.round(x[4],3) #coil_width_ratio
  #kmr_pu = np.round(x[5],3) #radially_magnetized_PM_width_ratio
  #stacklength_pu = np.round(x[6],3)
  #ryo_pu = np.round(x[7],3)
  #hs_pu = np.round(x[8],3)
  #ks_pu = np.round(x[9],3)
  #shoe_theta_pu = np.round(x[10],3)
  #hrod_pu = np.round(x[11],3) #rod in stator teeth distance from tip

  hyo_pu = x[0]
  hyi_pu = x[1]
  hc_pu  = x[2]
  hmr_pu = x[3]
  kc_pu  = x[4]#coil_width_ratio
  kmr_pu = x[5]#radially_magnetized_PM_width_ratio
  stacklength_pu = x[6]
  ryo_pu = x[7]
  hs_pu = x[8]
  ks_pu = x[9]
  shoe_theta_pu = x[10]
  hrod_pu = x[11] #rod in stator teeth distance from tip

  if iteration_number == 1:
    print "First function call"
    print("hyo_pu = %f"%(hyo_pu))
    print("hyi_pu = %f"%(hyi_pu))
    print("hc_pu = %f"%(hc_pu))
    print("hmr_pu = %f"%(hmr_pu))
    print("kc_pu = %f"%(kc_pu))
    print("kmr_pu = %f"%(kmr_pu))
    print("stacklength_pu = %f"%(stacklength_pu))
    print("ryo_pu = %f"%(ryo_pu))
    print("hs_pu = %f"%(hs_pu))
    print("ks_pu = %f"%(ks_pu))
    print("shoe_theta_pu = %f"%(shoe_theta_pu))    

  hyo_pu_array.append(hyo_pu)
  hyi_pu_array.append(hyi_pu)
  hc_pu_array.append(hc_pu)
  hmr_pu_array.append(hmr_pu)
  kc_pu_array.append(kc_pu)
  kmr_pu_array.append(kmr_pu)
  hrod_pu_array.append(hrod_pu)
  stacklength_pu_array.append(stacklength_pu)
  ryo_pu_array.append(ryo_pu)  
  
  hyo = hyo_min + hyo_pu*(hyo_max-hyo_min)
  hyi = hyi_min + hyi_pu*(hyi_max-hyi_min)
  hc = hc_min + hc_pu*(hc_max-hc_min)
  hmr = hmr_min + hmr_pu*(hmr_max-hmr_min)
  kc = kc_min + kc_pu*(kc_max-kc_min)
  kmr = kmr_min + kmr_pu*(kmr_max-kmr_min)
  hrod = hrod_min + hrod_pu*(hrod_max-hrod_min)
  stacklength = stacklength_min + stacklength_pu*(stacklength_max-stacklength_min)
  ryo = ryo_min + ryo_pu*(ryo_max-ryo_min)
  hs = hs_min + hs_pu*(hs_max-hs_min)
  ks = ks_min + ks_pu*(ks_max-ks_min)
  shoe_theta = shoe_theta_min + shoe_theta_pu*(shoe_theta_max-shoe_theta_min)
  
  #print("hyo = %f"%(hyo))
  #print("hyi = %f"%(hyi))
  #print("hc = %f"%(hc))
  #print("hmr = %f"%(hmr))
  #print("kc = %f"%(kc))
  #print("kmr = %f"%(kmr))
  #print("hrod = %f"%(hrod))
  #print("stacklength = %f"%(stacklength))
  #print("ryo = %f"%(ryo))
  
  #hs_ratio= x[7] #stator_teeth_to_coil_height_ratio (keep 1 so that stator teeth fully present)
  #kmt = x[8] #tangentially_magnetized_PM_width_ratio
  
  #x_ryo = x[1]
  
  #these formulas are being used in SORSPM_semfem.py
  #kmt = (1 - kmr)*x[8] #tangentially_magnetized_PM_width_ratio
  #km_gap = 1 - kmr - kmt #remainder is either air or iron gap
  
  
  #x_l = x[10] #stacklength_in_mm
  #km_gap_teeth = x[9] #rotor_gap_teeth_height_ratio
  #hmt_slot_depth_ratio = x[9] #rotor_slot_depth_ratio
  #x_ri  = x[6]
  #hmt_magnet_height_ratio_of_slot = x[11] #tangentially_magnetised_PM_height_ratio_of_slot
  
  hyo_array.append(hyo)
  hyi_array.append(hyi)
  hc_array.append(hc)
  hmr_array.append(hmr)
  kc_array.append(kc)
  kmr_array.append(kmr)
  hrod_array.append(hrod)
  stacklength_array.append(stacklength)
  ryo_array.append(ryo)  
  
  
  
  
  
  
  
  #ryo = (ryo_max - ryo_min)*x_ryo + ryo_min
  #ri  = (ryo-ri_min-hyo_min-hyi_min-hc_min-g-hmr_min)*x_ri + ri_min
  #hyo = (ryo-ri-hyo_min-hyi_min-hc_min-g-hmr_min)*x_hyo + hyo_min
  #hyi = (ryo-ri-hyo-hyi_min-hc_min-g-hmr_min)*x_hyi + hyi_min
  #hc  = (ryo-ri-hyo-hyi-hc_min-g-hmr_min)*x_hc + hc_min
  #hmr = (ryo-ri-hyo-hyi-hc-g-hmr_min)*x_hmr + hmr_min
  #pretty_variable_table.add_row(["hmr",hmr,"[mm]"])
  #l = (l_max-l_min)*x_l + l_min
  
  if debug == 1:
    #pretty_variable_table.add_row(["pole_pairs",pole_pairs,"[pairs]"])
    #pretty_variable_table.add_row(["x_ryo",x_ryo,"[p.u.]"])
    #pretty_variable_table.add_row(["x_ri",x_ri,"[p.u.]"])
    #pretty_variable_table.add_row(["x_hyo",x_hyo,"[p.u.]"])
    #pretty_variable_table.add_row(["x_hyi",x_hyi,"[p.u.]"])
    #pretty_variable_table.add_row(["x_hc",x_hc,"[p.u.]"])
    #pretty_variable_table.add_row(["x_hmr",x_hmr,"[p.u.]"])
    #pretty_variable_table.add_row(["x_l",x_l,"[p.u.]"])
    pretty_variable_table.add_row(["kc",kc_pu,kc,"[p.u.]"])
    pretty_variable_table.add_row(["kmr",kmr_pu,kmr,"[p.u.]"])
    #pretty_variable_table.add_row(["kmt",kmt,"[p.u.]"])
    #pretty_variable_table.add_row(["km_gap_teeth",km_gap_teeth,"[p.u.]"])
    #pretty_variable_table.add_row(["hmt_slot_depth_ratio",hmt_slot_depth_ratio,"[p.u.]"])
    #pretty_variable_table.add_row(["hmt_magnet_height_ratio_of_slot",hmt_magnet_height_ratio_of_slot,"[p.u.]"])
    #pretty_variable_table.add_row(["-------------","------------","------------"])
    #pretty_variable_table.add_row(["ryo",ryo,"[mm]"])
    #pretty_variable_table.add_row(["ri",ri,"[mm]"])
    pretty_variable_table.add_row(["hyo",hyo_pu,hyo,"[mm]"])
    pretty_variable_table.add_row(["hyi",hyi_pu,hyi,"[mm]"])
    pretty_variable_table.add_row(["hc",hc_pu,hc,"[mm]"])
    pretty_variable_table.add_row(["hmr",hmr_pu,hmr,"[mm]"])
    pretty_variable_table.add_row(["hrod",hrod_pu,hrod,"[mm]"])
    pretty_variable_table.add_row(["stacklength",stacklength_pu,stacklength,"[mm]"])
    pretty_variable_table.add_row(["ryo",ryo_pu,ryo,"[mm]"])
    pretty_variable_table.add_row(["hs",hs_pu,hs,"[mm]"])
    pretty_variable_table.add_row(["ks",ks_pu,ks,"[mm]"])
    pretty_variable_table.add_row(["shoe_theta",shoe_theta_pu,shoe_theta,"[deg]"])
    
    print pretty_variable_table
    
    pretty_variable_table.clear_rows()
  
  #insert previous input parameters into new row
  if single_weight_optimization == 1:
    #c.execute("INSERT INTO best.machine_semfem_output (power_factor,average_torque_dq,efficiency_indirect_dq,torque_ripple_semfem,total_mass,torque_density,magnet_mass,p_out_dq_torque,total_losses,current_density_rms,f_value,primary_key) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
    
    table_name = 'machine_input_parameters'
    pk2 = primary_key - 1
    pk1 = primary_key
    
    c = conn.execute("select * from %s"%(table_name))
    column_names = list(map(lambda x: x[0], c.description))  
  
    for j in range(0,len(operating_speeds)):
      for i in range(1,len(column_names)):
	#get value of each column from the previous input entry 
	c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name+'_rpm_%03d'%(operating_speeds[j]),pk2))
	val = c.fetchone()[0]      
	if i == 1:
	  try:	      
	      #create next entry (next entry is thus created)	  
	      c.execute("INSERT INTO %s (primary_key) VALUES (%d)"%(table_name+'_rpm_%03d'%(operating_speeds[j]),pk1))
	  except:
	    pass

	#update the new row according to the previous row's entries
	c.execute("UPDATE %s SET %s = ? WHERE primary_key = ?"%(table_name+'_rpm_%03d'%(operating_speeds[j]),column_names[i]),(val,pk1))
  
  
  #write new variable and dimension values into optimise.db
  for j in range(0,len(operating_speeds)):
    #c.execute("UPDATE %s SET stator_teeth_to_coil_height_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(hs_ratio,primary_key))
    c.execute("UPDATE %s SET radially_magnetized_PM_width_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(kmr,primary_key))
    #c.execute("UPDATE %s SET tangentially_magnetized_PM_width_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(kmt,primary_key))
    c.execute("UPDATE %s SET coil_width_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(kc,primary_key))
    #c.execute("UPDATE %s SET rotor_gap_teeth_height_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(km_gap_teeth,primary_key))
    #c.execute("UPDATE %s SET rotor_slot_depth_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(hmt_slot_depth_ratio,primary_key))
    #c.execute("UPDATE %s SET tangentially_magnetised_PM_height_ratio_of_slot = ? WHERE primary_key = ?"%(input_table_name[j]),(hmt_magnet_height_ratio_of_slot,primary_key))
    
    #c.execute("UPDATE %s SET poles = ? WHERE primary_key = ?"%(input_table_name[j]),(poles,primary_key))
    #c.execute("UPDATE %s SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(ryo,primary_key))
    #c.execute("UPDATE %s SET stator_yoke_inner_radius_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(ri,primary_key))
    c.execute("UPDATE %s SET rotor_yoke_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hyo,primary_key))
    c.execute("UPDATE %s SET stator_yoke_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hyi,primary_key))
    c.execute("UPDATE %s SET coil_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hc,primary_key))
    c.execute("UPDATE %s SET radially_magnetized_PM_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hmr,primary_key))
    c.execute("UPDATE %s SET stator_rod_distance_from_tooth_tip = ? WHERE primary_key = ?"%(input_table_name[j]),(hrod,primary_key))
    c.execute("UPDATE %s SET stacklength_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(stacklength,primary_key))
    c.execute("UPDATE %s SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(ryo,primary_key))
  
    c.execute("UPDATE %s SET stator_shoe_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hs,primary_key))
    c.execute("UPDATE %s SET stator_shoe_width_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(ks,primary_key))
    c.execute("UPDATE %s SET stator_shoe_wedge_degrees = ? WHERE primary_key = ?"%(input_table_name[j]),(shoe_theta,primary_key))
  
  #c.execute("UPDATE %s SET stacklength_in_mm = ? WHERE primary_key = ?",(l,primary_key))
  #print "optimization.py -- going to update and commit parameters"
  conn.commit()
  
  if (single_weight_optimization == 1 and primary_key>1):
    c.execute("SELECT MAX(primary_key) FROM best."+output_table_name[operating_point])
    primary_key_best = c.fetchone()[0]
    print("primary_key_best = %d"%(primary_key_best))
  else:
    primary_key_best = 1
  
  if enable_multiple_operating_points:
    loops = len(operating_speeds)
  else:
    loops = 1
  
  f_best = 0
  #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
  for i in range(0,loops): #SOLID BARS
    if enable_multiple_operating_points:
      operating_point = i
      
    if (iteration_number > 0 and single_weight_optimization == 0) or (single_weight_optimization == 1 and primary_key>1):
      #if single_weight_optimization == 1:
	#previous_primary_key = primary_key - 1
      #else:
	#previous_primary_key = primary_key
      
      #print(previous_primary_key)
      
      #for s in range(0,len(f_string_names_list)): 
	#c.execute("UPDATE machine_semfem_output SET %s = ? WHERE primary_key = ?"%(f_string_names_list[s]),(f_objectives_list[s],primary_key)) 

      #print("KLIP primary_key = %d"%(primary_key))  
      f_power_factor[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"power_factor",primary_key_best))
      f_average_torque_dq[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"average_torque_dq",primary_key_best))
      f_torque_ripple_semfem[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"torque_ripple_semfem",primary_key_best))
      f_efficiency_indirect_dq[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"efficiency_indirect_dq",primary_key_best))
      f_total_mass[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"total_mass",primary_key_best))
      f_torque_density[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"torque_density",primary_key_best))
      f_pm_mass[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"magnet_mass",primary_key_best))
      if enable_objective_constraints == 1:
	f_pmech[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"p_out_dq_torque",primary_key_best))
	f_current_density_rms[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"current_density_rms",primary_key_best))
	f_total_losses[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"total_losses",primary_key_best))
      
      f_power_factor_normalised[operating_point] = abs(f_power_factor[operating_point]/ideal_pf)
      f_average_torque_dq_normalised[operating_point] = abs(f_average_torque_dq[operating_point]/ideal_torque_average)
      f_torque_ripple_semfem_normalised[operating_point] = abs(f_torque_ripple_semfem[operating_point]/ideal_torque_ripple)
      f_efficiency_indirect_dq_normalised[operating_point] = abs(f_efficiency_indirect_dq[operating_point]/ideal_eff)
      f_total_mass_normalised[operating_point] = abs(f_total_mass[operating_point]/ideal_mass_total)   
      f_torque_density_normalised[operating_point] = abs(f_torque_density[operating_point]/ideal_torque_density)
      f_pm_mass_normalised[operating_point] = abs(f_pm_mass[operating_point]/ideal_pm_mass)
      if enable_objective_constraints == 1:
	f_pmech_normalised[operating_point] = abs(f_pmech[operating_point]/ideal_pmech_out)
	f_current_density_rms_normalised[operating_point] = abs(f_current_density_rms[operating_point]/ideal_J_rms)
	f_total_losses_normalised[operating_point] = abs(f_total_losses[operating_point]/ideal_total_losses)
  
      if enable_objective_constraints == 0:
	f_best = -weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] -weight_pf[operating_point]*f_power_factor_normalised[operating_point] -weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] +weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
      
      #print("operating_point = %d"%(operating_point))
      print("1f_best = %f"%(f_best))
	
      ###efficiency (maximize)
      if f_efficiency_indirect_dq_normalised[operating_point] > efficiency_constraint[1]/ideal_eff:
	f_best = f_best - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]
      else:
	#penalty = (f_efficiency_indirect_dq_normalised - efficiency_constraint[1]/ideal_eff)**2
	penalty = (1+abs(f_efficiency_indirect_dq_normalised[operating_point] - efficiency_constraint[1]/ideal_eff))**2
	f_best = f_best - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] + efficiency_constraint[0][operating_point]*penalty
      print("2f_best = %f"%(f_best))
      
      ###torque (maximize)
      if f_average_torque_dq_normalised[operating_point] > torque_constraint[1]/ideal_torque_average:
	f_best = f_best - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]
      else:
	#penalty = (f_average_torque_dq_normalised - torque_constraint[1]/ideal_torque_average)**2
	penalty = (1+abs(f_average_torque_dq_normalised[operating_point] - torque_constraint[1]/ideal_torque_average))**2
	f_best = f_best - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + torque_constraint[0][operating_point]*penalty     
      print("3f_best = %f"%(f_best))
      
      ###total mass (minimize)
      if f_total_mass_normalised[operating_point] < total_mass_constraint[1]/ideal_mass_total:  
	f_best = f_best + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]
      else:
	#penalty = (f_total_mass_normalised - total_mass_constraint[1]/ideal_mass_total)**2
	penalty = (1+abs(f_total_mass_normalised[operating_point] - total_mass_constraint[1]/ideal_mass_total))**2
	f_best = f_best + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + total_mass_constraint[0][operating_point]*penalty
      print("4f_best = %f"%(f_best))
      
      ###torque ripple (minimize)
      if f_torque_ripple_semfem_normalised[operating_point] < torque_ripple_constraint[1]/ideal_torque_ripple:
	f_best = f_best + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]
      else:
	#penalty = (f_torque_ripple_semfem_normalised - torque_ripple_constraint[1]/ideal_torque_ripple)**2
	penalty = (1+abs(f_torque_ripple_semfem_normalised[operating_point] - torque_ripple_constraint[1]/ideal_torque_ripple))**2
	f_best = f_best + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] + torque_ripple_constraint[0][operating_point]*penalty 
      print("5f_best = %f"%(f_best))
      
      ###power factor (maximize)
      if f_power_factor_normalised[operating_point] > power_factor_constraint[1]/ideal_pf:
	f_best = f_best - weight_pf[operating_point]*f_power_factor_normalised[operating_point]
      else:
	#penalty = (f_power_factor_normalised-power_factor_constraint[1]/ideal_pf)**2
	penalty = (1+abs(f_power_factor_normalised[operating_point]-power_factor_constraint[1]/ideal_pf))**2
	f_best = f_best - weight_pf[operating_point]*f_power_factor_normalised[operating_point] + power_factor_constraint[0][operating_point]*penalty
      print("6f_best = %f"%(f_best))
      
      ###torque density (maximize)
      if f_torque_density_normalised[operating_point] > torque_density_constraint[1]/ideal_torque_density:
	f_best = f_best - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]
      else:
	#penalty = (f_torque_density_normalised - torque_density_constraint/ideal_torque_density)**2
	penalty = (1+abs(f_torque_density_normalised[operating_point] - torque_density_constraint[1]/ideal_torque_density))**2
	f_best = f_best - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] + torque_density_constraint[0][operating_point]*penalty
      print("7f_best = %f"%(f_best))
      
      ###magnet mass (minimize)
      if f_pm_mass_normalised[operating_point] < magnet_mass_constraint[1]/ideal_pm_mass:
	f_best = f_best + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
      else:
	#penalty = (f_pm_mass_normalised - magnet_mass_constraint[1]/ideal_pm_mass)**2
	penalty = (1+abs(f_pm_mass_normalised[operating_point] - magnet_mass_constraint[1]/ideal_pm_mass))**2
	f_best = f_best + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] + magnet_mass_constraint[0][operating_point]*penalty
      print("8f_best = %f"%(f_best))
      
      ###current density (?)
      if f_current_density_rms_normalised[operating_point] < current_density_rms_constraint[1]/ideal_J_rms:
	f_best = f_best + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point]
      else:
	#penalty = (f_current_density_rms_normalised - current_density_rms_constraint[1]/ideal_J_rms)**2
	penalty = (1+abs(f_current_density_rms_normalised[operating_point] - current_density_rms_constraint[1]/ideal_J_rms))**2
	f_best = f_best + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point] + current_density_rms_constraint[0][operating_point]*penalty
      print("9f_best = %f"%(f_best))
      
      ###mechanical output power (minimize-since it is usually larger than the desired number)
      if f_pmech_normalised[operating_point] > p_mech_constraint[1]/ideal_pmech_out:
	f_best = f_best + weight_pmech[operating_point]*f_pmech_normalised[operating_point]
      else:
	#penalty = (f_pmech_normalised - p_mech_constraint[1]/ideal_pmech_out)**2
	penalty = (1+abs(f_pmech_normalised[operating_point] - p_mech_constraint[1]/ideal_pmech_out))**2
	f_best = f_best + weight_pmech[operating_point]*f_pmech_normalised[operating_point] + p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant
	print("penalty_best = %f"%(penalty))
      print("f_best_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
      print("p_best_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant = %f"%(p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant))
      print("10f_best = %f"%(f_best))
      
      ###total losses (minimize)
      if f_total_losses[operating_point] < total_losses_constraint[1]/ideal_total_losses:
	f_best = f_best + weight_total_losses[operating_point]*f_total_losses[operating_point]
      else:
	#penalty = (f_total_losses - total_losses_constraint[1]/ideal_total_losses)**2
	penalty = (1+abs(f_total_losses[operating_point] - total_losses_constraint[1]/ideal_total_losses))**2
	f_best = f_best + weight_total_losses[operating_point]*f_total_losses[operating_point] + total_losses_constraint[0][operating_point]*penalty
      print("11f_best = %f"%(f_best))

  #print "optimization.py -- going to run semfem script"
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db'"%(primary_key))
  
  
  
  ####################################
  ##ROUND LITZ
  ####################################
  ##obtain inductance L and PM flux linkage
  #table_extension_string = '_rpm_%03d'%(operating_speeds[0])
  #print "Simulating machine at %03d rpm"%(operating_speeds[0])
  ##close connection to the database
  #conn.close()
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  ##copy inductance L and PM flux linkage to other input tables
  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()  
  
  #synchronous_inductance_single_turn = db_input_select(input_table_name[0],"synchronous_inductance_single_turn",primary_key)
  #PM_flux_linkage_single_turn = db_input_select(input_table_name[0],"PM_flux_linkage_single_turn",primary_key)
  
  #for i in range(1,len(operating_speeds)):
    #c.execute("UPDATE %s SET synchronous_inductance_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(synchronous_inductance_single_turn,primary_key))
    #c.execute("UPDATE %s SET PM_flux_linkage_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(PM_flux_linkage_single_turn,primary_key))
  #conn.commit()
  
  ##simulate top speed in order to determine number of turns (and of course the operating point performance in general)
  #table_extension_string = '_rpm_%03d'%(operating_speeds[-1])
  #print "Simulating machine at %03d rpm"%(operating_speeds[-1])
  ##close connection to the database
  #conn.close()  
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()
  ##obtain turns per coil which was determined from top speed
  #turnsPerCoil = db_input_select(output_table_name[-1],"turnsPerCoil",primary_key)
  ##print("Received turnsPerCoil = %d"%(turnsPerCoil))
  #c.execute("UPDATE %s SET force_this_number_of_turns = ? WHERE primary_key = ?"%(input_table_name[1]),(turnsPerCoil,primary_key))
  #conn.commit()
  #conn.close()
  
  ##simulate base speed
  #table_extension_string = '_rpm_%03d'%(operating_speeds[1])
  #print "Simulating machine at %03d rpm"%(operating_speeds[1])
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))  

  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor() 

  ####################################
  ##SOLID BARS
  ####################################
  #obtain inductance L and PM flux linkage at 100rpm (base speed)
  table_extension_string = '_rpm_%03d'%(operating_speeds[0])
  print "Simulating machine at %03d rpm"%(operating_speeds[0])
  #close connection to the database
  conn.close()
  os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  #copy inductance L and PM flux linkage to other input tables
  #open connection to the database
  conn = sqlite3.connect('optimise.db')
  c = conn.cursor()  
  
  synchronous_inductance_single_turn = db_input_select(input_table_name[0],"synchronous_inductance_single_turn",primary_key)
  PM_flux_linkage_single_turn = db_input_select(input_table_name[0],"PM_flux_linkage_single_turn",primary_key)
  
  #copy to 465rpm (even though we are not using 465rpm yet)
  for i in range(1,len(operating_speeds)):
    c.execute("UPDATE %s SET synchronous_inductance_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(synchronous_inductance_single_turn,primary_key))
    c.execute("UPDATE %s SET PM_flux_linkage_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(PM_flux_linkage_single_turn,primary_key))
  conn.commit()
  
  ##simulate top speed in order to determine if sufficient pcu allowance for flux weakening
  #table_extension_string = '_rpm_%03d'%(operating_speeds[-1])
  #print "Simulating machine at %03d rpm"%(operating_speeds[-1])
  ##close connection to the database
  #conn.close()  
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))  

  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()
  ####################################


  ##attach best.db to current db connection
  #c.execute("ATTACH DATABASE 'best.db' as best")
 
  if enable_multiple_operating_points:
    loops = len(operating_speeds)
  else:
    loops = 1
  
  f = 0
  #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
  for i in range(0,loops): #SOLID BARS
    if enable_multiple_operating_points:
      operating_point = i
    
    #read results of semfem simulation
    f_power_factor[operating_point] = abs(db_input_select(output_table_name[operating_point],"power_factor",primary_key))
    f_average_torque_dq[operating_point] = abs(db_input_select(output_table_name[operating_point],"average_torque_dq",primary_key))
    f_torque_ripple_semfem[operating_point] = abs(db_input_select(output_table_name[operating_point],"torque_ripple_semfem",primary_key))
    f_efficiency_indirect_dq[operating_point] = abs(db_input_select(output_table_name[operating_point],"efficiency_indirect_dq",primary_key))
    f_total_mass[operating_point] = abs(db_input_select(output_table_name[operating_point],"total_mass",primary_key))
    f_torque_density[operating_point] = abs(db_input_select(output_table_name[operating_point],"torque_density",primary_key))
    f_pm_mass[operating_point] = abs(db_input_select(output_table_name[operating_point],"magnet_mass",primary_key))
    f_pmech[operating_point] = abs(db_input_select(output_table_name[operating_point],"p_out_dq_torque",primary_key))
    f_current_density_rms[operating_point] = abs(db_input_select(output_table_name[operating_point],"current_density_rms",primary_key))
    f_total_losses[operating_point] = abs(db_input_select(output_table_name[operating_point],"total_losses",primary_key))

    #pretty_output_table.add_row(["iteration_number",iteration_number,"---"])
    ##pretty_output_table.add_row(["poles",poles,"[p.u.]"])
    #pretty_output_table.add_row(["f_power_factor",f_power_factor[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["f_average_torque_dq",f_average_torque_dq[operating_point],"[N.m]"])
    #pretty_output_table.add_row(["f_torque_ripple_semfem",f_torque_ripple_semfem[operating_point],"[%]"])
    #pretty_output_table.add_row(["f_efficiency_indirect_dq",f_efficiency_indirect_dq[operating_point],"[%]"])
    #pretty_output_table.add_row(["f_total_mass",f_total_mass[operating_point],"[kg]"])
    #pretty_output_table.add_row(["f_torque_density",f_torque_density[operating_point],"[N.m/kg]"])
    #pretty_output_table.add_row(["f_pm_mass",f_pm_mass[operating_point],"[kg]"])
    #pretty_output_table.add_row(["f_pmech",f_pmech[operating_point],"[W]"])
    #pretty_output_table.add_row(["f_current_density_rms",f_current_density_rms[operating_point],"[A/mm2]"])
    #pretty_output_table.add_row(["f_total_losses",f_total_losses[operating_point],"[W]"])
  
    #pretty_output_table.add_row(["-------------","------------","------------"])
    #pretty_output_table.add_row(["primary_key",primary_key,""])
    #pretty_output_table.add_row(["weight_eff",weight_eff[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_average",weight_torque_average[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_ripple",weight_torque_ripple[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pf",weight_pf[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_mass_total",weight_mass_total[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_density",weight_torque_density[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pm_mass",weight_pm_mass[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pmech",weight_pmech[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_J_rms",weight_J_rms[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_total_losses",weight_total_losses[operating_point],"[p.u.]"])  

    #objective cost function
    f_power_factor_normalised[operating_point] = f_power_factor[operating_point]/ideal_pf
    f_average_torque_dq_normalised[operating_point] = f_average_torque_dq[operating_point]/ideal_torque_average
    f_torque_ripple_semfem_normalised[operating_point] = f_torque_ripple_semfem[operating_point]/ideal_torque_ripple
    f_efficiency_indirect_dq_normalised[operating_point] = f_efficiency_indirect_dq[operating_point]/ideal_eff
    f_total_mass_normalised[operating_point] = f_total_mass[operating_point]/ideal_mass_total
    f_torque_density_normalised[operating_point] = f_torque_density[operating_point]/ideal_torque_density
    f_pm_mass_normalised[operating_point] = f_pm_mass[operating_point]/ideal_pm_mass
    if enable_objective_constraints == 1:
      f_pmech_normalised[operating_point] = abs(f_pmech[operating_point]/ideal_pmech_out)
      f_current_density_rms_normalised[operating_point] = abs(f_current_density_rms[operating_point]/ideal_J_rms)
      f_total_losses_normalised[operating_point] = abs(f_total_losses[operating_point]/ideal_total_losses)  
  
    ##read weights
    #weight_eff = db_input_select(input_table_name[0],"weight_eff",primary_key)
    #weight_pf = db_input_select(input_table_name[0],"weight_pf",primary_key)
    #weight_mass_total = db_input_select(input_table_name[0],"weight_mass_total",primary_key)
    #weight_torque_average = db_input_select(input_table_name[0],"weight_torque_average",primary_key)
    #weight_torque_ripple = db_input_select(input_table_name[0],"weight_torque_ripple",primary_key)

    print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
    print("f_torque_ripple_semfem_normalised[operating_point] = %f"%(f_torque_ripple_semfem_normalised[operating_point]))
    print("f_torque_density_normalised[operating_point] = %f"%(f_torque_density_normalised[operating_point]))
    
    #print pretty_output_table
    #pretty_output_table.clear_rows()
  
    if debug_f == 1:
      print "weights"
      print("weight_eff[operating_point] = %.3f"%(weight_eff[operating_point]))
      print("weight_torque_average[operating_point] = %.3f"%(weight_torque_average[operating_point]))
      print("weight_torque_ripple[operating_point] = %.3f"%(weight_torque_ripple[operating_point]))
      print("weight_mass_total[operating_point] = %.3f"%(weight_mass_total[operating_point]))
      print("weight_pf[operating_point] = %.3f"%(weight_pf[operating_point]))
      print("weight_torque_density[operating_point] = %.3f"%(weight_torque_density[operating_point]))
      print("weight_pm_mass[operating_point] = %.3f"%(weight_pm_mass[operating_point]))
      if enable_objective_constraints == 1:
	print("weight_pmech[operating_point] = %.3f"%(weight_pmech[operating_point]))
	print("weight_J_rms[operating_point] = %.3f"%(weight_J_rms[operating_point]))
	print("weight_total_losses[operating_point] = %.3f"%(weight_total_losses[operating_point]))
      
      print("normalised")
      print("f_efficiency_indirect_dq_normalised[operating_point] = %f"%(f_efficiency_indirect_dq_normalised[operating_point]))
      print("f_average_torque_dq_normalised[operating_point] = %f"%(f_average_torque_dq_normalised[operating_point]))
      print("f_torque_ripple_semfem_normalised[operating_point] = %f"%(f_torque_ripple_semfem_normalised[operating_point]))
      print("f_total_mass_normalised[operating_point] = %f"%(f_total_mass_normalised[operating_point]))
      print("f_power_factor_normalised[operating_point] = %f"%(f_power_factor_normalised[operating_point]))
      print("f_torque_density_normalised[operating_point] = %f"%(f_torque_density_normalised[operating_point]))
      print("f_pm_mass_normalised[operating_point] = %f"%(f_pm_mass_normalised[operating_point]))
      if enable_objective_constraints == 1:
	print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
	print("f_current_density_rms_normalised[operating_point] = %f"%(f_current_density_rms_normalised[operating_point]))
	print("f_total_losses_normalised[operating_point] = %f"%(f_total_losses_normalised[operating_point]))
    
    if enable_objective_constraints == 0:
      f = -weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] -weight_pf[operating_point]*f_power_factor_normalised[operating_point] -weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] +weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]

    #print("operating_point = %d"%(operating_point))
    #print("1f = %f"%(f))
    ###efficiency (maximize)
    if f_efficiency_indirect_dq_normalised[operating_point] > efficiency_constraint[1]/ideal_eff:
      f = f - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]
    else:
      #penalty = (f_efficiency_indirect_dq_normalised - efficiency_constraint[1]/ideal_eff)**2
      penalty = (1+abs(f_efficiency_indirect_dq_normalised[operating_point] - efficiency_constraint[1]/ideal_eff))**2
      f = f - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] + efficiency_constraint[0][operating_point]*penalty
    #print("2f = %f"%(f))
    ###torque (maximize)
    if f_average_torque_dq_normalised[operating_point] > torque_constraint[1]/ideal_torque_average:
      f = f - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]
    else:
      #penalty = (f_average_torque_dq_normalised - torque_constraint[1]/ideal_torque_average)**2
      penalty = (1+abs(f_average_torque_dq_normalised[operating_point] - torque_constraint[1]/ideal_torque_average))**2
      f = f - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + torque_constraint[0][operating_point]*penalty     
    #print("3f = %f"%(f))
    ###total mass (minimize)
    if f_total_mass_normalised[operating_point] < total_mass_constraint[1]/ideal_mass_total:  
      f = f + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]
    else:
      #penalty = (f_total_mass_normalised - total_mass_constraint[1]/ideal_mass_total)**2
      penalty = (1+abs(f_total_mass_normalised[operating_point] - total_mass_constraint[1]/ideal_mass_total))**2
      f = f + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + total_mass_constraint[0][operating_point]*penalty
    #print("4f = %f"%(f))
    ###torque ripple (minimize)
    if f_torque_ripple_semfem_normalised[operating_point] < torque_ripple_constraint[1]/ideal_torque_ripple:
      f = f + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]
    else:
      #penalty = (f_torque_ripple_semfem_normalised - torque_ripple_constraint[1]/ideal_torque_ripple)**2
      penalty = (1+abs(f_torque_ripple_semfem_normalised[operating_point] - torque_ripple_constraint[1]/ideal_torque_ripple))**2
      f = f + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] + torque_ripple_constraint[0][operating_point]*penalty 
    #print("5f = %f"%(f))
    ###power factor (maximize)
    if f_power_factor_normalised[operating_point] > power_factor_constraint[1]/ideal_pf:
      f = f - weight_pf[operating_point]*f_power_factor_normalised[operating_point]
    else:
      #penalty = (f_power_factor_normalised-power_factor_constraint[1]/ideal_pf)**2
      penalty = (1+abs(f_power_factor_normalised[operating_point]-power_factor_constraint[1]/ideal_pf))**2
      f = f - weight_pf[operating_point]*f_power_factor_normalised[operating_point] + power_factor_constraint[0][operating_point]*penalty
    #print("6f = %f"%(f))
    ###torque density (maximize)
    if f_torque_density_normalised[operating_point] > torque_density_constraint[1]/ideal_torque_density:
      f = f - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]
    else:
      #penalty = (f_torque_density_normalised - torque_density_constraint/ideal_torque_density)**2
      penalty = (1+abs(f_torque_density_normalised[operating_point] - torque_density_constraint[1]/ideal_torque_density))**2
      f = f - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] + torque_density_constraint[0][operating_point]*penalty
    #print("7f = %f"%(f))
    ###magnet mass (minimize)
    if f_pm_mass_normalised[operating_point] < magnet_mass_constraint[1]/ideal_pm_mass:
      f = f + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
    else:
      #penalty = (f_pm_mass_normalised - magnet_mass_constraint[1]/ideal_pm_mass)**2
      penalty = (1+abs(f_pm_mass_normalised[operating_point] - magnet_mass_constraint[1]/ideal_pm_mass))**2
      f = f + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] + magnet_mass_constraint[0][operating_point]*penalty
    #print("8f = %f"%(f))
    ###current density (?)
    if f_current_density_rms_normalised[operating_point] < current_density_rms_constraint[1]/ideal_J_rms:
      f = f + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point]
    else:
      #penalty = (f_current_density_rms_normalised - current_density_rms_constraint[1]/ideal_J_rms)**2
      penalty = (1+abs(f_current_density_rms_normalised[operating_point] - current_density_rms_constraint[1]/ideal_J_rms))**2
      f = f + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point] + current_density_rms_constraint[0][operating_point]*penalty
    #print("9f = %f"%(f))
    ###mechanical output power (minimize-since it is usually larger than the desired number)
    if f_pmech_normalised[operating_point] > p_mech_constraint[1]/ideal_pmech_out:
      f = f + weight_pmech[operating_point]*f_pmech_normalised[operating_point]
    else:
      #penalty = (f_pmech_normalised - p_mech_constraint[1]/ideal_pmech_out)**2
      penalty = (1+abs(f_pmech_normalised[operating_point] - p_mech_constraint[1]/ideal_pmech_out))**2
      #print("p_mech_penalty = %g"%(penalty))
      #print("p_mech_penalty_term = %g"%(weight_pmech*p_mech_constraint[0]*penalty))
      #print("p_mech_score = %g"%(weight_pmech*f_pmech_normalised))
      f = f + weight_pmech[operating_point]*f_pmech_normalised[operating_point] + p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant
      #print("penalty = %f"%(penalty))
    #print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
    #print("p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant = %f"%(p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant))
    #print("10f = %f"%(f))
    ###total losses (minimize)
    if f_total_losses[operating_point] < total_losses_constraint[1]/ideal_total_losses:
      f = f + weight_total_losses[operating_point]*f_total_losses[operating_point]
    else:
      #penalty = (f_total_losses - total_losses_constraint[1]/ideal_total_losses)**2
      penalty = (1+abs(f_total_losses[operating_point] - total_losses_constraint[1]/ideal_total_losses))**2
      f = f + weight_total_losses[operating_point]*f_total_losses[operating_point] + total_losses_constraint[0][operating_point]*penalty
    #print("11f = %f"%(f))
  #f_pmech_normalised_array.append(f_pmech_normalised[operating_point])
  #f_total_mass_normalised_array.append(f_total_mass_normalised[operating_point])
  #f_torque_ripple_semfem_normalised_array.append(f_torque_ripple_semfem_normalised[operating_point])
  #f_efficiency_indirect_dq_normalised_array.append(f_efficiency_indirect_dq_normalised[operating_point])
  #f_torque_density_normalised_array.append(f_torque_density_normalised[operating_point])
  
  #f_array.append(f)
  
  #f_string_names_list = []
  #f_string_names_list.append('f_pmech_normalised')
  #f_string_names_list.append('f_total_mass_normalised')
  #f_string_names_list.append('f_torque_ripple_semfem_normalised')
  #f_string_names_list.append('f_efficiency_indirect_dq_normalised')
  #f_string_names_list.append('f_torque_density_normalised')
  #f_string_names_list.append('f_pm_mass_normalised')
  #f_string_names_list.append('f_power_factor_normalised')
  #f_string_names_list.append('f_average_torque_dq_normalised')
  
  #f_objectives_list = []
  #f_objectives_list.append(f_pmech_normalised[operating_point])
  #f_objectives_list.append(f_total_mass_normalised[operating_point])
  #f_objectives_list.append(f_torque_ripple_semfem_normalised[operating_point])
  #f_objectives_list.append(f_efficiency_indirect_dq_normalised[operating_point])
  #f_objectives_list.append(f_torque_density_normalised[operating_point])
  #f_objectives_list.append(f_pm_mass_normalised[operating_point])
  #f_objectives_list.append(f_power_factor_normalised[operating_point])
  #f_objectives_list.append(f_average_torque_dq_normalised[operating_point])
  
  if (iteration_number == 0 and single_weight_optimization == 0) or (primary_key == 1 and single_weight_optimization == 1):
    ##add f_xxx columns to optimise.db table
    #table = 'machine_semfem_output'
    #for d in range(0,1):
      ##if d == 1:
	##table = 'best.machine_semfem_output'
      #for s in range(0,len(f_string_names_list)):
	#c.execute("ALTER TABLE %s ADD COLUMN %s REAL"%(table,f_string_names_list[s]))

    #create new database file, best.db
    shutil.copy('optimise.db','best.db')
    
    ##attach best.db to current db connection
    #c.execute("ATTACH DATABASE 'best.db' as best")    
    
    #c.execute("DELETE FROM best.machine_input_parameters")
    #c.execute("DELETE FROM best.machine_semfem_output")

  #attach best.db to current db connection
  c.execute("ATTACH DATABASE 'best.db' as best")    
  
  c.execute("DELETE FROM best.machine_input_parameters")
  c.execute("DELETE FROM best.machine_semfem_output")

  #for s in range(0,len(f_string_names_list)): 
    #c.execute("UPDATE machine_semfem_output SET %s = ? WHERE primary_key = ?"%(f_string_names_list[s]),(f_objectives_list[s],primary_key)) 
  
  conn.commit()
  
  #if debug_f == 1:
    #print "result"
    #print("-weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] = %f"%(-weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]))
    #print("-weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] = %f"%(-weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]))
    #print("weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] = %f"%(weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]))
    #print("weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] = %f"%(weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]))
    #print("-weight_pf[operating_point]*f_power_factor_normalised[operating_point] = %f"%(-weight_pf[operating_point]*f_power_factor_normalised[operating_point]))
    #print("-weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] = %f"%(-weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]))
    #print("weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] = %f"%(weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]))
    #print("f = %f"%(f))

  print("f = %f"%(f))
  f_best_changed = 0
  if (iteration_number == 0 and single_weight_optimization == 0) or (primary_key == 1 and single_weight_optimization == 1):
    f_best = f
    #f_power_factor_best = abs(db_input_select(output_table_name[operating_point],"power_factor",primary_key))
    #f_average_torque_dq_best = abs(db_input_select(output_table_name[operating_point],"average_torque_dq",primary_key))
    #f_torque_ripple_semfem_best = abs(db_input_select(output_table_name[operating_point],"torque_ripple_semfem",primary_key))
    #f_efficiency_indirect_dq_best = abs(db_input_select(output_table_name[operating_point],"efficiency_indirect_dq",primary_key))
    #f_total_mass_best = abs(db_input_select(output_table_name[operating_point],"total_mass",primary_key))
    #f_torque_density_best = abs(db_input_select(output_table_name[operating_point],"torque_density",primary_key))
    #f_pm_mass_best = abs(db_input_select(output_table_name[operating_point],"magnet_mass",primary_key))
    ##if enable_objective_constraints == 1:
    #f_pmech_best = abs(db_input_select(output_table_name[operating_point],"p_out_semfem_torque",primary_key))
    #f_current_density_rms_best = abs(db_input_select(output_table_name[operating_point],"current_density_rms",primary_key))
    #f_total_losses_best = abs(db_input_select(output_table_name[operating_point],"total_losses",primary_key))    
    
    iteration_number_best = iteration_number
    f_best_changed = 1

  elif f<f_best:
    #print("YESSS f<f_best")
    f_best = f
    #f_power_factor_best = f_power_factor[operating_point]
    #f_average_torque_dq_best = f_average_torque_dq[operating_point]
    #f_torque_ripple_semfem_best = f_torque_ripple_semfem[operating_point]
    #f_efficiency_indirect_dq_best = f_efficiency_indirect_dq[operating_point]
    #f_total_mass_best = f_total_mass[operating_point]
    #f_torque_density_best = f_torque_density[operating_point]
    #f_pm_mass_best = f_pm_mass[operating_point]
    ##if enable_objective_constraints == 1:
    #f_pmech_best = f_pmech[operating_point]
    #f_current_density_rms_best = f_current_density_rms[operating_point]
    #f_total_losses_best = f_total_losses[operating_point]
    
    iteration_number_best = iteration_number
    f_best_changed = 1

  if f_best_changed == 1:
    #print("WHOOP going to update best.db with value from primary_key = %d"%(primary_key))
    #copy latest entry from optimise.db to best.db since this is now the new best
    if enable_multiple_operating_points:
      loops = len(operating_speeds)
    else:
      loops = 1
      
    #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
    for i in range(0,loops): #SOLID BARS
      if enable_multiple_operating_points:
	operating_point = j
      
      table_name = input_table_name[operating_point]
      for s in range(0,2):
	if s == 1: table_name = output_table_name[operating_point]
	pk2 = primary_key
	#pk1 = primary_key_best
	pk1 = primary_key
	
	c = conn.execute("select * from %s"%(table_name))
	column_names = list(map(lambda x: x[0], c.description))  
      
	for i in range(1,len(column_names)):
	  c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name,pk2))
	    
	  val = c.fetchone()[0]
	  if i == 1:
	    try:
	      c.execute("INSERT INTO best.%s (primary_key) VALUES (%d)"%(table_name,pk1))
	    except:
	      #print "Did not insert"
	      pass

	  try:
	    c.execute("UPDATE best.%s SET %s = ? WHERE primary_key = ?"%(table_name,column_names[i]),(val,pk1))      
	  except:
	    print("update did not work")
    
    
    conn.commit()
    #conn.close()
    #sys.exit()
    
    
    
    
    
    
    
    
    
    
    
    #print("YESSS f_best_changed == 1")
    ##print "optimization.py -- going to update best results"
    ##save the specific inputs that were used to achieve (best)
    ##c.execute("UPDATE best.machine_input_parameters SET stator_teeth_to_coil_height_ratio = ? WHERE primary_key = ?",(hs_ratio,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET radially_magnetized_PM_width_ratio = ? WHERE primary_key = ?",(kmr,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET tangentially_magnetized_PM_width_ratio = ? WHERE primary_key = ?",(kmt,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET coil_width_ratio = ? WHERE primary_key = ?",(kc,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_gap_teeth_height_ratio = ? WHERE primary_key = ?",(km_gap_teeth,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_slot_depth_ratio = ? WHERE primary_key = ?",(hmt_slot_depth_ratio,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET tangentially_magnetised_PM_height_ratio_of_slot = ? WHERE primary_key = ?",(hmt_magnet_height_ratio_of_slot,primary_key))
    
    ##c.execute("UPDATE best.machine_input_parameters SET poles = ? WHERE primary_key = ?",(poles,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?",(ryo,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET stator_yoke_inner_radius_in_mm = ? WHERE primary_key = ?",(ri,primary_key))
    #c.execute("UPDATE best.machine_conn.close()input_parameters SET rotor_yoke_height_in_mm = ? WHERE primary_key = ?",(hyo,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stator_yoke_height_in_mm = ? WHERE primary_key = ?",(hyi,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET coil_height_in_mm = ? WHERE primary_key = ?",(hc,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET radially_magnetized_PM_height_in_mm = ? WHERE primary_key = ?",(hmr,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stator_rod_distance_from_tooth_tip = ? WHERE primary_key = ?",(hrod,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stacklength_in_mm = ? WHERE primary_key = ?",(stacklength,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?",(ryo,primary_key))
    
    ##c.execute("UPDATE best.machine_input_parameters SET stacklength_in_mm = ? WHERE primary_key = ?",(l,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))

    ##save the most important outputs that were obtained
    ##try:
    #c.execute("SELECT value_int FROM best.info WHERE variable_name = 'have_already_commited_best_solution'")
    #have_already_commited_best_solution = c.fetchone()[0]
    ##print "caught this value"
    ##print("have_already_commited_best_solution = %d"%(have_already_commited_best_solution))
    ##except:
      ##have_already_commited_best_solution = 1
    
    #if have_already_commited_best_solution == 1:
      #print("YESSS going to update   best.machine_semfem_output")
      ##if single_weight_optimization == 1:
	##c.execute("UPDATE best.machine_semfem_output SET primary_key = ?",(primary_key,))
      #if single_weight_optimization == 1:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key_best))
      #else:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
      ##have_already_commited_best_solution = 1
      ##c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = have_already_commited_best_solution",(1,))
      
    #elif have_already_commited_best_solution == 0:
      #print("YESSS first update   best.machine_semfem_output")
      #try:
	#c.execute("INSERT INTO best.machine_semfem_output (power_factor,average_torque_dq,efficiency_indirect_dq,torque_ripple_semfem,total_mass,torque_density,magnet_mass,p_out_dq_torque,total_losses,current_density_rms,f_value,primary_key) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
	##c.execute("INSERT INTO best.info (variable_name,value_int) VALUES (?,?)",('have_already_commited_best_solution',1))
	#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(1,))
      #except:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
	#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(1,))
    #conn.commit()
    
    #print "optimization.py -- finished with update of best results"

  if display_poly_from_this_iteration == -1:
    pass
  elif iteration_number>=display_poly_from_this_iteration:
    os.system("fpl2 project_sorspm_semfem/drawing.poly")

  iteration_number = iteration_number + 1
  #c.execute("UPDATE machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))
  
  if single_weight_optimization == 1:
    primary_key = iteration_number
  
  #print "optimization.py -- end of weighted_objective_function"
  
  #print 'hos'
  if single_weight_optimization == 1:
    iterations = range(0,iteration_number-1)
  else:
    iterations = range(0,iteration_number)

 


    
  
  #ax1 = pl.subplot(211)
  #if iteration_number == 1:
    ##fig = pl.figure(1)
    ##ax = fig.add_subplot(2, 1, 1)  # specify (nrows, ncols, axnum)
    #pl.ion()
    #pl.xlabel(r"iteration_number")
    #pl.ylabel(r"f_objectives")
    ##pl.xlim(xmin=0)
    ##pl.title("Pareto Approximation of Efficiency against PM Mass")
    ##pl.scatter(pm_mass,eff, alpha = 0.5)
  
  ##if iteration_number>2:
    ##line3.remove()
    ##scat3.remove()
  
  
  ##total_mass_max_normalised = [x / max(f_total_mass_normalised_array) for x in f_total_mass_normalised_array]
  #total_mass_max_normalised = [x / f_total_mass_normalised_array[0] for x in f_total_mass_normalised_array]
  
  #line1 = pl.plot(iterations,f_array,label='f',c=colors[0])
  #line2 = pl.plot(iterations,f_pmech_normalised_array,label='f_pmech',c=colors[1])
  #line3 = pl.plot(iterations,total_mass_max_normalised,label='f_total_mass',c=colors[2])
  #line4 = pl.plot(iterations,f_torque_ripple_semfem_normalised_array,label='f_torque_ripple',c=colors[3])
  #line5 = pl.plot(iterations,f_efficiency_indirect_dq_normalised_array,label='f_efficiency',c=colors[4])
  #line6 = pl.plot(iterations,f_torque_density_normalised_array,label='f_torque_density',c=colors[5])
  
  #scat1 = pl.scatter(iterations,f_array, alpha = 0.5,c=colors[0])
  #scat2 = pl.scatter(iterations,f_pmech_normalised_array, alpha = 0.5,c=colors[1])
  #scat3 = pl.scatter(iterations,total_mass_max_normalised, alpha = 0.5,c=colors[2])
  #scat4 = pl.scatter(iterations,f_torque_ripple_semfem_normalised_array, alpha = 0.5,c=colors[3])
  #scat5 = pl.scatter(iterations,f_efficiency_indirect_dq_normalised_array, alpha = 0.5,c=colors[4])
  #scat6 = pl.scatter(iterations,f_torque_density_normalised_array, alpha = 0.5,c=colors[5])
  
  #if iteration_number == 1:
    #pl.grid(True)
    #leg = pl.legend(loc='upper right')
    #for legobj in leg.legendHandles:
      #legobj.set_linewidth(2.0)
  ##pl.pause(0.05)  
  
  #ax2 = pl.subplot(212, sharex=ax1)
  #if iteration_number == 1:
    ##fig = pl.figure(2)
    #pl.ion()
    ##pl.xlabel(r"iteration_number")
    #pl.ylabel(r"input variables [p.u.]")
    ##pl.xlim(xmin=0)
  
  #line_input_variables.append(pl.plot(iterations,hyo_pu_array,label='hyo',c=colors[0]))
  #line_input_variables.append(pl.plot(iterations,hyi_pu_array,label='hyi',c=colors[1]))
  #line_input_variables.append(pl.plot(iterations,hc_pu_array,label='hc',c=colors[2]))
  #line_input_variables.append(pl.plot(iterations,hmr_pu_array,label='hmr',c=colors[3]))
  #line_input_variables.append(pl.plot(iterations,kc_pu_array,label='kc',c=colors[4]))
  #line_input_variables.append(pl.plot(iterations,kmr_pu_array,label='kmr',c=colors[5]))
  #line_input_variables.append(pl.plot(iterations,hrod_pu_array,label='hrod',c=colors[6]))
  #line_input_variables.append(pl.plot(iterations,stacklength_pu_array,label='l',c=colors[7]))
  #line_input_variables.append(pl.plot(iterations,ryo_pu_array,label='ryo',c=colors[9]))

  #scat_input_variables.append(pl.scatter(iterations,hyo_pu_array,alpha = 0.5,c=colors[0]))
  #scat_input_variables.append(pl.scatter(iterations,hyi_pu_array,alpha = 0.5,c=colors[1]))
  #scat_input_variables.append(pl.scatter(iterations,hc_pu_array,alpha = 0.5,c=colors[2]))
  #scat_input_variables.append(pl.scatter(iterations,hmr_pu_array,alpha = 0.5,c=colors[3]))
  #scat_input_variables.append(pl.scatter(iterations,kc_pu_array,alpha = 0.5,c=colors[4]))
  #scat_input_variables.append(pl.scatter(iterations,kmr_pu_array,alpha = 0.5,c=colors[5]))
  #scat_input_variables.append(pl.scatter(iterations,hrod_pu_array,alpha = 0.5,c=colors[6]))
  #scat_input_variables.append(pl.scatter(iterations,stacklength_pu_array,alpha = 0.5,c=colors[7]))
  #scat_input_variables.append(pl.scatter(iterations,ryo_pu_array,alpha = 0.5,c=colors[9]))

  ##pl.xlim([0,70])
  ##pl.xlim(xmin=0)

  #if iteration_number == 1:
    #pl.grid(True)
    #leg = pl.legend(loc='upper right')
    #for legobj in leg.legendHandles:
      #legobj.set_linewidth(2.0)
  #pl.pause(0.05)
  
  
  
  
  
  
  
  
  
  
  #fail = 0

  #return f,0,fail
  if use_pyOpt == 1:
    return f,0,0
  if use_nlopt == 1:
    print("end of function")
    print("primary_key_best = %d"%(primary_key_best))
    print("f_best = %f"%(f_best))
    print("primary_key = %d"%(primary_key))
    print("f = %f"%(f))
    print("f_best_changed = %d"%(f_best_changed))
    
    #print "return the following value"
    #print(f)
    return f
  #return f,g,fail



def fc(x,grad):
  f_torque_ripple_semfem[operating_point] = abs(db_input_select("machine_semfem_output","torque_ripple_semfem",primary_key))
  print("fc constraint")
  print ("f_torque_ripple_semfem = %g"%(f_torque_ripple_semfem[operating_point]))
  print("return value = %g"%(f_torque_ripple_semfem[operating_point] - torque_ripple_constraint[1]))
  return f_torque_ripple_semfem[operating_point] - torque_ripple_constraint[1]

#main script section
if use_pyOpt == 1:
  opt_prob = pyOpt.Optimization('weighted objective function',weighted_objective_function)
  opt_prob.addObj('f')

  #volgorde maak saak waarin mens die variables hier declare!!
  #opt_prob.addVar('x_hyo','c',lower=0,upper=1,value=x_hyo_init) #outer yoke height
  #opt_prob.addVar('x_hyi','c',lower=0,upper=1,value=x_hyi_init) #inner yoke height
  #opt_prob.addVar('x_hc','c',lower=0,upper=1,value=x_hc_init) #teeth height
  #opt_prob.addVar('x_hmr','c',lower=0,upper=1,value=x_hmr_init) #magnet height
  #opt_prob.addVar('kc','c',lower=kc_min,upper=kc_max,value=kc) #teeth width
  #opt_prob.addVar('kmr','c',lower=kmr_min,upper=kmr_max,value=kmr) #magnet width

  opt_prob.addVar('hyo','c',lower=hyo_min,upper=hyo_max,value=hyo) #outer yoke height
  opt_prob.addVar('hyi','c',lower=hyi_min,upper=hyi_max,value=hyi) #inner yoke height
  opt_prob.addVar('hc','c',lower=hc_min,upper=hc_max,value=hc) #teeth height
  opt_prob.addVar('hmr','c',lower=hmr_min,upper=hmr_max,value=hmr) #magnet height
  opt_prob.addVar('kc','c',lower=kc_min,upper=kc_max,value=kc) #teeth width
  opt_prob.addVar('kmr','c',lower=kmr_min,upper=kmr_max,value=kmr) #magnet width
  opt_prob.addVar('hrod','c',lower=hrod_min,upper=hrod_max,value=hrod) #rod distance from stator tooth tip


  #opt_prob.addVar('pole_pairs','c',lower=int(pole_pairs_min),upper=int(pole_pairs_max),value=pole_pairs)
  #opt_prob.addVar('x_ryo','c',lower=0,upper=1,value=x_ryo_init)
  #opt_prob.addVar('x_ri','c',lower=0,upper=1,value=x_ri_init) 
  #opt_prob.addVar('hs_ratio','c',lower=0,upper=1,value=hs_ratio)
  #opt_prob.addVar('kmt','c',lower=kmt_min,upper=kmt_max,value=kmt)
  #opt_prob.addVar('x_l','c',lower=0,upper=1,value=x_l_init)
  #opt_prob.addVar('km_gap_teeth','c',lower=0,upper=1,value=km_gap_teeth)
  #opt_prob.addVar('hmt_slot_depth_ratio','c',lower=hmt_slot_depth_ratio_min,upper=hmt_slot_depth_ratio_max,value=hmt_slot_depth_ratio)
  #opt_prob.addVar('hmt_magnet_height_ratio_of_slot','c',lower=0,upper=1,value=hmt_magnet_height_ratio_of_slot)



  ##instantiate optimizer
  #conmin = pyOpt.CONMIN()
  ##conmin.setOption('ITMAX',10)
  #conmin.setOption('DELFUN',epsilon1)
  #conmin.setOption('DABFUN',epsilon2)
  #conmin.setOption('IPRINT',2)

  ##use finite differences because the gradient is unknown
  #[fstr, xstr, inform] = conmin(opt_prob,sens_type='FD',store_hst=True,sens_step=1e-3)#sens_step=1e-2

  sdpen = pyOpt.SDPEN()
  sdpen.setOption('ifile','sdpen.out')
  sdpen.setOption('alfa_stop',epsilon1) #default is 1e-6

  #[fstr, xstr, inform] = sdpen(opt_prob,store_hst=True)
  sdpen(opt_prob,store_hst=False)

#def myfunc(x, grad):
    #print "x[0] = %g"%(x[0])
    #print "x[1] = %g"%(x[1])
    #print "x[2] = %g"%(x[2])
    #print "x[3] = %g"%(x[3])
    #print "x[4] = %g"%(x[4])
    #print "x[5] = %g"%(x[5])
  
    #if grad.size > 0:
        #grad[0] = 0.0
        #grad[1] = 0.5 / np.sqrt(x[1])
    #return np.sqrt(x[1])


if use_nlopt == 1:
  
  #lb = np.array([hyo_min,hyi_min,hc_min,hmr_min,kc_min,kmr_min,hrod_min,stacklength_min,ryo_min])
  #ub = np.array([hyo_max,hyi_max,hc_max,hmr_max,kc_max,kmr_max,hrod_max,stacklength_max,ryo_max])
  #x0 = np.array([hyo,hyi,hc,hmr,kc,kmr,hrod,stacklength,ryo])

  #lb = np.array([0,0,0,0,0,0,0,0,0])
  #ub = np.array([1,1,1,1,1,1,1,1,1])
  #x0 = np.array([hyo_pu,hyi_pu,hc_pu,hmr_pu,kc_pu,kmr_pu,hrod_pu,stacklength_pu,ryo_pu])

  lb = np.array([0,0,0,0,0,0,0,0,0,0,0,0])
  ub = np.array([1,1,1,1,1,1,1,1,1,1,1,1])
  x0 = np.array([hyo_pu,hyi_pu,hc_pu,hmr_pu,kc_pu,kmr_pu,stacklength_pu,ryo_pu,hs_pu,ks_pu,shoe_theta_pu,hrod_pu])
  
  #starting solution
  print "Starting solution"
  print("hyo_pu = %f"%(hyo_pu))
  print("hyi_pu = %f"%(hyi_pu))
  print("hc_pu = %f"%(hc_pu))
  print("hmr_pu = %f"%(hmr_pu))
  print("kc_pu = %f"%(kc_pu))
  print("kmr_pu = %f"%(kmr_pu))
  print("stacklength_pu = %f"%(stacklength_pu))
  print("ryo_pu = %f"%(ryo_pu))
  print("hs_pu = %f"%(hs_pu))
  print("ks_pu = %f"%(ks_pu))
  print("shoe_theta_pu = %f"%(shoe_theta_pu))
  print("hrod_pu = %f"%(hrod_pu))
  
  #nlopt_algorithm = nlopt.LN_SBPLX
  #nlopt_algorithm = nlopt.GN_DIRECT_L #Global optimisation method - works rather well...
  nlopt_algorithm = nlopt.GN_DIRECT #Global optimisation method - works rather well... This is my preferred global optimisation method
  #nlopt_algorithm = nlopt.LN_BOBYQA #also works OK (tried on 2017/11/29) with mesh 3.0 and steps 31, did not produce better results
  #nlopt_algorithm = nlopt.LD_SLSQP #converges after only 1 step
  #nlopt_algorithm = nlopt.LD_MMA #converges after only 1 step
  #nlopt_algorithm = nlopt.LN_SBPLX #THIS is my preferred LOCAL optimisation "polishing" tool
  #nlopt_algorithm = nlopt.LN_NEWUOA_BOUND
  #nlopt_algorithm = nlopt.LN_COBYLA #works OKish..does not sufficiently change stacklength and outer diameter to fit pmech constraints (which would be an obvious move).  
  #opt = nlopt.opt(nlopt_algorithm, 9)
  opt = nlopt.opt(nlopt_algorithm, 12)
  
  opt.set_lower_bounds(lb)
  opt.set_upper_bounds(ub)  
  
  opt.set_min_objective(weighted_objective_function)
  #opt.set_min_objective(myfunc)
  
  #opt.add_inequality_constraint(fc, tol=1e-3)
  #opt.add_inequality_constraint(fc)
  #opt.add_inequality_constraint(weighted_objective_function)
  
  #opt.set_maxeval(300)
  opt.set_maxeval(10000)
  #opt.set_maxeval(5)
  #opt.set_ftol_rel(1e-3)
  opt.set_ftol_rel(1e-4)
  #opt.set_ftol_abs(1e-3)
  opt.set_ftol_abs(1e-4)
  opt.set_xtol_rel(1e-4)
  #opt.set_xtol_rel(1e-3)
  #opt.set_maxtime(350) #used this exact value for many optimisations (number in seconds)
  opt.set_maxtime(100000)
  
  
  
  xopt = opt.optimize(x0)
  #xopt = opt.optimize([3.71,3.0,32.69,3.0,0.7635,0.8])
  
  print "hyo = %g"%(xopt[0])
  print "hyi = %g"%(xopt[1])
  print "hc  = %g"%(xopt[2])
  print "hmr = %g"%(xopt[3])
  print "kc  = %g"%(xopt[4])
  print "kmr = %g"%(xopt[5])
  #print "hrod = %g"%(xopt[6])
  #print "stacklength = %g"%(xopt[7])
  #print "ryo = %g"%(xopt[8])

  print "stacklength = %g"%(xopt[6])
  print "ryo = %g"%(xopt[7])

  opt_val = opt.last_optimum_value()
  
  print(opt_val)
  
  result = opt.last_optimize_result()
  
  print(result)

#print "optimization.py -- almost end of script"
#c.execute("UPDATE machine_input_parameters SET completed_optimisation = ? WHERE primary_key = ?",(1,primary_key))

conn.commit()
#print "optimization.py -- finished final commit"

end_time = time.time()

#time.sleep(3)

if use_pyOpt == 1:
  print opt_prob.solution(0)

#iteration_number = db_input_select(input_table_name[0],"iteration_number",primary_key)
#pl.figure(1)
#pl.xlabel(r"iteration_number")
#pl.ylabel(r"f_objectives")
##pl.title("Pareto Approximation of Efficiency against PM Mass")
##pl.scatter(pm_mass,eff, alpha = 0.5)
#iterations = range(0,iteration_number)
#pl.plot(iterations,f_array,label='f')
#pl.plot(iterations,f_pmech_normalised_array,label='f_pmech_normalised')
##pl.plot(pm_mass,eff,'bo')
#pl.grid(True)
#pl.legend(loc='lower left')
#pl.show()

raw_seconds = end_time-start_time
hours = int(raw_seconds/(60*60))
minutes = int(raw_seconds/60) - hours*60
seconds = int(raw_seconds) - hours*60*60 - minutes*60
time_msg = '%d [hours], %d [minutes], %d [seconds]'%(hours,minutes,seconds)
print('time_elapsed = '+time_msg)
now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

#print("time_elapsed = %d [hours], %d [minutes], %d [seconds]"%(hours,minutes,seconds))
conn.close()
#print "optimization.py -- finished close"

fromaddr = 'andreasjoss22@gmail.com'
toaddrs  = 'andreasjoss22@gmail.com'

optimisation_msg = "Optimisation completed at %s, after %d iterations were done.\n\nThe entire optimisation took %s"%(now,iteration_number,time_msg)

msg = "\r\n".join([
  "From: user_me@gmail.com",
  "To: user_you@gmail.com",
  "Subject: Optimisation Completed",
  "",
  optimisation_msg
  ])


username = 'andreasjoss22@gmail.com'
password = 'danielle24'
server = smtplib.SMTP('smtp.gmail.com:587')
server.ehlo()
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg)
server.quit() 